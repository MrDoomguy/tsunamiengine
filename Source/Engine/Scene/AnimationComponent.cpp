#include "AnimationComponent.h"

AnimationComponent::AnimationComponent(ID3D11Device* pDevice, const AnimationData& animData, Animator* animator) :
	IComponent(IComponent::Type::ANIMATION)
{
	mData = animData;
	mAnimator = animator;

	if (animator != nullptr) mState = AnimationComponent::State::PLAY;

	// resize bone cache to num of bones
	mCurrTransformData.resize(animtypes::MAX_JOINTS);
	mCurrTransformData.assign(mCurrTransformData.size(), XMMatrixIdentity());

	mTransformBuffer = new ConstantBuffer(pDevice, animtypes::MAX_JOINTS * sizeof(XMMATRIX));
}

AnimationComponent::~AnimationComponent()
{
	if (mAnimator != nullptr) delete mAnimator;
}

void AnimationComponent::Update(const float dt)
{
	if (mState == AnimationComponent::State::PLAY)
	{
		mCurrentTime += dt;

		if (mAnimator == nullptr)
		{
			float tps = mData.clips[mCurrentClip].mTicksPerSecond != 0 ? mData.clips[mCurrentClip].mTicksPerSecond : 25.0f;

			float timeInTicks = tps * mCurrentTime * mPlaybackSpeed;
			float animationTime = std::fmodf(timeInTicks, mData.clips[mCurrentClip].mDuration);

			// set transform in skeleton
			this->TraverseSkeleton(animationTime, mData.bindPose.mRoot, XMMatrixIdentity());

			// copy back data into buffer
			for (uint8_t i = 0; i < mData.skeleton.numBones; i++)
			{
				mCurrTransformData[i] = mData.skeleton.jointProps[i].finalTransform;
			}
		}
		else
		{
			std::vector<animtypes::ClipBlendInfo> clips = mAnimator->GetUpdatedBlendInfo();
			
			// set transform in skeleton
			this->TraverseSkeleton(mData.bindPose.mRoot, XMMatrixIdentity(), clips);

			// copy back data into buffer
			for (uint8_t i = 0; i < mData.skeleton.numBones; i++)
			{
				mCurrTransformData[i] = mData.skeleton.jointProps[i].finalTransform;
			}
		}

		// update GPU buffer with fresh interpolated data
		GPUMemoryManager::instance().UpdateBuffer(*mTransformBuffer->GetPtr(), mCurrTransformData.data());
	}
}

void AnimationComponent::Play(const std::string& clipName, const float animationTime)
{
	if (mData.clips.find(clipName) != mData.clips.end())
	{
		mCurrentClip = clipName;
		mCurrentTime = animationTime;
		mState = AnimationComponent::State::PLAY;

		return;
	}

	std::wstring msg = L"Play animation could not locate the desired clip: \"" + str_to_wstr(clipName) + L"\"\nAvailable clip names:\n";

	for (auto& c : mData.clips)
	{
		msg += str_to_wstr(c.first) + L"\n";
	}

	Logger::instance().Msg(LOG_LAYER::LOG_WARNING, msg);
}

void AnimationComponent::Play(const std::string& clipName)
{
	this->Play(clipName, 0.0f);
}

void AnimationComponent::Resume()
{
	this->Play(mCurrentClip);
}

void AnimationComponent::Pause()
{
	mState = AnimationComponent::State::PAUSE;
}

void AnimationComponent::Stop()
{
	mState = AnimationComponent::State::STOP;
	mCurrentTime = 0.0f;
}

bool AnimationComponent::IsPlaying()
{
	return (mState == AnimationComponent::State::PLAY);
}

void AnimationComponent::SetPlaybackSpeed(const float s)
{
	mPlaybackSpeed = s;
}

ConstantBuffer* AnimationComponent::GetTransformBuffer() const
{
	return mTransformBuffer;
}

void AnimationComponent::TraverseSkeleton(const float animTime, const JointBindPose* pNode, const XMMATRIX& parentTransform)
{
	std::string jointName = pNode->mName;

	AnimationClip& pClip = mData.clips[mCurrentClip];

	XMMATRIX jointTransform = pNode->mTransform;

	JointAnimPose* pJointAnim = pClip.FindAnimPose(pNode->mName);

	if (pJointAnim != nullptr)
	{
		// interpolate position
		XMMATRIX T = XMMatrixTranslationFromVector(XMLoadFloat3(&InterpolatePosition(animTime, pJointAnim)));

		// interpolate scale
		XMMATRIX S = XMMatrixScalingFromVector(XMLoadFloat3(&InterpolateScale(animTime, pJointAnim)));

		// interpolate rotation quaternion
		XMMATRIX R = XMMatrixRotationQuaternion(XMLoadFloat4(&InterpolateRotation(animTime, pJointAnim)));

		// combine
		jointTransform = S * R * T;
	}

	XMMATRIX globalTransform = jointTransform * parentTransform;

	if (mData.skeleton.jointMapping.find(jointName) != mData.skeleton.jointMapping.end())
	{
		uint8_t jointIdx = mData.skeleton.jointMapping[jointName];
		mData.skeleton.jointProps[jointIdx].finalTransform = mData.skeleton.jointProps[jointIdx].offset * globalTransform * mData.skeleton.invRootTransform;
	}

	for (size_t i = 0; i < pNode->mChildren.size(); i++)
	{
		this->TraverseSkeleton(animTime, pNode->mChildren[i], globalTransform);
	}
}

void AnimationComponent::TraverseSkeleton(const JointBindPose* pNode, const XMMATRIX& parentTransform, const std::vector<animtypes::ClipBlendInfo>& clip)
{
	std::string jointName = pNode->mName;

	XMMATRIX jointTransform = pNode->mTransform;

	XMVECTOR pos = XMVectorReplicate(0.f);
	XMVECTOR scl = XMVectorReplicate(0.f);
	XMVECTOR rot = XMVectorReplicate(0.f);
	float sum = 0;

	bool update = false;

	for (auto c : clip)
	{
		float tps = mData.clips[c.name].mTicksPerSecond != 0 ? mData.clips[c.name].mTicksPerSecond : 25.0f;

		float timeInTicks = tps * mCurrentTime * mPlaybackSpeed;

		float animationTime = std::fmodf(timeInTicks, mData.clips[c.name].mDuration);

		AnimationClip& pClip = mData.clips[c.name];

		JointAnimPose* pJointAnim = pClip.FindAnimPose(jointName);

		// TODO: use de casteljau for dim > 3! (binary representation)
		// TODO: move this to blend tree?!!
		if (pJointAnim != nullptr)
		{
			if (!update)
			{
				pos = XMLoadFloat3(&InterpolatePosition(animationTime, pJointAnim));
				scl = XMLoadFloat3(&InterpolateScale(animationTime, pJointAnim));
				rot = XMLoadFloat4(&InterpolateRotation(animationTime, pJointAnim));
				sum = c.weight;
			}
			else
			{
				pos = XMVectorLerp(XMLoadFloat3(&InterpolatePosition(animationTime, pJointAnim)), pos, sum / (c.weight + sum));
				scl = XMVectorLerp(XMLoadFloat3(&InterpolateScale(animationTime, pJointAnim)), scl, sum / (c.weight + sum));
				rot = XMQuaternionSlerp(XMLoadFloat4(&InterpolateRotation(animationTime, pJointAnim)), rot, sum / (c.weight + sum));
				sum += c.weight;
			}

			update = true;
		}
	}

	if (update)
	{
		// interpolate position
		XMMATRIX T = XMMatrixTranslationFromVector(pos);

		// interpolate scale
		XMMATRIX S = XMMatrixScalingFromVector(scl);

		// interpolate rotation quaternion
		XMMATRIX R = XMMatrixRotationQuaternion(rot);

		// combine
		jointTransform = S * R * T;
	}

	XMMATRIX globalTransform = jointTransform * parentTransform;

	if (mData.skeleton.jointMapping.find(jointName) != mData.skeleton.jointMapping.end())
	{
		uint8_t jointIdx = mData.skeleton.jointMapping[jointName];
		mData.skeleton.jointProps[jointIdx].finalTransform = mData.skeleton.jointProps[jointIdx].offset * globalTransform * mData.skeleton.invRootTransform;
	}

	for (size_t i = 0; i < pNode->mChildren.size(); i++)
	{
		this->TraverseSkeleton(pNode->mChildren[i], globalTransform, clip);
	}
}

XMFLOAT3 AnimationComponent::InterpolatePosition(const float animTime, const JointAnimPose* pJoint)
{
	// need at least 2 values
	if (pJoint->mPosition.size() == 1)
		return pJoint->mPosition[0].value;

	size_t idx = this->FindPositionIndex(animTime, pJoint);
	size_t nextIdx = idx + 1;
	assert(nextIdx < pJoint->mPosition.size());

	float deltaTime = pJoint->mPosition[nextIdx].time - pJoint->mPosition[idx].time;
	// I think it's a useless precaution
	float factor = MathHelper::Saturate((animTime - pJoint->mPosition[idx].time) / deltaTime);
	
	const XMFLOAT3& startPos = pJoint->mPosition[idx].value;
	const XMFLOAT3& endPos = pJoint->mPosition[nextIdx].value;

	XMFLOAT3 result;
	XMStoreFloat3(&result, XMVectorLerp(XMLoadFloat3(&startPos), XMLoadFloat3(&endPos), factor));

	return result;
}

XMFLOAT3 AnimationComponent::InterpolateScale(const float animTime, const JointAnimPose* pJoint)
{
	// need at least 2 values
	if (pJoint->mScale.size() == 1)
		return pJoint->mScale[0].value;

	size_t idx = this->FindScaleIndex(animTime, pJoint);
	size_t nextIdx = idx + 1;
	assert(nextIdx < pJoint->mScale.size());

	float deltaTime = pJoint->mScale[nextIdx].time - pJoint->mScale[idx].time;
	// I think it's a useless precaution
	float factor = MathHelper::Saturate((animTime - pJoint->mScale[idx].time) / deltaTime);

	const XMFLOAT3& startScale = pJoint->mScale[idx].value;
	const XMFLOAT3& endScale = pJoint->mScale[nextIdx].value;

	XMFLOAT3 result;
	XMStoreFloat3(&result, XMVectorLerp(XMLoadFloat3(&startScale), XMLoadFloat3(&endScale), factor));

	return result;
}

XMFLOAT4 AnimationComponent::InterpolateRotation(const float animTime, const JointAnimPose* pJoint)
{
	// need at least 2 values
	if (pJoint->mRotationQuat.size() == 1)
		return pJoint->mRotationQuat[0].value;

	size_t idx = this->FindRotationIndex(animTime, pJoint);
	size_t nextIdx = idx + 1;
	assert(nextIdx < pJoint->mRotationQuat.size());

	float deltaTime = pJoint->mRotationQuat[nextIdx].time - pJoint->mRotationQuat[idx].time;
	// I think it's a useless precaution
	float factor = MathHelper::Saturate((animTime - pJoint->mRotationQuat[idx].time) / deltaTime);

	const XMFLOAT4& startRot = pJoint->mRotationQuat[idx].value;
	const XMFLOAT4& endRot = pJoint->mRotationQuat[nextIdx].value;

	XMFLOAT4 result;
	XMStoreFloat4(&result, XMQuaternionNormalize(XMQuaternionSlerp(XMLoadFloat4(&startRot), XMLoadFloat4(&endRot), factor)));

	return result;
}

size_t AnimationComponent::FindPositionIndex(const float animTime, const JointAnimPose* pJoint)
{
	for (size_t i = 0; i < pJoint->mPosition.size() - 1; i++)
	{
		if (animTime < pJoint->mPosition[i + 1].time)
			return i;
	}

	core::TsunamiFatalError(L"[ANIMATION]: FindPositionIndex return illegal index. Abort.");

	return SIZE_T_ERROR;
}

size_t AnimationComponent::FindScaleIndex(const float animTime, const JointAnimPose* pJoint)
{
	for (size_t i = 0; i < pJoint->mScale.size() - 1; i++)
	{
		if (animTime < pJoint->mScale[i + 1].time)
			return i;
	}

	core::TsunamiFatalError(L"[ANIMATION]: FindScaleIndex return illegal �ndex. Abort.");

	return SIZE_T_ERROR;
}

size_t AnimationComponent::FindRotationIndex(const float animTime, const JointAnimPose* pJoint)
{
	for (size_t i = 0; i < pJoint->mRotationQuat.size() - 1; i++)
	{
		if (animTime < pJoint->mRotationQuat[i + 1].time)
			return i;
	}

	core::TsunamiFatalError(L"[ANIMATION]: FindRotationIndex return illegal �ndex. Abort.");

	return SIZE_T_ERROR;
}
