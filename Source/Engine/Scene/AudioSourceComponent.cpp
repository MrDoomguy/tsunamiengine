#include "AudioSourceComponent.h"

AudioSourceComponent::AudioSourceComponent() :
	IComponent(IComponent::Type::AUDIO_SOURCE)
{
}

AudioSourceComponent::~AudioSourceComponent()
{
}

void AudioSourceComponent::AddClip(const std::string& name, AudioClip* clip)
{
	if (mClips.find(name) != mClips.end())
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Clip already present in this source. Doing NOP instead.");
		return;
	}
	
	mClips.insert({ name, clip });
}

AudioClip* AudioSourceComponent::GetClip(const std::string& name)
{
	if (mClips.find(name) != mClips.end())
		return mClips[name];

	return nullptr;
}

void AudioSourceComponent::Play(const std::string& clip)
{
	// safe guard for invalid internal state e.g. first time play
	if (mClips.find(mCurrClip) != mClips.end())
	{
		if (mClips[mCurrClip]->IsPlaying())
		{
			mClips[mCurrClip]->Stop();
		}
	}

	mCurrClip = clip;
	mClips[clip]->Play();
}

void AudioSourceComponent::Stop()
{
	if (mClips.find(mCurrClip) != mClips.end())
	{
		if (mClips[mCurrClip]->IsPlaying())
		{
			mClips[mCurrClip]->Stop();
		}
	}
}
