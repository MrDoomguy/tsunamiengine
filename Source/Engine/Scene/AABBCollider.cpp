#include "AABBCollider.h"

AABBCollider::AABBCollider(XMFLOAT3 size, const XMFLOAT3& pos)
	: OBBCollider(size, pos)
{
	mInheritRot = false;
}

AABBCollider::AABBCollider(ICollider * parent, XMFLOAT3 size, const XMFLOAT3 & pos)
	: OBBCollider(parent, size, pos)
{
	mInheritRot = false;
}


AABBCollider::~AABBCollider()
{
}

bool AABBCollider::CollisionDetection(ICollider* other)
{
	if (AABBCollider* ab = dynamic_cast<AABBCollider*>(other))
	{
		XMFLOAT3 p1;
		XMFLOAT3 p2;
		XMStoreFloat3(&p1, GetWorldPosition());
		XMStoreFloat3(&p2, ab->GetWorldPosition());
		XMFLOAT3 depth = GetDepth(p1, p2, GetWorldHalfedSize(), ab->GetWorldHalfedSize());

		if (depth.x > 0 && depth.y > 0 && depth.z > 0) return true;
		return false;
	}

	return OBBCollider::CollisionDetection(other);
}

void AABBCollider::GetContactInfo(Collision * collision, ICollider * other)
{
	if (AABBCollider* ab = dynamic_cast<AABBCollider*>(other))
	{
		XMFLOAT3 p1;
		XMFLOAT3 p2;
		XMFLOAT3 halfedSize1 = GetWorldHalfedSize();
		XMFLOAT3 halfedSize2 = ab->GetWorldHalfedSize();

		XMStoreFloat3(&p1, GetWorldPosition());
		XMStoreFloat3(&p2, ab->GetWorldPosition());

		XMFLOAT3 depth = GetDepth(p1, p2, halfedSize1, halfedSize2);
		XMFLOAT3 sign = GetSigns(p1, p2);

		HandleFaceInFaceCase(&sign, &p1, &depth, p2, halfedSize1, halfedSize2);

		XMVECTOR sg = XMLoadFloat3(&sign);
		collision->dir = XMVector3Normalize(sg * XMLoadFloat3(&XMFLOAT3(depth.y * depth.z, depth.x * depth.z, depth.x * depth.y)));
		XMFLOAT3 dir;
		XMStoreFloat3(&dir, collision->dir);
		collision->depth = GetCollisionDepth(depth, dir);
		collision->point = XMLoadFloat3(&p1) - sg * (XMLoadFloat3(&halfedSize1) - XMLoadFloat3(&depth) / 2);
	}
	else
	{
		OBBCollider::GetContactInfo(collision, other);
	}
}

XMFLOAT3 AABBCollider::GetSigns(const XMFLOAT3 & p1, const XMFLOAT3 & p2)
{
	return XMFLOAT3(p2.x < p1.x ? 1.f : -1.f, p2.y < p1.y ? 1.f : -1.f, p2.z < p1.z ? 1.f : -1.f);
}

XMFLOAT3 AABBCollider::GetDepth(const XMFLOAT3 & p1, const XMFLOAT3 & p2, const XMFLOAT3 & size, const XMFLOAT3 & otherSize)
{
	return XMFLOAT3(
		size.x + otherSize.x - abs(p2.x - p1.x), 
		size.y + otherSize.y - abs(p2.y - p1.y),
		size.z + otherSize.z - abs(p2.z - p1.z));
}

float AABBCollider::GetCollisionDepth(const XMFLOAT3 & depth, const XMFLOAT3 & dir)
{
	if (depth.x > depth.y)
	{
		if (depth.y > depth.z)
		{
			float adir = abs(dir.z);
			if (adir < FLT_EPSILON) return depth.z;
			return depth.z / adir;
		}
		float adir = abs(dir.y);
		if (adir < FLT_EPSILON) return depth.y;
		return depth.y / adir;
	}
	else if (depth.x > depth.z)
	{
		float adir = abs(dir.z);
		if (adir < FLT_EPSILON) return depth.z;
		return depth.z / adir;
	}

	float adir = abs(dir.x);
	if (adir < FLT_EPSILON) return depth.x;
	return depth.x / adir;

}

void AABBCollider::HandleFaceInFaceCase(XMFLOAT3* sign, XMFLOAT3 * p1, XMFLOAT3* depth, const XMFLOAT3 & p2, const XMFLOAT3 & size, const XMFLOAT3 & otherSize)
{
	if (p1->x + size.x <= p2.x + otherSize.x && p1->x - size.x >= p2.x - otherSize.x)
	{
		depth->x = size.x;
		sign->x = 0;
	}
	else if (p1->x + size.x > p2.x + otherSize.x && p1->x - size.x < p2.x - otherSize.x)
	{
		depth->x = otherSize.x;
		sign->x = 0;
		p1->x = p2.x;
	}
	if (p1->y + size.y <= p2.y + otherSize.y && p1->y - size.y >= p2.y - otherSize.y)
	{
		depth->y = size.y;
		sign->y = 0;
	}
	else if (p1->y + size.y > p2.y + otherSize.y && p1->y - size.y < p2.y - otherSize.y)
	{
		depth->y = otherSize.y;
		sign->y = 0;
		p1->y = p2.y;
	}
	if (p1->z + size.z <= p2.z + otherSize.z && p1->z - size.z >= p2.z - otherSize.z)
	{
		depth->z = size.z;
		sign->z = 0;
	}
	else if (p1->z + size.z > p2.z + otherSize.z && p1->z - size.z < p2.z - otherSize.z)
	{
		depth->z = otherSize.z;
		sign->z = 0;
		p1->z = p2.z;
	}

}
