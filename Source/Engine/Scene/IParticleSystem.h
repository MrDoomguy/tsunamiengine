#pragma once
#include "../Render/Data/PrimitiveMesh.h"
#include "IPhysicComponent.h"
#include "../Render/Data/VertexBuffer.h"


//template <class T>
class IParticleSystem : public IPhysicComponent
{
public:
	IParticleSystem(PrimitiveMesh* mesh) : IPhysicComponent(), mMaxCount(mesh->GetVertexCount()), mMesh(mesh) {};
	IParticleSystem(ICollider* collider, PrimitiveMesh* mesh) : IPhysicComponent(collider), mMaxCount(mesh->GetVertexCount()) {};
	~IParticleSystem() {}

	virtual void InstantiateParticles() = 0;

	int GetMaxCount() { return mMaxCount; }

protected:

	int mMaxCount;

	PrimitiveMesh* mMesh;
};