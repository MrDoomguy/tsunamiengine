#include "TransformComponent.h"

TransformComponent::TransformComponent(TransformComponent* pParent, const bool inheritTranslation, const bool inheritScale, const bool inheritRotation) :
	IComponent(IComponent::Type::TRANSFORM),
	mPosition{ XMFLOAT3(0.0f, 0.0f, 0.0f) },
	mRotation{ XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
	mScale{ XMFLOAT3(1.0f, 1.0f, 1.0f) },
	mForward{ XMFLOAT3(0.0f, 0.0f, 1.0f) },
	mUp{ XMFLOAT3(0.0f, 1.0f, 0.0f) },
	mRight{ XMFLOAT3(1.0f, 0.0f, 0.0f) },
	mParent{ pParent },
	mInheritTranslate{ inheritTranslation},
	mInheritScale{ inheritScale },
	mInheritRotation{ inheritRotation }
{
	mParent->mChildren.push_back(this);
	MakeWorldMatrix();
}

TransformComponent::TransformComponent() :
	IComponent(IComponent::Type::TRANSFORM),
	mPosition{ XMFLOAT3(0.0f, 0.0f, 0.0f) },
	mRotation{ XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
	mScale{ XMFLOAT3(1.0f, 1.0f, 1.0f) },
	mForward{ XMFLOAT3(0.0f, 0.0f, 1.0f) },
	mUp{ XMFLOAT3(0.0f, 1.0f, 0.0f) },
	mRight{ XMFLOAT3(1.0f, 0.0f, 0.0f) },
	mParent{ nullptr },
	mInheritTranslate{ false },
	mInheritScale{ false },
	mInheritRotation{ false }
{
	MakeWorldMatrix();
}

TransformComponent::~TransformComponent()
{
}

void TransformComponent::SetPosition(const XMFLOAT3& position)
{
	mDirty = true;
	mPosition = position;
	MakeWorldMatrix();
}

void TransformComponent::SetRotation(const XMFLOAT3& rotation)
{
	mDirty = true;
	XMStoreFloat4(&mRotation, XMQuaternionRotationRollPitchYaw(XMConvertToRadians(rotation.x), XMConvertToRadians(rotation.y), XMConvertToRadians(rotation.z)));
	MakeWorldMatrix();
}

void TransformComponent::SetRotation(const XMFLOAT4& rotation)
{
	mDirty = true;
	mRotation = rotation;
	MakeWorldMatrix();
}

void TransformComponent::SetScale(const XMFLOAT3& scale)
{
	mDirty = true;
	mScale = scale;
	MakeWorldMatrix();
}

XMFLOAT3 TransformComponent::GetPosition() const
{
	return mPosition;
}

XMFLOAT4 TransformComponent::GetRotation() const
{
	return mRotation;
}

XMFLOAT3 TransformComponent::GetEuler() const
{
	double ysqr = mRotation.y *  mRotation.y;
	double t0 = -2.0f * (ysqr + mRotation.z *  mRotation.z) + 1.0f;
	double t1 = +2.0f * (mRotation.x * mRotation.y - mRotation.w * mRotation.z);
	double t2 = -2.0f * (mRotation.x * mRotation.z + mRotation.w * mRotation.y);
	double t3 = +2.0f * (mRotation.y * mRotation.z - mRotation.w * mRotation.x);
	double t4 = -2.0f * (mRotation.x * mRotation.x + ysqr) + 1.0f;

	t2 = t2 > 1.0f ? 1.0f : t2;
	t2 = t2 < -1.0f ? -1.0f : t2;

	XMFLOAT3 res;

	res.y = -XMConvertToDegrees(std::asinf(static_cast<float>(t2)));
	res.x = -XMConvertToDegrees(std::atan2f(static_cast<float>(t3), static_cast<float>(t4)));
	res.z = -XMConvertToDegrees(std::atan2f(static_cast<float>(t1), static_cast<float>(t0)));

	return res;
}

XMFLOAT3 TransformComponent::GetScale() const
{
	return mScale;
}

XMFLOAT3 TransformComponent::GetWorldPosition() const
{
	if (mParent != nullptr && mInheritTranslate)
	{
		XMFLOAT3 r;
		XMStoreFloat3(&r, XMVector4Transform(XMLoadFloat4(&XMFLOAT4(0, 0, 0, 1)), GetWorld()));
		return r;
	}
	return mPosition;
}

XMFLOAT4 TransformComponent::GetWorldRotation() const
{
	if (mParent != nullptr && mInheritRotation)
	{
		XMFLOAT4 r;
		XMStoreFloat4(&r, XMQuaternionMultiply(XMLoadFloat4(&mParent->GetWorldRotation()), XMLoadFloat4(&mRotation)));
		return r;
	}
	return mRotation;
}

XMFLOAT3 TransformComponent::GetWorldEuler() const
{
	if (mParent != nullptr && mInheritRotation)
	{
		XMFLOAT4 rot = GetWorldRotation();
		double ysqr = rot.y *  rot.y;
		double t0 = -2.0f * (ysqr + rot.z *  rot.z) + 1.0f;
		double t1 = +2.0f * (rot.x * rot.y - rot.w * rot.z);
		double t2 = -2.0f * (rot.x * rot.z + rot.w * rot.y);
		double t3 = +2.0f * (rot.y * rot.z - rot.w * rot.x);
		double t4 = -2.0f * (rot.x * rot.x + ysqr) + 1.0f;

		t2 = t2 > 1.0f ? 1.0f : t2;
		t2 = t2 < -1.0f ? -1.0f : t2;

		XMFLOAT3 res;

		res.y = -XMConvertToDegrees(std::asinf(static_cast<float>(t2)));
		res.x = -XMConvertToDegrees(std::atan2f(static_cast<float>(t3), static_cast<float>(t4)));
		res.z = -XMConvertToDegrees(std::atan2f(static_cast<float>(t1), static_cast<float>(t0)));

		return res;
	}

	return GetEuler();
}

XMFLOAT3 TransformComponent::GetWorldScale() const
{
	if (mParent != nullptr && mInheritScale)
	{
		XMFLOAT3 r;
		r.x = XMVectorGetX(XMVector3Length(XMVector4Transform(XMLoadFloat4(&XMFLOAT4(1, 0, 0, 0)), GetWorld())));
		r.y = XMVectorGetX(XMVector3Length(XMVector4Transform(XMLoadFloat4(&XMFLOAT4(0, 1, 0, 0)), GetWorld())));
		r.z = XMVectorGetX(XMVector3Length(XMVector4Transform(XMLoadFloat4(&XMFLOAT4(0, 0, 1, 0)), GetWorld())));
		return r;
	}
	return mScale;
}

XMFLOAT3 TransformComponent::GetForward() const
{
	return mForward;
}

XMFLOAT3 TransformComponent::GetRight() const
{
	return mRight;
}

XMFLOAT3 TransformComponent::GetUp() const
{
	return mUp;
}

XMFLOAT4 TransformComponent::GetLookAt(const XMFLOAT3& target)
{
	XMVECTOR F = XMVector3Normalize(XMLoadFloat3(&target) - XMLoadFloat3(&mPosition));

	XMFLOAT3 f;
	XMStoreFloat3(&f, F);

	float h = XMVectorGetX(XMVector3Length(F * XMLoadFloat3(&XMFLOAT3(1, 0, 1))));

	if (h < FLT_EPSILON)
	{
		XMFLOAT4 rot;
		XMStoreFloat4(&rot, XMQuaternionRotationAxis(XMLoadFloat3(&XMFLOAT3(1, 0, 0)), XM_PI * (f.y > 0 ? -0.5f : 0.5f)));
		return rot;
	}

	float rotAngleX = -asin(f.y);
	float rotAngleY = asin(f.x / h);
	if (f.z < 0) rotAngleY = DirectX::XM_PI - rotAngleY;

	XMFLOAT4 rot;

	XMStoreFloat4(&rot, XMQuaternionMultiply(XMQuaternionRotationAxis(XMLoadFloat3(&XMFLOAT3(1, 0, 0)), rotAngleX), XMQuaternionRotationAxis(XMLoadFloat3(&XMFLOAT3(0, 1, 0)), rotAngleY)));
	return rot;
}

void TransformComponent::LookAt(const XMFLOAT3& target)
{
	SetRotation(GetLookAt(target));
}

void TransformComponent::Translate(const XMFLOAT3& offset)
{
	XMFLOAT3 p;
	XMStoreFloat3(&p, XMVectorAdd(XMLoadFloat3(&mPosition), XMLoadFloat3(&offset)));

	SetPosition(p);
}

void TransformComponent::Rotate(const XMFLOAT3& axis, float angle)
{
	XMFLOAT4 rot;
	XMStoreFloat4(&rot, XMQuaternionMultiply(XMLoadFloat4(&mRotation), XMQuaternionRotationAxis(XMLoadFloat3(&axis), XMConvertToRadians(angle))));
	SetRotation(rot);
}

void TransformComponent::Rotate(const XMFLOAT4& rotation)
{
	XMFLOAT4 rot;
	XMStoreFloat4(&rot, XMQuaternionMultiply(XMLoadFloat4(&mRotation), XMLoadFloat4(&rotation)));
	SetRotation(rot);
}

void TransformComponent::Scale(const XMFLOAT3& offset)
{
	XMFLOAT3 s;
	XMStoreFloat3(&s, XMVectorMultiply(XMLoadFloat3(&mScale), XMLoadFloat3(&offset)));

	SetScale(s);
}

void TransformComponent::Roll(float angle)
{
	XMFLOAT4 rot;
	XMStoreFloat4(&rot, XMQuaternionRotationAxis(XMLoadFloat3(&mForward), XMConvertToRadians(angle)));

	Rotate(rot);
}

void TransformComponent::Pitch(float angle)
{
	XMFLOAT4 rot;
	XMStoreFloat4(&rot, XMQuaternionRotationAxis(XMLoadFloat3(&mRight), XMConvertToRadians(angle)));

	Rotate(rot);
}

void TransformComponent::Yaw(float angle)
{
	XMFLOAT4 rot;
	XMStoreFloat4(&rot, XMQuaternionRotationAxis(XMLoadFloat3(&mUp), XMConvertToRadians(angle)));

	Rotate(rot);
}

void TransformComponent::Walk(const float distance)
{
	XMVECTOR offset = XMVectorReplicate(distance);
	XMVECTOR F = XMLoadFloat3(&mForward);
	XMVECTOR P = XMLoadFloat3(&mPosition);

	XMFLOAT3 p;
	// V1*V2+V3
	XMStoreFloat3(&p, XMVectorMultiplyAdd(offset, F, P));

	SetPosition(p);
}

void TransformComponent::Strafe(const float distance)
{
	XMVECTOR offset = XMVectorReplicate(distance);
	XMVECTOR R = XMLoadFloat3(&mRight);
	XMVECTOR P = XMLoadFloat3(&mPosition);

	XMFLOAT3 p;
	// V1*V2+V3
	XMStoreFloat3(&p, XMVectorMultiplyAdd(offset, R, P));

	SetPosition(p);
}

void TransformComponent::Fly(const float distance)
{
	XMVECTOR offset = XMVectorReplicate(distance);
	XMVECTOR U = XMLoadFloat3(&mUp);
	XMVECTOR P = XMLoadFloat3(&mPosition);

	XMFLOAT3 p;
	// V1*V2+V3
	XMStoreFloat3(&p, XMVectorMultiplyAdd(offset, U, P));

	SetPosition(p);
}

void TransformComponent::SetParent(TransformComponent* pParent, const bool inheritTranslation, const bool inheritScale, const bool inheritRotation)
{
	if (mParent != nullptr)
	{
		for (int i = 0; i < mParent->mChildren.size(); i++)
		{
			if (mParent->mChildren[i] == this)
			{
				mParent->mChildren.erase(mParent->mChildren.begin() + i);
			}
		}
	}
	if (pParent != nullptr)
	{
		pParent->mChildren.push_back(this);
		mInheritTranslate = inheritTranslation;
		mInheritScale = inheritScale;
		mInheritRotation = inheritRotation;
	}
	mParent = pParent;
	MakeWorldMatrix();
}

void TransformComponent::AddChild(TransformComponent * pChild, const bool inheritTranslation, const bool inheritScale, const bool inheritRotation)
{
	pChild->SetParent(this, inheritTranslation, inheritScale, inheritRotation);
}

void TransformComponent::RemoveChild(int i)
{
	if (i < mChildren.size()) mChildren[i]->SetParent(nullptr);
}

void TransformComponent::InheritTranslate(const bool inherit)
{
	mInheritTranslate = inherit;
}

void TransformComponent::InheritScale(const bool inherit)
{
	mInheritScale = inherit;
}

void TransformComponent::InheritRotation(const bool inherit)
{
	mInheritRotation = inherit;
}

bool TransformComponent::HasParent()
{
	return mParent != nullptr;
}

bool TransformComponent::HasChildren()
{
	return mChildren.size() > 0;
}

XMMATRIX TransformComponent::GetWorld() const
{
	return XMLoadFloat4x4(&mTransformCache);
}

XMMATRIX TransformComponent::GetLocal() const
{
	return XMLoadFloat4x4(&mInverseTransformCache);
}

void TransformComponent::Print()
{
	XMFLOAT3 rot = GetEuler();
	std::cout << ".: Transform Component Debug Print:." << std::endl;
	std::cout << "Position:\t" << mPosition.x << "\t" << mPosition.y << "\t" << mPosition.z << std::endl;
	std::cout << "Rotation:\t" << rot.x << "\t" << rot.y << "\t" << rot.z << std::endl;
	std::cout << "Scale:\t\t" << mScale.x << "\t" << mScale.y << "\t" << mScale.z << std::endl;
	std::cout << "Right:\t\t" << mRight.x << "\t" << mRight.y << "\t" << mRight.z << std::endl;
	std::cout << "Up:\t\t" << mUp.x << "\t" << mUp.y << "\t" << mUp.z << std::endl;
	std::cout << "Forward:\t" << mForward.x << "\t" << mForward.y << "\t" << mForward.z << std::endl;
	std::cout << "World:\n" << mTransformCache._11 << "\t" << mTransformCache._12 << "\t" << mTransformCache._13 << "\t" << mTransformCache._14 << "\n" << mTransformCache._21 << "\t" << mTransformCache._22 << "\t" << mTransformCache._23 << "\t" << mTransformCache._24 << "\n" << mTransformCache._31 << "\t" << mTransformCache._32 << "\t" << mTransformCache._33 << "\t" << mTransformCache._34 << "\n" << mTransformCache._41 << "\t" << mTransformCache._42 << "\t" << mTransformCache._43 << "\t" << mTransformCache._44 << "\n";
	std::cout << "Local:\n" << mInverseTransformCache._11 << "\t" << mInverseTransformCache._12 << "\t" << mInverseTransformCache._13 << "\t" << mInverseTransformCache._14 << "\n" << mInverseTransformCache._21 << "\t" << mInverseTransformCache._22 << "\t" << mInverseTransformCache._23 << "\t" << mInverseTransformCache._24 << "\n" << mInverseTransformCache._31 << "\t" << mInverseTransformCache._32 << "\t" << mInverseTransformCache._33 << "\t" << mInverseTransformCache._34 << "\n" << mInverseTransformCache._41 << "\t" << mInverseTransformCache._42 << "\t" << mInverseTransformCache._43 << "\t" << mInverseTransformCache._44 << "\n";
}

void TransformComponent::Print(std::wstringstream& s)
{
	XMFLOAT3 rot = GetEuler();
	s << std::fixed << std::setprecision(4);
	s << L"\nPosition:\t" << mPosition.x << "\t" << mPosition.y << "\t" << mPosition.z;
	s << L"\nRotation:\t" << rot.x << "\t" << rot.y << "\t" << rot.z;
	s << L"\nScale:\t" << mScale.x << "\t" << mScale.y << "\t" << mScale.z;
	s << L"\nRight:\t" << mRight.x << "\t" << mRight.y << "\t" << mRight.z;
	s << L"\nUp:\t" << mUp.x << "\t" << mUp.y << "\t" << mUp.z;
	s << L"\nForward:\t" << mForward.x << "\t" << mForward.y << "\t" << mForward.z;
}

void TransformComponent::Print(std::ofstream& s)
{
	XMFLOAT3 rot = GetEuler();
	s << std::fixed << std::setprecision(4);
	s << "\nPosition:\t" << mPosition.x << "\t" << mPosition.y << "\t" << mPosition.z;
	s << "\nRotation:\t" << rot.x << "\t" << rot.y << "\t" << rot.z;
	s << "\nScale:\t" << mScale.x << "\t" << mScale.y << "\t" << mScale.z << "\n\n";
}

void TransformComponent::MakeWorldMatrix()
{
	if (mParent != nullptr)
	{
		XMMATRIX PT = XMMatrixIdentity();
		if (mInheritScale) PT *= mParent->MakeScale();
		if (mInheritRotation) PT *= mParent->MakeRotation();
		if (mInheritTranslate) PT *= mParent->MakeTranslation();

		PT = PT * XMLoadFloat4x4(&mParent->mParentTransformCache);
		XMStoreFloat4x4(&mParentTransformCache, PT);
		XMStoreFloat4x4(&mTransformCache, MakeScale() * MakeRotation() * MakeTranslation() * PT);

		PT = XMMatrixIdentity();
		if (mInheritTranslate) PT *= mParent->MakeInvTranslation();
		if (mInheritRotation) PT *= mParent->MakeInvRotation();
		if (mInheritScale) PT *= mParent->MakeInvScale();

		PT = XMLoadFloat4x4(&mParent->mInverseParentTransformCache) * PT;
		XMStoreFloat4x4(&mInverseParentTransformCache, PT);
		XMStoreFloat4x4(&mInverseTransformCache, PT * MakeInvTranslation() * MakeInvRotation() * MakeInvScale());
	}
	else
	{
		XMStoreFloat4x4(&mTransformCache, MakeScale() * MakeRotation() * MakeTranslation());
		XMStoreFloat4x4(&mInverseTransformCache, MakeInvTranslation() * MakeInvRotation() * MakeInvScale());
		XMStoreFloat4x4(&mParentTransformCache, XMMatrixIdentity());
		XMStoreFloat4x4(&mInverseParentTransformCache, XMMatrixIdentity());
	}

	XMStoreFloat3(&mRight, XMVector3Rotate(XMLoadFloat3(&XMFLOAT3(1, 0, 0)), XMLoadFloat4(&GetWorldRotation())));
	XMStoreFloat3(&mUp, XMVector3Rotate(XMLoadFloat3(&XMFLOAT3(0, 1, 0)), XMLoadFloat4(&GetWorldRotation())));
	XMStoreFloat3(&mForward, XMVector3Rotate(XMLoadFloat3(&XMFLOAT3(0, 0, 1)), XMLoadFloat4(&GetWorldRotation())));

	for (auto c : mChildren) c->MakeWorldMatrix();
}

XMMATRIX TransformComponent::MakeTranslation()
{
	return XMMatrixTranslationFromVector(XMLoadFloat3(&mPosition));
}

XMMATRIX TransformComponent::MakeScale()
{
	return XMMatrixScalingFromVector(XMLoadFloat3(&mScale));
}

XMMATRIX TransformComponent::MakeRotation()
{
	return XMMatrixRotationQuaternion(XMLoadFloat4(&mRotation));
}

XMMATRIX TransformComponent::MakeInvTranslation()
{
	return XMMatrixTranslationFromVector(-XMLoadFloat3(&mPosition));
}

XMMATRIX TransformComponent::MakeInvScale()
{
	return XMMatrixScaling(1 / mScale.x, 1 / mScale.y, 1 / mScale.z);
}

XMMATRIX TransformComponent::MakeInvRotation()
{
	return XMMatrixRotationQuaternion(XMQuaternionInverse(XMLoadFloat4(&mRotation)));
}
