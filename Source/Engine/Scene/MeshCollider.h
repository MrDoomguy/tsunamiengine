#pragma once
#include "ICollider.h"
#include "OBBCollider.h"
#include "../../Engine/Render/Data/MeshHelper.h"
#include "../../Engine/Render/Data/IndexedMesh.h"

struct AreaVertex
{
	XMFLOAT3 pos;
	XMFLOAT3 norArea;
};
struct Triangle
{
	XMFLOAT3 p0;
	XMFLOAT3 p1;
	XMFLOAT3 p2;
};

class MeshCollider :
	public ICollider
{
public:
	explicit MeshCollider(IndexedMesh* mesh, XMFLOAT3 pos = XMFLOAT3(0, 0, 0), XMFLOAT3 rot = XMFLOAT3(0, 0, 0));
	explicit MeshCollider(ICollider* parent, IndexedMesh* mesh, XMFLOAT3 pos = XMFLOAT3(0, 0, 0), XMFLOAT3 rot = XMFLOAT3(0, 0, 0));
	~MeshCollider();
	virtual bool CollisionDetection(ICollider* other) override;
	virtual void GetContactInfo(Collision* collision, ICollider * other) override;

	ID3D11ShaderResourceView* const * GetMesh() const { return mMesh->GetSRV(); }
	int GetMeshCount() { return mMeshCount; }

protected:
	IndexedMesh* mMesh = nullptr;
	int mMeshCount;
};

