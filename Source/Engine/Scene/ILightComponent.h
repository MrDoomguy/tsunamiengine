#ifndef _ILIGHTCOMPONENT_H_
#define _ILIGHTCOMPONENT_H_

#include "IComponent.h"
#include <DirectXMath.h>
#include <cstdint>

using namespace DirectX;

class ILightComponent : public IComponent
{
public:
	enum class Type : std::uint8_t
	{
		DIRECTIONAL = 0x00,
		POINT = 0x01,
		SPOT = 0x02
	};

	explicit ILightComponent(const ILightComponent::Type type, const XMFLOAT4& ambient, const XMFLOAT4& diffuse, const XMFLOAT4& specular)
		: IComponent(IComponent::Type::LIGHT), mLightType{ type }, mAmbient{ ambient }, mDiffuse{ diffuse }, mSpecular{ specular } { }
	~ILightComponent() { }

	virtual XMFLOAT4 GetAmbient() const { return mAmbient; }
	virtual void SetAmbient(const XMFLOAT4& ambient) { mAmbient = ambient; }

	virtual XMFLOAT4 GetDiffuse() const { return mDiffuse; }
	virtual void SetDiffuse(const XMFLOAT4& diffuse) { mDiffuse = diffuse; }

	virtual XMFLOAT4 GetSpecular() const { return mSpecular; }
	virtual void SetSpecular(const XMFLOAT4& specular) { mSpecular = specular; }

	ILightComponent::Type GetLightType() const { return mLightType; }

protected:
	ILightComponent::Type mLightType;
	XMFLOAT4 mAmbient;
	XMFLOAT4 mDiffuse;
	XMFLOAT4 mSpecular;
};

#endif
