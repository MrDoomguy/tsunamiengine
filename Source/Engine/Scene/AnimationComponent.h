#ifndef _ANIMATIONCOMPONENT_H_
#define _ANIMATIONCOMPONENT_H_

#include "../Animation/AnimationTypes.h"
#include "../Animation/AnimationCallbacks.h"

#include "../Render/Data/SkeletalMesh.h"
#include "../Render/Data/ConstantBuffer.h"

#include "../General/GPUMemoryManager.h"
#include "../Animation/Animator.h"
#include <stack>

#include "IComponent.h"

using namespace animtypes;

class AnimationComponent : public IComponent
{
public:
	explicit AnimationComponent(ID3D11Device* pDevice, const AnimationData& animData, Animator* animator = nullptr);
	~AnimationComponent();

	void Update(const float dt);

	void Play(const std::string& clipName, const float animationTime);
	void Play(const std::string& clipName);
	void Resume();
	void Pause();
	void Stop();

	bool IsPlaying();

	void SetPlaybackSpeed(const float s);

	Animator* GetAnimator() { return mAnimator; }

	ConstantBuffer* GetTransformBuffer() const;

	void RegisterTriggerCallback(PFN_TRIGGER_CALLBACK pTriggerFnc);
	void RegisterPoseCallback(PFN_PASSIVE_POSE_CALLBACK pPassiveFnc);

private:
	enum class State : std::uint8_t
	{
		PLAY =	0b00000001,
		PAUSE = 0b00000010,
		STOP =	0b00000100
	};

	// update the cache if necessary
	// read from skeleton
	// interpolate from clip * bind pose
	void TraverseSkeleton(const float animTime, const JointBindPose* pNode, const XMMATRIX& parentTransform);
	void TraverseSkeleton(const JointBindPose* pNode, const XMMATRIX& parentTransform, const std::vector<animtypes::ClipBlendInfo>& clip);

	// keyframe interpolation helper
	XMFLOAT3 InterpolatePosition(const float animTime, const JointAnimPose* pJoint);
	XMFLOAT3 InterpolateScale(const float animTime, const JointAnimPose* pJoint);
	XMFLOAT4 InterpolateRotation(const float animTime, const JointAnimPose* pJoint);
	
	size_t FindPositionIndex(const float animTime, const JointAnimPose* pJoint);
	size_t FindScaleIndex(const float animTime, const JointAnimPose* pJoint);
	size_t FindRotationIndex(const float animTime, const JointAnimPose* pJoint);

	// skeleton + clips + bind pose
	AnimationData mData;

	Animator* mAnimator;
	
	// animation state
	AnimationComponent::State mState = AnimationComponent::State::STOP;

	// current clip being played
	std::string mCurrentClip = "";
	float mCurrentTime = 0.0f;
	float mPlaybackSpeed = 1.0f;

	// Buffered Data
	std::vector<XMMATRIX> mCurrTransformData;
	ConstantBuffer* mTransformBuffer;

	// TODO : reserved for future use
	// will be used for clip blending
	// on state machine besed architecture.
	std::string mLastClip = "";
};

#endif
