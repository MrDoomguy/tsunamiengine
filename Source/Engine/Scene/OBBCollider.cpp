#include "OBBCollider.h"

OBBCollider::OBBCollider(XMFLOAT3 size, const XMFLOAT3& pos, const XMFLOAT3& rot)
	: ICollider(pos, rot)
{
	mHalfedSize = XMFLOAT3(size.x / 2, size.y / 2, size.z / 2);
}

OBBCollider::OBBCollider(ICollider * parent, XMFLOAT3 size, const XMFLOAT3 & pos, const XMFLOAT3 & rot)
	: ICollider(parent, pos, rot)
{
	mHalfedSize = XMFLOAT3(size.x / 2, size.y / 2, size.z / 2);
}


OBBCollider::~OBBCollider()
{
}

bool OBBCollider::CollisionDetection(ICollider * other)
{
	if (SphereCollider* sc = dynamic_cast<SphereCollider*>(other))
	{
		XMVECTOR center = TransformVertexLocal(sc->GetWorldPosition());

		// TODO: move to aabb!!
		XMFLOAT3 closestPt;
		float dist;

		XMFLOAT3 scale;
		XMStoreFloat3(&scale, GetWorldScale());
		float radius = sc->GetRadius() * 3 / (scale.x + scale.y + scale.z);

		if (abs(XMVectorGetX(center)) - radius > mHalfedSize.x ||
			abs(XMVectorGetY(center)) - radius > mHalfedSize.y ||
			abs(XMVectorGetZ(center)) - radius > mHalfedSize.z)
		{
			return false;
		}

		dist = XMVectorGetX(center);
		if (dist > mHalfedSize.x) dist = mHalfedSize.x;
		if (dist < -mHalfedSize.x) dist = -mHalfedSize.x;
		closestPt.x = dist;

		dist = XMVectorGetY(center);
		if (dist > mHalfedSize.y) dist = mHalfedSize.y;
		if (dist < -mHalfedSize.y) dist = -mHalfedSize.y;
		closestPt.y = dist;

		dist = XMVectorGetZ(center);
		if (dist > mHalfedSize.z) dist = mHalfedSize.z;
		if (dist < -mHalfedSize.z) dist = -mHalfedSize.z;
		closestPt.z = dist;

		dist = XMVectorGetX(XMVector3LengthSq(XMLoadFloat3(&closestPt) - center));
		if (dist > radius * radius) return false;
		return true;
	}
	else if (OBBCollider* ob = dynamic_cast<OBBCollider*>(other))
	{
		XMVECTOR toCenter = ob->GetWorldPosition() - GetWorldPosition();
		BoxAxes b1;
		BoxAxes b2;

		GetBoxAxes(b1);
		ob->GetBoxAxes(b2);

		XMFLOAT3 h1 = GetWorldHalfedSize();
		XMFLOAT3 h2 = ob->GetWorldHalfedSize();

		for (int i = 0; i < 3; i++)
		{
			if (PenetrationOnAxis(h1, h2, b1, b2, b1[i], toCenter) < 0) return false;
		}
		for (int i = 0; i < 3; i++)
		{
			if (PenetrationOnAxis(h1, h2, b1, b2, b2[i], toCenter) < 0) return false;
		}
		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				if (PenetrationOnAxis(h1, h2, b1, b2, XMVector3Cross(b1[j], b2[i]), toCenter) < 0) return false;
			}
		}
		return true;
	}
	else if (MeshCollider* mc = dynamic_cast<MeshCollider*>(other))
	{
		return mc->CollisionDetection(this);
	}
	Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"NYI");
	return false;
}

void OBBCollider::GetContactInfo(Collision * collision, ICollider * other)
{
	if (SphereCollider* sc = dynamic_cast<SphereCollider*>(other))
	{
		XMVECTOR center = TransformVertexLocal(sc->GetWorldPosition());

		// TODO: move to aabb!!
		XMFLOAT3 closestPt;
		float dist;

		XMFLOAT3 scale;
		XMStoreFloat3(&scale, GetWorldScale());
		float radius = sc->GetRadius() * 3 / (scale.x + scale.y + scale.z);

		dist = XMVectorGetX(center);
		if (dist > mHalfedSize.x) dist = mHalfedSize.x;
		if (dist < -mHalfedSize.x) dist = -mHalfedSize.x;
		closestPt.x = dist;

		dist = XMVectorGetY(center);
		if (dist > mHalfedSize.y) dist = mHalfedSize.y;
		if (dist < -mHalfedSize.y) dist = -mHalfedSize.y;
		closestPt.y = dist;

		dist = XMVectorGetZ(center);
		if (dist > mHalfedSize.z) dist = mHalfedSize.z;
		if (dist < -mHalfedSize.z) dist = -mHalfedSize.z;
		closestPt.z = dist;

		collision->point = TransformVertexWorld(XMLoadFloat3(&closestPt));
		XMVECTOR diff = collision->point - sc->GetWorldPosition();
		collision->dir = XMVector3Normalize(diff);
		collision->depth = (sc->GetRadius() - XMVectorGetX(XMVector3Length(diff)));
	}
	else if (OBBCollider* ob = dynamic_cast<OBBCollider*>(other))
	{
		XMVECTOR pos1 = GetWorldPosition();
		XMVECTOR pos2 = ob->GetWorldPosition();
		XMVECTOR toCenter = pos2 - pos1;
		BoxAxes b1;
		BoxAxes b2;

		GetBoxAxes(b1);
		ob->GetBoxAxes(b2);

		XMFLOAT3 h1 = GetWorldHalfedSize();
		XMFLOAT3 h2 = ob->GetWorldHalfedSize();

		float pen = FLT_MAX;
		unsigned best = 0xffffff;

		for (int i = 0; i < 3; i++)
		{
			TryAxis(h1, h2, b1, b2, b1[i], toCenter, i, pen, best);
		}
		
		for (int i = 0; i < 3; i++)
		{
			TryAxis(h1, h2, b1, b2, b2[i], toCenter, i + 3, pen, best);
		}

		// Store the best axis-major, in case we run into almost
		// parallel edge collisions later

		unsigned bestSingleAxis = best;

		for (int j = 0; j < 3; j++)
		{
			for (int i = 0; i < 3; i++)
			{
				TryAxis(h1, h2, b1, b2, XMVector3Cross(b1[j], b2[i]), toCenter, 6 + i + j * 3, pen, best);
			}
		}

		// Make sure we've got a result.

		assert(best != 0xffffff);

		if (best < 3)
		{
			PointFaceContact(b1, b2, ob->mHalfedSize, toCenter, best, collision->point, collision->dir);
			collision->point = ob->TransformVertexWorld(collision->point);
			collision->depth = pen;
			return;
		}
		else if (best < 6)
		{
			PointFaceContact(b2, b1, mHalfedSize, -toCenter, best - 3, collision->point, collision->dir);
			collision->dir *= -1;
			collision->point = TransformVertexWorld(collision->point);
			collision->depth = pen;
			return; 
		}
		else
		{
			best -= 6;
			unsigned b1AxisIndex = best / 3;
			unsigned b2AxisIndex = best % 3;
			XMVECTOR axis1 = b1[b1AxisIndex];
			XMVECTOR axis2 = b2[b2AxisIndex];
			XMVECTOR axis = XMVector3Normalize(XMVector3Cross(axis1, axis2));

			if (XMVectorGetX(XMVector3Dot(axis, toCenter)) > 0) axis *= -1.f;

			float size1;
			float size2;

			if (b1AxisIndex == 0)
			{
				size1 = h1.x;
				h1.x = 0;
			}
			else if (XMVectorGetX(XMVector3Dot(b1[0], axis)) > 0) h1.x *= -1.f;
			if (b2AxisIndex == 0)
			{
				size2 = h2.x;
				h2.x = 0;
			}
			else if (XMVectorGetX(XMVector3Dot(b2[0], axis)) > 0) h2.x *= -1.f;
			if (b1AxisIndex == 1)
			{
				size1 = h1.y;
				h1.y = 0;
			}
			else if (XMVectorGetX(XMVector3Dot(b1[1], axis)) > 0) h1.y *= -1.f;
			if (b2AxisIndex == 1)
			{
				size2 = h2.y;
				h2.y = 0;
			}
			else if (XMVectorGetX(XMVector3Dot(b2[1], axis)) > 0) h2.y *= -1.f;
			if (b1AxisIndex == 2)
			{
				size1 = h1.z;
				h1.z = 0;
			}
			else if (XMVectorGetX(XMVector3Dot(b1[2], axis)) > 0) h1.z *= -1.f;
			if (b2AxisIndex == 2)
			{
				size2 = h2.z;
				h2.z = 0;
			}
			else if (XMVectorGetX(XMVector3Dot(b2[2], axis)) > 0) h2.z *= -1.f;

			XMVECTOR p1 = TransformVertexWorld(XMLoadFloat3(&h1));
			XMVECTOR p2 = other->TransformVertexWorld(XMLoadFloat3(&h2));

			collision->point = ContactPoint(p1, axis1, size1, p2, axis2, size2, bestSingleAxis > 2);
			collision->dir = axis;
			collision->depth = pen;
		}
	}
	else if (MeshCollider* mc = dynamic_cast<MeshCollider*>(other))
	{
		mc->GetContactInfo(collision, this);
		collision->dir *= -1;
	}
}

void OBBCollider::PointFaceContact(const BoxAxes b1, const BoxAxes b2, XMFLOAT3 halfSize, const XMVECTOR& toCenter, unsigned best, XMVECTOR& localPoint, XMVECTOR& dir)
{
	dir = b1[best];
	if (XMVectorGetX(XMVector3Dot(dir, toCenter)) > 0)
	{
		dir *= -1.f;
	}

	if (XMVectorGetX(XMVector3Dot(b2[0], dir)) < 0) halfSize.x *= -1.f;
	if (XMVectorGetY(XMVector3Dot(b2[1], dir)) < 0) halfSize.y *= -1.f;
	if (XMVectorGetZ(XMVector3Dot(b2[2], dir)) < 0) halfSize.z *= -1.f;

	localPoint = XMLoadFloat3(&halfSize);
}
