#include "CameraComponent.h"

CameraComponent::CameraComponent(const uint32_t width, const uint32_t height, const float fovy, const float aspect, const float zn, const float zf)
	: IComponent(IComponent::Type::CAMERA)
{
	SetFrustum(width, height, fovy, aspect, zn, zf);
}

CameraComponent::~CameraComponent()
{
}

void CameraComponent::SetFrustum(const uint32_t width, const uint32_t height, const float fovy, const float aspect, const float zn, const float zf)
{
	mWidth = width;
	mHeight = height;

	mFovY = XMConvertToRadians(fovy);
	mAspect = aspect;
	mNearZ = zn;
	mFarZ = zf;

	mViewZ = mHeight / (2.0f*tanf(mFovY/2.0f));

	mNearWindowHeight = 2.0f * mNearZ * tanf(0.5f*mFovY);
	mFarWindowHeight = 2.0f * mFarZ * tanf(0.5f*mFovY);

	switch (mMode)
	{
	case CameraComponent::LensMode::PERSPECTIVE:
		this->SetPerspective();
		break;
	case CameraComponent::LensMode::ORTHO:
		this->SetOrtho();
		break;
	default:
		break;
	}
}

void CameraComponent::SetPerspective()
{
	mMode = CameraComponent::LensMode::PERSPECTIVE;
	XMMATRIX P = XMMatrixPerspectiveFovLH(mFovY, mAspect, mNearZ, mFarZ);
	XMStoreFloat4x4(&mProj, P);
}

void CameraComponent::SetOrtho()
{
	mMode = CameraComponent::LensMode::ORTHO;
	XMMATRIX P = XMMatrixOrthographicLH((float)mWidth/10, (float)mHeight/10, mNearZ, mFarZ);
	XMStoreFloat4x4(&mProj, P);
}

CameraComponent::LensMode CameraComponent::GetLensMode() const
{
	return mMode;
}

float CameraComponent::GetNearZ() const
{
	return mNearZ;
}

float CameraComponent::GetFarZ() const
{
	return mFarZ;
}

float CameraComponent::GetAspect() const
{
	return mAspect;
}

float CameraComponent::GetFovY() const
{
	return XMConvertToDegrees(mFovY);
}

float CameraComponent::GetFovX() const
{
	float halfWidth = 0.5f*GetNearWindowWidth();
	return XMConvertToDegrees(2.0f*atanf(halfWidth / mNearZ));
}

float CameraComponent::GetViewZ() const
{
	return mViewZ;
}

uint32_t CameraComponent::GetWidth() const
{
	return mWidth;
}

uint32_t CameraComponent::GetHeight() const
{
	return mHeight;
}

float CameraComponent::GetNearWindowWidth() const
{
	return mAspect * mNearWindowHeight;
}

float CameraComponent::GetNearWindowHeight() const
{
	return mNearWindowHeight;
}

float CameraComponent::GetFarWindowWidth() const
{
	return mAspect * mFarWindowHeight;
}

float CameraComponent::GetFarWindowHeight() const
{
	return mFarWindowHeight;
}

XMMATRIX CameraComponent::GetProj() const
{
	return XMLoadFloat4x4(&mProj);
}
