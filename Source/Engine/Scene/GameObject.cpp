#include "GameObject.h"

#include "../General/Logger.h"
#include "IPhysicComponent.h"

GameObject::GameObject(const std::string& name) :
	mName{ name }
{
}

GameObject::GameObject()
{
	mName = std::string("GameObject_" + std::to_string(GUIDGen::GetNext()));
}

GameObject::~GameObject()
{
	for (auto& c : mComponents)
	{
		delete c.second;
	}

	for (auto& s : mScripts)
	{
		delete s.second;
	}
}

void GameObject::SetName(const std::string& name)
{
	mName = name;
}

std::string GameObject::GetName() const
{
	return mName;
}

void GameObject::SetParent(GameObject* pParentGO, const bool inheritTranslation, const bool inheritScale, const bool inheritRotation)
{
	if ((this->FindComponent(IComponent::Type::TRANSFORM) != nullptr) && (pParentGO->GetTransform() != nullptr))
	{
		dynamic_cast<TransformComponent*>(mComponents[IComponent::Type::TRANSFORM])->SetParent(pParentGO->GetTransform(), inheritTranslation, inheritScale, inheritRotation);
	}
}

void GameObject::AddComponent(IComponent* pComponent)
{
	if (this->FindComponent(pComponent->GetType()) != nullptr)
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Overwriting exixsting component in GameObject. Check GameObject construction!");

	pComponent->mGo = this;
	mComponents[pComponent->GetType()] = pComponent;
}

void GameObject::RemoveComponent(const IComponent::Type type)
{
	if (this->FindComponent(type) == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Attempting to remove non existing component from GameObject. Doing NOP instead");
		return;
	}

	delete mComponents[type];
	mComponents.erase(type);
}

void GameObject::AddScript(IScript* script)
{
	if (this->FindScript(script->GetName()) != nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Attemping to add already existing script to GameObject. Doing NOP instead!");
		return;
	}

	script->mGo = this;
	mScripts[script->GetName()] = script;
}

void GameObject::RemoveScript(const std::string& name)
{
	if (this->FindScript(name) == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Attemping to remove non existing script to GameObject. Doing NOP instead!");
		return;
	}

	delete mScripts[name];
	mScripts.erase(name);
}

IComponent* GameObject::GetComponent(const IComponent::Type type)
{
	return this->FindComponent(type);
}

IScript* GameObject::GetScript(const std::string& name)
{
	return this->FindScript(name);
}

std::vector<IScript*> GameObject::GetScript() const
{
	std::vector<IScript*> scripts;
	for (auto s : mScripts)
	{
		scripts.push_back(s.second);
	}

	return scripts;
}

IMaterial::Type GameObject::GetMaterialType()
{
	RenderComponent* dwb = dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER));
	if (dwb != nullptr)
	{
		return dwb->GetMaterial()->GetType();
	}

	return IMaterial::Type::INVALID;
}

IMaterial* GameObject::GetMaterial()
{
	RenderComponent* dwb = dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER));
	if (dwb != nullptr)
	{
		return dwb->GetMaterial();
	}

	return nullptr;
}

// TODO: return pointer to transform which will be changed for the fixed udpate call!
TransformComponent* GameObject::GetTransform()
{
	// returns the needed transform (if physics thread then it returns the physicstransform)
	if (PhysicsEngine::instance().GetThreadId() == std::this_thread::get_id())
	{
		IPhysicComponent* pc = dynamic_cast<IPhysicComponent*>(this->FindComponent(IComponent::Type::PHYSIC));
		if (pc != nullptr)
		{
			return pc->GetPhysicsTransform();
		}
	}
	TransformComponent* tsf = dynamic_cast<TransformComponent*>(this->FindComponent(IComponent::Type::TRANSFORM));
	if (tsf != nullptr)
	{
		return tsf;
	}

	return nullptr;
}

CameraComponent* GameObject::GetCamera()
{
	CameraComponent* cam = dynamic_cast<CameraComponent*>(this->FindComponent(IComponent::Type::CAMERA));
	if (cam != nullptr)
	{
		return cam;
	}

	return nullptr;
}

AnimationComponent* GameObject::GetAnimation()
{
	AnimationComponent* a = dynamic_cast<AnimationComponent*>(this->FindComponent(IComponent::Type::ANIMATION));
	if (a != nullptr)
	{
		return a;
	}

	return nullptr;
}

bool GameObject::IsAnimated()
{
	return dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER))->GetMesh()->IsSkinnedMesh();
}

bool GameObject::IsRenderable()
{
	RenderComponent* r = dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER));
	if (r != nullptr)
	{
		return true;
	}

	return false;
}

void GameObject::Draw(ID3D11DeviceContext* pContext)
{
	RenderComponent* r = dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER));
	if (r != nullptr)
	{
		r->Draw(pContext, this->GetTransform(), this->GetAnimation());
	}
}

void GameObject::DrawAuto(ID3D11DeviceContext * pContext)
{
	RenderComponent* r = dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER));
	if (r != nullptr)
	{
		r->Draw(pContext);
	}
}

void GameObject::Draw(ID2D1RenderTarget* pRenderTarget, IDWriteFactory* pWriteFactory, ID2D1SolidColorBrush* pBrush)
{
	RenderComponent* r = dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER));
	if (r != nullptr)
	{
		r->DrawFont(pRenderTarget, pWriteFactory, pBrush);
	}
}

void GameObject::Dispatch(ID3D11DeviceContext* pContext, UINT threadX, UINT threadY, UINT threadZ)
{
	RenderComponent* r = dynamic_cast<RenderComponent*>(this->FindComponent(IComponent::Type::RENDER));
	if (r != nullptr)
	{
		r->Dispatch(pContext, this->GetTransform(), threadX, threadY, threadZ);
	}
}

void GameObject::Awake()
{
	for (auto& s : mScripts)
	{
		s.second->Awake();
	}
}

void GameObject::Start()
{
	for (auto& s : mScripts)
	{
		s.second->Start();
	}
}

void GameObject::Update(float dt)
{
	for (auto& s : mScripts)
	{
		if (s.second->IsEnabled())
		{
			s.second->Update(dt);
		}
	}
	// update joints transform if the mesh has an animation
	auto a = this->GetAnimation();
	if (a != nullptr)
	{
		a->Update(dt);
	}
}

void GameObject::FixedUpdate(float dt)
{
	for (auto& s : mScripts)
	{
		if (s.second->IsEnabled())
		{
			s.second->FixedUpdate(dt);	
		}
	}
}

void GameObject::Enable(bool enable)
{
	mEnabled = enable;
	TransformComponent* t = dynamic_cast<TransformComponent*>(FindComponent(IComponent::Type::TRANSFORM));

	if (t != nullptr)
	{
		for (auto c : t->GetChildren())
		{
			c->GetGameObject()->Enable(enable);
		}
	}
}

IComponent* GameObject::FindComponent(const IComponent::Type type)
{
	auto it = mComponents.find(type);
	if (it != mComponents.end())
		return it->second;

	return nullptr;
}

IScript* GameObject::FindScript(const std::string& name)
{
	auto it = mScripts.find(name);
	if (it != mScripts.end())
		return it->second;

	return nullptr;
}
