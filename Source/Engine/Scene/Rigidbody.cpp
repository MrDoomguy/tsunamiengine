#include "Rigidbody.h"
#include "GameObject.h"
#include "SDFPhysic.h"

Rigidbody::Rigidbody(ICollider * collider, float bounce)
	: IPhysicComponent(collider)
{
	mMassInverse = 0;
	mKinematic = true;
	mBounce = bounce;

	mVelocity = XMFLOAT3(0, 0, 0);

	UseGravity(false);

	mAngularVelocity = XMFLOAT3(0, 0, 0);
	mAngularMomentum = XMFLOAT3(0, 0, 0);
	mTorque = XMFLOAT3(0, 0, 0);

	XMStoreFloat3x3(&mInertiaTensorInverse, XMMatrixSet(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	XMStoreFloat3x3(&mActualInertiaTensorInverse, XMMatrixSet(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
}

Rigidbody::Rigidbody(ICollider* collider, IndexedMesh* mesh, float mass, float bounce, bool useGravity)
	: IPhysicComponent(collider)
{
	geotypes::MeshData* meshData = mesh->GetMesh();
	
	int vertexCount = static_cast<int>(meshData->vertices.size());
	mMassInverse = 1.f / mass;
	mBounce = bounce;
	mKinematic = false;

	mVelocity = XMFLOAT3(0, 0, 0);

	UseGravity(useGravity);

	mAngularVelocity = XMFLOAT3(0, 0, 0);
	mAngularMomentum = XMFLOAT3(0, 0, 0);
	mTorque = XMFLOAT3(0, 0, 0);

	XMMATRIX sum = XMMatrixSet(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	XMVECTOR origin = XMVectorZero();
	float areaSum = 0.f;

	float* masses = new float[vertexCount];

	for (int i = 0; i < vertexCount; i++)
	{
		float area = meshData->vertices[i].area;
		origin += XMLoadFloat3(&meshData->vertices[i].position) * area;
		masses[i] = mass * area;

		areaSum += area;
	}

	origin /= areaSum;

	XMStoreFloat3(&mOriginOffset, origin);

	for (int i = 0; i < vertexCount; i++)
	{
		XMStoreFloat3(&meshData->vertices[i].position, XMLoadFloat3(&meshData->vertices[i].position) - origin);

		XMFLOAT3 mv = meshData->vertices[i].position;

		float localMass = masses[i] / areaSum;

		sum +=
			XMMatrixSet(
				mv.x, mv.x, mv.x, 0,
				mv.y, mv.y, mv.y, 0,
				mv.z, mv.z, mv.z, 0,
				0, 0, 0, 0) *
			XMMatrixSet(
				mv.x, 0, 0, 0,
				0, mv.y, 0, 0,
				0, 0, mv.z, 0,
				0, 0, 0, 0) * localMass;
	}

	mesh->Update(meshData->vertices.data());

	XMFLOAT3X3 diag;
	XMStoreFloat3x3(&diag, sum);
	float d = diag._11 + diag._22 + diag._33;
	sum = XMMatrixSet(d, 0, 0, 0, 0, d, 0, 0, 0, 0, d, 0, 0, 0, 0, 1) - sum;

	XMStoreFloat3x3(&mInertiaTensorInverse, XMMatrixInverse(nullptr, sum));
	XMStoreFloat3x3(&mActualInertiaTensorInverse, XMMatrixSet(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

	delete masses;
}

Rigidbody::Rigidbody(ICollider * collider, const std::vector<IndexedMesh*>& mesh, float mass, float bounce, bool useGravity, const float* meshScale)
	: IPhysicComponent(collider)
{
	int vertexCount = 0;
	for(int i = 0; i < static_cast<int>(mesh.size()); i++) vertexCount += static_cast<int>(mesh[i]->GetMesh()->vertices.size());

	mMassInverse = 1.f / mass;
	mBounce = bounce;
	mKinematic = false;

	mVelocity = XMFLOAT3(0, 0, 0);

	UseGravity(useGravity); 

	mAngularVelocity = XMFLOAT3(0, 0, 0);
	mAngularMomentum = XMFLOAT3(0, 0, 0);
	mTorque = XMFLOAT3(0, 0, 0);

	XMMATRIX sum = XMMatrixSet(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	XMVECTOR origin = XMVectorZero();
	float areaSum = 0.f;

	float* masses = new float[vertexCount];

	for (int j = 0; j < static_cast<int>(mesh.size()); j++)
	{
		float sc = 1.f;
		if (meshScale != nullptr) sc = meshScale[j];
		for (int i = 0; i < static_cast<int>(mesh[j]->GetMesh()->vertices.size()); i++)
		{
			float area = mesh[j]->GetMesh()->vertices[i].area * sc * sc;
			origin += XMLoadFloat3(&mesh[j]->GetMesh()->vertices[i].position) * sc * area;
			masses[i] = mass * area;

			areaSum += area;
		}
	}

	origin /= areaSum;

	XMStoreFloat3(&mOriginOffset, origin);

	for (int j = 0; j < static_cast<int>(mesh.size()); j++)
	{
		for (int i = 0; i < static_cast<int>(mesh[j]->GetMesh()->vertices.size()); i++)
		{
			XMStoreFloat3(&mesh[j]->GetMesh()->vertices[i].position, XMLoadFloat3(&mesh[j]->GetMesh()->vertices[i].position) - origin);

			XMFLOAT3 mv = mesh[j]->GetMesh()->vertices[i].position;

			float localMass = masses[i] / areaSum;

			sum +=
				XMMatrixSet(
					mv.x, mv.x, mv.x, 0,
					mv.y, mv.y, mv.y, 0,
					mv.z, mv.z, mv.z, 0,
					0, 0, 0, 0) *
				XMMatrixSet(
					mv.x, 0, 0, 0,
					0, mv.y, 0, 0,
					0, 0, mv.z, 0,
					0, 0, 0, 0) * localMass;
		}
		mesh[j]->Update(mesh[j]->GetMesh()->vertices.data());
	}

	XMFLOAT3X3 diag;
	XMStoreFloat3x3(&diag, sum);
	float d = diag._11 + diag._22 + diag._33;
	sum = XMMatrixSet(d, 0, 0, 0, 0, d, 0, 0, 0, 0, d, 0, 0, 0, 0, 1) - sum;

	XMStoreFloat3x3(&mInertiaTensorInverse, XMMatrixInverse(nullptr, sum));
	XMStoreFloat3x3(&mActualInertiaTensorInverse, XMMatrixSet(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

	delete masses;
}

Rigidbody::~Rigidbody()
{
}

void Rigidbody::FixedUpdate(float dt)
{
	if (mKinematic) return;

	XMVECTOR vel = XMLoadFloat3(&mVelocity);
	XMVECTOR angularVel = XMLoadFloat3(&mAngularVelocity);
	XMVECTOR quatRot = XMLoadFloat4(&mPhysicsTransform->GetRotation());

	XMFLOAT3 dPos;
	XMFLOAT4 dRot;

	XMStoreFloat3(&dPos, vel * dt);
	XMStoreFloat4(&dRot, XMQuaternionNormalize(quatRot + XMQuaternionMultiply(quatRot, angularVel) * dt / 2));

	mPhysicsTransform->Translate(dPos);
	mPhysicsTransform->SetRotation(dRot);
}

void Rigidbody::Clear()
{
	ResetAcceleration();

	mVelocity = XMFLOAT3(0, 0, 0);
	mImpulse = XMFLOAT3(0, 0, 0);
	mAngularVelocity = XMFLOAT3(0, 0, 0);
	mAngularMomentum = XMFLOAT3(0, 0, 0);
	mTorque = XMFLOAT3(0, 0, 0);
}

void Rigidbody::Print()
{
	XMFLOAT3 pos = mPhysicsTransform->GetPosition();
	XMFLOAT3 rot = mPhysicsTransform->GetEuler();
	std::cout << "Rigidbody Position: " << pos.x << " " << pos.y << " " << pos.z << " " << std::endl;
	std::cout << "Rigidbody Rotation: " << rot.x << " " << rot.y << " " << rot.z << " " << std::endl;
	std::cout << "Rigidbody Velocity: " << mVelocity.x << " " << mVelocity.y << " " << mVelocity.z << " " << std::endl;
	std::cout << "Rigidbody Acceleration: " << mAcceleration.x << " " << mAcceleration.y << " " << mAcceleration.z << " " << std::endl;
	std::cout << "Rigidbody mAngularVelocity: " << mAngularVelocity.x << " " << mAngularVelocity.y << " " << mAngularVelocity.z << " " << std::endl;
	std::cout << "Rigidbody mAngularMomentum: " << mAngularMomentum.x << " " << mAngularMomentum.y << " " << mAngularMomentum.z << " " << std::endl;
	std::cout << std::endl;
}

void Rigidbody::HandleCollision(const Rigidbody & other, const Collision & collision)
{
	if (mKinematic || mMassInverse == 0) return;
	
	XMFLOAT3 impulse;
	XMFLOAT3 pos;

	XMMATRIX ia = XMLoadFloat3x3(&mActualInertiaTensorInverse);
	XMMATRIX ib = XMLoadFloat3x3(&other.mActualInertiaTensorInverse);

	// TODO: check this!
	XMVECTOR xa = collision.point - XMLoadFloat3(&mPhysicsTransform->GetPosition());
	XMVECTOR xb = collision.point - XMLoadFloat3(&other.mPhysicsTransform->GetPosition());
	XMVECTOR va = XMLoadFloat3(&mVelocity) + XMVector3Cross(XMLoadFloat3(&mAngularVelocity), xa);
	XMVECTOR vb = XMLoadFloat3(&other.mVelocity) + XMVector3Cross(XMLoadFloat3(&other.mAngularVelocity), xb);
		
	XMVECTOR rotMassa = XMVector3Cross(XMVector3Transform(XMVector3Cross(xa, collision.dir), ia), xa);
	XMVECTOR rotMassb = XMVector3Cross(XMVector3Transform(XMVector3Cross(xb, collision.dir), ib), xb);

	float coll = XMVectorGetX(XMVector3Dot(-(1 + mBounce) * (va - vb), collision.dir));
	if (coll < 0) coll = 0;
	XMStoreFloat3(&impulse, collision.dir * coll
		/ (mMassInverse + other.mMassInverse + XMVectorGetX(XMVector3Dot(rotMassa + rotMassb, collision.dir))));
	XMStoreFloat3(&pos, xa);

	AddImpulse(impulse, pos);

	// TODO: replace by impulse position update
	XMStoreFloat3(&pos, collision.dir * (collision.depth * mMassInverse / (mMassInverse + other.mMassInverse)));
	mPhysicsTransform->Translate(pos);
}

void Rigidbody::HandleCollision(ICollider& child, ICollider& other)
{
	if (Rigidbody* rb = dynamic_cast<Rigidbody*>(other.GetPhysicComponent()))
	{
		Collision collision;
		child.GetContactInfo(&collision, &other);
		HandleCollision(*rb, collision);
		collision.dir *= -1;
		rb->HandleCollision(*this, collision);
		return;
	}
	else if (SDFPhysic* sp = dynamic_cast<SDFPhysic*>(other.GetPhysicComponent()))
	{
		sp->HandleCollision(other, child);
		return;
	}
	Logger::instance().Msg(LOG_ERROR, L"NYI Rigidbody::HandleCollision");
}

void Rigidbody::UpdateForces(float dt)
{
	/* TODO:
	velocity *= real_pow(linearDamping, duration);
	rotation *= real_pow(angularDamping, duration);
	*/

	if (mKinematic) return;

	XMVECTOR vel = XMLoadFloat3(&mVelocity);
	XMVECTOR angularMom = XMLoadFloat3(&mAngularMomentum);

	XMStoreFloat3(&mVelocity, vel + XMLoadFloat3(&mAcceleration) * dt + XMLoadFloat3(&mImpulse));
	
	XMMATRIX rot = XMMatrixRotationQuaternion(XMLoadFloat4(&mPhysicsTransform->GetRotation()));
	XMFLOAT3 s = mPhysicsTransform->GetScale();
	XMMATRIX scale = XMMatrixScaling(1.f/s.x, 1.f/s.y, 1.f/s.z);
	XMMATRIX inertia = scale * scale * XMMatrixTranspose(rot) * XMLoadFloat3x3(&mInertiaTensorInverse) * rot;

	XMStoreFloat3x3(&mActualInertiaTensorInverse, inertia);

	angularMom += dt * XMLoadFloat3(&mTorque);
	XMStoreFloat3(&mAngularMomentum, angularMom);
	XMStoreFloat3(&mAngularVelocity, XMVector3TransformNormal(angularMom, inertia));

#if PHYSICS_DEBUG
	Print();
#endif

	ResetAcceleration();
}

void Rigidbody::EvaluateSimulation(float dt)
{
	if (mKinematic) return;

	// TODO: interpolate between old and newer position, use lerp and slerp in tranform!
	m.lock();
	if (!mTransform->IsDirty())
	{
		mTransform->SetPosition(mRendererTransform->GetPosition());
		mTransform->SetRotation(mRendererTransform->GetRotation());
		mTransform->SetScale(mRendererTransform->GetScale());
		mTransform->SetDirty(false);
	}
	m.unlock();
}

void Rigidbody::UseGravity(bool useGravity)
{
	mGravity = useGravity;
	ResetAcceleration();
}

void Rigidbody::AddImpulse(XMFLOAT3 & force)
{
	if (mKinematic)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "add impulse without effect, object is kinematic!");
		return;
	}
	XMStoreFloat3(&mImpulse, XMLoadFloat3(&mImpulse) + XMLoadFloat3(&force) * mMassInverse);
}

void Rigidbody::AddImpulse(XMFLOAT3 & force, XMFLOAT3 & point)
{
	if (mKinematic)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "add impulse without effect, object is kinematic!");
		return;
	}

	// TODO: look if better to use dot(force, point) * point
	XMVECTOR p = XMLoadFloat3(&point);
	XMVECTOR f = XMLoadFloat3(&force);

	XMStoreFloat3(&mImpulse, XMLoadFloat3(&mImpulse) + f * mMassInverse);
	XMStoreFloat3(&mAngularMomentum, XMLoadFloat3(&mAngularMomentum) + XMVector3Cross(p, f));
}

void Rigidbody::AddForce(XMFLOAT3& force)
{
	if (mKinematic)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "add force without effect, object is kinematic!");
		return;
	}
	XMFLOAT3 acc;
	XMStoreFloat3(&acc, XMLoadFloat3(&force) * mMassInverse);
	AddAcceleration(acc);
}

void Rigidbody::AddForce(XMFLOAT3 & force, XMFLOAT3 & point)
{
	if (mKinematic)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "add force without effect, object is kinematic!");
		return;
	}
	// TODO: look if really better to use dot(force, point) * (-point)
	XMVECTOR p = XMLoadFloat3(&point);
	XMVECTOR f = XMLoadFloat3(&force);
	XMFLOAT3 acc;

	XMStoreFloat3(&acc, f * mMassInverse);
	XMStoreFloat3(&mTorque, XMLoadFloat3(&mTorque) + XMVector3Cross(p, f));
	
	AddAcceleration(acc);
}

void Rigidbody::AddTorque(XMFLOAT3 & torque)
{
	XMStoreFloat3(&mTorque, XMLoadFloat3(&mTorque) + XMLoadFloat3(&torque));
}

void Rigidbody::AddAcceleration(XMFLOAT3& acc)
{
	if (mKinematic)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "add acceleration without effect, object is kinematic!");
		return;
	}
	XMStoreFloat3(&mAcceleration, XMLoadFloat3(&mAcceleration) + XMLoadFloat3(&acc));
}

void Rigidbody::ResetAcceleration()
{
	mAcceleration = XMFLOAT3(0, mGravity ? GRAVITY : 0, 0);
	mImpulse = XMFLOAT3(0, 0, 0);
	mTorque = XMFLOAT3(0, 0, 0);
}
