#ifndef _ICOMPONENT_H_
#define _ICOMPONENT_H_

#include <cstdint>

//class GameObject;

class IComponent
{
	friend class GameObject;
public:
	enum class Type : std::uint8_t
	{
		TRANSFORM = 0x0,
		RENDER = 0x1,
		CAMERA = 0x2,
		LIGHT = 0x3,
		PHYSIC = 0x4,
		RENDER_FONT = 0x5,
		ANIMATION = 0x6,
		AUDIO_SOURCE = 0x7,
		AUDIO_LISTENER = 0x8,
	};

	explicit IComponent(const Type type) : mType{ type } { }
	virtual ~IComponent() { }
	
	IComponent::Type GetType()
	{
		return mType;
	}

	//void SetGameObject(GameObject *go) { mGo = go; }
	GameObject* GetGameObject() { return mGo; }

protected:
	Type mType;
	GameObject *mGo;
};

#endif
