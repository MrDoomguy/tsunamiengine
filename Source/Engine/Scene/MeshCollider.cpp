#include "MeshCollider.h"

MeshCollider::MeshCollider(IndexedMesh* mesh, XMFLOAT3 pos, XMFLOAT3 rot)
	: ICollider(pos, rot)
{
	Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "use meshcollider with parent, else it won't work for now!!");
	mMeshCount = (int)mesh->GetMesh()->vertices.size();
	mMesh = mesh;
}

MeshCollider::MeshCollider(ICollider * parent, IndexedMesh * mesh, XMFLOAT3 pos, XMFLOAT3 rot)
	: ICollider(parent, pos, rot)
{
	mMeshCount = (int)mesh->GetMesh()->vertices.size();
	mMesh = mesh;
}

MeshCollider::~MeshCollider()
{
	delete mMesh;
}

void MeshCollider::GetContactInfo(Collision * collision, ICollider * other)
{
	if(mParent != nullptr) mParent->GetContactInfo(collision, other);
}

bool MeshCollider::CollisionDetection(ICollider* other)
{
	// HACK: replace by real meshcollider
	if (mParent != nullptr) return mParent->CollisionDetection(other);
	return false;
}