#pragma once

#include "IPhysicComponent.h"
#include "AABBCollider.h"
#include "../Render/Data/AnimatedVolumeTexture.h"
#include "../Physics/Shader/SDFGrid.h"
#include "../General/GPUMemoryManager.h"

#include <mutex>

// integrate sdfgrid here?
class SDFPhysic : public IPhysicComponent
{
public:
	SDFPhysic(Device * pDevice, AnimatedVolumeTexture * sdfTexture, const SDFGrid::SDFProperties & props, ID3D11SamplerState * sampler, VolumeTextureRW* obstacleGrid, AnimatedVolumeTexture* velTexture = nullptr);
	~SDFPhysic();

	virtual void FixedUpdate(float dt) override;
	virtual void HandleCollision(ICollider& child, ICollider& other) override;
	virtual void UpdateForces(float dt) override {};
	virtual void EvaluateSimulation(float dt) override;

protected:
	virtual void Start() override;
	SDFGrid *shaderSdf = nullptr;
};