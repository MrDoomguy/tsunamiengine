#include "SceneBuilder.h"

#include "../../Game/Scripts/TestScript.hpp"
#include "../../Game/Scripts/TestScript2.hpp"
#include "../../Game/Scripts/GUIScript.hpp"
#include "../../Game/Scripts/OnScreenTimer.hpp"
#include "../../Game/Scripts/OnScore.hpp"
#include "../../Game/Scripts/EditorScript.hpp"
#include "../../Game/Scripts/FlyingCamScript.hpp"
#include "../../Game/Scripts/SurfboardControl.hpp"
#include "../../Game/Scripts/MoveCameraScript.hpp"
#include "../../Game/Scripts/BalanceScript.hpp"
#include "../../Game/Scripts/GameManagerScript.hpp"
#include "../../Game/Scripts/AnimateLight.hpp"
#include "../../Game/Scripts/particleTest.hpp"
#include "../../Game/Scripts/AnimationTest.hpp"
#include "../../Game/Scripts/DevelopmentDemoScript.hpp"
#include "../../Game/Scripts/DevelopmentDemoScriptP2.hpp"
#include "../../Game/Scripts/TraceFlyScript.hpp"
#include "../../Game/Scripts/Highscore.hpp"
#include "../General/GPUMemoryManager.h"
#include "../Render/Data/PBSAnimatedVolumetricMaterial.h"
#include "../Render/Data/ParticleMaterial.h"
#include "../Scene/SimpleParticleSystem.h"
#include "../Scene/SDFParticleSystem.h"
#include "SDFPhysic.h"
#include "SphereCollider.h"

SceneBuilder::SceneBuilder()
{
}

SceneBuilder::~SceneBuilder()
{
}

void SceneBuilder::MakeWater(Device * pDevice, Scene * pScene, GameObject** particleSystem)
{
	ID3D11SamplerState* sampler = nullptr;
	GPUMemoryManager::instance().TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_CLAMP, &sampler);

	AnimatedVolumeTexture* waterSdf = new AnimatedVolumeTexture();
	AnimatedVolumeTexture* waterVel = new AnimatedVolumeTexture();

	VolumeTextureRW* obstacleGrid = new VolumeTextureRW();

	SDFGrid::SDFProperties sdfProps;
	sdfProps.buoyancyFactor = 60.f;
	sdfProps.dampingFactor = 230.f;
	sdfProps.flowFactor = 70.f;

	// TODO: depending on object!
	sdfProps.frictionFactor = 0.02f;

	GPUMemoryManager::instance().AnimatedVolumeTextureFromFile("Assets/Fluidsim/iceWaveFlip_res_050_t_124_10.uni_.uni", waterSdf, &sdfProps.dim);
	waterSdf->SetSpeed(30.f);
	waterSdf->SetFactor(1.f);

	sdfProps.extent = XMUINT3(waterSdf->GetWidth(), waterSdf->GetHeight(), waterSdf->GetDepth());

	GPUMemoryManager::instance().AnimatedVolumeTextureFromFile("Assets/Fluidsim/iceWaveFlip_res_050_t_124_10_vel.uni_.uni", waterVel, &sdfProps.velDim);
	waterVel->SetSpeed(15.f);
	waterVel->SetFactor(1.f);

	sdfProps.velExtent = XMUINT3(waterVel->GetWidth(), waterVel->GetHeight(), waterVel->GetDepth());

	// TODO: make direct in rendering the map of the mesh to the sdf
	GPUMemoryManager::instance().RWRGBAVolumeTextureFromMemory(obstacleGrid, XMINT3(sdfProps.dim.x, sdfProps.dim.y, sdfProps.dim.z));

	PBSAnimatedVolumetricMaterial* waterMat = new PBSAnimatedVolumetricMaterial(waterSdf, obstacleGrid, sampler);
	waterMat->Init(pDevice->GetDevice());
	waterMat->SetBaseColor(XMFLOAT3(0.5f, 0.8f, 1.0f));
	waterMat->SetRoughness(0.1f);
	waterMat->SetMetallic(0.9f);
	waterMat->SetIndexOfRefraction(1.33f);

	GameObject* water = new GameObject();
	TransformComponent* wt = new TransformComponent();
	wt->Scale(XMFLOAT3(13.f / (float)sdfProps.dim.z, 13.f / (float)sdfProps.dim.z, 13.f / (float)sdfProps.dim.z));
	wt->SetRotation(XMFLOAT3(0, 90, 0));
	water->AddComponent(wt);
	water->AddComponent(new RenderComponent(waterMat));
	water->AddComponent(new SDFPhysic(pDevice, waterSdf, sdfProps, sampler, obstacleGrid, waterVel));

	pScene->AddGameObject(water);

	if (particleSystem != nullptr)
	{
		SDFParticleSystem::InitProps initProps;
		initProps.curvatureMin = .7f;
		initProps.curvatureMax = .8f;
		initProps.turbulenceMin = 10.f;
		initProps.turbulenceMax = 30.f;
		initProps.obstVelMin = 0.f;
		initProps.obstVelMax = .01f;
		initProps.energyMin = 50.f;
		initProps.energyMax = 500.f;
		initProps.maxTu = 100;
		initProps.maxWc = 10000;
		initProps.maxOb = 10000;

		PrimitiveMesh * particleMesh = new PrimitiveMesh();
		ParticleMaterial * particleMaterial = new ParticleMaterial();
		particleMaterial->Init(pDevice->GetDevice());
		GPUMemoryManager::instance().PointMeshFromMemory(particleMesh, 1000000);
		GameObject* ps = new GameObject();
		ps->AddComponent(new TransformComponent(water->GetTransform(), true, true, true));
		ps->AddComponent(new RenderComponent(particleMaterial, particleMesh));
		ps->AddComponent(new SDFParticleSystem(pDevice, waterSdf, sampler, sdfProps, initProps, 0.1f, 0.05f, particleMesh, obstacleGrid, waterVel));

		//particleSystem->AddScript(new ParticleTest(std::string("particle test"), dynamic_cast<SDFParticleSystem*>(particleSystem->GetComponent(IComponent::Type::PHYSIC))));

		*particleSystem = ps;

		pScene->AddGameObject(ps);
	}
}

void SceneBuilder::MakeAmbient(Device * pDevice, Scene * pScene)
{
	XMFLOAT3 ambientPos = XMFLOAT3(4.5f, -2.f, 12.25f);
	XMFLOAT3 ambientScale = XMFLOAT3(0.985f, 0.985f, 0.985f);
	float bounce = 0.1f;

	GameObject* bridge = new GameObject("Bridge");
	IndexedMesh* bridgeMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/bridge.obj", true, bridgeMesh);
	bridge->AddComponent(new TransformComponent());
	bridge->GetTransform()->SetPosition(ambientPos);
	bridge->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* bridgeMat = new PBSSolidMaterial();
	bridgeMat->Init(pDevice->GetDevice());
	bridgeMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/bridge_d.dds"));
	bridge->AddComponent(new RenderComponent(bridgeMat, bridgeMesh));

	GameObject* mainBasin = new GameObject("MainBasin");
	IndexedMesh* basinMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/mainBasin.obj", true, basinMesh);
	mainBasin->AddComponent(new TransformComponent());
	mainBasin->GetTransform()->SetPosition(ambientPos);
	mainBasin->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* basinMat = new PBSSolidMaterial();
	basinMat->Init(pDevice->GetDevice());
	basinMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/main_basin_d.dds"));
	mainBasin->AddComponent(new RenderComponent(basinMat, basinMesh));

	OBBCollider* groundCollider = new OBBCollider(XMFLOAT3(13, basinMesh->GetMesh()->bounding.size.y, basinMesh->GetMesh()->bounding.size.z), XMFLOAT3(-4.55f, 1.5f, -9.2f));
	groundCollider->AddChild(new OBBCollider(XMFLOAT3(13, 0.1f, 7.36f), XMFLOAT3(0.f, 0.5f, 3.2f), XMFLOAT3(-10, 0, 0)));
	groundCollider->AddChild(new OBBCollider(XMFLOAT3(13, 1.3f, 3.17f), XMFLOAT3(0, -.85f, -2.2f)));
	groundCollider->AddChild(new OBBCollider(XMFLOAT3(13, 0.1f, 8.f), XMFLOAT3(0, -1.5f, -7.8f)));

	mainBasin->AddComponent(new Rigidbody(groundCollider, bounce));

	/*PBSSolidMaterial* collMat = new PBSSolidMaterial();
	collMat->Init(pDevice->GetDevice());
	IndexedMesh* boxMesh2 = new IndexedMesh();
	GPUMemoryManager::instance().MakeBox(boxMesh2, false);

	GameObject* collTest = new GameObject("test");
	collTest->AddComponent(new TransformComponent(mainBasin->GetTransform()));
	collTest->GetTransform()->SetScale(XMFLOAT3(13, basinMesh->GetMesh()->bounding.size.y, basinMesh->GetMesh()->bounding.size.z));
	collTest->GetTransform()->SetPosition(XMFLOAT3(-4.55f, 1.5f, -9.2f));

	GameObject* collTest2 = new GameObject("test2");
	collTest2->AddComponent(new TransformComponent(collTest->GetTransform(), true, false, true));
	collTest2->GetTransform()->SetScale(XMFLOAT3(13, 1.3f, 3.17f));
	collTest2->GetTransform()->SetPosition(XMFLOAT3(0, -.85f, -2.2f));
	//collTest2->GetTransform()->SetRotation(XMFLOAT3(-10, 0, 0));
	collTest2->AddComponent(new RenderComponent(collMat, boxMesh2));
	pScene->AddGameObject(collTest);
	pScene->AddGameObject(collTest2);*/

	GameObject* smallBasin = new GameObject("SmallBasin");
	IndexedMesh* smallBasinMssh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/smallBasin.obj", true, smallBasinMssh);
	smallBasin->AddComponent(new TransformComponent());
	smallBasin->GetTransform()->SetPosition(ambientPos);
	smallBasin->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* smallBasinMat = new PBSSolidMaterial();
	smallBasinMat->Init(pDevice->GetDevice());
	smallBasinMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/small_basin_d.dds"));
	smallBasin->AddComponent(new RenderComponent(smallBasinMat, smallBasinMssh));
	smallBasin->AddComponent(new Rigidbody(new OBBCollider(smallBasinMssh->GetMesh()->bounding.size, XMFLOAT3(4.95f, 1.4f, -8.15f)), bounce));

	GameObject* wallRight = new GameObject("WallRight");
	IndexedMesh* wallRightMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/wallRight.obj", true, wallRightMesh);
	wallRight->AddComponent(new TransformComponent());
	wallRight->GetTransform()->SetPosition(ambientPos);
	wallRight->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* wallRightMat = new PBSSolidMaterial();
	wallRightMat->Init(pDevice->GetDevice());
	wallRightMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/wall_right_d.dds"));
	wallRight->AddComponent(new RenderComponent(wallRightMat, wallRightMesh));

	GameObject* wallLeft = new GameObject("WallLeft");
	IndexedMesh* wallLeftMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/wallLeft.obj", true, wallLeftMesh);
	wallLeft->AddComponent(new TransformComponent());
	wallLeft->GetTransform()->SetPosition(ambientPos);
	wallLeft->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* wallLeftMat = new PBSSolidMaterial();
	wallLeftMat->Init(pDevice->GetDevice());
	wallLeftMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/wall_left_d.dds"));
	wallLeft->AddComponent(new RenderComponent(wallLeftMat, wallLeftMesh));
	wallLeft->AddComponent(new Rigidbody(new OBBCollider(wallLeftMesh->GetMesh()->bounding.size, XMFLOAT3(-11.65f, 1.4f, -12.25f)), bounce));

	GameObject* hillRight = new GameObject("HillRight");
	IndexedMesh* hillRightMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/hillRight.obj", true, hillRightMesh);
	hillRight->AddComponent(new TransformComponent());
	hillRight->GetTransform()->SetPosition(ambientPos);
	hillRight->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* hillRightMat = new PBSSolidMaterial();
	hillRightMat->Init(pDevice->GetDevice());
	hillRightMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/hill_right_d.dds"));
	hillRight->AddComponent(new RenderComponent(hillRightMat, hillRightMesh));

	GameObject* hillLeft = new GameObject("HillLeft");
	IndexedMesh* hillLeftMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/hillLeft.obj", true, hillLeftMesh);
	hillLeft->AddComponent(new TransformComponent());
	hillLeft->GetTransform()->SetPosition(ambientPos);
	hillLeft->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* hillLeftMat = new PBSSolidMaterial();
	hillLeftMat->Init(pDevice->GetDevice());
	hillLeftMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/hill_left_d.dds"));
	hillLeft->AddComponent(new RenderComponent(hillLeftMat, hillLeftMesh));

	pScene->AddGameObject(bridge);
	pScene->AddGameObject(mainBasin);
	pScene->AddGameObject(smallBasin); 
	pScene->AddGameObject(wallRight);
	pScene->AddGameObject(wallLeft);
	pScene->AddGameObject(hillLeft);
	pScene->AddGameObject(hillRight);
}

void SceneBuilder::MakeSurfer(Device * pDevice, Scene * pScene, GameObject** surferOut, GameObject** surfboardOut)
{
	bool move = true;

	IndexedMesh* surfMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/surfboard_thp.obj", false, surfMesh); 

	SkeletalMesh* amesh = GPUMemoryManager::instance().SkeletalMeshFromFile("Assets/Meshes/surfer_anim.fbx", true);
	animtypes::AnimationData* animData = GPUMemoryManager::instance().AnimationFromFile("Assets/Meshes/surfer_anim.fbx");

	BlendState* state = new BlendState(2);
	state->AddTreeNode("Armature|idle", { 0, 0 });
	state->AddTreeNode("Armature|right", { 1, 0 });
	state->AddTreeNode("Armature|left", { -1, 0 });
	state->AddTreeNode("Armature|front", { 0, 1 });
	state->AddTreeNode("Armature|back", { 0, -1 });

	MeshHelper::BOX_DESC boxDesc;
	boxDesc.width = amesh->GetMesh()->bounding.size.x * 20;
	boxDesc.height = amesh->GetMesh()->bounding.size.z * 80;
	boxDesc.depth = amesh->GetMesh()->bounding.size.y * 120;

	IndexedMesh* boxMesh = new IndexedMesh();
	GPUMemoryManager::instance().MakeBox(boxMesh, false, &boxDesc);

	PBSSolidMaterial* surfBoardMat = new PBSSolidMaterial();
	surfBoardMat->Init(pDevice->GetDevice());
	ColorMap* boardDiffuse = new ColorMap();
	GPUMemoryManager::instance().ColorMapFromFile(std::string("Assets/Textures/surfboard_diffuse_mip.dds"), boardDiffuse);
	surfBoardMat->SetNormalMap(GPUMemoryManager::instance().GetColorMap("Assets/Textures/surfboard_n.dds"));
	surfBoardMat->SetColorTexture(boardDiffuse);
	surfBoardMat->SetMetallic(0.f);
	surfBoardMat->SetRoughness(10.f);
	ID3D11SamplerState* sphereSampler;
	GPUMemoryManager::instance().TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP, &sphereSampler);
	surfBoardMat->SetTextureSampler(sphereSampler);

	GameObject* surfboard = new GameObject();
	surfboard->AddComponent(new TransformComponent());
	surfboard->GetTransform()->SetPosition(XMFLOAT3(0.f, 0.f, 0.f));
	surfboard->GetTransform()->SetRotation(XMFLOAT3(0, 90, 0));

	OBBCollider* boardCollider = new OBBCollider(surfMesh->GetMesh()->bounding.size);
	boardCollider->AddChild(new MeshCollider(new OBBCollider(surfMesh->GetMesh()->bounding.size), surfMesh));

	// should be around 40l
	surfboard->GetTransform()->SetScale(XMFLOAT3(0.08f, 0.08f, 0.08f));
	if (move) surfboard->AddComponent(new Rigidbody(boardCollider, surfMesh, 4.f, .1f, true));
	else surfboard->AddComponent(new Rigidbody(boardCollider, .1f));
	surfboard->AddComponent(new RenderComponent(surfBoardMat, surfMesh));

	//surfboard->AddScript(new TestScript(std::string("test physics")));

	*surfboardOut = surfboard;

	pScene->AddGameObject(surfboard);

	PBSSolidMaterial* surferMat = new PBSSolidMaterial();
	surferMat->Init(pDevice->GetDevice()); 
	ColorMap* surferDiffuse = new ColorMap();
	GPUMemoryManager::instance().ColorMapFromFile(std::string("Assets/Textures/surfer_tex.dds"), surferDiffuse);
	surferMat->SetColorTexture(surferDiffuse);

	Animator* animator = new Animator();
	animator->SetState(state);

	AnimationComponent* ac = new AnimationComponent(pDevice->GetDevice(), *animData, animator);

	//GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/sphere.obj", false, boxMesh);
	// should be around 40l
	OBBCollider* surfboardColl = new OBBCollider(boxMesh->GetMesh()->bounding.size);
	surfboardColl->AddChild(new MeshCollider(boxMesh));

	GameObject* surfer = new GameObject();
	surfer->AddComponent(new TransformComponent(surfboard->GetTransform()));    
	surfer->GetTransform()->SetPosition(XMFLOAT3(0, 0.08f * boxDesc.height / 2, 0));
	surfer->GetTransform()->SetScale(XMFLOAT3(0.08f, 0.08f, 0.08f));
	if(move) surfer->AddComponent(new Rigidbody(surfboardColl, boxMesh, 200.f, .1f, true));
	else surfer->AddComponent(new Rigidbody(surfboardColl, 0.f));

	/*surfer->AddComponent(new TransformComponent());
	surfer->GetTransform()->SetPosition(XMFLOAT3(0, 0, 0));
	surfer->GetTransform()->SetScale(XMFLOAT3(0.0064f, 0.0064f, 0.0064f));
	surfer->AddComponent(new Rigidbody(surfboardColl, boxMesh, 100.f, .5f, true));
	surfer->AddComponent(new RenderComponent(surferMat, boxMesh));*/

	//surfer->AddScript(new TestScript(std::string("test physics")));

	GameObject* surferAnim = new GameObject();
	surferAnim->AddComponent(new TransformComponent(surfer->GetTransform()));
	surferAnim->GetTransform()->SetPosition(XMFLOAT3(0, -boxDesc.height / 2 + 5.f, 0));
	surferAnim->AddComponent(new RenderComponent(surferMat, amesh));
	surferAnim->AddComponent(ac);

	*surferOut = surfer;
	
	pScene->AddGameObject(surfer);
	pScene->AddGameObject(surferAnim);

	if(move) surfboard->AddScript(new SurfboardControl(std::string("surfboardControl"), 100, 0.2f, XMFLOAT2(130.f, 20.f), 2.f, animator));
}

void SceneBuilder::MakePhysicsDemo(Device* pDevice, FontEngine* pFontEngine, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene)
{
	GameObject* surfer;
	GameObject* surfboard;
	GameObject* particle;

	MakeWater(pDevice, pScene, &particle);
	MakeSurfer(pDevice, pScene, &surfer, &surfboard);
	MakeAmbient(pDevice, pScene);

	GameObject* mainCamera = new GameObject();
	TransformComponent* ct = new TransformComponent();
	ct->SetPosition(XMFLOAT3(0.f, 3.f, 0.f));
	mainCamera->AddComponent(ct);
	mainCamera->AddComponent(new CameraComponent(screenWidth, screenHeight, 60.0f, screenWidth / (float)screenHeight, 0.1f, 1000.0f));

	//mainCamera->AddScript(new MoveCameraScript(std::string("cam")));
	mainCamera->AddScript(new FlyingCamScript(std::string("cam"), surfer->GetTransform(), XMFLOAT3(0, 5.f, -5.f), 5.f, 5.f, std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(0, 5.f, -6.f), XMFLOAT3(0, 1, 0)), 1.f,
	{
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(6, 0.8f, 2), XMFLOAT3(0, 1, 0))
	} ));

	GameObject* skydome = new GameObject();
	IndexedMesh* skySphere = new IndexedMesh();
	GPUMemoryManager::instance().MakeSphere(skySphere, true);
	skydome->AddComponent(new TransformComponent());
	skydome->GetTransform()->SetScale(XMFLOAT3(500, 500, 500));
	skydome->SetParent(mainCamera, true, false, false);
	SkyMaterial* skyMat = new SkyMaterial();
	skyMat->Init(pDevice->GetDevice());
	skyMat->SetSampler(GPUMemoryManager::instance().GetDefaultTextureSampler());
	skyMat->SetSkyProbe(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	skydome->AddComponent(new RenderComponent(skyMat, skySphere));

	GameObject* sun = new GameObject();
	TransformComponent* st = new TransformComponent();
	st->SetRotation(XMFLOAT3(135, 45, 0));
	sun->AddComponent(st);
	sun->AddComponent(new DirectionalLightComponent(
		XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
		XMFLOAT4(0.25f, 0.25f, 0.25f, 1.0f)));
	AudioSourceComponent* aux0 = new AudioSourceComponent();
	aux0->AddClip("BG", GPUMemoryManager::instance().LoadAudioClipFromFile("Assets/Sounds/template_music.ogg", true));
	aux0->Play("BG");
	sun->AddComponent(aux0);

	GameObject* timer = new GameObject();
	FontMaterial* fontMat = new FontMaterial();
	fontMat->Init(pFontEngine->GetWriteFactory());
	fontMat->SetRect(0, 0, 1280, 720);
	fontMat->SetFontSize(50.0f);
	fontMat->SetText(L"Hello Font Material!");
	fontMat->SetFontName(L"Another X Display tfb");
	fontMat->SetSolidColor(D2D1::ColorF(D2D1::ColorF::DeepPink));
	timer->AddComponent(new RenderComponent(fontMat));
	timer->AddScript(new OnScreenTimer(std::string("OnScreenTimer"), 0.02f));

	GameObject* score = new GameObject();
	FontMaterial* scoreMat = new FontMaterial(*fontMat);
	score->AddComponent(new RenderComponent(scoreMat));
	score->AddScript(new OnScore(std::string("Score"), 0.02f));

	GameObject* highscoreList = new GameObject();
	FontMaterial* highListMat = new FontMaterial(*fontMat);
	highscoreList->AddComponent(new RenderComponent(highListMat));

	GameObject* highscore = new GameObject();
	FontMaterial* highscoreMat = new FontMaterial(*fontMat);
	highscore->AddComponent(new RenderComponent(highscoreMat));
	highscore->AddScript(new Highscore(std::string("highscore"), highscoreList, L"HighscoreList", 10, 0.02f));

	GameObject* manager = new GameObject("Manager");
	manager->AddScript(new GameManagerScript(std::string("manager"), surfboard, surfer, dynamic_cast<OnScreenTimer*>(timer->GetScript("OnScreenTimer")), dynamic_cast<OnScore*>(score->GetScript("Score")), dynamic_cast<SurfboardControl*>(surfboard->GetScript("surfboardControl")), .2f, 5.f, 100.f, dynamic_cast<Highscore*>(highscore->GetScript("highscore")), dynamic_cast<FlyingCamScript*>(mainCamera->GetScript("cam"))));
	AudioSourceComponent* aux = new AudioSourceComponent();
	aux->AddClip("Ambient", GPUMemoryManager::instance().LoadAudioClipFromFile("Assets/Sounds/park_ambient.ogg", true));
	aux->Play("Ambient");
	manager->AddComponent(aux);

	pScene->SetEnvProbePosition(XMFLOAT3(0.0f, 2.0f, 0.0f));

	pScene->AddGameObject(highscore);
	pScene->AddGameObject(highscoreList);
	pScene->AddGameObject(manager);
	pScene->AddGameObject(timer);
	pScene->AddGameObject(skydome);
	pScene->AddGameObject(mainCamera);
	pScene->AddGameObject(score);
	pScene->AddGameObject(sun);
}

void SceneBuilder::MakeRenderingDemo(ID3D11Device * pDevice, FontEngine * pFontEngine, const uint32_t screenWidth, const uint32_t screenHeight, Scene * pScene)
{
	GameObject* bridge = new GameObject("Bridge");
	IndexedMesh* bridgeMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/surfboard_thp.obj", false, bridgeMesh);
	//GPUMemoryManager::instance().MakeBox(bridgeMesh, true);
	bridge->AddComponent(new TransformComponent());
	PBSSolidMaterial* bridgeMat = new PBSSolidMaterial();
	bridgeMat->Init(pDevice);
	bridgeMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/surfboard_diffuse_mip.dds"));
	bridgeMat->SetNormalMap(GPUMemoryManager::instance().GetColorMap("Assets/Textures/surfboard_n.dds"));
	bridge->AddComponent(new RenderComponent(bridgeMat, bridgeMesh));

	GameObject* sun = new GameObject();
	TransformComponent* st = new TransformComponent();
	st->SetRotation(XMFLOAT3(135, 45, 0));
	sun->AddComponent(st);
	sun->AddComponent(new DirectionalLightComponent(
		XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
		XMFLOAT4(0.25f, 0.25f, 0.25f, 1.0f)));

	GameObject* mainCamera = new GameObject();
	TransformComponent* ct = new TransformComponent();
	ct->SetPosition(XMFLOAT3(0, 5, -5));
	ct->LookAt(XMFLOAT3(0.0f, 0.0f, 0.0f));
	mainCamera->AddComponent(ct);
	mainCamera->AddComponent(new CameraComponent(screenWidth, screenHeight, 60.0f, screenWidth / (float)screenHeight, 0.1f, 1000.0f));
	mainCamera->AddScript(new MoveCameraScript(std::string("CamMove")));

	GameObject* skydome = new GameObject();
	IndexedMesh* skySphere = new IndexedMesh();
	GPUMemoryManager::instance().MakeSphere(skySphere, true);
	skydome->AddComponent(new TransformComponent());
	skydome->GetTransform()->SetScale(XMFLOAT3(500, 500, 500));
	skydome->SetParent(mainCamera, true, false, false);
	SkyMaterial* skyMat = new SkyMaterial();
	skyMat->Init(pDevice);
	skyMat->SetSampler(GPUMemoryManager::instance().GetDefaultTextureSampler());
	skyMat->SetSkyProbe(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	skydome->AddComponent(new RenderComponent(skyMat, skySphere));

	pScene->AddGameObject(mainCamera);
	pScene->AddGameObject(sun);
	pScene->AddGameObject(skydome);
	pScene->AddGameObject(bridge);
}

void SceneBuilder::MakeDevelopmentDemo(Device * pDevice, const uint32_t screenWidth, const uint32_t screenHeight, Scene * pScene)
{
	GameObject* surfer;
	GameObject* surfboard;
	GameObject* particleSystem;

	MakeSurfer(pDevice, pScene, &surfer, &surfboard);
	MakeWater(pDevice, pScene, &particleSystem);

	XMFLOAT3 ambientPos = XMFLOAT3(4.5f, -2.f, 12.25f);
	XMFLOAT3 ambientScale = XMFLOAT3(0.985f, 0.985f, 0.985f);

	GameObject* bridge = new GameObject("Bridge");
	IndexedMesh* bridgeMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/bridge.obj", true, bridgeMesh);
	bridge->AddComponent(new TransformComponent());
	bridge->GetTransform()->SetPosition(ambientPos);
	bridge->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* bridgeMat = new PBSSolidMaterial();
	bridgeMat->Init(pDevice->GetDevice());
	bridgeMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/bridge_d.dds"));
	bridge->AddComponent(new RenderComponent(bridgeMat, bridgeMesh));

	GameObject* mainBasin = new GameObject("MainBasin");
	IndexedMesh* basinMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/mainBasin.obj", true, basinMesh);
	mainBasin->AddComponent(new TransformComponent());
	mainBasin->GetTransform()->SetPosition(ambientPos);
	mainBasin->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* basinMat = new PBSSolidMaterial();
	basinMat->Init(pDevice->GetDevice());
	basinMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/main_basin_d.dds"));
	mainBasin->AddComponent(new RenderComponent(basinMat, basinMesh));

	GameObject* smallBasin = new GameObject("SmallBasin");
	IndexedMesh* smallBasinMssh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/smallBasin.obj", true, smallBasinMssh);
	smallBasin->AddComponent(new TransformComponent());
	smallBasin->GetTransform()->SetPosition(ambientPos);
	smallBasin->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* smallBasinMat = new PBSSolidMaterial();
	smallBasinMat->Init(pDevice->GetDevice());
	smallBasinMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/small_basin_d.dds"));
	smallBasin->AddComponent(new RenderComponent(smallBasinMat, smallBasinMssh));

	GameObject* wallRight = new GameObject("WallRight");
	IndexedMesh* wallRightMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/wallRight.obj", true, wallRightMesh);
	wallRight->AddComponent(new TransformComponent());
	wallRight->GetTransform()->SetPosition(ambientPos);
	wallRight->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* wallRightMat = new PBSSolidMaterial();
	wallRightMat->Init(pDevice->GetDevice());
	wallRightMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/wall_right_d.dds"));
	wallRight->AddComponent(new RenderComponent(wallRightMat, wallRightMesh));

	GameObject* wallLeft = new GameObject("WallLeft");
	IndexedMesh* wallLeftMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/wallLeft.obj", true, wallLeftMesh);
	wallLeft->AddComponent(new TransformComponent());
	wallLeft->GetTransform()->SetPosition(ambientPos);
	wallLeft->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* wallLeftMat = new PBSSolidMaterial();
	wallLeftMat->Init(pDevice->GetDevice());
	wallLeftMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/wall_left_d.dds"));
	wallLeft->AddComponent(new RenderComponent(wallLeftMat, wallLeftMesh));

	GameObject* hillRight = new GameObject("HillRight");
	IndexedMesh* hillRightMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/hillRight.obj", true, hillRightMesh);
	hillRight->AddComponent(new TransformComponent());
	hillRight->GetTransform()->SetPosition(ambientPos);
	hillRight->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* hillRightMat = new PBSSolidMaterial();
	hillRightMat->Init(pDevice->GetDevice());
	hillRightMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/hill_right_d.dds"));
	hillRight->AddComponent(new RenderComponent(hillRightMat, hillRightMesh));

	GameObject* hillLeft = new GameObject("HillLeft");
	IndexedMesh* hillLeftMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/hillLeft.obj", true, hillLeftMesh);
	hillLeft->AddComponent(new TransformComponent());
	hillLeft->GetTransform()->SetPosition(ambientPos);
	hillLeft->GetTransform()->SetScale(ambientScale);
	PBSSolidMaterial* hillLeftMat = new PBSSolidMaterial();
	hillLeftMat->Init(pDevice->GetDevice());
	hillLeftMat->SetColorTexture(GPUMemoryManager::instance().GetColorMap("Assets/Textures/hill_left_d.dds"));
	hillLeft->AddComponent(new RenderComponent(hillLeftMat, hillLeftMesh));

	GameObject* mainCamera = new GameObject();
	TransformComponent* ct = new TransformComponent();
	ct->SetPosition(XMFLOAT3(0, 5, 0));
	mainCamera->AddComponent(ct);
	mainCamera->AddComponent(new CameraComponent(screenWidth, screenHeight, 60.0f, screenWidth / (float)screenHeight, 0.1f, 1000.0f));

	mainCamera->AddScript(new TraceFlyScript(std::string("cam"), {
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(-3, 2, 15), XMFLOAT3(-3, 1, 20)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(10, 6, 18), XMFLOAT3(10, 6, 25)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(15, 8, 12), XMFLOAT3(20, 8, 9)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(15, 8, 5), XMFLOAT3(7, 3, 2)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(4, 6, 0), XMFLOAT3(0, 4, 10)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(-4, 4, 0), XMFLOAT3(0, 4, 10)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(-10, 7, 5), XMFLOAT3(1, 2, 5)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(-10, 7, 5), XMFLOAT3(0, 1, -5)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(-4, 6, -5), XMFLOAT3(0, 2, 0)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(0, 4, -15), XMFLOAT3(0, 2, 0)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(10, 1, -10), XMFLOAT3(0, 0, -5)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(8, -1, -8), XMFLOAT3(0, 0, -5)),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(5, 5, -5), surfer->GetTransform()->GetWorldPosition()),
		std::pair<XMFLOAT3, XMFLOAT3>(XMFLOAT3(0, 5, -5), surfer->GetTransform()->GetWorldPosition())
	}, false, 20.f, 2));

	mainCamera->AddScript(new DevelopmentDemoScriptP2(std::string("demo"), dynamic_cast<TraceFlyScript*>(mainCamera->GetScript("cam")), particleSystem, surfer->GetTransform()->GetChild(0)->GetGameObject(), surfboard));

	GameObject* skydome = new GameObject();
	IndexedMesh* skySphere = new IndexedMesh();
	GPUMemoryManager::instance().MakeSphere(skySphere, true);
	skydome->AddComponent(new TransformComponent());
	skydome->GetTransform()->SetScale(XMFLOAT3(500, 500, 500));
	skydome->SetParent(mainCamera, true, false, false);
	SkyMaterial* skyMat = new SkyMaterial();
	skyMat->Init(pDevice->GetDevice());
	skyMat->SetSampler(GPUMemoryManager::instance().GetDefaultTextureSampler());
	skyMat->SetSkyProbe(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	skydome->AddComponent(new RenderComponent(skyMat, skySphere)); AudioSourceComponent* aux = new AudioSourceComponent();
	aux->AddClip("Ambient", GPUMemoryManager::instance().LoadAudioClipFromFile("Assets/Sounds/park_ambient.ogg", true));
	aux->Play("Ambient");
	skydome->AddComponent(aux);

	GameObject* sun = new GameObject();
	TransformComponent* st = new TransformComponent();
	st->SetRotation(XMFLOAT3(135, 45, 0));
	sun->AddComponent(st);
	sun->AddComponent(new DirectionalLightComponent(
		XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
		XMFLOAT4(0.25f, 0.25f, 0.25f, 1.0f)));
	AudioSourceComponent* aux0 = new AudioSourceComponent();
	aux0->AddClip("BG", GPUMemoryManager::instance().LoadAudioClipFromFile("Assets/Sounds/template_music.ogg", true));
	aux0->Play("BG");
	sun->AddComponent(aux0);

	pScene->AddGameObject(skydome);
	pScene->AddGameObject(sun);
	pScene->AddGameObject(mainCamera);
	pScene->AddGameObject(bridge);
	pScene->AddGameObject(mainBasin);
	pScene->AddGameObject(smallBasin);
	pScene->AddGameObject(wallRight);
	pScene->AddGameObject(wallLeft);
	pScene->AddGameObject(hillLeft);
	pScene->AddGameObject(hillRight);
}

void SceneBuilder::MakeCtrlDemo(Device * pDevice, const uint32_t screenWidth, const uint32_t screenHeight, Scene * pScene)
{
	ID3D11SamplerState* sphereSampler;
	GPUMemoryManager::instance().TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP, &sphereSampler);

	PBSSolidMaterial* boxMat = new PBSSolidMaterial();
	boxMat->Init(pDevice->GetDevice());
	boxMat->SetTextureSampler(sphereSampler);

	IndexedMesh* boxMesh = new IndexedMesh();
	GPUMemoryManager::instance().MakeBox(boxMesh, false);

	GameObject* box = new GameObject();
	box->AddComponent(new TransformComponent());
	box->GetTransform()->SetPosition(XMFLOAT3(0, -2, 4));
	box->GetTransform()->SetScale(XMFLOAT3(4, 1, 4));
	box->AddComponent(new RenderComponent(boxMat, boxMesh));
	box->AddComponent(new Rigidbody(new OBBCollider(XMFLOAT3(1, 1, 1)), 0.f));

	GameObject* mainCamera = new GameObject();
	mainCamera->AddComponent(new TransformComponent());
	mainCamera->AddComponent(new CameraComponent(screenWidth, screenHeight, 60.0f, screenWidth / (float)screenHeight, 0.1f, 1000.0f));

	GameObject* skydome = new GameObject();
	IndexedMesh* skySphere = new IndexedMesh();
	GPUMemoryManager::instance().MakeSphere(skySphere, true);
	skydome->AddComponent(new TransformComponent());
	skydome->GetTransform()->SetScale(XMFLOAT3(500, 500, 500));
	skydome->SetParent(mainCamera, true, false, false);
	SkyMaterial* skyMat = new SkyMaterial();
	skyMat->Init(pDevice->GetDevice());
	skyMat->SetSampler(GPUMemoryManager::instance().GetDefaultTextureSampler());
	skyMat->SetSkyProbe(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	skydome->AddComponent(new RenderComponent(skyMat, skySphere));

	IndexedMesh* surfMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/surfer_anim.fbx", false, surfMesh);

	PBSSolidMaterial* surfBoardMat = new PBSSolidMaterial();
	surfBoardMat->Init(pDevice->GetDevice());
	//ColorMap* boardDiffuse = new ColorMap();
	//GPUMemoryManager::instance().ColorMapFromFile(std::string("Assets/Textures/surfboard_diffuse_mip.dds"), boardDiffuse);
	//surfBoardMat->SetColorTexture(boardDiffuse);
	surfBoardMat->SetMetallic(0.f);
	surfBoardMat->SetRoughness(10.f);
	surfBoardMat->SetTextureSampler(sphereSampler);

	SkeletalMesh* amesh = GPUMemoryManager::instance().SkeletalMeshFromFile("Assets/Meshes/surfer_anim.fbx", true);
	animtypes::AnimationData* animData = GPUMemoryManager::instance().AnimationFromFile("Assets/Meshes/surfer_anim.fbx");
	PBSSolidMaterial* amat = new PBSSolidMaterial();
	amat->Init(pDevice->GetDevice());

	BlendState* state = new BlendState(3);
	state->AddTreeNode("Armature|idle", { 0, 0, 0 });
	state->AddTreeNode("Armature|right", { 1, 0, 0 });
	state->AddTreeNode("Armature|left", { -1, 0, 0 });
	state->AddTreeNode("Armature|front", { 0, 1, 0 });
	state->AddTreeNode("Armature|back", { 0, -1, 0 });
	state->AddTreeNode("Armature|rightSlide", { 0, 0, 1 });
	state->AddTreeNode("Armature|leftSlide", { 0, 0, -1 });

	Animator* animator = new Animator();
	animator->SetState(state);

	AnimationComponent* ac = new AnimationComponent(pDevice->GetDevice(), *animData, animator);

	MeshHelper::BOX_DESC boxDesc;
	boxDesc.width = amesh->GetMesh()->bounding.size.x * 50;
	boxDesc.height = amesh->GetMesh()->bounding.size.z * 80;
	boxDesc.depth = amesh->GetMesh()->bounding.size.y * 120;

	IndexedMesh* boxMesh2 = new IndexedMesh();
	GPUMemoryManager::instance().MakeBox(boxMesh2, false, &boxDesc);

	// should be around 40l
	OBBCollider* surfboardColl = new OBBCollider(boxMesh2->GetMesh()->bounding.size);
	surfboardColl->AddChild(new MeshCollider(boxMesh2));

	GameObject* box2 = new GameObject();
	box2->AddComponent(new TransformComponent());
	box2->GetTransform()->SetPosition(XMFLOAT3(0, 0, 4.f));
	box2->GetTransform()->SetScale(XMFLOAT3(0.0064f, 0.0064f, 0.0064f));
	box2->AddComponent(new Rigidbody(surfboardColl, boxMesh2, 40.f, .0f, true));
	box2->AddComponent(new RenderComponent(boxMat, boxMesh2));

	box2->AddScript(new TestScript(std::string("test physics")));

	GameObject* surfboard = new GameObject();
	surfboard->AddComponent(new TransformComponent(box2->GetTransform()));
	surfboard->GetTransform()->SetPosition(XMFLOAT3(0, -boxDesc.height / 2, 0));
	surfboard->AddComponent(new RenderComponent(surfBoardMat, amesh));
	surfboard->AddComponent(ac);

	/*GameObject* box2 = new GameObject();
	box2->AddComponent(new TransformComponent());
	box2->GetTransform()->SetPosition(XMFLOAT3(0.f, 3.f, 4.f));
	box2->GetTransform()->SetRotation(XMFLOAT3(45, 0, 0));// 90, 0));
	box2->GetTransform()->SetScale(XMFLOAT3(1, 0.1f, 0.1f));// 0.08f, 0.08f, 0.08f));
	box2->AddComponent(new Rigidbody(new OBBCollider(XMFLOAT3(1, 1, 1)), boxMesh, 10, 0, true));// MeshCollider(surfMesh, new OBBCollider(surfMesh->GetMesh()->bounding.size)), meshes, 4.f, .0f, false));
	box2->AddComponent(new RenderComponent(boxMat, boxMesh));// surfBoardMat, surfMesh));*/

	GameObject* sun = new GameObject();
	TransformComponent* st = new TransformComponent();
	st->SetRotation(XMFLOAT3(135, 45, 0));
	sun->AddComponent(st);
	sun->AddComponent(new DirectionalLightComponent(
		XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
		XMFLOAT4(0.25f, 0.25f, 0.25f, 1.0f)));
	/*AudioSourceComponent* aux0 = new AudioSourceComponent();
	aux0->AddClip("BG", GPUMemoryManager::instance().LoadAudioClipFromFile("Assets/Sounds/template_music.ogg", true));
	aux0->Play("BG");
	sun->AddComponent(aux0);*/

	pScene->AddGameObject(box2);
	pScene->AddGameObject(box);
	pScene->AddGameObject(skydome);
	pScene->AddGameObject(surfboard);
	pScene->AddGameObject(mainCamera);
	pScene->AddGameObject(sun);
}

void SceneBuilder::MakeAnimationDemo(ID3D11Device* pDevice, FontEngine* pFontEngine, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene)
{
	GameObject* mainCamera = new GameObject();
	TransformComponent* ct = new TransformComponent();
	ct->SetPosition(XMFLOAT3(0.f, 0.f, 3.f));
	//ct->Rotate(XMFLOAT3(1, 0, 0), 40);
	//ct->Rotate(XMFLOAT3(0, 1, 0), -80);
	mainCamera->AddComponent(ct);
	mainCamera->AddComponent(new CameraComponent(screenWidth, screenHeight, 60.0f, screenWidth / (float)screenHeight, 0.1f, 1000.0f));
	//mainCamera->AddScript(new MoveCameraScript(std::string("Controller")));

	GameObject* animation = new GameObject();
	animation->AddComponent(new TransformComponent());
	animation->GetTransform()->SetScale(XMFLOAT3(0.02f, 0.02f, 0.02f));
	SkeletalMesh* amesh = GPUMemoryManager::instance().SkeletalMeshFromFile("Assets/Meshes/surfer_anim.fbx", true);
	animtypes::AnimationData* animData = GPUMemoryManager::instance().AnimationFromFile("Assets/Meshes/surfer_anim.fbx");
	PBSSolidMaterial* amat = new PBSSolidMaterial();
	amat->Init(pDevice);
	animation->AddComponent(new RenderComponent(amat, amesh));

	BlendState* state = new BlendState(2);
	state->AddTreeNode("Armature|idle", { 0, 0 });
	state->AddTreeNode("Armature|right", { 1, 0 });
	state->AddTreeNode("Armature|left", { -1, 0 });
	state->AddTreeNode("Armature|front", { 0, 1 });
	state->AddTreeNode("Armature|back", { 0, -1 });

	Animator* animator = new Animator();
	animator->SetState(state);

	AnimationComponent* ac = new AnimationComponent(pDevice, *animData, animator);
	//ac->Play("Armature|idle");
	animation->AddComponent(ac);

	animation->AddScript(new AnimationTest(std::string("animationtest")));

	GameObject* sphere = new GameObject("Sphere");
	sphere->AddComponent(new TransformComponent());
	sphere->GetTransform()->Translate(XMFLOAT3(3.0f, 0.0f, 0.0f));
	PBSSolidMaterial* sphereMat = new PBSSolidMaterial();
	sphereMat->Init(pDevice);
	IndexedMesh* sphereMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/bridge.fbx", true, sphereMesh);
	sphere->AddComponent(new RenderComponent(sphereMat, sphereMesh));

	GameObject* sun = new GameObject();
	TransformComponent* st = new TransformComponent();
	st->SetRotation(XMFLOAT3(135, 45, 0));
	//st->SetRotation(XMFLOAT3(90, 0, 0));
	sun->AddComponent(st);
	sun->AddComponent(new DirectionalLightComponent(
		XMFLOAT4(0.05f, 0.05f, 0.05f, 1.0f),
		XMFLOAT4(0.0f, 0.0f, 0.0f, 3.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)));

	pScene->AddGameObject(animation);
	//pScene->AddGameObject(sphere);
	pScene->AddGameObject(mainCamera);
	pScene->AddGameObject(sun);
}

void SceneBuilder::MakeEditorScene(Device* pDevice, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene)
{
	PBSAnimatedVolumetricMaterial* waterMat = new PBSAnimatedVolumetricMaterial();
	waterMat->Init(pDevice->GetDevice());
	waterMat->SetBaseColor(XMFLOAT3(0.5f, 0.8f, 1.0f));
	waterMat->SetRoughness(0.1f);
	waterMat->SetMetallic(0.9f);
	waterMat->SetIndexOfRefraction(1.33f);
	//waterMat->SetSpeed(30.f);
	ID3D11SamplerState* sampler = nullptr;
	GPUMemoryManager::instance().TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_CLAMP, &sampler);
	waterMat->SetTextureSampler(sampler);

	AnimatedVolumeTexture* waterTex = new AnimatedVolumeTexture();

	GPUMemoryManager::instance().AnimatedVolumeTextureFromFile("Assets/Fluidsim/iceWaveFlip_res_050_t_050_slower.uni", waterTex);

	waterMat->SetVolumeTexture(waterTex);
	//SDFPhysic* waterPhy = new SDFPhysic("Assets/Fluidsim/iceWaveFlip_res_050_t_050_10.uni", 10.0f, 5.f, 100.f, 0.2f);// 76.f);// 2.f, 5.f);
	//const DirectX::XMUINT3 waterSize = waterPhy->GetSize();
	//waterMat->SetVolumeExtent(waterSize);

	GameObject* water = new GameObject();
	TransformComponent* wt = new TransformComponent();
	//wt->Scale(XMFLOAT3(0.065 * testScale, 0.065 * testScale, 0.065 * testScale));
	wt->Scale(XMFLOAT3(13.f / (float)waterTex->GetSize().z, 13.f / (float)waterTex->GetSize().z, 13.f / (float)waterTex->GetSize().z));
	//wt->Scale(XMFLOAT3(0.125f, 0.125f, 0.125f));
	wt->Rotate(XMFLOAT3(0, 1, 0), 90);
	water->AddComponent(wt);
	water->AddComponent(new RenderComponent(waterMat));
	//water->AddComponent(waterPhy);

	GameObject* mainCamera = new GameObject();
	TransformComponent* ct = new TransformComponent();
	ct->SetPosition(XMFLOAT3(-0.f, 5.f, -5.f));
	ct->Rotate(XMFLOAT3(1, 0, 0), 40);
	//ct->Rotate(XMFLOAT3(0, 1, 0), -80);
	mainCamera->AddComponent(ct);
	mainCamera->AddComponent(new CameraComponent(screenWidth, screenHeight, 60.0f, screenWidth / (float)screenHeight, 0.1f, 1000.0f));
	mainCamera->AddScript(new EditorScript(std::string("Editor")));
	mainCamera->AddScript(new MoveCameraScript(std::string("Controller")));


	PBSSolidMaterial* surfBoardMat = new PBSSolidMaterial();
	surfBoardMat->Init(pDevice->GetDevice());
	ColorMap* boardDiffuse = new ColorMap();
	GPUMemoryManager::instance().ColorMapFromFile(std::string("Assets/Textures/surfboard_diffuse_mip.dds"), boardDiffuse);
	surfBoardMat->SetColorTexture(boardDiffuse);
	ID3D11SamplerState* sphereSampler;
	GPUMemoryManager::instance().TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP, &sphereSampler);
	surfBoardMat->SetTextureSampler(sphereSampler);

	IndexedMesh* surfMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/surfboard_thp.obj", false, surfMesh);

	GameObject* surfboard = new GameObject("Surfboard");
	surfboard->AddComponent(new TransformComponent());
	surfboard->GetTransform()->SetPosition(XMFLOAT3(0, 0.f, 2.f));

	// should be around 40l
	surfboard->GetTransform()->SetScale(XMFLOAT3(0.08f, 0.08f, 0.08f));
	//surfboard->AddComponent(new Rigidbody(new OBBCollider(surfMesh->GetMesh()->bounding.size), surfMesh->GetMesh(), powf(1.5f, 3.f), .0f, true));
	surfboard->AddComponent(new RenderComponent(surfBoardMat, surfMesh));

	//surfMesh->Update(pDevice->GetContext());

	surfboard->AddScript(new TestScript2(std::string("Test2")));

	XMFLOAT3 ambientPos = XMFLOAT3(4.5f, -2.f, 12.25f);
	XMFLOAT3 ambientScale = XMFLOAT3(0.985f,0.985f,0.985f);

	GameObject* bridge = new GameObject("Bridge");
	bridge->AddComponent(new TransformComponent());
	bridge->GetTransform()->SetPosition(ambientPos);
	bridge->GetTransform()->SetScale(ambientScale);
	//bridge->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -90);
	PBSSolidMaterial* bridgeMat = new PBSSolidMaterial();
	bridgeMat->Init(pDevice->GetDevice());
	bridgeMat->SetBaseColor(XMFLOAT3(0.0f, 0.0f, 1.0f));
	bridgeMat->SetMetallic(0.0f);
	bridgeMat->SetRoughness(0.1f);
	bridgeMat->SetNormalMap(GPUMemoryManager::instance().GetColorMap("Assets/Textures/brick_01_n.dds"));
	
	IndexedMesh* bridgeMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/bridge.obj", true, bridgeMesh);
	
	bridge->AddComponent(new RenderComponent(bridgeMat, bridgeMesh));

	GameObject* mainBasin = new GameObject("MainBasin");
	mainBasin->AddComponent(new TransformComponent());
	mainBasin->GetTransform()->SetPosition(ambientPos);
	mainBasin->GetTransform()->SetScale(ambientScale);
	//mainBasin->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -90);
	PBSSolidMaterial* basinMat = new PBSSolidMaterial();
	basinMat->Init(pDevice->GetDevice());
	IndexedMesh* basinMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/mainBasin.obj", true, basinMesh);
	mainBasin->AddComponent(new RenderComponent(basinMat, basinMesh));

	GameObject* smallBasin = new GameObject("SmallBasin");
	smallBasin->AddComponent(new TransformComponent());
	smallBasin->GetTransform()->SetPosition(ambientPos);
	smallBasin->GetTransform()->SetScale(ambientScale);
	//smallBasin->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -90);
	PBSSolidMaterial* smallBasinMat = new PBSSolidMaterial();
	smallBasinMat->Init(pDevice->GetDevice());
	IndexedMesh* smallBasinMssh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/smallBasin.obj", true, smallBasinMssh);
	smallBasin->AddComponent(new RenderComponent(smallBasinMat, smallBasinMssh));

	GameObject* wallRight = new GameObject("WallRight");
	wallRight->AddComponent(new TransformComponent());
	wallRight->GetTransform()->SetPosition(ambientPos);
	wallRight->GetTransform()->SetScale(ambientScale);
	//wallRight->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -90);
	PBSSolidMaterial* wallRightMat = new PBSSolidMaterial();
	wallRightMat->Init(pDevice->GetDevice());
	IndexedMesh* wallRightMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/wallRight.obj", true, wallRightMesh);
	wallRight->AddComponent(new RenderComponent(wallRightMat, wallRightMesh));

	GameObject* wallLeft = new GameObject("WallLeft");
	wallLeft->AddComponent(new TransformComponent());
	wallLeft->GetTransform()->SetPosition(ambientPos);
	wallLeft->GetTransform()->SetScale(ambientScale);
	//wallLeft->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -90);
	PBSSolidMaterial* wallLeftMat = new PBSSolidMaterial();
	wallLeftMat->Init(pDevice->GetDevice());
	IndexedMesh* wallLeftMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/wallLeft.obj", true, wallLeftMesh);
	wallLeft->AddComponent(new RenderComponent(wallLeftMat, wallLeftMesh));

	GameObject* hillRight = new GameObject("HillRight");
	hillRight->AddComponent(new TransformComponent());
	hillRight->GetTransform()->SetPosition(ambientPos);
	hillRight->GetTransform()->SetScale(ambientScale);
	//hillRight->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -90);
	PBSSolidMaterial* hillRightMat = new PBSSolidMaterial();
	hillRightMat->Init(pDevice->GetDevice());
	IndexedMesh* hillRightMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/hillRight.obj", true, hillRightMesh);
	hillRight->AddComponent(new RenderComponent(hillRightMat, hillRightMesh));

	GameObject* hillLeft = new GameObject("HillLeft");
	hillLeft->AddComponent(new TransformComponent());
	hillLeft->GetTransform()->SetPosition(ambientPos);
	hillLeft->GetTransform()->SetScale(ambientScale);
	//hillLeft->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -90);
	PBSSolidMaterial* hillLeftMat = new PBSSolidMaterial();
	hillLeftMat->Init(pDevice->GetDevice());
	IndexedMesh* hillLeftMesh = new IndexedMesh();
	GPUMemoryManager::instance().MeshFromFile("Assets/Meshes/Ambient/hillLeft.obj", true, hillLeftMesh);
	hillLeft->AddComponent(new RenderComponent(hillLeftMat, hillLeftMesh));

	GameObject* skydome = new GameObject();
	skydome->AddComponent(new TransformComponent());
	skydome->GetTransform()->SetScale(XMFLOAT3(500, 500, 500));
	skydome->SetParent(mainCamera, true, false, false);
	SkyMaterial* skyMat = new SkyMaterial();
	skyMat->Init(pDevice->GetDevice());
	skyMat->SetSampler(GPUMemoryManager::instance().GetDefaultTextureSampler());
	skyMat->SetSkyProbe(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	IndexedMesh* skySphere = new IndexedMesh();
	GPUMemoryManager::instance().MakeSphere(skySphere, true);
	skydome->AddComponent(new RenderComponent(skyMat, skySphere));

	GameObject* sun = new GameObject();
	TransformComponent* st = new TransformComponent();
	st->SetRotation(XMFLOAT3(135, 45, 0));
	//st->SetRotation(XMFLOAT3(90, 0, 0));
	sun->AddComponent(st);
	sun->AddComponent(new DirectionalLightComponent(
		XMFLOAT4(0.05f, 0.05f, 0.05f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
		XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)));
	sun->AddScript(new AnimateLight(std::string("AnimateLight")));

	GameObject* guiSurf = new GameObject();
	GUIMaterial* guiMat = new GUIMaterial();
	guiMat->Init(pDevice->GetDevice());
	VolumeTexture* guiTexture = new VolumeTexture();
	GPUMemoryManager::instance().VolumeTextureFromFile(std::string("Assets/Textures/surfGUI_strip_hq.dds"), guiTexture);
	guiMat->SetSprite(guiTexture);
	ID3D11SamplerState* guiSampler = nullptr;
	GPUMemoryManager::instance().TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_POINT, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_CLAMP, &guiSampler);
	guiMat->SetSampler(guiSampler);
	guiSurf->AddComponent(new RenderComponent(guiMat));

	//surfboard->AddScript(new BalanceScript(std::string("BalanceScript"), guiMat));

	//pScene->AddGameObject(guiSurf);
	pScene->AddGameObject(skydome);
	pScene->AddGameObject(bridge);
	//pScene->AddGameObject(mainBasin);
	pScene->AddGameObject(smallBasin);
	pScene->AddGameObject(wallRight);
	pScene->AddGameObject(wallLeft);
	pScene->AddGameObject(hillLeft);
	pScene->AddGameObject(hillRight);
	pScene->AddGameObject(water);
	//pScene->AddGameObject(surfboard);
	pScene->AddGameObject(mainCamera);
	pScene->AddGameObject(sun);
}
