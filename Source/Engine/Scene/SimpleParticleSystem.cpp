#include "SimpleParticleSystem.h"

SimpleParticleSystem::SimpleParticleSystem(PrimitiveMesh* mesh) : IParticleSystem(mesh)
{
	mParticles = new geotypes::BaseParticleVertex[mMaxCount];
	mVelocities = new XMFLOAT3[mMaxCount];
	for (int i = 0; i < mMaxCount; i++)
	{
		mParticles[i].scale = -1;
	}
	mHead = 0;
}


void SimpleParticleSystem::Start()
{
}

void SimpleParticleSystem::FixedUpdate(float dt)
{
	InstantiateParticles();
	m.lock();
	for (int i = 0; i < mMaxCount; i++)
	{
		if (mParticles[i].scale > 0)
		{
			XMStoreFloat3(&mParticles[i].position, XMLoadFloat3(&mParticles[i].position) + XMLoadFloat3(&mVelocities[i]) * dt);
			mParticles[i].ttl -= dt * 0.1f;
			if (mParticles[i].ttl < 0)
			{
				mParticles[i].scale = -1;
			}			
		}
	}
	m.unlock();
}

void SimpleParticleSystem::UpdateForces(float dt)
{
	for (int i = 0; i < mMaxCount; i++)
	{
		if (mParticles[i].scale > 0)
		{
			XMStoreFloat3(&mVelocities[i], XMLoadFloat3(&mVelocities[i]) + XMLoadFloat3(&XMFLOAT3(0, GRAVITY, 0)) * dt);
		}
	}
}

void SimpleParticleSystem::EvaluateSimulation(float dt)
{
	m.lock();
	mMesh->Update(mParticles);
	m.unlock();
}

void SimpleParticleSystem::InstantiateParticles()
{
	int count = std::rand() % (int)std::ceil(mMaxCount / 1000.f) + 1;

	m.lock();
	for (int i = mHead; i < mHead + count; i++)
	{
		int idx = i % mMaxCount;
		mParticles[idx].position = XMFLOAT3(0, 0, 0);
		mParticles[idx].scale = (float)(std::rand() % 100) * 0.01f + 0.1f;
		mVelocities[idx] = XMFLOAT3((50.f - (float)(std::rand() % 100)) * 0.3f, (50.f - (float)(std::rand() % 100)) * 0.3f, (50.f - (float)(std::rand() % 100)) * 0.3f);
		mParticles[idx].ttl = 1.f;
		mParticles[idx].state = 0;
	}
	mHead = (mHead + count) % mMaxCount;
	m.unlock();
}

