#pragma once
#include "ICollider.h"

class SphereCollider :
	public ICollider
{
public:
	friend class AABBCollider;
	SphereCollider(float radius, const XMFLOAT3& pos = XMFLOAT3(0, 0, 0));
	SphereCollider(ICollider* parent, float radius, XMFLOAT3 offsetPos = XMFLOAT3(0, 0, 0));
	~SphereCollider();
	//const virtual Type GetType() override{ return SPHERE; }
	float GetRadius() 
	{
		XMFLOAT3 s = mTransform.GetWorldScale();
		return mRadius * (s.x + s.y + s.z) / 3.f;
	}
	virtual bool CollisionDetection(ICollider* other) override;
	virtual void GetContactInfo(Collision* collision, ICollider * other) override;

private:
	float mRadius;
};

