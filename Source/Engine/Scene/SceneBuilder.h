#ifndef _SCENEBUILDER_H_
#define _SCENEBUILDER_H_

#include "Scene.h"

#include "../Render/Data/IndexedMesh.h"
#include "../Render/Data/PBSSolidMaterial.h"
#include "../Render/Data/SkyMaterial.h"
#include "../Render//Data/PBSVolumetric.h"
#include "../Render/Data/GUIMaterial.h"
#include "../Scene/DirectionalLightComponent.h"
#include "../Scene/CameraComponent.h"
#include "AudioSourceComponent.h"
#include "../Scene/GameObject.h"

#include "../Render/FontEngine.h"

class SceneBuilder
{
public:
	SceneBuilder();
	~SceneBuilder();

	void MakeWater(Device* pDevice, Scene* pScene, GameObject** particleSystem);
	void MakeAmbient(Device* pDevice, Scene* pScene);
	void MakeSurfer(Device* pDevice, Scene* pScene, GameObject** surferOut, GameObject** surfboardOut);

	void MakeDevelopmentDemo(Device* pDevice, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene);
	void MakeCtrlDemo(Device* pDevice, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene);
	void MakePhysicsDemo(Device* pDevice, FontEngine* pFontEngine, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene);
	void MakeRenderingDemo(ID3D11Device* pDevice, FontEngine* pFontEngine, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene);
	void MakeAnimationDemo(ID3D11Device* pDevice, FontEngine* pFontEngine, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene);

	void MakeEditorScene(Device* pDevice, const uint32_t screenWidth, const uint32_t screenHeight, Scene* pScene);
};

#endif
