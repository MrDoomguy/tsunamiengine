#include "Scene.h"

#include "../Render/IPipeline.h"
#include "../Render/Data/VolumetricMaterial.h"
#include "Rigidbody.h"
#include "SphereCollider.h"
#include "../Physics/Shader/SDFGrid.h"

Scene::Scene(const std::string& name) :
	mName{ name }
{
}

Scene::Scene() :
	mName{ std::string("Scene_") + std::to_string(GUIDGen::GetNext())}
{
}

Scene::~Scene()
{
}

void Scene::AddGameObject(GameObject* go, const Tags* pTag)
{
	mGameObjects.push_back(go);
	
	// Render shortcuts
	if (go->IsRenderable())
	{
		switch (go->GetMaterialType())
		{
		case IMaterial::Type::PBS_SOLID:
		{
			if (go->IsAnimated())
				mAnimationQueue.push_back(go);
			else
				mOpaqueQueue.push_back(go);
			break;
		}
		case IMaterial::Type::PBS_VOLUMETRIC:
			mRaytraceQueue.push_back(go);
			break;
		case IMaterial::Type::PARTICLE:
			mParticleQueue.push_back(go);
			break;
		case IMaterial::Type::GUI:
			mGUIQueue.push_back(go);
			break;
		case IMaterial::Type::FONT:
			mFontQueue.push_back(go);
			break;
		case IMaterial::Type::SKY:
			mTagged[Tags::SKY] = go;
		case IMaterial::Type::INVALID:
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Tried to add invalid material to scene, can produce unexpected behaviour");
			break;
		}
	}

	// Light shortcuts
	ILightComponent* light = dynamic_cast<ILightComponent*>(go->GetComponent(IComponent::Type::LIGHT));
	if (light != nullptr)
	{
		if (light->GetLightType() == ILightComponent::Type::DIRECTIONAL && mDirectionalLights.size() < IPipeline::MAX_DIR_LIGHTS)
		{
			mDirectionalLights.push_back(go);
		}
		else
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Can't add directional light, too many lights! :(.");
	}

	// Tags
	CameraComponent* cam = dynamic_cast<CameraComponent*>(go->GetComponent(IComponent::Type::CAMERA));
	if (cam != nullptr)
	{
		mTagged[Tags::MAIN_CAMERA] = go;
	}

	IPhysicComponent* physic = dynamic_cast<IPhysicComponent*>(go->GetComponent(IComponent::Type::PHYSIC));
	if (physic != nullptr)
	{
		PhysicsEngine::instance().AddPhysicObject(physic);
	}
}

void Scene::Awake()
{
	for (auto& go : mGameObjects)
	{
		go->Awake();
	}
}

void Scene::Start()
{
	for (auto& go : mGameObjects)
	{
		go->Start();
	}
}

void Scene::Update(float dt)
{
	updateMutex.lock();
	for (auto& go : mGameObjects)
	{
		if (go->IsEnabled())
		{
			go->Update(dt);
		}
	}
	updateMutex.unlock();
}

void Scene::FixedUpdate(float dt)
{
	updateMutex.lock();
	for (auto& go : mGameObjects)
	{
		// TODO: save as membervariables?
		IPhysicComponent* pc = dynamic_cast<IPhysicComponent*>(go->GetComponent(IComponent::Type::PHYSIC));		
		TransformComponent* t = dynamic_cast<TransformComponent*>(go->GetComponent(IComponent::Type::TRANSFORM));

		// synchronizes the go transform (if modified in the update function) and the physics transform
		if (t != nullptr && pc != nullptr && t->IsDirty())
		{
			TransformComponent* pt = pc->GetPhysicsTransform();
			pt->SetPosition(t->GetPosition());
			pt->SetRotation(t->GetRotation());
			pt->SetScale(t->GetScale());
			pc->SwapTransforms();
			t->SetDirty(false);
		}
	}
	for (auto& go : mGameObjects)
	{
		if (go->IsEnabled())
		{
			go->FixedUpdate(dt);
		}
	}
	updateMutex.unlock();
}

void Scene::OnResize(ID3D11Device* pDevice, uint32_t width, uint32_t height)
{
	auto cam = mTagged[Tags::MAIN_CAMERA]->GetCamera();
	cam->SetFrustum(width, height, cam->GetFovY(), width / (float)height, cam->GetNearZ(), cam->GetFarZ());
}

void Scene::NotifyFontQueue(GameObject* go)
{
	mFontQueue.push_back(go);
}

std::vector<GameObject*> Scene::GetRaytraceQueue()
{
	return mRaytraceQueue;
}

std::vector<GameObject*> Scene::GetOpaqueQueue()
{
	return mOpaqueQueue;
}

std::vector<GameObject*> Scene::GetParticleQueue()
{
	return mParticleQueue;
}

std::vector<GameObject*> Scene::GetGUIQueue()
{
	return mGUIQueue;
}

std::vector<GameObject*> Scene::GetFontQueue()
{
	return mFontQueue;
}

std::vector<GameObject*> Scene::GetAnimationQueue()
{
	return mAnimationQueue;
}

bool Scene::ExecuteOpaque() const
{
	return (mOpaqueQueue.size() > 0);
}

bool Scene::ExecuteParticle() const
{
	return (mParticleQueue.size() > 0);
}

bool Scene::ExecuteRaytrace() const
{
	return (mRaytraceQueue.size() > 0);
}

bool Scene::ExecuteGUI() const
{
	return (mGUIQueue.size() > 0);
}

bool Scene::ExecuteFont() const
{
	return (mFontQueue.size() > 0);
}

bool Scene::ExecuteAnimation() const
{
	return (mAnimationQueue.size() > 0);
}

GameObject* Scene::GetCamera()
{
	if (mTagged[Tags::MAIN_CAMERA])
		return mTagged[Tags::MAIN_CAMERA];

	return nullptr;
}

GameObject* Scene::GetSky()
{
	if (mTagged.find(Tags::SKY) != mTagged.end())
		return mTagged[Tags::SKY];
	return nullptr;
}

std::vector<GameObject*> Scene::GetDirLights()
{
	return mDirectionalLights;
}

SolidPipeline::PerFrame Scene::GetSolidPerFrameData()
{
	SolidPipeline::PerFrame spf;
	ZeroMemory(&spf, sizeof(SolidPipeline::PerFrame));

	auto& cam = mTagged[Tags::MAIN_CAMERA];

	spf.viewProj = XMMatrixMultiply(cam->GetTransform()->GetLocal(), cam->GetCamera()->GetProj());
	spf.nDirLights = static_cast<uint32_t>(mDirectionalLights.size());
	spf.viewDir = cam->GetTransform()->GetForward();
	spf.eyePosW = cam->GetTransform()->GetPosition();
	for (size_t i = 0; i < mDirectionalLights.size(); i++)
	{
		auto l = dynamic_cast<DirectionalLightComponent*>(mDirectionalLights[i]->GetComponent(IComponent::Type::LIGHT));
		spf.dirLights[i].ambient = l->GetAmbient();
		spf.dirLights[i].diffuse = l->GetDiffuse();
		spf.dirLights[i].specular = l->GetSpecular();
		spf.dirLights[i].direction = mDirectionalLights[i]->GetTransform()->GetForward();
	}

	return spf;
}

ParticlePipeline::PerFrame Scene::GetParticlePerFrameData()
{
	ParticlePipeline::PerFrame pfd;
	ZeroMemory(&pfd, sizeof(ParticlePipeline::PerFrame));

	auto& cam = mTagged[Tags::MAIN_CAMERA];

	pfd.viewM = cam->GetTransform()->GetLocal();
	pfd.projM = cam->GetCamera()->GetProj();
	pfd.normalTrans = cam->GetTransform()->GetWorld();
	pfd.screenSize = XMUINT2(cam->GetCamera()->GetWidth(), cam->GetCamera()->GetHeight());
	pfd.viewZ = cam->GetCamera()->GetViewZ();

	return pfd;
}

RaytracePipeline::PerFrame Scene::GetRaytracePerFrameData()
{
	RaytracePipeline::PerFrame rpf;
	ZeroMemory(&rpf, sizeof(RaytracePipeline::PerFrame));

	auto& cam = mTagged[Tags::MAIN_CAMERA];

	rpf.view = cam->GetTransform()->GetWorld();
	rpf.viewLocal = cam->GetTransform()->GetLocal();
	rpf.nearPlane = cam->GetCamera()->GetNearZ();
	rpf.farPlane = cam->GetCamera()->GetFarZ();
	rpf.viewAngle = cam->GetCamera()->GetFovY();
	rpf.viewZ = cam->GetCamera()->GetViewZ();
	rpf.imgPlaneWidth = static_cast<float>(cam->GetCamera()->GetWidth());
	rpf.imgPlaneHeight = static_cast<float>(cam->GetCamera()->GetHeight());
	rpf.viewProj = XMMatrixMultiply(cam->GetTransform()->GetLocal(), cam->GetCamera()->GetProj());

	// TODO: set with current time
	rpf.offset = XMUINT3(0, 0, 0);// PhysicShader::SDFGrid::GetOffsetOfTime(0);
	rpf.dt = 0;

	rpf.nDirLights = static_cast<uint32_t>(mDirectionalLights.size());
	for (size_t i = 0; i < mDirectionalLights.size(); i++)
	{
		auto l = dynamic_cast<DirectionalLightComponent*>(mDirectionalLights[i]->GetComponent(IComponent::Type::LIGHT));
		rpf.dirLights[i].ambient = l->GetAmbient();
		rpf.dirLights[i].diffuse = l->GetDiffuse();
		rpf.dirLights[i].specular = l->GetSpecular();
		rpf.dirLights[i].direction = mDirectionalLights[i]->GetTransform()->GetForward();
	}

	// HACK: hacked sphere renderer

	/*int count = 0;
	for (auto& go : mGameObjects)
	{
		Rigidbody *rg;
		if ((rg = dynamic_cast<Rigidbody*>(go->GetComponent(IComponent::Type::PHYSIC))) != nullptr && count < IPipeline::MAX_DIR_LIGHTS)
		{
			SphereCollider *sc = dynamic_cast<SphereCollider*>(rg->GetCollider());
			XMFLOAT3 s = rg->GetPhysicsTransform()->GetScale();

			rpf.spheres[count].pos = go->GetTransform()->GetPosition();
			rpf.spheres[count++].radius = sc->GetRadius() / 3.f;
		}
	}

	rpf.nSpheres = count;*/
	
	return rpf;
}

ParticleShadingPipeline::PerFrame Scene::GetParticleShadingPerFrameData()
{
	ParticleShadingPipeline::PerFrame pfd;
	ZeroMemory(&pfd, sizeof(ParticleShadingPipeline::PerFrame));

	auto& cam = mTagged[Tags::MAIN_CAMERA];

	pfd.screenSize = XMUINT2(cam->GetCamera()->GetWidth(), cam->GetCamera()->GetHeight());

	return pfd;
}

GUIPipeline::PerFrame Scene::GetGUIPerFrameData()
{
	auto cam = mTagged[Tags::MAIN_CAMERA]->GetCamera();

	GUIPipeline::PerFrame gpf;
	gpf.imgPlaneSize = XMFLOAT2(static_cast<float>(cam->GetWidth()), static_cast<float>(cam->GetHeight()));

	return gpf;
}

std::string Scene::GetName()
{
	return mName;
}

void Scene::SetEnvProbePosition(const XMFLOAT3& pos)
{
	mProbePos = pos;
}

XMFLOAT3 Scene::GetEnvProbePosition()
{
	return mProbePos;
}

void Scene::ReadPrefab(const char* filename)
{
	tinyxml2::XMLDocument prefab;
	tinyxml2::XMLError res;
	prefab.LoadFile(filename);

	GameObject* go = new GameObject();
}
