#ifndef _SCENE_H_
#define _SCENE_H_

#include "GameObject.h"
#include "../Render/ParticlePipeline.h"
#include "../Render/SolidPipeline.h"
#include "../Render/RaytracePipeline.h"
#include "../Render/ParticleShadingPipeline.h"
#include "../Render/GUIPipeline.h"

#include <vector>

#include "../General/TsunamiStd.h"

class Scene
{
public:
	enum Tags
	{
		MAIN_CAMERA,
		SKY
	};

	Scene(const std::string& name);
	Scene();
	~Scene();

	void AddGameObject(GameObject* go, const Tags* pTag=nullptr);

	void Awake();
	void Start();
	void Update(float dt);
	void FixedUpdate(float dt);

	void OnResize(ID3D11Device* pDevice, uint32_t width, uint32_t height);

	void NotifyFontQueue(GameObject* go);

	std::vector<GameObject*> GetRaytraceQueue();
	std::vector<GameObject*> GetOpaqueQueue();
	std::vector<GameObject*> GetParticleQueue();
	std::vector<GameObject*> GetGUIQueue();
	std::vector<GameObject*> GetFontQueue();
	std::vector<GameObject*> GetAnimationQueue();

	bool ExecuteOpaque() const;
	bool ExecuteParticle() const;
	bool ExecuteRaytrace() const;
	bool ExecuteGUI() const;
	bool ExecuteFont() const;
	bool ExecuteAnimation() const;

	GameObject* GetCamera();
	GameObject* GetSky();

	std::vector<GameObject*> GetDirLights();

	SolidPipeline::PerFrame GetSolidPerFrameData();
	ParticlePipeline::PerFrame GetParticlePerFrameData();
	RaytracePipeline::PerFrame GetRaytracePerFrameData();
	ParticleShadingPipeline::PerFrame GetParticleShadingPerFrameData();
	GUIPipeline::PerFrame GetGUIPerFrameData();

	std::string GetName();
	void SetEnvProbePosition(const XMFLOAT3& pos);
	XMFLOAT3 GetEnvProbePosition();

private:
	void ReadPrefab(const char* filename);

	std::vector<GameObject*> mGameObjects;

	std::vector<GameObject*> mEmptyObjects;
	std::vector<GameObject*> mOpaqueQueue;
	std::vector<GameObject*> mAnimationQueue;
	std::vector<GameObject*> mParticleQueue;
	std::vector<GameObject*> mRaytraceQueue;
	std::vector<GameObject*> mGUIQueue;
	std::vector<GameObject*> mFontQueue;

	std::vector<GameObject*> mDirectionalLights;

	std::unordered_map<Tags, GameObject*> mTagged;
	std::mutex updateMutex;

	std::string mName;
	XMFLOAT3 mProbePos;
};

#endif
