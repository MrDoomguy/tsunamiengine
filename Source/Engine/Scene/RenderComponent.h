#ifndef _RENDERCOMPONENT_H_
#define _RENDERCOMPONENT_H_

#include "IComponent.h"

#include "../Render/Data/IMaterial.h"
//#include "../Render/Data/IMesh.h"
#include "../Render/Data/IndexedMesh.h"
#include "../Render/Data/SkeletalMesh.h"
#include "../Render/Data/MeshHelper.h"
#include "../Render/Data/FontMaterial.h"
#include "../Render/Data/PBSSolidMaterial.h"
#include "../Render/Data/ParticleMaterial.h"

class RenderComponent : public IComponent
{
public:
	RenderComponent(IMaterial* pMaterial=nullptr, IMesh* pMesh=nullptr)  :
		IComponent(IComponent::Type::RENDER), mMaterial{ pMaterial }, mMesh{ pMesh }
	{
	}

	~RenderComponent()
	{ }

	void SetMaterial(IMaterial* pMaterial) { mMaterial = pMaterial; }
	void SetMesh(IMesh* pMesh) { mMesh = pMesh; }

	void Draw(ID3D11DeviceContext* pContext, const TransformComponent* pTransform, const AnimationComponent* pAnim)
	{
		mMaterial->Set(pContext, pTransform, pAnim);

		mMesh->Draw(pContext);

		mMaterial->Reset(pContext);
	}

	void Draw(ID3D11DeviceContext* pContext)
	{
		dynamic_cast<PrimitiveMesh*>(mMesh)->DrawAuto(pContext);
	}

	void DrawFont(ID2D1RenderTarget* pRenderTarget, IDWriteFactory* pWriteFactory, ID2D1SolidColorBrush* pBrush)
	{
		auto mat = dynamic_cast<FontMaterial*>(mMaterial);
		mat->Set(pWriteFactory);
		pBrush->SetColor(mat->GetColor());
		pRenderTarget->DrawTextW(mat->GetText().c_str(), static_cast<uint32_t>(mat->GetText().size()), mat->GetTextFormat(), mat->GetRect(), pBrush);
	}

	void Dispatch(ID3D11DeviceContext* pContext, const TransformComponent* transform, UINT threadX, UINT threadY, UINT threadZ)
	{
		mMaterial->Set(pContext, transform);

		pContext->Dispatch(threadX, threadY, threadZ);

		mMaterial->Reset(pContext);
	}

	IMaterial* GetMaterial() { return mMaterial; }
	IMesh* GetMesh() { return mMesh; }

private:
	IMaterial* mMaterial;
	IMesh* mMesh;
};

#endif