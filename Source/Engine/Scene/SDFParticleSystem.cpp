#include "SDFParticleSystem.h"

SDFParticleSystem::SDFParticleSystem(Device* device, AnimatedVolumeTexture* SDFResource, ID3D11SamplerState* sampler, const SDFGrid::SDFProperties& sdfProps, const InitProps& initProps, float ttlFactor, float maxScale, PrimitiveMesh* mesh, VolumeTextureRW* obstacleGrid, AnimatedVolumeTexture* velResource)
	: IParticleSystem(mesh),
	mWaterPropsData(sdfProps),
	mWaterProps(device->GetDevice(), sizeof(SDFGrid::SDFProperties)),
	mSDFResource(SDFResource),
	mSampler(sampler),
	mVel(device->GetDevice(), mMaxCount * sizeof(XMFLOAT3), sizeof(XMFLOAT3), true, false, D3D11_CPU_ACCESS_WRITE),
	mPerFrame(device->GetDevice(), sizeof(SDFGrid::PerFrameData)),
	mParticleProps(device->GetDevice(), sizeof(ParticleProps)),
	mHead(device->GetDevice(), sizeof(uint32_t), sizeof(uint32_t), true, false, D3D11_CPU_ACCESS_WRITE), 
	mVelResource(velResource),
	mInitProps(device->GetDevice(), sizeof(InitProps)),
	mObstacleGrid(obstacleGrid)
{
	mDevice = device;

	mParticleInit = GPUMemoryManager::instance().GetComputeShader("./Assets/Shaders/Compute/Physics/SDFInitParticle.cso");
	mParticleMovement = GPUMemoryManager::instance().GetComputeShader("./Assets/Shaders/Compute/Physics/SDFParticleInteract.cso");

	mParticlePropsData.size = mMaxCount;
	mParticlePropsData.ttlFactor = ttlFactor;
	mParticlePropsData.maxScale = maxScale;

	mDevice->GetContext()->UpdateSubresource(mWaterProps.GetSubresource(), 0, nullptr, &sdfProps, 0, 0);
	mDevice->GetContext()->UpdateSubresource(mInitProps.GetSubresource(), 0, nullptr, &initProps, 0, 0);

	D3D11_MAPPED_SUBRESOURCE res;
	mHead.Map(device->GetContext(), D3D11_MAP::D3D11_MAP_WRITE, &res, 0);
	*((uint32_t*)res.pData) = 0;
	mHead.Unmap(device->GetContext(), 0);
}

SDFParticleSystem::~SDFParticleSystem()
{
}

void SDFParticleSystem::SetInitProps(const InitProps & initProps)
{
	mDevice->LockContext();
	mDevice->GetContext()->UpdateSubresource(mInitProps.GetSubresource(), 0, nullptr, &initProps, 0, 0);
	mDevice->UnlockContext();
}

void SDFParticleSystem::Start()
{
}

void SDFParticleSystem::FixedUpdate(float dt)
{
	int blockSize = (int)std::ceil(mMaxCount / (float)THREAD_COUNT_1D);

	mParticlePropsData.dt = dt;

	float time = Application::instance().GetTotalTime();
	float t = time * mSDFResource->GetSpeed();

	SDFGrid::PerFrameData perFrame;
	perFrame.offset = mSDFResource->GetOffsetOfTime((int)t);
	perFrame.nextOffset = mSDFResource->GetOffsetOfTime((int)t + 1);
	perFrame.blend = t - (int)t;
	perFrame.sdfFactor = mSDFResource->GetFactor();

	if (mVelResource != nullptr)
	{
		t = time * mVelResource->GetSpeed();

		perFrame.velOffset = mVelResource->GetOffsetOfTime((int)t);
		perFrame.velNextOffset = mVelResource->GetOffsetOfTime((int)t + 1);
		perFrame.velBlend = t - (int)t;
		perFrame.velFactor = mVelResource->GetFactor() * mVelResource->GetSpeed() * mSDFResource->GetSize().x / mVelResource->GetSize().x;
	} 

	mDevice->LockContext();

	ID3D11DeviceContext* context = mDevice->GetContext();

	context->CSSetShader(mParticleMovement->GetObjectPtr(), nullptr, 0);

	context->UpdateSubresource(mParticleProps.GetSubresource(), 0, nullptr, &mParticlePropsData, 0, 0);
	context->UpdateSubresource(mPerFrame.GetSubresource(), 0, nullptr, &perFrame, 0, 0);

	ID3D11Buffer* buffer[] = { *mWaterProps.GetPtr(), *mPerFrame.GetPtr(), *mParticleProps.GetPtr(), *mInitProps.GetPtr() };
	context->CSSetConstantBuffers(0, 4, buffer); 

	ID3D11UnorderedAccessView* uav[] = { *mMesh->GetUAV(), *mVel.GetUAV(), *mHead.GetUAV() };
	context->CSSetUnorderedAccessViews(0, 3, uav, nullptr);

	ID3D11ShaderResourceView* srv[3];
	srv[0] = *mSDFResource->GetSRV();
	srv[1] = mVelResource != nullptr ? *mVelResource->GetSRV() : nullptr;
	srv[2] = mObstacleGrid != nullptr ? *mObstacleGrid->GetSRV() : nullptr;
	
	context->CSSetShaderResources(0, 3, srv);

	context->CSSetSamplers(0, 1, &mSampler);

	float downScale = .5f;
	// HACK: use instead of hardcoded downscale a fixed size

	context->Dispatch(blockSize, 1, 1);
	context->CSSetShader(mParticleInit->GetObjectPtr(), nullptr, 0);
	context->Dispatch((int)std::ceil(downScale * mSDFResource->GetWidth() / (float)THREAD_COUNT_3D), (int)std::ceil(downScale * mSDFResource->GetHeight() / (float)THREAD_COUNT_3D), (int)std::ceil(downScale * mSDFResource->GetDepth() / (float)THREAD_COUNT_3D));

	
	//ID3D11Buffer* nullBuffer[] = { nullptr, nullptr, nullptr };
	ID3D11ShaderResourceView* nullSRV[] = { nullptr, nullptr, nullptr };
	ID3D11UnorderedAccessView* nullUAV[] = { nullptr, nullptr, nullptr };
	context->CSSetShaderResources(0, 3, nullSRV);
	context->CSSetUnorderedAccessViews(0, 3, nullUAV, nullptr);

	mDevice->UnlockContext();
}

void SDFParticleSystem::EvaluateSimulation(float dt)
{
	mMesh->UpdateVB();
}

