#ifndef _TRANSFORMCOMPONENT_H_
#define _TRANSFORMCOMPONENT_H_

#include "IComponent.h"
#include <DirectXMath.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <vector>

using namespace DirectX;

// TODO: lock for go with physicscomponents! only kinematics can be moved
class TransformComponent : public IComponent
{
public:
	enum AXIS_MASK : std::uint16_t
	{
		T_x = 0x0001,
		T_y = 0x0002,
		T_z = 0x0004,
		R_x = 0x0008,
		R_y = 0x0010,
		R_z = 0x0020,
		S_x = 0x0040,
		S_y = 0x0080,
		S_z = 0x0100
	};

	TransformComponent(TransformComponent* pParent, const bool inheritTranslation = true, const bool inheritScale = true, const bool inheritRotation = true);
	TransformComponent();
	~TransformComponent();

	void SetPosition(const XMFLOAT3& position);
	void SetRotation(const XMFLOAT3& rotation);
	void SetRotation(const XMFLOAT4& rotation);
	void SetScale(const XMFLOAT3& scale);

	XMFLOAT3 GetPosition() const;
	XMFLOAT4 GetRotation() const;
	XMFLOAT3 GetEuler() const;
	XMFLOAT3 GetScale() const;

	XMFLOAT3 GetWorldPosition() const;
	XMFLOAT4 GetWorldRotation() const;
	XMFLOAT3 GetWorldEuler() const;
	XMFLOAT3 GetWorldScale() const;

	XMFLOAT3 GetForward() const;
	XMFLOAT3 GetRight() const;
	XMFLOAT3 GetUp() const;

	void LookAt(const XMFLOAT3& target);
	XMFLOAT4 GetLookAt(const XMFLOAT3& target);

	void Translate(const XMFLOAT3& offset);
	void Rotate(const XMFLOAT3& axis, float angle);
	void Rotate(const XMFLOAT4& rotation);
	void Scale(const XMFLOAT3& offset);

	void Roll(float angle);
	void Pitch(float angle);
	void Yaw(float angle);

	void Walk(const float distance);
	void Strafe(const float distance);
	void Fly(const float distance);

	void SetParent(TransformComponent* pParent, const bool inheritTranslation = true, const bool inheritScale = true, const bool inheritRotation = true);
	void AddChild(TransformComponent* pChild, const bool inheritTranslation = true, const bool inheritScale = true, const bool inheritRotation = true);
	void RemoveChild(int i);
	void InheritTranslate(const bool inherit);
	void InheritScale(const bool inherit);
	void InheritRotation(const bool inherit);

	bool HasParent();
	bool HasChildren();

	TransformComponent* GetParent() { return mParent; }
	TransformComponent* GetChild(int i) { return i < mChildren.size() ? mChildren[i] : nullptr; }
	const std::vector<TransformComponent*>& GetChildren() { return mChildren; }

	/*void FreezeTranslation(const uint8_t flag);
	void FreezeScale(const uint8_t flag);
	void FreezeRotation(const uint8_t flag);*/

	XMMATRIX GetWorld() const;
	XMMATRIX GetLocal() const;

	bool IsDirty() { return mDirty; }
	void SetDirty(bool dirty) { mDirty = dirty; }

	void Print();
	void Print(std::wstringstream& s);
	void Print(std::ofstream& s);

private:
	void MakeWorldMatrix();

	/*XMFLOAT3 GetTranslationAxisMask();
	XMFLOAT3 GetScaleAxisMask();
	XMFLOAT3 GetRotationAxisMask();*/

	TransformComponent* mParent;
	//uint16_t mTransformMask;
	bool mInheritTranslate;
	bool mInheritScale;
	bool mInheritRotation;
	
	XMMATRIX MakeTranslation();
	XMMATRIX MakeScale();
	XMMATRIX MakeRotation();

	XMMATRIX MakeInvTranslation();
	XMMATRIX MakeInvScale();
	XMMATRIX MakeInvRotation();

	XMFLOAT3 mPosition;
	XMFLOAT4 mRotation;
	XMFLOAT3 mScale;

	XMFLOAT3 mForward;
	XMFLOAT3 mRight;
	XMFLOAT3 mUp;

	XMFLOAT4X4 mTransformCache;  
	XMFLOAT4X4 mParentTransformCache;
	XMFLOAT4X4 mInverseTransformCache;
	XMFLOAT4X4 mInverseParentTransformCache;

	bool mDirty = false;

	std::vector<TransformComponent*> mChildren;
};

#endif
