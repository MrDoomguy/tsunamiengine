#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#include <unordered_map>
#include "IComponent.h"
#include "ILightComponent.h"
#include "IScript.h"

#include "RenderComponent.h"
#include "TransformComponent.h"
#include "CameraComponent.h"
#include "DirectionalLightComponent.h"

class GameObject
{
public:
	GameObject(const std::string& name);
	GameObject();
	~GameObject();

	void SetName(const std::string& name);
	std::string GetName() const;

	// Parenting only supports transform chaining
	void SetParent(GameObject* pParentGO, const bool inheritTranslation = true, const bool inheritScale = true, const bool inheritRotation = true);

	void AddComponent(IComponent* pComponent);
	void RemoveComponent(const IComponent::Type type);

	void AddScript(IScript* script);
	void RemoveScript(const std::string& name);

	IComponent* GetComponent(const IComponent::Type type);
	IScript* GetScript(const std::string& name);
	std::vector<IScript*> GetScript() const;

	// shortcuts
	IMaterial::Type GetMaterialType();
	IMaterial* GetMaterial();
	TransformComponent* GetTransform();
	CameraComponent* GetCamera();
	AnimationComponent* GetAnimation();
	bool IsAnimated();
	bool IsRenderable();

	void Draw(ID3D11DeviceContext* pContext);
	void DrawAuto(ID3D11DeviceContext* pContext);
	void Draw(ID2D1RenderTarget* pRenderTarget, IDWriteFactory* pWriteFactory, ID2D1SolidColorBrush* pBrush);
	void Dispatch(ID3D11DeviceContext* pContext, UINT threadX, UINT threadY, UINT threadZ);

	void Awake();
	void Start();
	void Update(float dt);
	void FixedUpdate(float dt);

	void Enable(bool enable);
	bool IsEnabled() { return mEnabled; }

protected:
	bool mEnabled = true;

private:
	IComponent* FindComponent(const IComponent::Type type);
	IScript* FindScript(const std::string& name);

	std::unordered_map<IComponent::Type, IComponent*> mComponents;
	std::unordered_map<std::string, IScript*> mScripts;

	std::string mName;

};

#endif
