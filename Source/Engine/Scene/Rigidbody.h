#pragma once
#include "IPhysicComponent.h"
#include "TransformComponent.h"
#include "SphereCollider.h"


class Rigidbody : public IPhysicComponent
{
public:
	// kinematic rigidbody
	Rigidbody(ICollider *collider, float bounce);
	// mesh only needed to calculate inertia + center of mass
	// TODO: support also animated meshes
	Rigidbody(ICollider *collider, IndexedMesh* mesh, float mass, float bounce, bool useGravity = true);
	Rigidbody(ICollider *collider, const std::vector<IndexedMesh*>& mesh, float mass, float bounce, bool useGravity = true, const float* meshScale = nullptr);
	~Rigidbody();

	virtual void FixedUpdate(float dt) override;
	virtual void HandleCollision(ICollider& child, ICollider& other) override;
	virtual void UpdateForces(float dt) override;
	virtual void EvaluateSimulation(float dt) override;

	void SetVelocity(XMFLOAT3 v) { mVelocity = v; }
	void SetAngularVelocity(XMFLOAT3 v) { mAngularVelocity = v; }
	XMFLOAT3 GetVelocity() { return mVelocity; }
	XMFLOAT3 GetAngularVelocity() { return mAngularVelocity; }

	XMFLOAT3 GetForce() { return XMFLOAT3(mAcceleration.x / mMassInverse, mAcceleration.y / mMassInverse, mAcceleration.z / mMassInverse); }
	XMFLOAT3 GetTorque() { return mTorque; }

	void UseGravity(bool useGravity);
	void AddImpulse(XMFLOAT3& force);
	void AddImpulse(XMFLOAT3& force, XMFLOAT3& point);
	void AddForce(XMFLOAT3& force);
	void AddForce(XMFLOAT3& force, XMFLOAT3& point);
	void AddTorque(XMFLOAT3& torque);
	void AddAcceleration(XMFLOAT3& acc);
	void ResetAcceleration();
	bool IsKinematic() { return mKinematic; }

	void Clear();

	void Print();

protected:
	virtual void Start() override 
	{
		XMFLOAT3 s = mPhysicsTransform->GetScale();
		mPhysicsTransform->Translate(XMFLOAT3(mOriginOffset.x * s.x, mOriginOffset.y * s.y, mOriginOffset.z * s.z));
		mCollider->Translate(XMFLOAT3(-mOriginOffset.x, -mOriginOffset.y, -mOriginOffset.z));
	};
	void HandleCollision(const Rigidbody &other, const Collision& collision);

	float mMassInverse;
	float mBounce;

	XMFLOAT3X3 mInertiaTensorInverse;
	XMFLOAT3X3 mActualInertiaTensorInverse;

	XMFLOAT3 mVelocity;
	XMFLOAT3 mAcceleration;

	// used for impulses, for angular momentum not needed as only used angularvelocity
	XMFLOAT3 mImpulse;

	// TODO: better XMVECTOR for temporary variables?!
	XMFLOAT3 mAngularVelocity;
	XMFLOAT3 mAngularMomentum;
	XMFLOAT3 mTorque;

	XMFLOAT3 mOriginOffset = XMFLOAT3(0, 0, 0);

	bool mGravity;
	bool mKinematic;
};

