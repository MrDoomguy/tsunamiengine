#ifndef _DIRECTIONALLIGHTCOMPONENT_H_
#define _DIRECTIONALLIGHTCOMPONENT_H_

#include "ILightComponent.h"

class DirectionalLightComponent : public ILightComponent
{
public:
	DirectionalLightComponent(const XMFLOAT4& ambient, const XMFLOAT4& diffuse, const XMFLOAT4& specular)
		: ILightComponent(ILightComponent::Type::DIRECTIONAL, ambient, diffuse, specular) { }
	DirectionalLightComponent()
		: ILightComponent(ILightComponent::Type::DIRECTIONAL, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f)) { }
	~DirectionalLightComponent() { }
};

#endif
