#include "SphereCollider.h"
#include "AABBCollider.h"
#include "MeshCollider.h"

// SphereCollider class

SphereCollider::SphereCollider(float radius, const XMFLOAT3& pos)
	: ICollider(pos, XMFLOAT3(0, 0, 0))
{
	mRadius = radius;
	mInheritRot = false;
}

SphereCollider::SphereCollider(ICollider * parent, float radius, XMFLOAT3 pos)
	: ICollider(parent, pos, XMFLOAT3(0, 0, 0))
{
	mRadius = radius;
	mInheritRot = false;
}

SphereCollider::~SphereCollider()
{
}

void SphereCollider::GetContactInfo(Collision * collision, ICollider * other)
{
	if (SphereCollider* sc = dynamic_cast<SphereCollider*>(other))
	{
		XMVECTOR p1, p2;
		p1 = GetWorldPosition();
		p2 = sc->GetWorldPosition();
		XMVECTOR diff;
		diff = p1 - p2;
		float r1 = GetRadius();
		float r2 = sc->GetRadius();

		// TODO: limit depth
		collision->depth = r1 + r2 - XMVectorGetX(XMVector3Length(diff));
		collision->dir = XMVector3Normalize(diff);
		collision->point = p1 - collision->dir * (r1 - collision->depth / 2);
	}
	else if (OBBCollider* ob = dynamic_cast<OBBCollider*>(other))
	{
		// already implented in the OBB class
		ob->GetContactInfo(collision, this);
		collision->dir *= -1;
	}
	else if (MeshCollider* mc = dynamic_cast<MeshCollider*>(other))
	{
		mc->GetContactInfo(collision, this);
		collision->dir *= -1;
	}
}

bool SphereCollider::CollisionDetection(ICollider* other)
{
	if (SphereCollider* sc = dynamic_cast<SphereCollider*>(other))
	{
		// TODO: get better accessibility (directly to position?!) friend!
		XMVECTOR p1, p2;
		p1 = GetWorldPosition();
		p2 = sc->GetWorldPosition();
		XMVECTOR diff;
		diff = p1 - p2;
		float r1 = GetRadius();
		float r2 = sc->GetRadius();
		if (XMVectorGetX(XMVector3LengthSq(diff)) < std::powf(r1 + r2, 2)) return true;
		return false;
	}
	else if (OBBCollider* ob = dynamic_cast<OBBCollider*>(other))
	{
		return ob->CollisionDetection(this);
	}
	else if (MeshCollider* mc = dynamic_cast<MeshCollider*>(other))
	{
		return mc->CollisionDetection(this);
	}
	Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"NYI SphereCollider::CollisionDetection");
	return false;
}
