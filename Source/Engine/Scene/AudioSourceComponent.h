#ifndef _AUDIOSOURCECOMPONENT_H_
#define _AUDIOSOURCECOMPONENT_H_

#include "IComponent.h"
#include "../General/TsunamiStd.h"

#include "../Audio/AudioClip.h"

class AudioSourceComponent : public IComponent
{
public:
	AudioSourceComponent();
	~AudioSourceComponent();

	void AddClip(const std::string& name, AudioClip* clip);
	AudioClip* GetClip(const std::string& name);

	void Play(const std::string& clip);
	void Stop();

private:
	std::unordered_map<std::string, AudioClip*> mClips;
	std::string mCurrClip;
};

#endif
