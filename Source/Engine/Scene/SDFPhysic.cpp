#include "SDFPhysic.h"
#include "Rigidbody.h"
#include "SphereCollider.h"

SDFPhysic::SDFPhysic(Device * pDevice, AnimatedVolumeTexture * sdfTexture, const SDFGrid::SDFProperties & props, ID3D11SamplerState * sampler, VolumeTextureRW* obstacleGrid, AnimatedVolumeTexture* velTexture)
{
	mCollider = new OBBCollider(XMFLOAT3((float)props.dim.x, (float)props.dim.y, (float)props.dim.z));
	shaderSdf = new SDFGrid(pDevice, props, sdfTexture, sampler, obstacleGrid, velTexture);
}

SDFPhysic::~SDFPhysic()
{
	delete shaderSdf;
}

void SDFPhysic::Start()
{
}

void SDFPhysic::FixedUpdate(float dt)
{
}

void SDFPhysic::HandleCollision(ICollider& child, ICollider& other)
{
	if (Rigidbody* rb = dynamic_cast<Rigidbody*>(other.GetPhysicComponent()))
	{
		if(MeshCollider* mc = dynamic_cast<MeshCollider*>(&other))
		{
			XMFLOAT3 force;
			XMFLOAT3 torque;

			SDFGrid::BuoyancyPerObjectData perObject;

			XMMATRIX trans = rb->GetPhysicsTransform()->GetWorld() * mPhysicsTransform->GetLocal();

			XMStoreFloat3(&perObject.com, XMVector3TransformCoord(XMVectorReplicate(0.f), trans));
			perObject.meshData.objToSDF = trans;		
			perObject.meshData.SDFToObj = mPhysicsTransform->GetWorld() * rb->GetPhysicsTransform()->GetLocal();
			perObject.meshData.count = mc->GetMeshCount();
			XMStoreFloat3(&perObject.vel, XMVector3Rotate(XMLoadFloat3(&rb->GetVelocity()), XMQuaternionInverse(XMLoadFloat4((&mPhysicsTransform->GetRotation())))));
			XMStoreFloat3(&perObject.rotVel, XMVector3Rotate(XMLoadFloat3(&rb->GetAngularVelocity()), XMQuaternionInverse(XMLoadFloat4((&mPhysicsTransform->GetRotation())))));
			perObject.scale = rb->GetPhysicsTransform()->GetWorldScale();
			perObject.sdfScale = mPhysicsTransform->GetWorldScale();

			if (shaderSdf->GetBuoyancyForce(&force, &torque, mc, perObject))
			{
				XMStoreFloat3(&force, XMVector3Rotate(XMLoadFloat3(&force), XMLoadFloat4(&mPhysicsTransform->GetRotation())));
				XMStoreFloat3(&torque, XMVector3Rotate(XMLoadFloat3(&torque), XMLoadFloat4(&mPhysicsTransform->GetRotation())));

				rb->AddForce(force);
				rb->AddTorque(torque);
			}

			return;
		}
		else if (rb->IsKinematic())
		{
			return;
		}
	}
	Logger::instance().Msg(LOG_ERROR, L"NYI SDFPhysic::HandleCollision");
}

void SDFPhysic::EvaluateSimulation(float dt)
{
}
