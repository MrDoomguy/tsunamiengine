#ifndef _ISCRIPT_H_
#define _ISCRIPT_H_

#include <string>
#include "TransformComponent.h"

//class GameObject;

class IScript
{
	friend class GameObject;
public:
	explicit IScript(std::string& scriptName) : mName{ scriptName } { }
	~IScript() { }

	virtual void Awake() { }
	virtual void Start() { }
	virtual void Update(float dt) { }
	virtual void FixedUpdate(float dt) { }

	// TODO: replace by friend
	//void SetGameObject(GameObject* go) { mGo = go; }
	std::string GetName() { return mName; }
	void Enable(bool enable) { mEnabled = enable; }
	bool IsEnabled() { return mEnabled; }

protected:
	GameObject* mGo;
	std::string mName;
	bool mEnabled = true;
};

#endif
