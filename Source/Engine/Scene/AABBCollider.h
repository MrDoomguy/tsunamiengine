#pragma once

#include "OBBCollider.h"

class AABBCollider :
	public OBBCollider
{
public:
	AABBCollider(XMFLOAT3 size, const XMFLOAT3& pos = XMFLOAT3(0, 0, 0));
	AABBCollider(ICollider* parent, XMFLOAT3 size, const XMFLOAT3& pos = XMFLOAT3(0, 0, 0));
	~AABBCollider();
	//const virtual Type GetType() override { return AABB; }
	virtual bool CollisionDetection(ICollider* other) override;
	virtual void GetContactInfo(Collision * collision, ICollider * other) override;

	virtual void GetBoxAxes(BoxAxes b) override
	{
		b[0] = XMLoadFloat3(&XMFLOAT3(1, 0, 0));
		b[1] = XMLoadFloat3(&XMFLOAT3(0, 1, 0));
		b[2] = XMLoadFloat3(&XMFLOAT3(0, 0, 1));
	}

protected:
	XMFLOAT3 GetSigns(const XMFLOAT3& p1, const XMFLOAT3& p2);
	XMFLOAT3 GetDepth(const XMFLOAT3& p1, const XMFLOAT3& p2, const XMFLOAT3& size, const XMFLOAT3& otherSize);
	float GetCollisionDepth(const XMFLOAT3& depth, const XMFLOAT3& dir);
	void HandleFaceInFaceCase(XMFLOAT3* sign, XMFLOAT3 * p1, XMFLOAT3* depth, const XMFLOAT3 & p2, const XMFLOAT3& size, const XMFLOAT3 & otherSize);
};

