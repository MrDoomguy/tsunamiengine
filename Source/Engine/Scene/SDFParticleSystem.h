#pragma once
#include "IParticleSystem.h"

#include "../Physics/Shader/SDFGrid.h"
#include "../Render/Data/ConstantBuffer.h"
#include "../Render/Data/StructuredBuffer.h"

class SDFParticleSystem : public IParticleSystem
{
public:
	struct InitProps
	{
		float turbulenceMin;
		float turbulenceMax;
		float curvatureMin;
		float curvatureMax;
		float obstVelMin;
		float obstVelMax;
		float energyMin;
		float energyMax;
		int maxWc;
		int maxTu;
		int maxOb;
		float _pad0;
	};

	SDFParticleSystem(Device* device, AnimatedVolumeTexture* SDFResource, ID3D11SamplerState* sampler, const SDFGrid::SDFProperties& sdfProps, const InitProps& initProps, float ttlFactor, float maxScale, PrimitiveMesh* mesh, VolumeTextureRW* obstacleGrid = nullptr, AnimatedVolumeTexture* velResource = nullptr);
	~SDFParticleSystem();

	void InstantiateParticles() override {}

	void SetInitProps(const InitProps& initProps);

	void Start() override;
	void FixedUpdate(float dt) override;

	void HandleCollision(ICollider &child, ICollider &other) override {}
	void UpdateForces(float dt) override {}

	void EvaluateSimulation(float dt) override;

private:
	struct ParticleProps
	{
		float dt;
		uint32_t size;
		float ttlFactor;
		float maxScale;
	};

	Device* mDevice;
	StructuredBuffer mVel;
	ConstantBuffer mPerFrame;
	ConstantBuffer mParticleProps;
	ConstantBuffer mWaterProps;
	ConstantBuffer mInitProps;

	int sample = 0;
	StructuredBuffer mHead;

	ParticleProps mParticlePropsData;
	SDFGrid::SDFProperties mWaterPropsData;

	ComputeShader* mParticleInit;
	ComputeShader* mParticleMovement;
	AnimatedVolumeTexture* mSDFResource;
	AnimatedVolumeTexture* mVelResource;
	VolumeTextureRW* mObstacleGrid;
	ID3D11SamplerState* mSampler;
};