#pragma once
#include "MeshCollider.h"
#include "SphereCollider.h"

typedef XMVECTOR BoxAxes[3];

// TODO: inerit from meshcollider?! create mesh manually (for buoyancy)
class OBBCollider :
	public ICollider
{
public:
	explicit OBBCollider(XMFLOAT3 size, const XMFLOAT3& pos = XMFLOAT3(0, 0, 0), const XMFLOAT3& rot = XMFLOAT3(0, 0, 0));
	explicit OBBCollider(ICollider* parent, XMFLOAT3 size, const XMFLOAT3& pos = XMFLOAT3(0, 0, 0), const XMFLOAT3& rot = XMFLOAT3(0, 0, 0));
	~OBBCollider();

	// Inherited via ICollider
	virtual bool CollisionDetection(ICollider * other) override;
	virtual void GetContactInfo(Collision * collision, ICollider * other) override;

	virtual void GetBoxAxes(BoxAxes b)
	{
		b[0] = XMLoadFloat3(&mTransform.GetRight());
		b[1] = XMLoadFloat3(&mTransform.GetUp());
		b[2] = XMLoadFloat3(&mTransform.GetForward());
	}

	// TODO: use scale of transform?!
	XMFLOAT3 GetWorldHalfedSize() 
	{ 
		XMFLOAT3 s = mTransform.GetWorldScale(); 
		return XMFLOAT3(mHalfedSize.x * s.x, mHalfedSize.y * s.y, mHalfedSize.z * s.z); 
	}

	XMFLOAT3 GetHalfedSize()
	{
		return mHalfedSize;
	}

protected:
	XMFLOAT3 mHalfedSize;
	
	void PointFaceContact(const BoxAxes b1, const BoxAxes b2, XMFLOAT3 halfSize, const XMVECTOR& toCenter, unsigned best, XMVECTOR& localPoint, XMVECTOR& dir);
	inline XMVECTOR ContactPoint(const XMVECTOR& p1, const XMVECTOR& d1, float oneSize, const XMVECTOR& p2, const XMVECTOR& d2, float twoSize, bool useOne)
	{
		float lengthSq1 = XMVectorGetX(XMVector3LengthSq(d1));
		float lengthSq2 = XMVectorGetX(XMVector3LengthSq(d2));
		float dpOneTwo = XMVectorGetX(XMVector3Dot(d2, d1));

		XMVECTOR toSt = p1 - p2;
		float dpStaOne = XMVectorGetX(XMVector3Dot(d1, toSt));
		float dpStaTwo = XMVectorGetX(XMVector3Dot(d2, toSt));

		float denom = lengthSq1 * lengthSq2 - dpOneTwo * dpOneTwo;

		if (abs(denom) < 1e-5f)
		{
			return useOne ? p1 : p2;
		}

		float mua = (dpOneTwo * dpStaTwo - lengthSq2 * dpStaOne) / denom;
		float mub = (lengthSq1 * dpStaTwo - dpOneTwo * dpStaOne) / denom;

		if (mua > oneSize ||
			mua < -oneSize ||
			mub > twoSize ||
			mub < -twoSize)
		{
			return useOne ? p1 : p2;
		}
		else
		{
			return (p1 + d1 * mua + p2 + d2 * mub) * 0.5f;
		}
	}
	inline float TransformToAxis(const XMFLOAT3& hs, const BoxAxes b, const XMVECTOR &axis)
	{
		return
			hs.x * abs(XMVectorGetX(XMVector3Dot(axis, b[0]))) +
			hs.y * abs(XMVectorGetY(XMVector3Dot(axis, b[1]))) +
			hs.z * abs(XMVectorGetZ(XMVector3Dot(axis, b[2])));
	}

	inline float PenetrationOnAxis(const XMFLOAT3& hs1, const XMFLOAT3& hs2, const BoxAxes b1, const BoxAxes b2, const XMVECTOR &axis, const XMVECTOR &toCenter)
	{
		float oneProject = TransformToAxis(hs1, b1, axis);
		float twoProject = TransformToAxis(hs2, b2, axis);

		float dist = abs(XMVectorGetX(XMVector3Dot(toCenter, axis)));

		return oneProject + twoProject - dist;
	}

	inline void TryAxis(const XMFLOAT3& hs1, const XMFLOAT3& hs2, const BoxAxes b1, const BoxAxes b2, const XMVECTOR &axis, const XMVECTOR &toCenter, unsigned index, float &smallestPenetration, unsigned &smallestCase)
	{
		if (XMVectorGetX(XMVector3LengthSq(axis)) < 1e-5) return;
		float penetration = PenetrationOnAxis(hs1, hs2, b1, b2, XMVector3Normalize(axis), toCenter);

		if (penetration < smallestPenetration)
		{
			smallestPenetration = penetration;
			smallestCase = index;
		}
	}
};

