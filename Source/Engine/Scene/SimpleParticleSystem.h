#pragma once
#include "IParticleSystem.h"

class SimpleParticleSystem : public IParticleSystem
{
public:
	SimpleParticleSystem(PrimitiveMesh* mesh);
	~SimpleParticleSystem()
	{
		delete mRenderParticles;
		delete mParticles;
		delete mVelocities;
	}

	void Start() override;
	void FixedUpdate(float dt) override;

	void HandleCollision(ICollider &child, ICollider &other) override {}
	void UpdateForces(float dt) override;

	void EvaluateSimulation(float dt) override;

	void InstantiateParticles() override;

private:
	geotypes::BaseParticleVertex* mRenderParticles;
	geotypes::BaseParticleVertex* mParticles;
	XMFLOAT3* mVelocities;
	int mHead;
};