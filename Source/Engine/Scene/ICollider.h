#pragma once
#include "../General/Logger.h"
#include <DirectXMath.h>
#include "../Physics/PhysicsEngine.h"
#include "TransformComponent.h"
#include <vector>

using namespace DirectX;

// TODO: make as component?! better solution for physics-collider connection
struct Collision
{
	float depth;
	DirectX::XMVECTOR point;
	DirectX::XMVECTOR dir;
};

class ICollider
{
public:
	explicit ICollider(XMFLOAT3 offsetPos, XMFLOAT3(offsetRot)) 
	{
		mHasChildren = false;
		// initialize children with this for easier collision handling
		mChildren.push_back(this);

		mTransform = TransformComponent();

		mTransform.SetPosition(offsetPos);
		mTransform.SetRotation(offsetRot);

		mInheritPos = true;
		mInheritRot = true;
	}
	explicit ICollider(ICollider* parent, XMFLOAT3 offsetPos, XMFLOAT3(offsetRot))
	{
		mHasChildren = false;
		// initialize children with this for easier collision handling
		mChildren.push_back(this);

		mTransform = TransformComponent();

		mTransform.SetPosition(offsetPos);
		mTransform.SetRotation(offsetRot);

		mInheritPos = true;
		mInheritRot = true;

		SetParent(parent);
	}

	~ICollider() 
	{
		if (mHasChildren)
		{
			for (auto child : mChildren)
			{
				delete child;
			}
		}
	}

	virtual void Start(IPhysicComponent* physicComponent, TransformComponent* physicTransform)
	{
		mPhysicComponent = physicComponent;
		mPhysicTransform = physicTransform;
		if (!mTransform.HasParent()) mTransform.SetParent(physicTransform, mInheritPos, true, mInheritRot);
		if (mHasChildren)
		{
			for (auto child : mChildren)
			{
				child->Start(physicComponent, physicTransform);
			}
		}
	}

	virtual bool CollisionDetection(ICollider* other) = 0;
	virtual void GetContactInfo(Collision * collision, ICollider* other) = 0;

	void SetParent(ICollider* coll)
	{
		if (mParent != nullptr)
		{
			for (int i = 0; i < mParent->mChildren.size(); i++)
			{
				if (mParent->mChildren[i] == this)
				{
					mParent->mChildren.erase(mParent->mChildren.begin() + i);
				}
			}
			if (mParent->mChildren.size() == 0) mParent->mChildren.push_back(mParent);
			if(mPhysicTransform != nullptr) mTransform.SetParent(mPhysicTransform, mInheritPos, true, mInheritRot);
		}
		if (coll != nullptr)
		{
			mTransform.SetParent(&coll->mTransform, mInheritPos, false, mInheritRot);

			if (!coll->mHasChildren)
			{
				coll->mChildren.pop_back();
				coll->mHasChildren = true;
			}
			
			coll->mChildren.push_back(this);
			if (coll->mPhysicTransform != nullptr) Start(coll->mPhysicComponent, coll->mPhysicTransform);
		}
		mParent = coll;
	}

	void AddChild(ICollider* coll)
	{
		coll->SetParent(this);
	}

	void Translate(XMFLOAT3 p)
	{
		mTransform.Translate(p);
	}

	std::vector<ICollider*> &GetChildren()
	{
		return mChildren;
	}

	IPhysicComponent* GetPhysicComponent()
	{
		return mPhysicComponent;
	}

	bool HasChildren()
	{
		return mHasChildren;
	}

	XMVECTOR GetWorldPosition() 
	{ 
		return XMLoadFloat3(&mTransform.GetWorldPosition());
	}

	XMVECTOR GetWorldScale()
	{
		return XMLoadFloat3(&mTransform.GetWorldScale());
	}

	virtual XMVECTOR TransformVertexWorld(const XMVECTOR& pos)
	{
		return XMVector3Transform(pos, mTransform.GetWorld());
	}

	virtual XMVECTOR TransformVertexLocal(const XMVECTOR& pos)
	{
		return XMVector3Transform(pos, mTransform.GetLocal());
	}


protected:
	bool mInheritPos;
	bool mInheritRot;
	bool mHasChildren;
	TransformComponent mTransform;
	IPhysicComponent* mPhysicComponent = nullptr;
	TransformComponent* mPhysicTransform = nullptr;
	std::vector<ICollider*> mChildren;
	ICollider* mParent = nullptr;
};