#pragma once

#include "IComponent.h"
#include "../Physics/PhysicsEngine.h"
#include "ICollider.h"
#include "GameObject.h"
#include <mutex>

class IPhysicComponent : public IComponent
{
public:
	explicit IPhysicComponent()
		: IComponent(IComponent::Type::PHYSIC)
	{
	}
	explicit IPhysicComponent(ICollider *collider) 
		: IComponent(IComponent::Type::PHYSIC) 
	{ 
		mCollider = collider;
	}
	~IPhysicComponent() 
	{ 
		PhysicsEngine::instance().RemovePhysicObject(this); 
		if(mCollider != nullptr) delete mCollider;
		delete mPhysicsTransform;
		delete mRendererTransform;
	}

	void CallStart()
	{
		mTransform = mGo->GetTransform();

		mRendererTransform = new TransformComponent();
		mRendererTransform->SetPosition(mTransform->GetPosition());
		mRendererTransform->SetRotation(mTransform->GetRotation());
		mRendererTransform->SetScale(mTransform->GetScale());

		mPhysicsTransform = new TransformComponent();
		mPhysicsTransform->SetPosition(mTransform->GetPosition());
		mPhysicsTransform->SetRotation(mTransform->GetRotation());
		mPhysicsTransform->SetScale(mTransform->GetScale());

		if(mCollider != nullptr) mCollider->Start(this, mPhysicsTransform);
		Start();
	}
	virtual void FixedUpdate(float dt) = 0;

	// takes the collider of his own hierachy and the other collider
	virtual void HandleCollision(ICollider &child, ICollider &other) = 0;
	virtual void UpdateForces(float dt) = 0;
	// TODO: use other time then dt?!
	virtual void EvaluateSimulation(float dt) = 0;

	void SwapTransforms()
	{
		m.lock();
		mRendererTransform->SetPosition(mPhysicsTransform->GetPosition());
		mRendererTransform->SetRotation(mPhysicsTransform->GetRotation());
		mRendererTransform->SetScale(mPhysicsTransform->GetScale());
		m.unlock();
	}

	void Enable(bool enable) { mEnabled = enable; }
	bool IsEnabled() { return mEnabled; }

	TransformComponent *GetPhysicsTransform() { return mPhysicsTransform; }

	// HACK: find better accessibility!
	ICollider* GetCollider() { return mCollider; }	

protected:
	virtual void Start() = 0;

	bool mEnabled = true;
	ICollider *mCollider = nullptr;

	// physics transform which will be asynchronous to the go transform
	// evaluate simulation will update the rendering update according to the renderingtransform, which will be updated only if finished a physics frame
	TransformComponent *mPhysicsTransform;
	TransformComponent *mRendererTransform;
	TransformComponent *mTransform;

	std::mutex m;
};
