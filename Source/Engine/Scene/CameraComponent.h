#ifndef _CAMERACOMPONENT_H_
#define _CAMERACOMPONENT_H_

#include "IComponent.h"
#include <DirectXMath.h>

using namespace DirectX;

class CameraComponent : public IComponent
{
public:
	enum class LensMode
	{
		PERSPECTIVE,
		ORTHO
	};

	explicit CameraComponent(const uint32_t width, const uint32_t height, const float fovy, const float aspect, const float zn, const float zf);
	//CameraComponent();
	~CameraComponent();

	void SetFrustum(const uint32_t width, const uint32_t height, const float fovy, const float aspect, const float zn, const float zf);

	void SetPerspective();
	void SetOrtho();
	CameraComponent::LensMode GetLensMode() const;

	float GetNearZ() const;
	float GetFarZ() const;
	float GetAspect() const;
	float GetFovY() const;
	float GetFovX() const;

	float GetViewZ() const;

	uint32_t GetWidth() const;
	uint32_t GetHeight() const;

	float GetNearWindowWidth() const;
	float GetNearWindowHeight() const;
	float GetFarWindowWidth() const;
	float GetFarWindowHeight() const;

	XMMATRIX GetProj() const;

private:
	float mNearZ;
	float mViewZ;
	float mFarZ;
	float mAspect;
	float mFovY;
	float mNearWindowHeight;
	float mFarWindowHeight;
	uint32_t mWidth;
	uint32_t mHeight;

	CameraComponent::LensMode mMode = CameraComponent::LensMode::PERSPECTIVE;

	XMFLOAT4X4 mProj;
};

#endif
