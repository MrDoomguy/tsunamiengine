#ifndef _CONFIGMANAGER_H_
#define _CONFIGMANAGER_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <unordered_map>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>

#include <iostream>

#include <cstdint>

typedef std::unordered_map<std::string, std::string> ALK;

class ConfigManager
{
public:
	static ConfigManager& instance()
	{
		static ConfigManager _instance;
		return _instance;
	}
	~ConfigManager();

	HRESULT Init(const std::wstring& filename);
	HRESULT WriteFile();

	template <typename T>
	T GetValueByKey(const std::string& key)
	{
		T retVal;
		std::istringstream iss(this->GetVal(key));
		iss >> retVal;

		return retVal;
	}

	template <typename T>
	void SetValueByKey(const std::string& key, const T& value)
	{
		if (m_configData.find(key) != m_configData.end())
		{
			std::istringstream iss(value);
			m_configData[key] = iss.str();
		}
	}

private:
	ConfigManager();
	ConfigManager(const ConfigManager&);
	ConfigManager &operator = (const ConfigManager &);

	bool ReadALK(const wchar_t* filename);
	bool WriteALK(const wchar_t* filename);

	std::string GetVal(const std::string& key)
	{
		return m_configData[key];
	}

	//configs available
	//audio, renderer, resolution, antialias, shader model, assets path
	ALK m_configData;
	std::wstring mPath;
};

#endif