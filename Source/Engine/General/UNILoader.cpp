#include "UNILoader.h"

UNILoader::UNILoader()
{

}

UNILoader::~UNILoader()
{
	if (fileHandle) gzclose((gzFile)fileHandle);
}

UNILoader::UniHeader UNILoader::ReadHeader(const std::string &path)
{
	if (fileHandle)
	{
		gzclose((gzFile)fileHandle);
		fileHandle = nullptr;
	}
	char ID[5] = { 0,0,0,0,0 };
	gzFile gzf = gzopen(path.c_str(), "rb");

	if (!gzf)
	{
		printf("file not found");
		exit(1);
	}

	gzread(gzf, ID, 4);

	PartUniHeader header;

	if (!strcmp(ID, "M4T2"))
	{
		if (gzread(gzf, &header, sizeof(header)) != sizeof(header))
		{
			printf("no header present");
			exit(1);
		}
	}
	else
	{
		printf("unkown header!");
		exit(1);
	}

	memcpy(&tmpHead, &header, sizeof(PartUniHeader));

	gzread(gzf, &tmpHead.dimt, sizeof(tmpHead.dimt));

	fileHandle = gzf;

	return tmpHead;
}

void UNILoader::ReadGrid(float * data)
{
	gzFile gzf = nullptr;

	if (!fileHandle)
	{
		printf("you have to give a path");
	}
	gzf = (gzFile)fileHandle;

	for (int t = 0; t < tmpHead.dimt; t++)
	{
		void* ptr = &(data[tmpHead.dimx * tmpHead.dimy * tmpHead.dimz * t * tmpHead.bytesPerElement / sizeof(float)]);
		gzread(gzf, ptr, tmpHead.bytesPerElement * tmpHead.dimx * tmpHead.dimy * tmpHead.dimz);
	}

	fileHandle = nullptr;
	gzclose(gzf);
}

void UNILoader::ReadGrid(const std::string & path, float * data)
{
	ReadHeader(path);
	ReadGrid(data);
}
