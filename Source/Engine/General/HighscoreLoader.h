#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>

#include <iostream>

#include <cstdint>

typedef std::pair<std::string, int> HighscoreEntry;

class HighscoreLoader
{
public:
	HighscoreLoader();
	~HighscoreLoader();

	void Init(const std::wstring& filename, int maxEntries);
	void WriteFile();

	HighscoreEntry GetHighscore()
	{
		return mHighscoreList[0];
	}

	HighscoreEntry GetMinScore()
	{
		return mHighscoreList[mMaxEntries - 1];
	}

	const std::vector<HighscoreEntry>& GetList() { return mHighscoreList; }

	void AddEntry(std::string name, int score)
	{
		AddEntry(HighscoreEntry(name, score));
	}

	void AddEntry(const HighscoreEntry& entry)
	{
		mHighscoreList.push_back(entry);
		std::sort(mHighscoreList.begin(), mHighscoreList.end(), sortByScore);
		while (mHighscoreList.size() > mMaxEntries) mHighscoreList.pop_back();
	}

private:
	static bool sortByScore(const HighscoreEntry& v0, const HighscoreEntry& v1) { return v0.second > v1.second; }

	bool ReadList(const wchar_t* filename);
	bool WriteList(const wchar_t* filename);

	std::vector<HighscoreEntry> mHighscoreList;
	std::wstring mPath;
	
	int mMaxEntries;
};
