#ifndef _GUIDGENERATOR_H_
#define _GUIDGENERATOR_H_

#include <cstdint>

typedef uint64_t guid;
static const guid MAX_GUID = UINT64_MAX;
static const guid INVALID_GUID = 0;

class GUIDGen
{
public:
	static guid GetNext()
	{
		if (mCurrent < MAX_GUID)
			return mCurrent++;
		return INVALID_GUID;
	}
private:
	static guid mCurrent;
};

#endif