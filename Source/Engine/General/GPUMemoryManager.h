#ifndef _GPUMEMORYMANAGER_H_
#define _GPUMEMORYMANAGER_H_

#include "ImageTools\DDSTextureLoader.h"

#include "../Render/Data/ColorMap.h"
#include "../Render/Data/RenderTarget.h"
#include "../Render/Data/DepthStencilBuffer.h"
#include "../Render/Data/VolumeTexture.h"
#include "../Render/Data/VolumeTextureRW.h"
#include "../Render/Data/AnimatedVolumeTexture.h"
#include "../Render/Data/Texture2DRW.h"
#include "../Render/Data/CubeMap.h"

#include "../Render/Data/IndexedMesh.h"
#include "../Render/Data/SkeletalMesh.h"
#include "../Render/Data/MeshHelper.h"
#include "../Animation/AnimationTypes.h"
#include "../Render/Data/PrimitiveMesh.h"

#include "../Render/Data/IShader.h"
#include "../Render/Data/PixelShader.h"
#include "../Render/Data/VertexShader.h"
#include "../Render/Data/ComputeShader.h"
#include "../Render/Data/GeometryShader.h"

#include "../Audio/AudioEngine.h"
#include "../Audio/AudioClip.h"

#include "../General/UNILoader.h"

#include <string>
#include <locale>
#include <codecvt>
#include <fstream>
#include <cstdint>

#include "TsunamiStd.h"

class Device;

class GPUMemoryManager
{
public:
	enum class SupportedFileExtensions : std::uint8_t
	{
		OGG_VORBIS = 0x1,
		EXTENSIONS_END
	};

	static GPUMemoryManager& instance()
	{
		static GPUMemoryManager _instance;
		return _instance;
	}

	void Init(Device* device)
	{
		HRESULT hr;

		mDevice = device;

		mAudioEngine = new AudioEngine();

		if (FAILED(hr = MakeDefaultColorMap()))
		{
			std::wstring err = L"Failed to create default color map. May result in unexpected behaviour." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}

		if (FAILED(hr = MakeDefaultCubeMap()))
		{
			std::wstring err = L"Failed to create default cube map. May result in unexpected behaviour." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}

		if (FAILED(hr = MakeDefaultNormalMap()))
		{
			std::wstring err = L"Failed to create default normal map. May result in unexpected behaviour." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}

		if (FAILED(hr = MakeDefaultTexture2DRW()))
		{
			std::wstring err = L"Failed to create default Texture2DRW. May result in unexpected behaviour." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}

		if (FAILED(hr = MakeDefaultSampler()))
		{
			std::wstring err = L"Failed to create default texture sampler. May result in unexpected behaviour." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}

		if (FAILED(hr = MakeDefaultNNSampler()))
		{
			std::wstring err = L"Failed to create default nn sampler. May result in unexpected behaviour." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}		

		if (FAILED(hr = MakeDefaultFilterSampler()))
		{
			std::wstring err = L"Failed to create default filter sampler. May result in unexpected behaviour." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}
	}

	~GPUMemoryManager() { };

	void Release()
	{
		SAFE_DELETE(mDefaultColorMap);
		SAFE_DELETE(mDefaultNormalMap);
		SAFE_RELEASE(mDefaultSampler);

		// TODO: safe release of maps?
	}

	// Textures
	HRESULT ColorMapFromFile(const std::string& filename, ColorMap* pColorMap);
	ColorMap* GetColorMap(const std::string& filename);
	HRESULT ColorMapFromSRV(ID3D11ShaderResourceView* pSRV, ColorMap* pColorMap);

	HRESULT VolumeTextureFromMemory(VolumeTexture* volumeTex, const XMINT3& size, void* pData = nullptr);
	HRESULT RGBVolumeTextureFromMemory(VolumeTexture* volumeTex, const XMINT3& size, void* pData = nullptr);
	HRESULT RGBAVolumeTextureFromMemory(VolumeTexture* volumeTex, const XMINT3& size, void* pData = nullptr);
	HRESULT VolumeTextureFromMemory(VolumeTexture* volumeTex, const XMINT3& size, DXGI_FORMAT format, int elementCount, void* pData = nullptr);
	HRESULT RWVolumeTextureFromMemory(VolumeTextureRW* volumeTex, const XMINT3& size, void* pData = nullptr);
	HRESULT RWRGBAVolumeTextureFromMemory(VolumeTextureRW* volumeTex, const XMINT3& size, void* pData = nullptr);
	HRESULT VolumeTextureFromFile(const std::string& filename, VolumeTexture* pTexture);
	HRESULT AnimatedVolumeTextureFromFile(const std::string& filename, AnimatedVolumeTexture* pTexture, XMUINT4* dim = nullptr);

	HRESULT Framebuffer(const uint32_t width, const uint32_t height, const bool readable, const bool useMSAA, RenderTarget* pRenderTarget);
	HRESULT SingleFramebuffer(const uint32_t width, const uint32_t height, const bool readable, const bool useMSAA, RenderTarget* pRenderTarget);
	HRESULT Framebuffer(ID3D11Texture2D* texture, const bool readable, RenderTarget* pRenderTarget);
	HRESULT DepthStencil(const uint32_t width, const uint32_t height, const bool readable, const bool useMSAA, DepthStencilBuffer* pDepthStencil);
	
	HRESULT TextureSampler(const D3D11_FILTER filter, const D3D11_TEXTURE_ADDRESS_MODE mode, ID3D11SamplerState** ppSampler);

	HRESULT MakeTexture2DRW(const uint32_t width, const uint32_t height, const bool readable, const DXGI_FORMAT format, Texture2DRW* pTexture);
	Texture2DRW* GetTexture2DRW(const std::string& name, const uint32_t width, const uint32_t height, const bool readable, const DXGI_FORMAT format);
	
	CubeMap* GetCubeMap(const std::string& name, const uint32_t size);
	CubeMap* GetCubeMap(const std::string& filename);

	// Geometry
	HRESULT MakeSphere(IndexedMesh* pMesh, const bool isStaticMesh, const MeshHelper::SPHERE_DESC* desc = nullptr);
	HRESULT MakeBox(IndexedMesh* pMesh, const bool isStaticMesh, const MeshHelper::BOX_DESC* desc = nullptr);
	HRESULT MakeFullscreenQuad(IndexedMesh* pMesh, const bool isStatic);
	HRESULT MeshFromFile(const char* filename, const bool isStatic, IndexedMesh* pMesh);
	HRESULT PointMeshFromMemory(PrimitiveMesh* pMesh, const int size);
	SkeletalMesh* SkeletalMeshFromFile(const char* filename, const bool isStatic);
	animtypes::AnimationData* AnimationFromFile(const char* filename);

	// Memory Functions
	void CopyResource(ID3D11Buffer* pDst, ID3D11Buffer* pSrc);
	void BufferData(ID3D11Buffer* pBuffer, void* pData, const size_t dataSizeBytes, const UINT subRes, const D3D11_MAP mapType, const UINT mapFlag);
	void UpdateBuffer(ID3D11Buffer* pBuffer, void* pData, const UINT rowPitch = 0, const UINT depthPitch = 0, const D3D11_BOX* pBox = nullptr, const UINT subRes = 0);

	// Shaders
	HRESULT MakeShader(const IShader::Desc& desc, IShader* pShader);
	GeometryShader* GetGeometryShader(const std::string& filename, const std::vector<D3D11_SO_DECLARATION_ENTRY> layout);
	GeometryShader* GetGeometryShader(const std::string& filename);
	ComputeShader* GetComputeShader(const std::string& filename);
	VertexShader* GetVertexShader(const std::string& filename, const std::vector<D3D11_INPUT_ELEMENT_DESC> layout);
	PixelShader* GetPixelShader(const std::string& filename);

	//
	// Audio
	//
	AudioEngine* GetAudioEngine();
	AudioClip* LoadAudioClipFromFile(const char* filename, const bool loop=true);

	//
	// Manager Functions
	//
	ColorMap* GetDefaultColorMap() { return mDefaultColorMap; }
	ColorMap* GetDefaultNormalMap() { return mDefaultNormalMap; }
	CubeMap* GetDefaultCubeMap() { return mCubeMaps["Default"]; }
	Texture2DRW* GetDefaultTexture2DRW() { return mTexture2DRW["Default"]; }
	ID3D11SamplerState* GetDefaultTextureSampler() { return mDefaultSampler; }
	ID3D11SamplerState* GetDefaultNNSampler(){ return mSamplers["DefaultNN"]; }
	ID3D11SamplerState* GetDefaultFilterSampler() { return mSamplers["DefaultFilter"]; }

private:
	// Mesh interop utils
	HRESULT FillMeshBuffer(const bool isStaticMesh, const geotypes::MeshData& meshData, IndexedMesh* pMesh);
	HRESULT FillMeshBuffer(const bool isStaticMesh, const std::vector<geotypes::MeshData>& meshData, IndexedMesh* pMesh);
	HRESULT FillMeshBuffer(const bool isStaticMesh, const geotypes::SkinnedMeshData& meshData, SkeletalMesh* pMesh);

	// Assimp utils
	geotypes::MeshData InitAssimpMesh(const aiMesh* pAssimpMesh);
	geotypes::SkinnedMeshData InitAssimpSkeletalMesh(const aiMesh* pAssimpMesh);
	void InitAssimpAnimationData(const aiScene* pAssimpScene, const uint32_t meshIdx, animtypes::AnimationData* pData);
	JointBindPose* TraverseAssimpNode(const aiNode* pNode, JointBindPose* pParent);

	HRESULT ReadBinaryFile(const char* filename, std::string* byteStream);

	HRESULT MakeDefaultColorMap();
	HRESULT MakeDefaultCubeMap();
	HRESULT MakeDefaultNormalMap();
	HRESULT MakeDefaultTexture2DRW();
	HRESULT MakeDefaultSampler();
	HRESULT MakeDefaultNNSampler();
	HRESULT MakeDefaultFilterSampler();

	// packs a 4d array with dimension dim into an 3d array with maximal size extent, returns the actual extent
	XMINT3 GPUMemoryManager::PackArray(float** arrOut, float* arrIn, const XMINT4& dim, const XMINT3& extent);

	SupportedFileExtensions IsFileExtensionSupported(const std::string& filename);

	void LoadOGG(const char* filename, AudioClip* clip);

	GPUMemoryManager() {};
	GPUMemoryManager(const GPUMemoryManager&);
	GPUMemoryManager &operator = (const GPUMemoryManager &);

	std::unordered_map<std::string, ColorMap*> mColorMaps;
	std::unordered_map<std::string, Texture2DRW*> mTexture2DRW;
	std::unordered_map<std::string, ITexture3D*> mVolumes;
	std::unordered_map<std::string, CubeMap*> mCubeMaps;
	
	std::unordered_map<std::string, ID3D11SamplerState*> mSamplers;
	
	std::unordered_map<std::string, IndexedMesh*> mMeshes;
	std::unordered_map<std::string, SkeletalMesh*> mSkeletalMeshes;
	std::unordered_map<std::string, animtypes::AnimationData*> mAnimationData;

	std::unordered_map<std::string, GeometryShader*> mGeometryShaders;
	std::unordered_map<std::string, VertexShader*> mVertexShaders;
	std::unordered_map<std::string, PixelShader*> mPixelShaders;
	std::unordered_map<std::string, ComputeShader*> mComputeShaders;

	std::unordered_map<std::string, AudioClip*> mAudioClips;

	enum class ResType : std::uint8_t
	{
		TEX2D,
		TEX3D,
		MESH,
		SHADER
	};

	std::unordered_map<SupportedFileExtensions, std::string> mFileExtensions = 
	{
		{SupportedFileExtensions::OGG_VORBIS, "ogg"}
	};

	Device* mDevice;
	AudioEngine* mAudioEngine;

	ColorMap* mDefaultColorMap;
	ColorMap* mDefaultNormalMap;
	ID3D11SamplerState* mDefaultSampler;
};

#endif
