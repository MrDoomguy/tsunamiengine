#include "Application.h"

Application::Application()
{
}

Application::~Application()
{
}

void Application::Init(const unsigned int width, const unsigned int height, const bool fullscreen, const std::wstring& appName)
{
	mClientWidth = width;
	mClientHeight = height;

	mApplicationName = appName;
}

void Application::Destroy()
{
	for (auto s : mScenes)
	{
		delete s.second;
	}
}

void Application::SetRenderer(Renderer* pRenderer)
{
	mRenderer = pRenderer;
}

void Application::SetGlobalTimer(Timer* pTimer)
{
	mTimer = pTimer;
}

void Application::SetScreenSize(const unsigned int width, const unsigned int height)
{
	mClientWidth = width;
	mClientHeight = height;
}

float Application::GetTotalTime()
{
	return mTimer->TotalTime();
}

float Application::GetDeltatime()
{
	return static_cast<float>(mTimer->DeltaTime());
}

unsigned int Application::GetScreenWidth()
{
	return mClientWidth;
}

unsigned int Application::GetScreenHeight()
{
	return mClientHeight;
}

Scene* Application::GetCurrentScene()
{
	return mRenderer->GetCurrentScene();
}

Device* Application::GetRenderDevice()
{
	return mRenderer->GetDevice();
}

IDWriteFactory* Application::GetFontRenderDevice()
{
	return mRenderer->GetFontEngine()->GetWriteFactory();
}

void Application::LoadScene(const std::string& sceneName)
{
}
