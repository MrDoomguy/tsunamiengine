#ifndef _APPLICATION_H_
#define _APPLICATION_H_

#include "Timer.h"
#include "TsunamiStd.h"

#include "../Render/Renderer.h"

#include "../Scene/Scene.h"

//
// Script interface to the engine resources
//
class Application
{
public:
	static Application& instance()
	{
		static Application _instance;
		return _instance;
	}

	~Application();

	void Init(const unsigned int width, const unsigned int height, const bool fullscreen, const std::wstring& appName);
	void Destroy();

	void SetRenderer(Renderer* pRenderer);
	void SetGlobalTimer(Timer* pTimer);
	void SetScreenSize(const unsigned int width, const unsigned int height);
	
	float GetTotalTime();
	float GetDeltatime();

	unsigned int GetScreenWidth();
	unsigned int GetScreenHeight();

	Scene* GetCurrentScene();
	Device* GetRenderDevice();
	IDWriteFactory* GetFontRenderDevice();

	void LoadScene(const std::string& sceneName);

private:
	Application();
	Application(const Application&);
	Application &operator = (const Application &);

	std::unordered_map<std::string, Scene*> mScenes;
	
	Renderer* mRenderer;
	Timer* mTimer;
	
	unsigned int mClientWidth;
	unsigned int mClientHeight;
	bool mFullscreen;

	std::wstring mApplicationName;
};

#endif