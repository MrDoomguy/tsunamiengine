#ifndef _MATHHELPER_H_
#define _MATHHELPER_H_

#include "../General/TsunamiStd.h"

using namespace DirectX;

namespace MathHelper
{
	static float RandF()
	{
		return (static_cast<float>(rand()) / static_cast<float>(RAND_MAX));
	}

	static float RandF(float a, float b)
	{
		return (a + RandF() * (b - a));
	}

	template<typename T>
	static T Min(const T& a, const T& b)
	{
		return (a < b ? a : b);
	}

	template<typename T>
	static T Max(const T& a, const T& b)
	{
		return (a < b ? a : b);
	}

	template<typename T>
	static T Lerp(const T& a, const T& b, float t)
	{
		return (a + (b - a) * t);
	}

	template<typename T>
	static T Clamp(const T& x, const T& low, const T& high)
	{
		return (x < low ? low : (x > high ? high : x));
	}

	template<typename T>
	static T Saturate(const T& x)
	{
		return Clamp(x, 0.0f, 1.0f);
	}

	template<typename T>
	static int32_t Floor(T x)
	{
		return (int32_t)(x - (x < 0 && (int32_t)x != x));
	}

	// Scratchpixel implementation
	static float Smoothstep(const float& t)
	{
		return t * t * (3 - 2 * t);
	}

	// Improved Perlin Noise implementation
	static float Smoothstep1(const float& t)
	{
		return t * t * t * (t * (t * 6 - 15) + 10);
	}

	static XMVECTOR Smoothstep1(const XMVECTOR& t)
	{
		return t * t * t * (t * (t * 6 - XMVectorReplicate(15)) + XMVectorReplicate(10));
	}

	static float Dot(const DirectX::XMFLOAT3& a, const DirectX::XMFLOAT3& b)
	{
		return (a.x * b.x + a.y * b.y + a.z * b.z);
	}

	static float Dot(const DirectX::XMFLOAT4& a, const DirectX::XMFLOAT4& b)
	{
		return (a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w);
	}

	// Returns the polar angle of the point (x,y) in [0, 2PI]
	static float AngleFromXY(float x, float y)
	{
		float theta = 0.0f;

		// Quadrant I or IV
		if (x >= 0.0f)
		{
			//If x = 0, then atanf(y/x) = +pi/2 if y > 0
			//               atanf(y/x) = -pi/2 if y < 0
			theta = atanf(y / x); //in [-pi/2, +pi/2]

			if (theta < 0.0f)
				theta += 2.0f * DirectX::XM_PI; //in [0, 2*pi]
		}
		else	// Quadrant II or III
		{
			theta = atanf(y / x) + DirectX::XM_PI;	//in [0, 2*pi]
		}

		return theta;
	}

	inline XMMATRIX ToXMMatrix(const aiMatrix4x4& m)
	{
		return XMMatrixTranspose(XMMatrixSet(m.a1, m.a2, m.a3, m.a4,
			m.b1, m.b2, m.b3, m.b4,
			m.c1, m.c2, m.c3, m.c4,
			m.d1, m.d2, m.d3, m.d4));
	}

	inline XMFLOAT4X4 ToXMFloat4x4(const aiMatrix4x4& mat)
	{
		aiMatrix4x4 m = mat;
		m.Transpose();
		return XMFLOAT4X4(m.a1, m.a2, m.a3, m.a4,
			m.b1, m.b2, m.b3, m.b4,
			m.c1, m.c2, m.c3, m.c4,
			m.d1, m.d2, m.d3, m.d4);
	}

	inline XMFLOAT3 ToXMFloat3(const aiVector3D& v)
	{
		return XMFLOAT3(v.x, v.y, v.z);
	}

	inline XMFLOAT4 ToXMFloat4(const aiQuaternion& q)
	{
		return XMFLOAT4(q.x, q.y, q.z, q.w);
	}
}

#endif