#ifndef _LOGGER_H_
#define _LOGGER_H_

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <string>
#include <iostream>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>

enum LOG_COLOR
{
	RED = FOREGROUND_RED,
	GREEN = FOREGROUND_GREEN,
	BLUE = FOREGROUND_BLUE,
	YELLOW = FOREGROUND_RED | FOREGROUND_GREEN,
	PURPLE = FOREGROUND_RED | FOREGROUND_BLUE,
	MAGENTA = FOREGROUND_GREEN | FOREGROUND_BLUE
};

enum LOG_LAYER
{
	LOG_ERROR = LOG_COLOR::RED,
	LOG_WARNING = LOG_COLOR::YELLOW,
	LOG_RENDER = LOG_COLOR::MAGENTA,
	LOG_GENERAL = LOG_COLOR::GREEN,
	LOG_EXCEPTION = LOG_COLOR::PURPLE
};

class Logger
{
public:
	static Logger& instance()
	{
		static Logger _instance;
		return _instance;
	}
	Logger(const Logger&) = delete;
	Logger &operator=(const Logger&) = delete;

	~Logger();

	void Init(const std::wstring& consoleTitle);
	void Destroy();

	void Msg(const LOG_LAYER layer, const std::wstring& msg);
	void Msg(const LOG_LAYER layer, const std::string& msg);
	void Exception(const std::error_code& code, const char* errorString, const std::wstring& msg);

private:
	Logger();

	void WriteHeader(const std::string& head, const LOG_LAYER layer);

	HANDLE mhStdOut;
	DWORD mfdwDefaultMode;
	CONSOLE_SCREEN_BUFFER_INFO mDefaultFramebuffer;

	bool mConsoleOpen = false;
};

#endif
