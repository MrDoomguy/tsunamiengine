#include "ConfigManager.h"

#include "Logger.h"

ConfigManager::ConfigManager()
{
}

ConfigManager::~ConfigManager()
{
}

HRESULT ConfigManager::Init(const std::wstring& filename)
{
	m_configData.clear();

	mPath = filename;

	if (!this->ReadALK(mPath.c_str()))
		return E_FAIL;

	return S_OK;
}

HRESULT ConfigManager::WriteFile()
{
	if (!this->WriteALK(mPath.c_str()))
		return E_FAIL;

	return S_OK;
}

bool ConfigManager::ReadALK(const wchar_t* filename)
{
	std::ifstream file;
	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try
	{
		file.open(filename, std::ios_base::in);
#if defined(_DEBUG) || defined(DEBUG)
		std::cout << "[Configuration Manager Dump]" << std::endl;
#endif
		std::string line;
		while (!file.eof())
		{
			std::getline(file, line);

			std::istringstream iss(line);

			std::string key;
			std::string value;
			iss >> key >> value;

			m_configData.insert(std::make_pair(key, value));

#if defined(_DEBUG) || defined(DEBUG)
			std::cout << key << " " << value << std::endl;
#endif
		}

		file.close();

		Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, L"Configuration file correctly loaded.");
	}
	catch (const std::ifstream::failure& e)
	{
		Logger::instance().Exception(e.code(), e.what(), L"Configuration Manager Failure, unable to open requested file.");
		return false;
	}

	return true;
}

// TODO : check add eof to file
bool ConfigManager::WriteALK(const wchar_t* filename)
{
	std::ofstream file;
	file.exceptions(std::ofstream::failbit | std::ofstream::badbit);

	try
	{
		file.open(filename, std::ios_base::out);

		std::ostringstream content;

		// serialization step
		for (auto& p : m_configData)
		{
			content << p.first << " " << p.second << "\n";
		}

		// writeback to file
		file << content.str();

		file.close();

#if defined(_DEBUG) | defined(DEBUG)
		Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, L"Configuration file succesfully written.");
#endif
	}
	catch (const std::ofstream::failure& e)
	{
		Logger::instance().Exception(e.code(), e.what(), L"Config Manager failed to open write output file.");
		return false;
	}

	return true;
}
