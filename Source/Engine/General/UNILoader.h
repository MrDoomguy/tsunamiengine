#pragma once
#ifndef _FILE_IO_H
#define _FILE_IO_H

#include "TsunamiStd.h"


class UNILoader
{
public:

	typedef struct
	{
	int dimx;
	int dimy;
	int dimz;
	int gridType;
	int elementType;
	int bytesPerElement;
	char info[256];
	unsigned long long timestamp;
	int dimt;
	} UniHeader;

	UNILoader();
	~UNILoader();
	
	// reads only header, only necessary to get some information like size of the file or type
	UniHeader ReadHeader(const std::string &path);

	void ReadGrid(float *data);
	void ReadGrid(const std::string &path, float *data);

private:
	typedef struct
	{
		int dimx;
		int dimy;
		int dimz;
		int gridType;
		int elementType;
		int bytesPerElement;
		char info[256];
		unsigned long long timestamp;
	} PartUniHeader;

	UniHeader tmpHead;
	void* fileHandle = nullptr;
};

#endif

