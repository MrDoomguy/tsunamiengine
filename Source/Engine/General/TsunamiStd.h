#ifndef _TSUNAMISTD_H_
#define _TSUNAMISTD_H_

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#ifndef _WINSOCKAPI_
#define _WINSOCKAPI_
#include <Windows.h>
#include <windowsx.h>
#include <wrl.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#undef _WINSOCKAPI_
#endif


#include <d3d11.h>
#include <dxgi.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")

#include <d2d1.h>
#include <dwrite.h>
#pragma comment(lib, "d2d1.lib")
#pragma comment(lib, "dwrite.lib")

// idea to support other audio engines, not really used right now
// as AudioEngine uses directly IXAudio2 interface
#define USE_XAUDIO 1
#if USE_XAUDIO
#include <xaudio2.h>
#pragma comment(lib, "Xaudio2.lib")
#endif

#include "vorbis\vorbisfile.h"
#if defined(_DEBUG) || defined (DEBUG)
#pragma comment(lib, "vorbis64/libvorbisd.lib")
#pragma comment(lib, "vorbis64/libvorbisfiled.lib")
#else
#pragma comment(lib, "vorbis64/libvorbis.lib")
#pragma comment(lib, "vorbis64/libvorbisfile.lib")
#endif

#include "assimp\Importer.hpp"
#include "assimp\scene.h"
#include "assimp\postprocess.h"
#if defined(_DEBUG) || defined(DEBUG)
#pragma comment(lib, "assimp64/assimp-vc130-mtd.lib")
#else
#pragma comment(lib, "assimp64/assimp-vc130-mt.lib")
#endif

#include "tinyxml2\tinyxml2.h"
#if _M_IX86 || __i386__ || _X86_
#if defined(_DEBUG) | defined(DEBUG)
#pragma comment(lib, "tinyxml232/tinyxml2d.lib")
#else
#pragma comment(lib, "tinyxml232/tinyxml2.lib")
#endif
#endif

#define THREAD_COUNT_1D 512
#define THREAD_COUNT_2D 16
#define THREAD_COUNT_3D 8


#define PHYSICS_DEBUG 0
#define EDITOR 0

#include <zlib64\zlib.h>
#pragma comment(lib, "zlibwapi.lib")

//#define _WINSOCKAPI_
//#define WIN32_LEAN_AND_MEAN

#include <cstdint>
#include <cstdlib>
#include <typeinfo>
#include <typeindex>
#include <stdexcept>

#include <memory>

#include <cstdio>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <locale>
#include <clocale>
#include <codecvt>
#include <mutex>

#include <vector>
#include <unordered_map>

#include "Logger.h"
#include "ConfigManager.h"
#include "GUIDGenerator.h"

#define TSUNAMI_VER_MAJOR 1
#define TSUNAMI_VER_MINOR 1
#define TSUNAMI_VER_PATCH 3

#define TYPEOF(c) typeid(c)

#define TSUNAMI_DECIMAL_PRECISION 5

#define MAX_TEX_SIZE 1024
#define MAX_VERTEX_SIZE 16384
#define BUFFER_SIZE_32K 32768

using namespace DirectX;
using namespace DirectX::PackedVector;

#define SAFE_RELEASE(p) { if((p)){p->Release(); p = nullptr;} }
#define SAFE_DELETE(p) { if((p)){delete p; p = nullptr;} }

// 32-bit famebuffer format
#define LDR_RT_FORMAT DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM
// Select wether we want HDR rendering
#define HDR_RENDER 1
#if HDR_RENDER

// activate to switch to a more performant framebuffer format
// HDR could be R11G11B10 to save bandwidth but would need to macro shaders to allow that
#define HDR_FAST 0
#if HDR_FAST
#define HDR_RT_FORMAT DXGI_FORMAT::DXGI_FORMAT_R11G11B10_FLOAT
#else
#define HDR_RT_FORMAT DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT
#endif

#else
// Use standand fb format for intermediate targets
#define HDR_RT_FORMAT LDR_RT_FORMAT

#endif

#define INDEX_FAST 1
#if INDEX_FAST
#define INDEX_FORMAT DXGI_FORMAT::DXGI_FORMAT_R16_UINT
typedef uint16_t INDEX_TYPE;
static const size_t INDEX_SIZE_BYTES = sizeof(uint16_t);
#else
#define INDEX_FORMAT DXGI_FORMAT::DXGI_FORMAT_R32_UINT
typedef uint32_t INDEX_TYPE;
static const size_t INDEX_SIZE_BYTES = sizeof(uint32_t);
#endif

static std::wstring str_to_wstr(const std::string& str)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring wstr = converter.from_bytes(str);
	return wstr;
}

static std::string wstr_to_str(const std::wstring& wstr)
{
	return (std::string(wstr.begin(), wstr.end()));
}

static void PrintVector3(const DirectX::XMFLOAT3& vec)
{
	std::cout << std::setprecision(TSUNAMI_DECIMAL_PRECISION) << "Vector3:\t" << vec.x << "\t" << vec.y << "\t" << vec.z << std::endl;
}

namespace Filesystem
{
	static std::string GetCurrentPath()
	{
		char full[_MAX_PATH];
		if (_fullpath(full, ".\\", _MAX_PATH) != NULL)
			return std::string(full);
		else
			return std::string("Invalid Path");
	}

	static std::wstring GetCurrentPathW()
	{
		wchar_t full[_MAX_PATH];
		if (_wfullpath(full, L".\\", _MAX_PATH) != NULL)
			return std::wstring(full);
		else
			return std::wstring(L"Invalid Path");
	}

	static const std::wstring GetAssetPathW(const wchar_t* toLoad)
	{
		std::wstring path = str_to_wstr(ConfigManager::instance().GetValueByKey<std::string>("AssetsPath"));
		path.append(toLoad);
		return path;
	}

	static const std::string GetAssetPath(const char* toLoad)
	{
		std::string path = ConfigManager::instance().GetValueByKey<std::string>("AssetsPath");
		path.append(toLoad);
		return path;
	}
}

static const std::wstring HR_to_wstr(HRESULT hr)
{
	std::wstring res = L"\n >> [HRESULT]: ";
	switch (hr)
	{
	case E_NOTIMPL:
		return res + L"E_NOTIMPL (Not implemented)";
	case E_NOINTERFACE:
		return res + L"E_NOINTERFACE (No such interface supported)";
	case E_POINTER:
		return res + L"E_POINTER (Pointer not valid)";
	case E_ABORT:
		return res + L"E_ABORT (Operation aborted)";
	case E_FAIL:
		return res + L"E_FAIL (Unspecified failure)";
	case E_UNEXPECTED:
		return res + L"E_UNEXPECTED (Unxepected failure)";
	case E_ACCESSDENIED:
		return res + L"E_ACCESSDENIED (General access denied)";
	case E_HANDLE:
		return res + L"E_HANDLE (Handle is not valid)";
	case E_OUTOFMEMORY:
		return res + L"E_OUTOFMEMORY (Failed to allocate necessary memory)";
	case E_INVALIDARG:
		return res + L"E_INVALIDARG (One or more arguments are not valid)";
	case S_OK:
		return res + L"S_OK (Operation successful)";
	default:
		return L"Unknown HRESULT :S";
	}
}

namespace core
{
	static void TsunamiFatalError(const std::wstring& errString)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, errString);

#if defined(_DEBUG) | defined(DEBUG)
		OutputDebugString(errString.c_str());
#else
		MessageBox(0, errString.c_str(), L"Fatal Error", MB_OK);
#endif

		exit(1);
	}

	static bool XMLCheckError(const tinyxml2::XMLError& e)
	{
		if (e != tinyxml2::XMLError::XML_SUCCESS)
			return false;

		return true;
	}

	static const std::wstring XMLErr_to_wstr(const tinyxml2::XMLError& e)
	{
		std::wstring res = L"\n>> [XMLERROR]: ";
		switch (e)
		{
		case tinyxml2::XMLError::XML_ERROR_COUNT:
			return res + L"XML_ERROR_COUNT";
		case tinyxml2::XMLError::XML_ERROR_ELEMENT_MISMATCH:
			return res + L"XML_ERROR_ELEMENT_MISMATCH";
		case tinyxml2::XMLError::XML_ERROR_EMPTY_DOCUMENT:
			return res + L"XML_ERROR_EMPTY_DOCUMENT";
		case tinyxml2::XMLError::XML_ERROR_FILE_COULD_NOT_BE_OPENED:
			return res + L"XML_ERROR_FILE_COULD_NOT_BE_OPENEND";
		case tinyxml2::XMLError::XML_ERROR_FILE_NOT_FOUND:
			return res + L"XML_ERROR_FILE_NOT_FOUND";
		default:
			return res + L"Unknown XML Error :S";
		}
	}
}

class com_exception : public std::exception
{
public:
	com_exception(HRESULT hr) : result(hr) {}

	virtual const char* what() const override
	{
		static char s_str[64] = { 0 };
		sprintf_s(s_str, "Failure with HRESULT of %08X", result);
		return s_str;
	}

private:
	HRESULT result;
};

inline void ThrowIfFailed(HRESULT hr)
{
	if (FAILED(hr))
	{
		throw com_exception(hr);
	}
}

#endif
