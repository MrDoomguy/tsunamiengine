#include "GPUMemoryManager.h"
#include "../Render/Device.h"

HRESULT GPUMemoryManager::ColorMapFromFile(const std::string& filename, ColorMap* pColorMap)
{
	HRESULT hr;

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring name = converter.from_bytes(filename);

	ID3D11Resource* pTex = dynamic_cast<ID3D11Resource*>(pColorMap->mTexture);

	DirectX::DDS_ALPHA_MODE alpha;
	if (FAILED(hr = DirectX::CreateDDSTextureFromFile(mDevice->GetDevice(), name.c_str(), &pTex, &pColorMap->mSRV, 0U, &alpha)))
	{
		delete pColorMap;
		return hr;
	}

	if (FAILED(hr = pTex->QueryInterface(IID_ID3D11Texture2D, (void**)&pColorMap->mTexture)))
	{
		delete pColorMap;
		return hr;
	}

	D3D11_TEXTURE2D_DESC td;
	pColorMap->mTexture->GetDesc(&td);

	pColorMap->mWidth = td.Width;
	pColorMap->mHeight = td.Height;

	return S_OK;
}

ColorMap* GPUMemoryManager::GetColorMap(const std::string& filename)
{
	if (mColorMaps.find(filename) != mColorMaps.end())
	{
		return mColorMaps[filename];
	}

	HRESULT hr;

	mColorMaps[filename] = new ColorMap();

	if (FAILED(hr = ColorMapFromFile(filename, mColorMaps[filename])))
	{
		std::wstring err = L"[Resource Factory]: Failed to load Color Map: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	return mColorMaps[filename];
}

HRESULT GPUMemoryManager::ColorMapFromSRV(ID3D11ShaderResourceView* pSRV, ColorMap* pColorMap)
{
	pColorMap->mSRV = pSRV;

	return S_OK;
}

HRESULT GPUMemoryManager::VolumeTextureFromMemory(VolumeTexture* volumeTex, const XMINT3& size, void* pData)
{
	return VolumeTextureFromMemory(volumeTex, size, DXGI_FORMAT_R32_FLOAT, 1, pData);
}

HRESULT GPUMemoryManager::RGBVolumeTextureFromMemory(VolumeTexture * volumeTex, const XMINT3 & size, void * pData)
{
	return VolumeTextureFromMemory(volumeTex, size, DXGI_FORMAT_R32G32B32_FLOAT, 3, pData);
}

HRESULT GPUMemoryManager::RGBAVolumeTextureFromMemory(VolumeTexture * volumeTex, const XMINT3 & size, void * pData)
{
	return VolumeTextureFromMemory(volumeTex, size, DXGI_FORMAT_R32G32B32A32_FLOAT, 4, pData);
}


HRESULT GPUMemoryManager::VolumeTextureFromMemory(VolumeTexture * volumeTex, const XMINT3 & size, DXGI_FORMAT format, int elementCount, void * pData)
{
	HRESULT hr;

	volumeTex->mWidth = size.x;
	volumeTex->mHeight = size.y;
	volumeTex->mDepth = size.z;

	D3D11_TEXTURE3D_DESC d3dDesc = { 0 };
	d3dDesc.Width = size.x;
	d3dDesc.Height = size.y;
	d3dDesc.Depth = size.z;
	d3dDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	d3dDesc.MipLevels = 1;
	d3dDesc.CPUAccessFlags = 0;
	d3dDesc.MiscFlags = 0;
	d3dDesc.Format = format;

	if (pData != nullptr)
	{
		D3D11_SUBRESOURCE_DATA sd;
		ZeroMemory(&sd, sizeof(D3D11_SUBRESOURCE_DATA));

		sd.pSysMem = pData;
		sd.SysMemPitch = static_cast<UINT>(size.x * sizeof(float) * elementCount);
		sd.SysMemSlicePitch = static_cast<UINT>(size.x * size.y * sizeof(float) * elementCount);

		if (FAILED(hr = mDevice->GetDevice()->CreateTexture3D(&d3dDesc, &sd, &volumeTex->mTexture)))
		{
			delete volumeTex;
			return hr;
		}
	}
	else
	{
		if (FAILED(hr = mDevice->GetDevice()->CreateTexture3D(&d3dDesc, nullptr, &volumeTex->mTexture)))
		{
			delete volumeTex;
			return hr;
		}
	}


	D3D11_SHADER_RESOURCE_VIEW_DESC d3dRD;
	ZeroMemory(&d3dRD, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	d3dRD.Texture3D.MipLevels = 1;
	d3dRD.Texture3D.MostDetailedMip = 0;
	d3dRD.Format = format;
	d3dRD.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE3D;

	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(volumeTex->mTexture, &d3dRD, &volumeTex->mSRV)))
	{
		delete volumeTex;
		return hr;
	}
	return S_OK;
}

HRESULT GPUMemoryManager::RWVolumeTextureFromMemory(VolumeTextureRW * volumeTex, const XMINT3 & size, void * pData)
{
	HRESULT hr;

	volumeTex->mWidth = size.x;
	volumeTex->mHeight = size.y;
	volumeTex->mDepth = size.z;

	D3D11_TEXTURE3D_DESC d3dDesc = { 0 };
	d3dDesc.Width = size.x;
	d3dDesc.Height = size.y;
	d3dDesc.Depth = size.z;
	d3dDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	d3dDesc.MipLevels = 1;
	d3dDesc.CPUAccessFlags = 0;
	d3dDesc.MiscFlags = 0;
	d3dDesc.Format = DXGI_FORMAT_R32_FLOAT;

	if (pData != nullptr)
	{
		D3D11_SUBRESOURCE_DATA sd;
		ZeroMemory(&sd, sizeof(D3D11_SUBRESOURCE_DATA));

		sd.pSysMem = pData;
		sd.SysMemPitch = static_cast<UINT>(size.x * sizeof(float));
		sd.SysMemSlicePitch = static_cast<UINT>(size.x * size.y * sizeof(float));

		if (FAILED(hr = mDevice->GetDevice()->CreateTexture3D(&d3dDesc, &sd, &volumeTex->mTexture)))
		{
			delete volumeTex;
			return hr;
		}
	}
	else
	{
		if (FAILED(hr = mDevice->GetDevice()->CreateTexture3D(&d3dDesc, nullptr, &volumeTex->mTexture)))
		{
			delete volumeTex;
			return hr;
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC d3dRD;
	ZeroMemory(&d3dRD, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	d3dRD.Texture3D.MipLevels = 1;
	d3dRD.Texture3D.MostDetailedMip = 0;
	d3dRD.Format = DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
	d3dRD.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE3D;

	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(volumeTex->mTexture, &d3dRD, &volumeTex->mSRV)))
	{
		delete volumeTex;
		return hr;
	}

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
	ZeroMemory(&uavDesc, sizeof(D3D11_UNORDERED_ACCESS_VIEW_DESC));

	uavDesc.Format = DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION::D3D11_UAV_DIMENSION_TEXTURE3D;
	uavDesc.Texture3D.WSize = size.z;
	if (FAILED(hr = mDevice->GetDevice()->CreateUnorderedAccessView(volumeTex->mTexture, &uavDesc, &volumeTex->mUAV)))
	{
		delete volumeTex;
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::RWRGBAVolumeTextureFromMemory(VolumeTextureRW * volumeTex, const XMINT3 & size, void * pData)
{
	HRESULT hr;

	volumeTex->mWidth = size.x;
	volumeTex->mHeight = size.y;
	volumeTex->mDepth = size.z;

	D3D11_TEXTURE3D_DESC d3dDesc = { 0 };
	d3dDesc.Width = size.x;
	d3dDesc.Height = size.y;
	d3dDesc.Depth = size.z;
	d3dDesc.Usage = D3D11_USAGE_DEFAULT;
	d3dDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	d3dDesc.MipLevels = 1;
	d3dDesc.CPUAccessFlags = 0;
	d3dDesc.MiscFlags = 0;
	d3dDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	if (pData != nullptr)
	{
		D3D11_SUBRESOURCE_DATA sd;
		ZeroMemory(&sd, sizeof(D3D11_SUBRESOURCE_DATA));

		sd.pSysMem = pData;
		sd.SysMemPitch = static_cast<UINT>(size.x * sizeof(float) * 4);
		sd.SysMemSlicePitch = static_cast<UINT>(size.x * size.y * sizeof(float) * 4);

		if (FAILED(hr = mDevice->GetDevice()->CreateTexture3D(&d3dDesc, &sd, &volumeTex->mTexture)))
		{
			delete volumeTex;
			return hr;
		}
	}
	else
	{
		if (FAILED(hr = mDevice->GetDevice()->CreateTexture3D(&d3dDesc, nullptr, &volumeTex->mTexture)))
		{
			delete volumeTex;
			return hr;
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC d3dRD;
	ZeroMemory(&d3dRD, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	d3dRD.Texture3D.MipLevels = 1;
	d3dRD.Texture3D.MostDetailedMip = 0;
	d3dRD.Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
	d3dRD.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE3D;

	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(volumeTex->mTexture, &d3dRD, &volumeTex->mSRV)))
	{
		delete volumeTex;
		return hr;
	}

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
	ZeroMemory(&uavDesc, sizeof(D3D11_UNORDERED_ACCESS_VIEW_DESC));

	uavDesc.Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION::D3D11_UAV_DIMENSION_TEXTURE3D;
	uavDesc.Texture3D.WSize = size.z;
	if (FAILED(hr = mDevice->GetDevice()->CreateUnorderedAccessView(volumeTex->mTexture, &uavDesc, &volumeTex->mUAV)))
	{
		delete volumeTex;
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::VolumeTextureFromFile(const std::string& filename, VolumeTexture* pTexture)
{
	HRESULT hr;

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring name = converter.from_bytes(filename);

	ID3D11Resource* pTex = dynamic_cast<ID3D11Resource*>(pTexture->mTexture);

	DirectX::DDS_ALPHA_MODE alpha;
	if (FAILED(hr = DirectX::CreateDDSTextureFromFile(mDevice->GetDevice(), name.c_str(), &pTex, &pTexture->mSRV, 0U, &alpha)))
	{
		delete pTexture;
		return hr;
	}

	if (FAILED(hr = pTex->QueryInterface(IID_ID3D11Texture3D, (void**)&pTexture->mTexture)))
	{
		delete pTexture;
		return hr;
	}

	D3D11_TEXTURE3D_DESC td;
	pTexture->mTexture->GetDesc(&td);

	pTexture->mWidth = td.Width;
	pTexture->mHeight = td.Height;
	pTexture->mDepth = td.Depth;

	return S_OK;
}

HRESULT GPUMemoryManager::AnimatedVolumeTextureFromFile(const std::string & filename, AnimatedVolumeTexture * pTexture, XMUINT4* dim)
{
	UNILoader fileio = UNILoader();
	float* tmpData = nullptr;

	UNILoader::UniHeader tmpHeader = fileio.ReadHeader(filename);

	int elementCount = tmpHeader.bytesPerElement / sizeof(float);

	std::cout << "Load SDFGrid " << tmpHeader.dimx << "x" << tmpHeader.dimy << "x" << tmpHeader.dimz << "x" << tmpHeader.dimt << " with " << elementCount << " Element(s)" << std::endl;

	tmpData = new float[tmpHeader.dimx * tmpHeader.dimy * tmpHeader.dimz * tmpHeader.dimt * elementCount];

	fileio.ReadGrid(tmpData);

	float* packedData;

	XMINT3 newExt = PackArray(&packedData, tmpData, { tmpHeader.dimx * elementCount, tmpHeader.dimy, tmpHeader.dimz, tmpHeader.dimt }, { MAX_TEX_SIZE, MAX_TEX_SIZE, MAX_TEX_SIZE });
	newExt.x /= elementCount;
	delete tmpData;

	HRESULT hr;

	if(dim != nullptr) *dim = XMUINT4(tmpHeader.dimx, tmpHeader.dimy, tmpHeader.dimz, tmpHeader.dimt);

	if (elementCount == 1)
	{
		if (FAILED(hr = VolumeTextureFromMemory(pTexture, newExt, packedData)))
		{
			delete pTexture;
			delete packedData;

			return hr;
		}
	}
	else if(elementCount == 3)
	{
		if (FAILED(hr = RGBVolumeTextureFromMemory(pTexture, newExt, packedData)))
		{
			delete pTexture;
			delete packedData;

			return hr;
		}
	}
	else if (elementCount == 4)
	{
		if (FAILED(hr = RGBAVolumeTextureFromMemory(pTexture, newExt, packedData)))
		{
			delete pTexture;
			delete packedData;

			return hr;
		}
	}
	else
	{
		delete packedData;

		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, "volumetexture format not supported!");
		return E_FAIL;
	}

	pTexture->SetDim(XMUINT4(tmpHeader.dimx, tmpHeader.dimy, tmpHeader.dimz, tmpHeader.dimt));

	delete packedData;

	return S_OK;
}

HRESULT GPUMemoryManager::Framebuffer(const uint32_t width, const uint32_t height, const bool readable, const bool useMSAA, RenderTarget* pRenderTarget)
{
	HRESULT hr;

	pRenderTarget->mWidth = width;
	pRenderTarget->mHeight = height;

	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Format = HDR_RT_FORMAT;//DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
	td.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	td.Width = width;
	td.Height = height;
	td.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_RENDER_TARGET;
	if (readable)
	{
		td.BindFlags |= D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	}
	td.ArraySize = 1;
	td.CPUAccessFlags = 0;
	td.MipLevels = 1;
	td.MiscFlags = 0;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	if (useMSAA)
	{
		td.SampleDesc.Count = 4;
		td.SampleDesc.Quality = 16;
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&td, nullptr, &pRenderTarget->mTexture)))
	{
		delete pRenderTarget;
		return hr;
	}

	D3D11_RENDER_TARGET_VIEW_DESC rd;
	ZeroMemory(&rd, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rd.Format = HDR_RT_FORMAT;//DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
	rd.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2D;
	if (useMSAA)
	{
		rd.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DMS;
	}
	rd.Texture2D.MipSlice = 0;
	
	if (FAILED(hr = mDevice->GetDevice()->CreateRenderTargetView(pRenderTarget->mTexture, &rd, &pRenderTarget->mRTV)))
	{
		delete pRenderTarget;
		return hr;
	}

	if (readable)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC sd;
		ZeroMemory(&sd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		sd.Format = HDR_RT_FORMAT;//DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2D;
		if (useMSAA)
		{
			sd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2DMS;
		}
		sd.Texture2D.MipLevels = 1;
		sd.Texture2D.MostDetailedMip = 0;

		if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(pRenderTarget->mTexture, &sd, &pRenderTarget->mSRV)))
		{
			delete pRenderTarget;
			return hr;
		}
	}

	return S_OK;
}

HRESULT GPUMemoryManager::SingleFramebuffer(const uint32_t width, const uint32_t height, const bool readable, const bool useMSAA, RenderTarget * pRenderTarget)
{
	HRESULT hr;

	pRenderTarget->mWidth = width;
	pRenderTarget->mHeight = height;

	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Format = DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
	td.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	td.Width = width;
	td.Height = height;
	td.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_RENDER_TARGET;
	if (readable)
	{
		td.BindFlags |= D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	}
	td.ArraySize = 1;
	td.CPUAccessFlags = 0;
	td.MipLevels = 1;
	td.MiscFlags = 0;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	if (useMSAA)
	{
		td.SampleDesc.Count = 4;
		td.SampleDesc.Quality = 16;
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&td, nullptr, &pRenderTarget->mTexture)))
	{
		delete pRenderTarget;
		return hr;
	}

	D3D11_RENDER_TARGET_VIEW_DESC rd;
	ZeroMemory(&rd, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rd.Format = DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
	rd.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2D;
	if (useMSAA)
	{
		rd.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DMS;
	}
	rd.Texture2D.MipSlice = 0;

	if (FAILED(hr = mDevice->GetDevice()->CreateRenderTargetView(pRenderTarget->mTexture, &rd, &pRenderTarget->mRTV)))
	{
		delete pRenderTarget;
		return hr;
	}

	if (readable)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC sd;
		ZeroMemory(&sd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		sd.Format = DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
		sd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2D;
		if (useMSAA)
		{
			sd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2DMS;
		}
		sd.Texture2D.MipLevels = 1;
		sd.Texture2D.MostDetailedMip = 0;

		if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(pRenderTarget->mTexture, &sd, &pRenderTarget->mSRV)))
		{
			delete pRenderTarget;
			return hr;
		}
	}

	return S_OK;
}


HRESULT GPUMemoryManager::Framebuffer(ID3D11Texture2D* texture, const bool readable, RenderTarget* pRenderTarget)
{
	HRESULT hr;

	D3D11_TEXTURE2D_DESC td;
	texture->GetDesc(&td);

	pRenderTarget->mTexture = texture;
	pRenderTarget->mWidth = td.Width;
	pRenderTarget->mHeight = td.Height;

	if (FAILED(hr = mDevice->GetDevice()->CreateRenderTargetView(pRenderTarget->mTexture, nullptr, &pRenderTarget->mRTV)))
	{
		delete pRenderTarget;
		return hr;
	}

	if (readable)
	{
		if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(pRenderTarget->mTexture, nullptr, &pRenderTarget->mSRV)))
		{
			delete pRenderTarget;
			return hr;
		}
	}

	return S_OK;
}

HRESULT GPUMemoryManager::DepthStencil(const uint32_t width, const uint32_t height, const bool readable, const bool useMSAA, DepthStencilBuffer* pDepthStencil)
{
	HRESULT hr;

	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Format = DXGI_FORMAT::DXGI_FORMAT_R32_TYPELESS;
	td.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	td.Width = width;
	td.Height = height;
	td.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_DEPTH_STENCIL;
	if (readable)
	{
		td.BindFlags |= D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	}
	td.ArraySize = 1;
	td.CPUAccessFlags = 0;
	td.MipLevels = 1;
	td.MiscFlags = 0;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	if (useMSAA)
	{
		td.SampleDesc.Count = 4;
		td.SampleDesc.Quality = 16;
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&td, nullptr, &pDepthStencil->mTexture)))
	{
		delete pDepthStencil;
		return hr;
	}

	D3D11_DEPTH_STENCIL_VIEW_DESC dd;
	ZeroMemory(&dd, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	dd.Format = DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT;
	if (readable)
	{
		dd.Format = DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT;
	}
	dd.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2D;
	if (useMSAA)
	{
		dd.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DMS;
	}
	dd.Texture2D.MipSlice = 0;
	dd.Flags = 0;

	if (FAILED(hr = mDevice->GetDevice()->CreateDepthStencilView(pDepthStencil->mTexture, &dd, &pDepthStencil->mDSV)))
	{
		delete pDepthStencil;
		return hr;
	}

	if (readable)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC sd;
		ZeroMemory(&sd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		sd.Format = DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
		sd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2D;
		if (useMSAA)
		{
			sd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2DMS;
		}
		sd.Texture2D.MipLevels = 1;
		sd.Texture2D.MostDetailedMip = 0;

		if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(pDepthStencil->mTexture, &sd, &pDepthStencil->mSRV)))
		{
			delete pDepthStencil;
			return hr;
		}
	}

	return S_OK;
}

HRESULT GPUMemoryManager::TextureSampler(const D3D11_FILTER filter, const D3D11_TEXTURE_ADDRESS_MODE mode, ID3D11SamplerState** ppSampler)
{
	HRESULT hr;

	D3D11_SAMPLER_DESC sd;
	ZeroMemory(&sd, sizeof(D3D11_SAMPLER_DESC));
	sd.Filter = filter;
	sd.AddressU = mode;
	sd.AddressV = mode;
	sd.AddressW = mode;
	sd.MaxAnisotropy = 16;
	sd.MipLODBias = 0.0f;
	sd.MinLOD = 0.0f;
	sd.MaxLOD = FLT_MAX;
	sd.BorderColor[0] = 0.0f;
	sd.BorderColor[1] = 0.0f;
	sd.BorderColor[2] = 0.0f;
	sd.BorderColor[3] = 0.0f;

	if (FAILED(hr = mDevice->GetDevice()->CreateSamplerState(&sd, ppSampler)))
	{
		SAFE_RELEASE((*ppSampler));

		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::MakeTexture2DRW(const uint32_t width, const uint32_t height, const bool readable, const DXGI_FORMAT format, Texture2DRW* pTexture)
{
	HRESULT hr;

	pTexture->mWidth = width;
	pTexture->mHeight = height;

	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Width = width;
	td.Height = height;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.Format = format;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	td.Usage = D3D11_USAGE_DEFAULT;
	td.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_UNORDERED_ACCESS;
	if (readable)
		td.BindFlags |= D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;
	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&td, nullptr, &pTexture->mTexture)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate Texture2DRW." + HR_to_wstr(hr));
		delete pTexture;
		return hr;
	}

	D3D11_UNORDERED_ACCESS_VIEW_DESC ud;
	ZeroMemory(&ud, sizeof(D3D11_UNORDERED_ACCESS_VIEW_DESC));
	ud.Format = format;
	ud.ViewDimension = D3D11_UAV_DIMENSION::D3D11_UAV_DIMENSION_TEXTURE2D;
	ud.Texture2D.MipSlice = 0;
	if (FAILED(hr = mDevice->GetDevice()->CreateUnorderedAccessView(pTexture->mTexture, &ud, &pTexture->mUAV)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create Texture2DRW UAV." + HR_to_wstr(hr));
		delete pTexture;
		return hr;
	}

	if (readable)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC sd;
		ZeroMemory(&sd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
		sd.Format = format;
		sd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2D;
		sd.Texture2D.MostDetailedMip = 0;
		sd.Texture2D.MipLevels = 1;
		if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(pTexture->mTexture, &sd, &pTexture->mSRV)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create Texture2DRW SRV." + HR_to_wstr(hr));
			delete pTexture;
			return hr;
		}
	}

	return S_OK;
}

Texture2DRW* GPUMemoryManager::GetTexture2DRW(const std::string& name, const uint32_t width, const uint32_t height, const bool readable, const DXGI_FORMAT format)
{
	if (mTexture2DRW.find(name) != mTexture2DRW.end())
	{
		return mTexture2DRW[name];
	}

	HRESULT hr;

	mTexture2DRW[name] = new Texture2DRW();

	if (FAILED(hr = MakeTexture2DRW(width, height, readable, format, mTexture2DRW[name])))
	{
		std::wstring err = L"Failed to create Texture2DRW: " + str_to_wstr(name) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	return mTexture2DRW[name];
}
CubeMap* GPUMemoryManager::GetCubeMap(const std::string& name, const uint32_t size)
{
	if (mCubeMaps.find(name) != mCubeMaps.end())
		return mCubeMaps[name];

	HRESULT hr;

	mCubeMaps[name] = new CubeMap();
	mCubeMaps[name]->mWidth = size;
	mCubeMaps[name]->mHeight = size;

	D3D11_TEXTURE2D_DESC texDesc;
	ZeroMemory(&texDesc, sizeof(D3D11_TEXTURE2D_DESC));
	texDesc.Width = size;
	texDesc.Height = size;
	texDesc.MipLevels = 0;
	texDesc.ArraySize = 6;
	texDesc.SampleDesc = { 1, 0 };
	texDesc.Format = HDR_RT_FORMAT;
	texDesc.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_FLAG::D3D11_BIND_RENDER_TARGET;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = D3D11_RESOURCE_MISC_FLAG::D3D11_RESOURCE_MISC_GENERATE_MIPS | D3D11_RESOURCE_MISC_FLAG::D3D11_RESOURCE_MISC_TEXTURECUBE;
	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&texDesc, nullptr, &mCubeMaps[name]->mTexture)))
	{
		core::TsunamiFatalError(L"Failed to Create CubeMap: " + str_to_wstr(name) + HR_to_wstr(hr));
	}

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	ZeroMemory(&rtvDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	rtvDesc.Texture2DArray.ArraySize = 1;
	rtvDesc.Texture2DArray.MipSlice = 0;
	for (uint8_t i = 0; i < 6; i++)
	{
		rtvDesc.Texture2DArray.FirstArraySlice = static_cast<UINT>(i);
		if (FAILED(hr = mDevice->GetDevice()->CreateRenderTargetView(mCubeMaps[name]->mTexture, &rtvDesc, &mCubeMaps[name]->mRTV[i])))
		{
			core::TsunamiFatalError(L"Failed to Create CubeMap: " + str_to_wstr(name) + L"\nRTV #" + std::to_wstring(i) + HR_to_wstr(hr));
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MipLevels = -1;
	srvDesc.TextureCube.MostDetailedMip = 0;
	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(mCubeMaps[name]->mTexture, &srvDesc, &mCubeMaps[name]->mSRV)))
	{
		core::TsunamiFatalError(L"Failed to Create CubeMap: " + str_to_wstr(name) + L" SRV" + HR_to_wstr(hr));
	}

	mCubeMaps[name]->mViewport.Width = size;
	mCubeMaps[name]->mViewport.Height = size;
	mCubeMaps[name]->mViewport.TopLeftX = 0.0f;
	mCubeMaps[name]->mViewport.TopLeftY = 0.0f;
	mCubeMaps[name]->mViewport.MinDepth = 0.0f;
	mCubeMaps[name]->mViewport.MaxDepth = 1.0f;

	return mCubeMaps[name];
}

CubeMap* GPUMemoryManager::GetCubeMap(const std::string& filename)
{
	if (mCubeMaps.find(filename) != mCubeMaps.end())
	{
		return mCubeMaps[filename];
	}

	mCubeMaps[filename] = new CubeMap();

	HRESULT hr;

	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
	std::wstring name = converter.from_bytes(filename);

	ID3D11Resource* pTex = dynamic_cast<ID3D11Resource*>(mCubeMaps[filename]->mTexture);

	DirectX::DDS_ALPHA_MODE alpha;
	if (FAILED(hr = DirectX::CreateDDSTextureFromFile(mDevice->GetDevice(), name.c_str(), &pTex, &mCubeMaps[filename]->mSRV, 0U, &alpha)))
	{
		delete mCubeMaps[filename];
		core::TsunamiFatalError(L"Failed to Load CubeMap: " + str_to_wstr(filename) + L"from disk." + HR_to_wstr(hr));
	}

	if (FAILED(hr = pTex->QueryInterface(IID_ID3D11Texture2D, (void**)&mCubeMaps[filename]->mTexture)))
	{
		delete mCubeMaps[filename];
		core::TsunamiFatalError(L"Failed to Allocate CubeMap: " + str_to_wstr(filename) + HR_to_wstr(hr));
	}

	D3D11_TEXTURE2D_DESC td;
	mCubeMaps[filename]->mTexture->GetDesc(&td);

	/*D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	ZeroMemory(&rtvDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rtvDesc.Format = HDR_RT_FORMAT;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	rtvDesc.Texture2DArray.ArraySize = 1;
	rtvDesc.Texture2DArray.MipSlice = 0;
	for (uint8_t i = 0; i < 6; i++)
	{
		rtvDesc.Texture2DArray.FirstArraySlice = static_cast<UINT>(i);
		if (FAILED(hr = mDevice->GetDevice()->CreateRenderTargetView(mCubeMaps[filename]->mTexture, &rtvDesc, &mCubeMaps[filename]->mRTV[i])))
		{
			core::TsunamiFatalError(L"Failed to Create CubeMap: " + str_to_wstr(filename) + L"\nRTV #" + std::to_wstring(i) + HR_to_wstr(hr));
		}
	}*/

	mCubeMaps[filename]->mWidth = td.Width;
	mCubeMaps[filename]->mHeight = td.Height;
	mCubeMaps[filename]->mViewport.Width = td.Width;
	mCubeMaps[filename]->mViewport.Height = td.Height;
	mCubeMaps[filename]->mViewport.TopLeftX = 0.0f;
	mCubeMaps[filename]->mViewport.TopLeftY = 0.0f;
	mCubeMaps[filename]->mViewport.MinDepth = 0.0f;
	mCubeMaps[filename]->mViewport.MaxDepth = 1.0f;

	return mCubeMaps[filename];
}

HRESULT GPUMemoryManager::MakeSphere(IndexedMesh* pMesh, const bool isStaticMesh, const MeshHelper::SPHERE_DESC* desc)
{
	HRESULT hr;

	MeshHelper geoGen;
	MeshHelper::SPHERE_DESC sd;
	geotypes::MeshData meshData;

	if (desc != nullptr)
		sd = *desc;

	geoGen.CreateSphere(sd, meshData);

	pMesh->mIndexCount = static_cast<uint32_t>(meshData.indices.size());

	if (FAILED(hr = FillMeshBuffer(isStaticMesh, meshData, pMesh)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create sphere mesh.");
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::MakeBox(IndexedMesh * pMesh, const bool isStaticMesh, const MeshHelper::BOX_DESC * desc)
{
	HRESULT hr;

	MeshHelper geoGen;
	MeshHelper::BOX_DESC sd;
	geotypes::MeshData meshData;

	if (desc != nullptr)
		sd = *desc;

	geoGen.CreateBox(sd, meshData);

	pMesh->mIndexCount = static_cast<uint32_t>(meshData.indices.size());

	if (FAILED(hr = FillMeshBuffer(isStaticMesh, meshData, pMesh)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create box mesh.");
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::MakeFullscreenQuad(IndexedMesh* pMesh, const bool isStatic)
{
	HRESULT hr;

	MeshHelper geoGen;
	geotypes::MeshData meshData;

	geoGen.CreateFullscreenQuad(meshData);

	pMesh->mIndexCount = static_cast<uint32_t>(meshData.indices.size());

	if (FAILED(hr = FillMeshBuffer(isStatic, meshData, pMesh)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create full screen quad mesh." + HR_to_wstr(hr));
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::MeshFromFile(const char* filename, const bool isStatic, IndexedMesh* pMesh)
{
	uint32_t flags = aiProcess_CalcTangentSpace | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices | aiProcess_Triangulate | aiProcess_GenUVCoords | aiProcess_SortByPType | 0;

	Assimp::Importer importer;
	const aiScene* pScene = importer.ReadFile(filename, flags);

	std::vector<geotypes::MeshData> entries;

	if (pScene)
	{
		entries.resize(pScene->mNumMeshes);

		for (uint32_t i = 0; i < entries.size(); i++)
		{
			const aiMesh* pAssimpMesh = pScene->mMeshes[i];

			entries[i] = InitAssimpMesh(pAssimpMesh);
		}

		if (entries.size() > 1)
			return FillMeshBuffer(isStatic, entries, pMesh);
		else
			return FillMeshBuffer(isStatic, entries[0], pMesh);
	}

	return S_OK;
}

HRESULT GPUMemoryManager::PointMeshFromMemory(PrimitiveMesh * pMesh, const int size)
{
	pMesh->mVertexCount = static_cast<uint32_t>(size);

	pMesh->mVB = new VertexBuffer(mDevice->GetDevice(), static_cast<UINT>(pMesh->mVertexCount * pMesh->mVertexSize), false, nullptr);

	// TODO: create index buffer wrapper
	D3D11_BUFFER_DESC id;
	ZeroMemory(&id, sizeof(D3D11_BUFFER_DESC));
	id.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_INDEX_BUFFER;
	id.ByteWidth = static_cast<UINT>(pMesh->mVertexCount * pMesh->mVertexSize);
	id.Usage = D3D11_USAGE::D3D11_USAGE_DYNAMIC;
	id.CPUAccessFlags = D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
	id.MiscFlags = 0;
	id.StructureByteStride = 0;

	pMesh->mSB = new StructuredBuffer(mDevice->GetDevice(), static_cast<UINT>(pMesh->mVertexCount * pMesh->mVertexSize), pMesh->mVertexSize, true, false, 0);

	pMesh->mStreamBuffer = new StreamOutBuffer(mDevice->GetDevice(), static_cast<UINT>(pMesh->mVertexCount * pMesh->mVertexSize * 6));

	return S_OK;
}

SkeletalMesh* GPUMemoryManager::SkeletalMeshFromFile(const char* filename, const bool isStatic)
{
	if (mSkeletalMeshes.find(filename) != mSkeletalMeshes.end())
	{
		return mSkeletalMeshes[filename];
	}

	mSkeletalMeshes[filename] = new SkeletalMesh();

	Assimp::Importer importer;
	const aiScene* pScene = importer.ReadFile(filename, 
		aiProcess_ValidateDataStructure		|
		aiProcess_CalcTangentSpace			| 
		aiProcess_GenSmoothNormals			| 
		aiProcess_JoinIdenticalVertices		| 
		aiProcess_ImproveCacheLocality		| 
		aiProcess_LimitBoneWeights			| 
		aiProcess_RemoveRedundantMaterials	| 
		aiProcess_Triangulate				| 
		aiProcess_GenUVCoords				| 
		aiProcess_SortByPType				| 
		aiProcess_FindDegenerates			| 
		aiProcess_FindInvalidData			| 
		0);

	if (!pScene)
	{
		std::wstring err = L"Failed to load skeletal mesh file: " + str_to_wstr(filename) + str_to_wstr("\n" + std::string(importer.GetErrorString()));
		core::TsunamiFatalError(err);
	}

	const aiMesh* pAssimpMesh = pScene->mMeshes[0];

	// Build mesh vertex data
	geotypes::SkinnedMeshData entry = InitAssimpSkeletalMesh(pAssimpMesh);

	// build d3d buffers with loaded vertex data
	FillMeshBuffer(isStatic, entry, mSkeletalMeshes[filename]);

	return mSkeletalMeshes[filename];
}

animtypes::AnimationData* GPUMemoryManager::AnimationFromFile(const char* filename)
{
	if (mAnimationData.find(filename) != mAnimationData.end())
	{
		return mAnimationData[filename];
	}

	mAnimationData[filename] = new animtypes::AnimationData();

	Assimp::Importer importer;
	const aiScene* pScene = importer.ReadFile(filename,
		aiProcess_ValidateDataStructure |
		aiProcess_CalcTangentSpace |
		aiProcess_GenSmoothNormals |
		aiProcess_JoinIdenticalVertices |
		aiProcess_ImproveCacheLocality |
		aiProcess_LimitBoneWeights |
		aiProcess_RemoveRedundantMaterials |
		aiProcess_Triangulate |
		aiProcess_GenUVCoords |
		aiProcess_SortByPType |
		aiProcess_FindDegenerates |
		aiProcess_FindInvalidData |
		0);

	if (!pScene)
	{
		std::wstring err = L"Failed to load skeletal mesh file: " + str_to_wstr(filename) + str_to_wstr("\n" + std::string(importer.GetErrorString()));
		core::TsunamiFatalError(err);
	}

	InitAssimpAnimationData(pScene, 0, mAnimationData[filename]);

	return mAnimationData[filename];
}

void GPUMemoryManager::CopyResource(ID3D11Buffer* pDst, ID3D11Buffer* pSrc)
{
	mDevice->LockContext();
	mDevice->GetContext()->CopyResource(pDst, pSrc);
	mDevice->UnlockContext();
}

void GPUMemoryManager::BufferData(ID3D11Buffer* pBuffer, void* pData, const size_t dataSizeBytes, const UINT subRes, const D3D11_MAP mapType, const UINT mapFlag)
{
	mDevice->LockContext();
	D3D11_MAPPED_SUBRESOURCE mSubRes;
	ZeroMemory(&mSubRes, sizeof(D3D11_MAPPED_SUBRESOURCE));
	
	mDevice->GetContext()->Map(pBuffer, subRes, mapType, mapFlag, &mSubRes);

	memcpy(mSubRes.pData, pData, dataSizeBytes);

	mDevice->GetContext()->Unmap(pBuffer, subRes);
	mDevice->UnlockContext();
}

void GPUMemoryManager::UpdateBuffer(ID3D11Buffer* pBuffer, void* pData, const UINT rowPitch, const UINT depthPitch, const D3D11_BOX* pBox, const UINT subRes)
{
	mDevice->LockContext();
	mDevice->GetContext()->UpdateSubresource(pBuffer, subRes, pBox, pData, rowPitch, depthPitch);
	mDevice->UnlockContext();
}

HRESULT GPUMemoryManager::MakeShader(const IShader::Desc& desc, IShader* pShader)
{
	HRESULT hr;
	if (FAILED(hr = pShader->Load(mDevice->GetDevice(), desc)))
	{
		delete pShader;
		return hr;
	}

	return S_OK;
}

GeometryShader * GPUMemoryManager::GetGeometryShader(const std::string & filename, const std::vector<D3D11_SO_DECLARATION_ENTRY> layout)
{
	if (mGeometryShaders.find(filename) != mGeometryShaders.end())
	{
		return mGeometryShaders[filename];
	}

	HRESULT hr;

	mGeometryShaders[filename] = new GeometryShader();

	std::string shaderCode;
	if (FAILED(hr = ReadBinaryFile(filename.c_str(), &shaderCode)))
	{
		std::wstring err = L"Failed to read Geometry Shader bin: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateGeometryShaderWithStreamOutput(shaderCode.data(), shaderCode.size(), layout.data(), static_cast<UINT>(layout.size()), nullptr, 0, 0, nullptr, &mGeometryShaders[filename]->mpObject)))
	{
		std::wstring err = L"Failed to create Geometry Shader Object: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	return mGeometryShaders[filename];
}
GeometryShader * GPUMemoryManager::GetGeometryShader(const std::string & filename)
{
	if (mGeometryShaders.find(filename) != mGeometryShaders.end())
	{
		return mGeometryShaders[filename];
	}

	HRESULT hr;

	mGeometryShaders[filename] = new GeometryShader();

	std::string shaderCode;
	if (FAILED(hr = ReadBinaryFile(filename.c_str(), &shaderCode)))
	{
		std::wstring err = L"Failed to read Geometry Shader bin: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateGeometryShader(shaderCode.data(), shaderCode.size(), nullptr, &mGeometryShaders[filename]->mpObject)))
	{
		std::wstring err = L"Failed to create Geometry Shader Object: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	return mGeometryShaders[filename];
}

ComputeShader* GPUMemoryManager::GetComputeShader(const std::string& filename)
{
	if (mComputeShaders.find(filename) != mComputeShaders.end())
	{
		return mComputeShaders[filename];
	}

	HRESULT hr;
	
	mComputeShaders[filename] = new ComputeShader();

	std::string shaderCode;
	if (FAILED(hr = ReadBinaryFile(filename.c_str(), &shaderCode)))
	{
		std::wstring err = L"Failed to read Compute Shader bin: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateComputeShader(shaderCode.data(), shaderCode.size(), nullptr, &mComputeShaders[filename]->mpObject)))
	{
		std::wstring err = L"Failed to Create Compute Shader Object: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	return mComputeShaders[filename];
}

VertexShader* GPUMemoryManager::GetVertexShader(const std::string& filename, const std::vector<D3D11_INPUT_ELEMENT_DESC> layout)
{
	if (mVertexShaders.find(filename) != mVertexShaders.end())
	{
		return mVertexShaders[filename];
	}

	HRESULT hr;

	mVertexShaders[filename] = new VertexShader(layout);

	std::string shaderCode;
	if (FAILED(hr = ReadBinaryFile(filename.c_str(), &shaderCode)))
	{
		std::wstring err = L"Failed to read Vertex Shader bin: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateVertexShader(shaderCode.data(), shaderCode.size(), nullptr, &mVertexShaders[filename]->mpObject)))
	{
		std::wstring err = L"Failed to create Vertex Shader Object: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateInputLayout(layout.data(), static_cast<UINT>(layout.size()), shaderCode.data(), shaderCode.size(), &mVertexShaders[filename]->mpVertexLayout)))
	{
		std::wstring err = L"Failed to create Vertex Shader Input Layout: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	return mVertexShaders[filename];
}

PixelShader* GPUMemoryManager::GetPixelShader(const std::string& filename)
{
	if (mPixelShaders.find(filename) != mPixelShaders.end())
	{
		return mPixelShaders[filename];
	}

	HRESULT hr;

	mPixelShaders[filename] = new PixelShader();

	std::string shaderCode;
	if (FAILED(hr = ReadBinaryFile(filename.c_str(), &shaderCode)))
	{
		std::wstring err = L"Failed to read Pixel Shader bin: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	if (FAILED(hr = mDevice->GetDevice()->CreatePixelShader(shaderCode.data(), shaderCode.size(), nullptr, &mPixelShaders[filename]->mpObject)))
	{
		std::wstring err = L"Failed to create Pixel Shader Object: " + str_to_wstr(filename) + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	return mPixelShaders[filename];
}

AudioEngine* GPUMemoryManager::GetAudioEngine()
{
	return mAudioEngine;
}

AudioClip* GPUMemoryManager::LoadAudioClipFromFile(const char* filename, const bool loop)
{
	if (mAudioClips.find(filename) != mAudioClips.end())
	{
		return mAudioClips[filename];
	}

	mAudioClips[filename] = new AudioClip(loop);

	SupportedFileExtensions ext = IsFileExtensionSupported(filename);
	if (ext != SupportedFileExtensions::EXTENSIONS_END)
	{
		switch (ext)
		{
		case GPUMemoryManager::SupportedFileExtensions::OGG_VORBIS:
		{
			LoadOGG(filename, mAudioClips[filename]);
			break;
		}
		default:
			core::TsunamiFatalError(L"Unsupported Audio file format. Abort.");
			break;
		}
	}

	return mAudioClips[filename];
}

HRESULT GPUMemoryManager::FillMeshBuffer(const bool isStaticMesh, const geotypes::MeshData& meshData, IndexedMesh* pMesh)
{
	pMesh->mVertexCount = static_cast<uint32_t>(meshData.vertices.size());
	pMesh->mIndexCount = static_cast<uint32_t>(meshData.indices.size());
	pMesh->mMesh = meshData;

	pMesh->mVB = new VertexBuffer(mDevice->GetDevice(), pMesh->mVertexSize * pMesh->mVertexCount, isStaticMesh, (void*)meshData.vertices.data());
	
	pMesh->mIB = new IndexBuffer(mDevice->GetDevice(), INDEX_SIZE_BYTES * pMesh->mIndexCount, isStaticMesh, (void*)meshData.indices.data());

	pMesh->mSB = new StructuredBuffer(mDevice->GetDevice(), pMesh->mVertexSize * pMesh->mVertexCount, pMesh->mVertexSize, true, false, 0);

	return S_OK;
}

HRESULT GPUMemoryManager::FillMeshBuffer(const bool isStaticMesh, const std::vector<geotypes::MeshData>& meshData, IndexedMesh* pMesh)
{
	geotypes::MeshData mergedMesh;
	for (uint32_t i = 0; i < meshData.size(); i++)
	{
		mergedMesh.vertices.insert(mergedMesh.vertices.end(), meshData[i].vertices.begin(), meshData[i].vertices.end());
		mergedMesh.indices.insert(mergedMesh.indices.end(), meshData[i].indices.begin(), meshData[i].indices.end());
	}

	return FillMeshBuffer(isStaticMesh, mergedMesh, pMesh);
}

HRESULT GPUMemoryManager::FillMeshBuffer(const bool isStaticMesh, const geotypes::SkinnedMeshData& meshData, SkeletalMesh* pMesh)
{
	pMesh->mVertexCount = static_cast<uint32_t>(meshData.vertices.size());
	pMesh->mIndexCount = static_cast<uint32_t>(meshData.indices.size());
	pMesh->mMesh = meshData;

	pMesh->mVB = new VertexBuffer(mDevice->GetDevice(), static_cast<UINT>(pMesh->mVertexSize * meshData.vertices.size()), isStaticMesh, (void*)meshData.vertices.data());

	pMesh->mIB = new IndexBuffer(mDevice->GetDevice(), static_cast<UINT>(INDEX_SIZE_BYTES * meshData.indices.size()), isStaticMesh, (void*)meshData.indices.data());

	pMesh->mSB = new StructuredBuffer(mDevice->GetDevice(), static_cast<UINT>(pMesh->mVertexSize * meshData.vertices.size()), sizeof(geotypes::SkinnedVertex), true, false, 0);

	return S_OK;
}

geotypes::MeshData GPUMemoryManager::InitAssimpMesh(const aiMesh* pAssimpMesh)
{
	geotypes::MeshData mesh;

	//int* areaCount = new int[pAssimpMesh->mNumVertices];
	//float* area = new float[pAssimpMesh->mNumVertices];

	aiVector3D zero(0.0f, 0.0f, 0.0f);
	for (uint32_t i = 0; i < pAssimpMesh->mNumVertices; i++)
	{
		aiVector3D* pPos = &(pAssimpMesh->mVertices[i]);
		aiVector3D* pNormal = ((pAssimpMesh->HasNormals()) ? &(pAssimpMesh->mNormals[i]) : &zero);
		aiVector3D* pTangent = ((pAssimpMesh->HasTangentsAndBitangents()) ? &(pAssimpMesh->mTangents[i]) : &zero);
		aiVector3D* pTexcoord = ((pAssimpMesh->HasTextureCoords(0)) ? &(pAssimpMesh->mTextureCoords[0][i]) : &zero);

		// Find for new min/max
		mesh.bounding.min.x = fminf(pPos->x, mesh.bounding.min.x);
		mesh.bounding.min.y = fminf(pPos->y, mesh.bounding.min.y);
		mesh.bounding.min.z = fminf(pPos->z, mesh.bounding.min.z);

		mesh.bounding.max.x = fmaxf(pPos->x, mesh.bounding.max.x);
		mesh.bounding.max.y = fmaxf(pPos->y, mesh.bounding.max.y);
		mesh.bounding.max.z = fmaxf(pPos->z, mesh.bounding.max.z);

		geotypes::Vertex v(
			pPos->x, pPos->y, pPos->z,
			pNormal->x, pNormal->y, pNormal->z,
			pTangent->x, pTangent->y, pTangent->z,
			pTexcoord->x, pTexcoord->y,
			0.f
		);

		mesh.vertices.push_back(v);

		//areaCount[i] = 0;
		//area[i] = 0.f;
	}

	// Absolute extent of the AABB
	mesh.bounding.size = XMFLOAT3(fabsf(mesh.bounding.max.x - mesh.bounding.min.x), fabsf(mesh.bounding.max.y - mesh.bounding.min.y), fabsf(mesh.bounding.max.z - mesh.bounding.min.z));

	for (uint32_t i = 0; i < pAssimpMesh->mNumFaces; i++)
	{
		const aiFace& pFace = pAssimpMesh->mFaces[i];
		
		mesh.indices.push_back(static_cast<INDEX_TYPE>(pFace.mIndices[0]));
		mesh.indices.push_back(static_cast<INDEX_TYPE>(pFace.mIndices[1]));
		mesh.indices.push_back(static_cast<INDEX_TYPE>(pFace.mIndices[2]));

		XMVECTOR p0 = XMLoadFloat3(&mesh.vertices[pFace.mIndices[0]].position);
		XMVECTOR p1 = XMLoadFloat3(&mesh.vertices[pFace.mIndices[1]].position);
		XMVECTOR p2 = XMLoadFloat3(&mesh.vertices[pFace.mIndices[2]].position);

		float a = XMVectorGetX(XMVector3Length(XMVector3Cross(p1 - p0, p2 - p0) / 2));

		mesh.vertices[pFace.mIndices[0]].area += a * XMVectorGetX(XMVector3AngleBetweenVectors(p1 - p0, p2 - p0)) / XM_PI;
		mesh.vertices[pFace.mIndices[1]].area += a * XMVectorGetX(XMVector3AngleBetweenVectors(p0 - p1, p2 - p1)) / XM_PI;
		mesh.vertices[pFace.mIndices[2]].area += a * XMVectorGetX(XMVector3AngleBetweenVectors(p0 - p2, p1 - p2)) / XM_PI;

		/*area[pFace.mIndices[0]] += a;
		area[pFace.mIndices[1]] += a;
		area[pFace.mIndices[2]] += a;

		areaCount[pFace.mIndices[0]]++;
		areaCount[pFace.mIndices[1]]++;
		areaCount[pFace.mIndices[2]]++;*/
	}

	/*for (uint32_t i = 0; i < pAssimpMesh->mNumVertices; i++)
	{
		if (areaCount[i] > 0)
		{
			mesh.vertices[i].area = area[i] /= (float)areaCount[i];
		}
	}*/

	//delete area;
	//delete areaCount;

	return mesh;
}

geotypes::SkinnedMeshData GPUMemoryManager::InitAssimpSkeletalMesh(const aiMesh* pAssimpMesh)
{
	geotypes::SkinnedMeshData mesh;

	aiVector3D zero(0.0f, 0.0f, 0.0f);
	for (uint32_t v = 0; v < pAssimpMesh->mNumVertices; v++)
	{
		aiVector3D* pPos = &(pAssimpMesh->mVertices[v]);
		aiVector3D* pNormal = ((pAssimpMesh->HasNormals()) ? &(pAssimpMesh->mNormals[v]) : &zero);
		aiVector3D* pTangent = ((pAssimpMesh->HasTangentsAndBitangents()) ? &(pAssimpMesh->mTangents[v]) : &zero);
		aiVector3D* pTexcoord = ((pAssimpMesh->HasTextureCoords(0)) ? &(pAssimpMesh->mTextureCoords[0][v]) : &zero);

		// Find for new min/max
		mesh.bounding.min.x = fminf(pPos->x, mesh.bounding.min.x);
		mesh.bounding.min.y = fminf(pPos->y, mesh.bounding.min.y);
		mesh.bounding.min.z = fminf(pPos->z, mesh.bounding.min.z);

		mesh.bounding.max.x = fmaxf(pPos->x, mesh.bounding.max.x);
		mesh.bounding.max.y = fmaxf(pPos->y, mesh.bounding.max.y);
		mesh.bounding.max.z = fmaxf(pPos->z, mesh.bounding.max.z);

		std::vector<animtypes::JointData> perVertexWeights;
		for (uint8_t b = 0; b < pAssimpMesh->mNumBones; b++)
		{
			for (uint32_t w = 0; w < pAssimpMesh->mBones[b]->mNumWeights; w++)
			{
				// look for the weigths of this vertex for each bone
				if (pAssimpMesh->mBones[b]->mWeights[w].mVertexId == v)
				{
					perVertexWeights.push_back({ b, pAssimpMesh->mBones[static_cast<size_t>(b)]->mWeights[w].mWeight });
				}
			}
		}

		std::qsort(perVertexWeights.data(), perVertexWeights.size(), sizeof(animtypes::JointData), [](const void* a, const void* b)
		{
			const animtypes::JointData arg1 = *static_cast<const animtypes::JointData*>(a);
			const animtypes::JointData arg2 = *static_cast<const animtypes::JointData*>(b);

			if (arg1.weight < arg2.weight) return 1;
			if (arg1.weight > arg2.weight) return -1;

			return 0;
		});

		if (perVertexWeights.size() < 4)
		{
			for (size_t i = perVertexWeights.size(); i < 4; i++)
			{
				perVertexWeights.push_back({ UINT8_MAX, 0.0f });
			}
		}

		geotypes::SkinnedVertex vert(
			pPos->x, pPos->y, pPos->z,
			pNormal->x, pNormal->y, pNormal->z,
			pTangent->x, pTangent->y, pTangent->z,
			pTexcoord->x, pTexcoord->y,
			0.0f,
			perVertexWeights[0].id, perVertexWeights[1].id, perVertexWeights[2].id, perVertexWeights[3].id,
			perVertexWeights[0].weight, perVertexWeights[1].weight, perVertexWeights[2].weight, perVertexWeights[3].weight
		);

		mesh.vertices.push_back(vert);
	}

	mesh.bounding.size = XMFLOAT3(fabsf(mesh.bounding.max.x - mesh.bounding.min.x), fabsf(mesh.bounding.max.y - mesh.bounding.min.y), fabsf(mesh.bounding.max.z - mesh.bounding.min.z));

	for (uint32_t i = 0; i < pAssimpMesh->mNumFaces; i++)
	{
		const aiFace& pFace = pAssimpMesh->mFaces[i];

		mesh.indices.push_back(static_cast<INDEX_TYPE>(pFace.mIndices[0]));
		mesh.indices.push_back(static_cast<INDEX_TYPE>(pFace.mIndices[1]));
		mesh.indices.push_back(static_cast<INDEX_TYPE>(pFace.mIndices[2]));
	}

	return mesh;
}

void GPUMemoryManager::InitAssimpAnimationData(const aiScene* pAssimpScene, const uint32_t meshIdx, animtypes::AnimationData* pData)
{
	const aiMesh* pMesh = pAssimpScene->mMeshes[meshIdx];

	pData->skeleton.invRootTransform = MathHelper::ToXMMatrix(pAssimpScene->mRootNode->mTransformation.Inverse());
	pData->skeleton.numBones = pAssimpScene->mMeshes[meshIdx]->mNumBones;

	// build matrix offset array
	for (uint8_t i = 0; i < pMesh->mNumBones; i++)
	{
		pData->skeleton.jointMapping.insert({ std::string(pMesh->mBones[i]->mName.C_Str()), i });
		pData->skeleton.jointProps.insert({ i, animtypes::JointInfo(MathHelper::ToXMMatrix(pMesh->mBones[i]->mOffsetMatrix), XMMatrixIdentity()) });
	}

	// navigate bind pose hierarchy
	pData->bindPose.mRoot = TraverseAssimpNode(pAssimpScene->mRootNode, new JointBindPose());

	// build animation clips
	for (uint8_t i = 0; i < pAssimpScene->mNumAnimations; i++)
	{
		// save clip data
		pData->clips[pAssimpScene->mAnimations[i]->mName.C_Str()].mDuration = (float)pAssimpScene->mAnimations[i]->mDuration;
		pData->clips[pAssimpScene->mAnimations[i]->mName.C_Str()].mTicksPerSecond = (float)pAssimpScene->mAnimations[i]->mTicksPerSecond;

		// go through channels (bones) and save poses for each keyframe in the clip
		for (uint8_t j = 0; j < pAssimpScene->mAnimations[i]->mNumChannels; j++)
		{
			const aiNodeAnim* pAnim = pAssimpScene->mAnimations[i]->mChannels[j];

			JointAnimPose* ap = new JointAnimPose();
			// the name of the current bone extracted from the animation clip
			ap->mName = pAnim->mNodeName.C_Str();
			
			// position keyframes
			for (uint16_t p = 0; p < pAnim->mNumPositionKeys; p++)
			{
				ap->mPosition.push_back(KeyFrame<XMFLOAT3>((float)pAnim->mPositionKeys[p].mTime, MathHelper::ToXMFloat3(pAnim->mPositionKeys[p].mValue)));
			}

			// scaling keyframes
			for (uint16_t s = 0; s < pAnim->mNumScalingKeys; s++)
			{
				ap->mScale.push_back(KeyFrame<XMFLOAT3>((float)pAnim->mScalingKeys[s].mTime, MathHelper::ToXMFloat3(pAnim->mScalingKeys[s].mValue)));
			}

			// rotation keyframes
			for (uint16_t r = 0; r < pAnim->mNumRotationKeys; r++)
			{
				ap->mRotationQuat.push_back(KeyFrame<XMFLOAT4>((float)pAnim->mRotationKeys[r].mTime, MathHelper::ToXMFloat4(pAnim->mRotationKeys[r].mValue)));
			}

			// save collection of keyframes poses on this clip
			pData->clips[pAssimpScene->mAnimations[i]->mName.C_Str()].mChannels.push_back(ap);
		}
	}
}

JointBindPose* GPUMemoryManager::TraverseAssimpNode(const aiNode* pNode, JointBindPose* pParent)
{
	JointBindPose* pJoint = new JointBindPose
	(
		pNode->mName.C_Str(),
		MathHelper::ToXMMatrix(pNode->mTransformation),
		pParent
	);

	for (uint8_t i = 0; i < pNode->mNumChildren; i++)
	{
		pJoint->mChildren.push_back(TraverseAssimpNode(pNode->mChildren[i], pParent));
	}

	return pJoint;
}

HRESULT GPUMemoryManager::ReadBinaryFile(const char* filename, std::string* byteStream)
{
	std::ifstream file(filename, std::ios::in | std::ios::binary);
	
	if (file.bad())
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Bad binary file." + HR_to_wstr(E_FAIL));
		return E_FAIL;
	}

	file.seekg(0, std::ios::end);
	byteStream->reserve((size_t)file.tellg());
	file.seekg(0, std::ios::beg);

	byteStream->assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

	file.close();

	return S_OK;
}

HRESULT GPUMemoryManager::MakeDefaultColorMap()
{
	HRESULT hr;

	mDefaultColorMap = new ColorMap();

	mDefaultColorMap->mWidth = 2;
	mDefaultColorMap->mHeight = 2;

	XMFLOAT4* color = new XMFLOAT4[4];
	color[0] = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	color[1] = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	color[2] = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	color[3] = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);

	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Width = 2;
	td.Height = 2;
	td.Usage = D3D11_USAGE::D3D11_USAGE_IMMUTABLE;
	td.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	td.Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;

	D3D11_SUBRESOURCE_DATA sd;
	ZeroMemory(&sd, sizeof(D3D11_SUBRESOURCE_DATA));
	sd.pSysMem = color;
	sd.SysMemPitch = static_cast<UINT>(2 * sizeof(XMFLOAT4));
	sd.SysMemSlicePitch = static_cast<UINT>(2 * 2 * sizeof(XMFLOAT4));

	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&td, &sd, &mDefaultColorMap->mTexture)))
	{
		delete mDefaultColorMap;
		return hr;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC rvd;
	ZeroMemory(&rvd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	rvd.Texture2D.MipLevels = 1;
	rvd.Texture2D.MostDetailedMip = 0;
	rvd.Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
	rvd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2D;

	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(mDefaultColorMap->mTexture, &rvd, &mDefaultColorMap->mSRV)))
	{
		delete mDefaultColorMap;
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::MakeDefaultCubeMap()
{
	HRESULT hr;

	mCubeMaps["Default"] = new CubeMap();
	mCubeMaps["Default"]->mWidth = 1;
	mCubeMaps["Default"]->mHeight = 1;

	D3D11_TEXTURE2D_DESC texDesc;
	ZeroMemory(&texDesc, sizeof(D3D11_TEXTURE2D_DESC));
	texDesc.Width = 1;
	texDesc.Height = 1;
	texDesc.MipLevels = 0;
	texDesc.ArraySize = 6;
	texDesc.SampleDesc = { 1, 0 };
	texDesc.Format = HDR_RT_FORMAT;
	texDesc.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	texDesc.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_FLAG::D3D11_BIND_RENDER_TARGET;
	texDesc.CPUAccessFlags = 0;
	texDesc.MiscFlags = D3D11_RESOURCE_MISC_FLAG::D3D11_RESOURCE_MISC_GENERATE_MIPS | D3D11_RESOURCE_MISC_FLAG::D3D11_RESOURCE_MISC_TEXTURECUBE;

	const XMFLOAT4 data[1] = { XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f) };
	D3D11_SUBRESOURCE_DATA sd[6];
	for (uint8_t i = 0; i < 6; i++)
	{
		sd[i].pSysMem = &data[0];
		sd[i].SysMemPitch = static_cast<UINT>(sizeof(XMFLOAT4));
		sd[i].SysMemSlicePitch = 0;
	}

	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&texDesc, &sd[0], &mCubeMaps["Default"]->mTexture)))
	{
		core::TsunamiFatalError(L"Failed to Create CubeMap: " + str_to_wstr("Default") + HR_to_wstr(hr));
	}

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	ZeroMemory(&rtvDesc, sizeof(D3D11_RENDER_TARGET_VIEW_DESC));
	rtvDesc.Format = texDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	rtvDesc.Texture2DArray.ArraySize = 1;
	rtvDesc.Texture2DArray.MipSlice = 0;
	for (uint8_t i = 0; i < 6; i++)
	{
		rtvDesc.Texture2DArray.FirstArraySlice = static_cast<UINT>(i);
		if (FAILED(hr = mDevice->GetDevice()->CreateRenderTargetView(mCubeMaps["Default"]->mTexture, &rtvDesc, &mCubeMaps["Default"]->mRTV[i])))
		{
			core::TsunamiFatalError(L"Failed to Create CubeMap: " + str_to_wstr("Default") + L"\nRTV #" + std::to_wstring(i) + HR_to_wstr(hr));
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	ZeroMemory(&srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	srvDesc.Format = texDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MipLevels = -1;
	srvDesc.TextureCube.MostDetailedMip = 0;
	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(mCubeMaps["Default"]->mTexture, &srvDesc, &mCubeMaps["Default"]->mSRV)))
	{
		core::TsunamiFatalError(L"Failed to Create CubeMap: " + str_to_wstr("Default") + L" SRV" + HR_to_wstr(hr));
	}

	mCubeMaps["Default"]->mViewport.Width = 1;
	mCubeMaps["Default"]->mViewport.Height = 1;
	mCubeMaps["Default"]->mViewport.TopLeftX = 0.0f;
	mCubeMaps["Default"]->mViewport.TopLeftY = 0.0f;
	mCubeMaps["Default"]->mViewport.MinDepth = 0.0f;
	mCubeMaps["Default"]->mViewport.MaxDepth = 1.0f;

	return S_OK;
}

HRESULT GPUMemoryManager::MakeDefaultNormalMap()
{
	HRESULT hr;

	mDefaultNormalMap = new ColorMap();

	mDefaultNormalMap->mWidth = 2;
	mDefaultNormalMap->mHeight = 2;

	XMFLOAT2* color = new XMFLOAT2[4];
	color[0] = XMFLOAT2(0.5f, 0.5f);
	color[1] = XMFLOAT2(0.5f, 0.5f);
	color[2] = XMFLOAT2(0.5f, 0.5f);
	color[3] = XMFLOAT2(0.5f, 0.5f);

	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Width = 2;
	td.Height = 2;
	td.Usage = D3D11_USAGE::D3D11_USAGE_IMMUTABLE;
	td.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	td.Format = DXGI_FORMAT::DXGI_FORMAT_R8G8_UNORM;

	D3D11_SUBRESOURCE_DATA sd;
	ZeroMemory(&sd, sizeof(D3D11_SUBRESOURCE_DATA));
	sd.pSysMem = color;
	sd.SysMemPitch = static_cast<UINT>(2 * sizeof(XMFLOAT2));
	sd.SysMemSlicePitch = static_cast<UINT>(2 * 2 * sizeof(XMFLOAT2));

	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&td, &sd, &mDefaultNormalMap->mTexture)))
	{
		delete mDefaultNormalMap;
		return hr;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC rvd;
	ZeroMemory(&rvd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	rvd.Texture2D.MipLevels = 1;
	rvd.Texture2D.MostDetailedMip = 0;
	rvd.Format = DXGI_FORMAT::DXGI_FORMAT_R8G8_UNORM;
	rvd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2D;

	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(mDefaultNormalMap->mTexture, &rvd, &mDefaultNormalMap->mSRV)))
	{
		delete mDefaultNormalMap;
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::MakeDefaultTexture2DRW()
{
	HRESULT hr;

	mTexture2DRW["Default"] = new Texture2DRW();

	mTexture2DRW["Default"]->mWidth = 2;
	mTexture2DRW["Default"]->mHeight = 2;

	XMFLOAT4* color = new XMFLOAT4[4];
	color[0] = XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f);
	color[1] = XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f);
	color[2] = XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f);
	color[3] = XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f);

	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(D3D11_TEXTURE2D_DESC));
	td.Width = 2;
	td.Height = 2;
	td.MipLevels = 1;
	td.ArraySize = 1;
	td.Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
	td.SampleDesc.Count = 1;
	td.SampleDesc.Quality = 0;
	td.Usage = D3D11_USAGE_DEFAULT;
	td.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	td.CPUAccessFlags = 0;
	td.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA sd;
	ZeroMemory(&sd, sizeof(D3D11_SUBRESOURCE_DATA));
	sd.pSysMem = color;
	sd.SysMemPitch = static_cast<UINT>(2 * sizeof(XMFLOAT4));
	sd.SysMemSlicePitch = static_cast<UINT>(2 * 2 * sizeof(XMFLOAT4));

	if (FAILED(hr = mDevice->GetDevice()->CreateTexture2D(&td, &sd, &mTexture2DRW["Default"]->mTexture)))
	{
		delete mTexture2DRW["Default"];
		return hr;
	}

	D3D11_UNORDERED_ACCESS_VIEW_DESC ud;
	ZeroMemory(&ud, sizeof(D3D11_UNORDERED_ACCESS_VIEW_DESC));
	ud.Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
	ud.ViewDimension = D3D11_UAV_DIMENSION::D3D11_UAV_DIMENSION_TEXTURE2D;
	ud.Texture2D.MipSlice = 0;
	if (FAILED(hr = mDevice->GetDevice()->CreateUnorderedAccessView(mTexture2DRW["Default"]->mTexture, &ud, &mTexture2DRW["Default"]->mUAV)))
	{
		delete mTexture2DRW["Default"];
		return hr;
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC rvd;
	ZeroMemory(&rvd, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	rvd.Texture2D.MipLevels = 1;
	rvd.Texture2D.MostDetailedMip = 0;
	rvd.Format = DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
	rvd.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_TEXTURE2D;
	if (FAILED(hr = mDevice->GetDevice()->CreateShaderResourceView(mTexture2DRW["Default"]->mTexture, &rvd, &mTexture2DRW["Default"]->mSRV)))
	{
		delete mTexture2DRW["Default"];
		return hr;
	}

	return S_OK;
}

HRESULT GPUMemoryManager::MakeDefaultSampler()
{
	return TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP, &mDefaultSampler);
}

HRESULT GPUMemoryManager::MakeDefaultNNSampler()
{
	return TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_POINT, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP, &mSamplers["DefaultNN"]);
}

HRESULT GPUMemoryManager::MakeDefaultFilterSampler()
{
	return TextureSampler(D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP, &mSamplers["DefaultFilter"]);
}

XMINT3 GPUMemoryManager::PackArray(float ** arrOut, float * arrIn, const XMINT4& dim, const XMINT3& extent)
{
	XMINT3 resExtent;

	XMINT3 texArraySize = XMINT3(extent.x / dim.x, extent.y / dim.y, extent.z / dim.z);

	int tt = dim.w;
	int stride;

	stride = min(texArraySize.x, tt);
	resExtent.x = (int)ceil(stride * dim.x);
	tt = (int)ceil((float)tt / stride);

	stride = min(texArraySize.y, tt);
	resExtent.y = (int)ceil(stride * dim.y);
	tt = (int)ceil((float)tt / stride);

	stride = min(texArraySize.z, tt);
	resExtent.z = stride * dim.z;

	float* tmp = new float[resExtent.x * resExtent.y * resExtent.z];

	for (int i = 0; i < resExtent.x * resExtent.y * resExtent.z; i++) tmp[i] = 1;

	for (int t = 0; t < dim.w; t++)
	{
		for (int z = 0; z < dim.z; z++)
		{
			for (int y = 0; y < dim.y; y++)
			{
				for (int x = 0; x < dim.x; x++)
				{
					XMINT3 offset;
					offset.x = (t % texArraySize.x) * dim.x;
					offset.y = ((t / texArraySize.x) % texArraySize.y) * dim.y;
					offset.z = (t / (texArraySize.x * texArraySize.y)) * dim.z;
					
					tmp[x + offset.x + (y + offset.y) * resExtent.x + (z + offset.z) * resExtent.x * resExtent.y] = arrIn[x + y * dim.x + z * dim.x * dim.y + t * dim.x * dim.y * dim.z];
				}
			}
		}
	}

	*arrOut = tmp;

	return resExtent;
}

GPUMemoryManager::SupportedFileExtensions GPUMemoryManager::IsFileExtensionSupported(const std::string& filename)
{
	std::string res = filename.substr(filename.find_last_of(".") + 1);
	
	for (auto ext : mFileExtensions)
	{
		if (res.compare(ext.second.c_str()) == 0)
		{
			return ext.first;
		}
	}

	return GPUMemoryManager::SupportedFileExtensions::EXTENSIONS_END;
}

void GPUMemoryManager::LoadOGG(const char* filename, AudioClip* clip)
{
	OggVorbis_File oggFile;
	// do not call fclose, managed by ogg vorbis file
	FILE* file;
	errno_t e;
	if ((e = fopen_s(&file, filename, "rb")) != 0)
	{
		char szBuffer[MAX_PATH];
		_strerror_s(szBuffer, MAX_PATH, nullptr);
		core::TsunamiFatalError(str_to_wstr(std::string(szBuffer)));
	}

	// for unknown reasons Windows don't like the ov_open method and requires a callback
#if defined(_WIN32) || defined(WIN32)
	if (ov_open_callbacks(file, &oggFile, nullptr, 0, OV_CALLBACKS_DEFAULT) < 0)
	{
		core::TsunamiFatalError(L"Failed to open OGG file: " + str_to_wstr(filename));
	}
#else
	if (ov_open(file, &oggFile, NULL, 0) < 0)
	{
		// TODO : [RESOUCE MANAGER] should not be blocking, right now we don't have a
		// default file to return instead so we crash
		core::TsunamiFatalError(L"Failed to open OGG file: " + str_to_wstr(filename));
	}
#endif
	
	vorbis_info* pInfo = ov_info(&oggFile, -1);

	clip->mWaveFormat.nChannels = pInfo->channels;
	clip->mWaveFormat.nSamplesPerSec = pInfo->rate;
	clip->mWaveFormat.wBitsPerSample = 16;	// ogg vorbis is always 16 bit
	clip->mWaveFormat.nAvgBytesPerSec = pInfo->rate * pInfo->channels * 2;
	clip->mWaveFormat.nBlockAlign = 2 * pInfo->channels;
	clip->mWaveFormat.wFormatTag = WAVE_FORMAT_PCM;

	// buffer data
	int bitStream;
	long bytesRead;
	int endian = 0;
	char* buffer = new char[BUFFER_SIZE_32K];

	do 
	{
		// read number of bytes in temp buffer
		bytesRead = ov_read(&oggFile, buffer, BUFFER_SIZE_32K, endian, 2, 1, &bitStream);
		
		// append data to tmp vector
		clip->mRawData.insert(clip->mRawData.end(), buffer, buffer + bytesRead);
	} while (bytesRead > 0);
	// also does fclose for file
	ov_clear(&oggFile);

	// check if we read anything at all
	assert(clip->mRawData.size() > 0);

	// assign raw data to XAUDIO2_BUFFER for submit
	clip->mBuffer.pAudioData = (BYTE*)&clip->mRawData[0];
	clip->mBuffer.AudioBytes = static_cast<UINT32>(clip->mRawData.size());

	// Create voice
	HRESULT hr;
	//mAudioEngine->CreateSourceVoice(&clip->mSource, clip->mWaveFormat);
	if (FAILED(hr = mAudioEngine->mEngine->CreateSourceVoice(&clip->mSource, &clip->mWaveFormat)))
	{
		core::TsunamiFatalError(L"Failed to create source voice." + HR_to_wstr(hr));
	}

	// write default data
	clip->mSource->SetVolume(clip->mVolume);

	if (FAILED(hr = clip->mSource->SubmitSourceBuffer(&clip->mBuffer)))
	{
		core::TsunamiFatalError(L"Failed to create souce voice queue." + HR_to_wstr(hr));
	}
}
