#include "Logger.h"

Logger::Logger()
{
}

Logger::~Logger()
{
}

void Logger::Init(const std::wstring& consoleTitle)
{
	if (!mConsoleOpen)
	{
		AllocConsole();
		AttachConsole(GetCurrentProcessId());

		int hConsole;
		FILE* pCout;
		mhStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
		hConsole = _open_osfhandle((uint64_t)mhStdOut, _O_TEXT);
		pCout = _fdopen(hConsole, "w");

		freopen_s(&pCout, "CONIN$", "r", stdin);
		freopen_s(&pCout, "CONOUT$", "w", stdout);
		freopen_s(&pCout, "CONOUT$", "w", stderr);

		SetConsoleTitle(consoleTitle.c_str());

		GetConsoleMode(mhStdOut, &mfdwDefaultMode);

		GetConsoleScreenBufferInfo(mhStdOut, &mDefaultFramebuffer);

		mConsoleOpen = true;
	}
}

void Logger::Destroy()
{
	FreeConsole();
}

void Logger::Msg(const LOG_LAYER layer, const std::wstring& msg)
{
	switch (layer)
	{
	case LOG_LAYER::LOG_ERROR:
		WriteHeader("[ERROR]: ", LOG_LAYER::LOG_ERROR);
		break;
	case LOG_LAYER::LOG_WARNING:
		WriteHeader("[WARNING]: ", LOG_LAYER::LOG_WARNING);
		break;
	case LOG_LAYER::LOG_RENDER:
		WriteHeader("[RENDER]: ", LOG_LAYER::LOG_RENDER);
		break;
	case LOG_LAYER::LOG_GENERAL:
		WriteHeader("[GENERAL]: ", LOG_LAYER::LOG_GENERAL);
		break;
	default:
		break;
	}

	std::wcout << msg << std::endl;
}

void Logger::Msg(const LOG_LAYER layer, const std::string& msg)
{
	switch (layer)
	{
	case LOG_LAYER::LOG_ERROR:
		WriteHeader("[ERROR]: ", LOG_LAYER::LOG_ERROR);
		break;
	case LOG_LAYER::LOG_WARNING:
		WriteHeader("[WARNING]: ", LOG_LAYER::LOG_WARNING);
		break;
	case LOG_LAYER::LOG_RENDER:
		WriteHeader("[RENDER]: ", LOG_LAYER::LOG_RENDER);
		break;
	case LOG_LAYER::LOG_GENERAL:
		WriteHeader("[GENERAL]: ", LOG_LAYER::LOG_GENERAL);
		break;
	default:
		break;
	}

	std::cout << msg << std::endl;
}

void Logger::Exception(const std::error_code& code, const char* errorString, const std::wstring & msg)
{
	WriteHeader("[EXCEPTION] Code:", LOG_LAYER::LOG_EXCEPTION);

	std::wcout << code << " || Message: " << errorString << std::endl << "\tCustom Message: " << std::endl;
}

void Logger::WriteHeader(const std::string& head, const LOG_LAYER layer)
{
	SetConsoleTextAttribute(mhStdOut, layer | FOREGROUND_INTENSITY);
	std::cout << head.c_str();
	SetConsoleTextAttribute(mhStdOut, mDefaultFramebuffer.wAttributes);
}

