#ifndef _STACK_H_
#define _STACK_H_

#include <cstdint>
#include <cstdlib>
#include "Logger.h"

template <typename T>
class Stack
{
public:
	// Allow reallocation
	Stack(const uint32_t initialSize, const bool canResize = true);
	// Prevent reallocation
	Stack();
	~Stack();

	void Push(const T& val);
	T* Pop();
	T* Top();

private:
	T* mData;
	uint32_t mSize;
	uint32_t mCurrElem;
	bool mCanResize;
};

#endif