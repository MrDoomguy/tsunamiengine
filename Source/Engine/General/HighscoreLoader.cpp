#include "HighscoreLoader.h"
#include "Logger.h"

HighscoreLoader::HighscoreLoader()
{
}

HighscoreLoader::~HighscoreLoader()
{
}

void HighscoreLoader::Init(const std::wstring & filename, int maxEntries)
{
	mHighscoreList.clear();

	mPath = filename;
	mMaxEntries = maxEntries;

	ReadList(mPath.c_str());

	for (int i = mHighscoreList.size(); i < mMaxEntries; i++)
	{
		mHighscoreList.push_back(HighscoreEntry("-", 0));
	}

	WriteFile();
}

void HighscoreLoader::WriteFile()
{
	if (!this->WriteList(mPath.c_str()))
		std::cout << mPath.c_str() << " Highscore list not found!" << std::endl;
}

bool HighscoreLoader::ReadList(const wchar_t * filename)
{
	std::ifstream file(filename, std::ios_base::in);
	if (!file.good()) return false;

	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);

	try
	{
		std::string line;
		while (!file.eof())
		{
			std::getline(file, line);

			std::istringstream iss(line);

			std::string key;
			int value;
			iss >> key >> value;

			AddEntry(key, value);
		}

		file.close();
	}
	catch (const std::ifstream::failure& e)
	{
		Logger::instance().Exception(e.code(), e.what(), L"Highscore Loader Failure, unable to open requested file.");
		return false;
	}

	return true;
}

bool HighscoreLoader::WriteList(const wchar_t * filename)
{
	std::ofstream file;
	file.exceptions(std::ofstream::failbit | std::ofstream::badbit);

	try
	{
		file.open(filename, std::ios_base::out);

		std::ostringstream content;

		// serialization step
		for (auto& p : mHighscoreList)
		{
			content << p.first << " " << p.second << std::endl;
		}
		// writeback to file
		std::string str = content.str();
		str.pop_back();
		file << str;

		file.close();
	}
	catch (const std::ofstream::failure& e)
	{
		Logger::instance().Exception(e.code(), e.what(), L"Highscore Loader Failure, unable to open requested file.");
		return false;
	}

	return true;
}
