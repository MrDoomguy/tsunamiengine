#ifndef _FILESYSTEM_H_
#define _FILESYSTEM_H_

#include "TsunamiStd.h"
#include "../../Engine/Render/Data/IShader.h"

#include <tchar.h>
#include <strsafe.h>
#pragma comment(lib, "User32.lib")

namespace Filesystem
{
	static std::vector<std::wstring> GetFilenames(const std::wstring& pathToFolder)
	{
		std::vector<std::wstring> filenames;
		WIN32_FIND_DATA ffd;
		LARGE_INTEGER fileSize;
		TCHAR szDir[MAX_PATH];
		size_t lengthOfArg;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		DWORD dwError = 0;

		// Check that the input path plus 3 is no longer than MAX_PATH.
		// Three chars are for the "\*" plus NULL appended.
		StringCchLength(pathToFolder.c_str(), MAX_PATH, &lengthOfArg);

		if (lengthOfArg > (MAX_PATH - 3))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Directory name is too long. Abort.");
			return;
		}

		// Prepare string for use with FindFile func.
		// First copy the string to a buffer, then append '\*' to the dir name.
		StringCchCopy(szDir, MAX_PATH, pathToFolder.c_str());
		StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

		// Find first fiel in directory
		hFind = FindFirstFile(szDir, &ffd);
		if (hFind == INVALID_HANDLE_VALUE)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to read first file from directory: " + pathToFolder);
		}

		// List all the files in the directory
		do
		{
			if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				filenames.push_back(ffd.cFileName);
			}
		} while (FindNextFile(hFind, &ffd) != 0);

		dwError = GetLastError();
		if (dwError != ERROR_NO_MORE_FILES)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to retrieve filenames.");
			return;
		}

		FindClose(hFind);
	}

	static uint8_t GetShaderType(std::wstring filename)
	{
		if (filename.find_first_of(L"_VS_"))
		{
			return ShaderType::VERTEX;
		}
		else if (filename.find_first_of(L"_PS_"))
		{
			return ShaderType::PIXEL;
		}
		else if (filename.find_first_of(L"_CS_"))
		{
			return ShaderType::COMPUTE;
		}
	}
}

#endif
