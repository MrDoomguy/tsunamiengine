#ifndef _TIMER_H_
#define _TIMER_H_

#include <chrono>
#include <cstdint>

class Timer
{
public:
	Timer();

	float TotalTime() const;
	double DeltaTime() const;

	void Reset();
	void Start();
	void Stop();
	void Tick();

private:
	typedef std::chrono::high_resolution_clock HRclock_t;
	typedef HRclock_t::time_point Timestamp_t;

	double mDeltaTime;
	double mSecondsPerCount;

	int64_t mDeltaPause;

	Timestamp_t mPrevTime;
	Timestamp_t mBaseTime;
	Timestamp_t mStopTime;

	bool mStopped;
};

#endif