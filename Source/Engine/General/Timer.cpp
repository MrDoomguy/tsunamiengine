#include "Timer.h"

Timer::Timer()
	: mDeltaPause(0), mDeltaTime(0.0), mSecondsPerCount(0.0), mStopped(false)
{
	mSecondsPerCount = 1.0 / static_cast<double>(HRclock_t::period::den);
}

float Timer::TotalTime() const
{
	if (mStopped)
	{
		int64_t live = (mStopTime - mBaseTime).count();
		return static_cast<float>((live - mDeltaPause) * mSecondsPerCount);
	}
	else
	{
		int64_t total = (HRclock_t::now() - mBaseTime).count();
		return static_cast<float>((total - mDeltaPause) * mSecondsPerCount);
	}
	
}

double Timer::DeltaTime() const
{
	return mDeltaTime;
}


void Timer::Reset()
{
	auto currentTime = HRclock_t::now();

	mBaseTime = currentTime;
	mPrevTime = currentTime;
}

void Timer::Start()
{
	if (mStopped)
	{
		auto currentTime = HRclock_t::now();

		mDeltaPause += (currentTime - mStopTime).count();

		mPrevTime = currentTime;
		mStopped = false;
	}
}

void Timer::Stop()
{
	if (!mStopped)
	{
		auto currentTime = HRclock_t::now();

		mStopTime = currentTime;
		mStopped = true;
	}
}

void Timer::Tick()
{
	if (mStopped)
	{
		mDeltaTime = 0.0;
		return;
	}

	auto currentTime = HRclock_t::now();

	std::chrono::duration<double> delta(currentTime - mPrevTime);
	mDeltaTime = delta.count();

	mPrevTime = currentTime;

	if (mDeltaTime < 0.0)
	{
		mDeltaTime = 0.0;
	}
}
