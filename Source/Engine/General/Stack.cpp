#include "Stack.h"

template<typename T>
Stack<T>::Stack(const uint32_t initialSize, const bool canResize)
{
	mCanResize = canResize;
	mSize = initialSize;
	mData = new T[mSize];
	mCurrElem = 0;
}

template<typename T>
Stack<T>::Stack()
{
	mCanResize = false;
	mSize = 16;
	mData = new T[mSize];
	mCurrElem = 0;
}

template<typename T>
Stack<T>::~Stack()
{
	delete mData;
}

template<typename T>
void Stack<T>::Push(const T& val)
{
	if (mCurrElem == mSize && mCanResize)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Stack OVERFLOW occurred. Reallocating.");

		mSize *= 2;
		mData = (T*)realloc(mData, mSize);
	}
	else
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Stack OVERFLOW occurred, cannot reallocate. NOP.");
		return;
	}

	mData[mCurrElem++] = val;
}

template<typename T>
T* Stack<T>::Pop()
{
	if (mCurrElem == 0)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Stack is empty. POP failed.");
		return nullptr;
	}
	else 
		return &mData[mCurrElem--];
}

template<typename T>
T* Stack<T>::Top()
{
	if (mCurrElem == 0)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Stack is empty. TOP failed.");
		return nullptr;
	}
	
	return &mData[mCurrElem - 1];
}

template class Stack<std::string>;
