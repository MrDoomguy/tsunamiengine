#ifndef _ANIMATOR_H_
#define _ANIMATOR_H_

#include "IState.h"
#include "BlendState.h"

// TODO: make customizable (like in unity)
// TODO: make some updatefct to update state

class Animator
{
public:
	Animator() { mActState = 0; }
	~Animator() 
	{
		for (auto s : mStates) { delete s; }
	}

	void SetState(IState* state) { mStates.push_back(state); }

	void SetBlendParams(const std::vector<float>& params) { dynamic_cast<BlendState*>(mStates[mActState])->SetFramePos(params); };

	std::vector<animtypes::ClipBlendInfo> GetUpdatedBlendInfo() { return mStates[mActState]->GetUpdatedBlendInfo(); };

private:
	// TODO: use map with names to identify states
	// TODO: use map of parameters (map with names)
	// TODO: use list of rules depending on parameters to switch between states (interpolated)
	std::vector<IState*> mStates;
	int mActState;
};

#endif