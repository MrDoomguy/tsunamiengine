#ifndef _JOINTANIMPOSE_H_
#define _JOINTANIMPOSE_H_

#include "../General/TsunamiStd.h"

template <typename T>
struct KeyFrame
{
	float time;
	T value;

	KeyFrame(const float _time, const T& _value) :
		time{ _time }, value{ _value }
	{}

	KeyFrame() {}
};

class JointAnimPose
{
public:
	JointAnimPose() {}
	~JointAnimPose() {}

	std::string mName;
	// one for each keyframe
	std::vector<KeyFrame<DirectX::XMFLOAT3>> mPosition;
	std::vector<KeyFrame<DirectX::XMFLOAT4>> mRotationQuat;
	std::vector<KeyFrame<DirectX::XMFLOAT3>> mScale;
};

#endif
