#ifndef _JOINTBINDPOSE_H_
#define _JOINTBINDPOSE_H_

#include "../General/TsunamiStd.h"

class JointBindPose
{
public:
	JointBindPose(const std::string& name, const XMMATRIX& transform, JointBindPose* parent) :
		mName{ name }, mTransform{ transform }, mParent{ parent }
	{
	}

	JointBindPose() :
		mParent{ nullptr }, mTransform{ XMMatrixIdentity() }
	{
	}
	
	~JointBindPose()
	{
		if (mChildren.size() > 0)
		{
			for (auto c : mChildren)
			{
				delete c;
			}
		}
	}

	std::string mName;
	DirectX::XMMATRIX mTransform;
	JointBindPose* mParent;
	std::vector<JointBindPose*> mChildren;
};

#endif
