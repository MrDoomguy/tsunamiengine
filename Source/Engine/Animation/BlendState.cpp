#include "BlendState.h"

BlendState::BlendState(BlendTree* pTree) :
	mTree{ pTree }
{
	mLoop = true;
}

BlendState::BlendState(BlendTreeNode& node) :
	mTree{ nullptr }
{
	mTree = new BlendTree(node);
	mLoop = true;
}

BlendState::BlendState(const uint8_t blendTreeDim)
{
	mTree = new BlendTree(blendTreeDim);
	mLoop = true;
}

BlendState::~BlendState()
{
	delete mTree;
}

void BlendState::AddTreeNode(const std::string& name, const std::vector<float>& pos)
{
	mTree->AddChild(name, pos);
}

void BlendState::SetFramePos(const std::vector<float>& framePos)
{
	mTree->SetPos(framePos);
}

std::vector<animtypes::ClipBlendInfo> BlendState::GetUpdatedBlendInfo()
{
	return mTree->GetBlendInfo();
}
