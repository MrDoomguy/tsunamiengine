#ifndef _ANIMATIONCLIP_H_
#define _ANIMATIONCLIP_H_

#include "JointAnimPose.h"

class AnimationClip
{
public:
	AnimationClip() {}
	~AnimationClip() {}

	JointAnimPose* FindAnimPose(const std::string& jointName)
	{
		for (auto j : mChannels)
		{
			if (j->mName == jointName)
				return j;
		}

		return nullptr;
	}

	float mDuration;
	float mTicksPerSecond;
	// bones animated in this clip
	std::vector<JointAnimPose*> mChannels;
};

#endif