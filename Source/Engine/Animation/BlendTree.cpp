#include "BlendTree.h"

BlendTree::BlendTree(const uint8_t dimensions)
{
	mDim = dimensions;
	for (int i = 0; i < mDim; i++) mCurrPos.push_back(0.f);
}

BlendTree::BlendTree(BlendTreeNode& head)
{
	mDim = static_cast<uint8_t>(head.Pos().size());
	mNodes.push_back(head);
	for (uint8_t i = 0; i < mDim; i++) mCurrPos.push_back(0.f);
}

BlendTree::~BlendTree()
{
}

void BlendTree::AddChild(const std::string& name, const std::vector<float>& params)
{
	if (static_cast<uint8_t>(params.size()) == mDim)
		mNodes.push_back(BlendTreeNode(name, params));
	else
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"[BlendTree]: Failed to add child, wrong number of dimensions. NOP.");
}

void BlendTree::SetPos(const std::vector<float>& pos)
{
	if (static_cast<uint8_t>(pos.size()) == mDim)
	{
		mCurrPos = pos;
		for (uint8_t i = 0; i < static_cast<uint8_t>(mCurrPos.size()); i++)
		{
			if (mCurrPos[i] < -1) mCurrPos[i] = -1;
			else if (mCurrPos[i] > 1) mCurrPos[i] = 1;
		}
	}
	else
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"[BlendTree]: Failed to set weights for this frame, wrong number of dimensions. NOP.");
}

std::vector<animtypes::ClipBlendInfo> BlendTree::GetBlendInfo()
{
	//auto res = mRoot->GetBlendInfo(mCurrPos);
	std::vector<animtypes::ClipBlendInfo> res;
	std::vector<std::thread> workers;
	//std::vector<float>& currPos = mCurrPos;
	for (auto n : mNodes)
	{
		/*workers.push_back(std::thread([&res, &n, currPos]() mutable
		{
			res.push_back(n.GetBlendInfo(currPos));
		}));*/
		res.push_back(n.GetBlendInfo(mCurrPos));
	}

	/*std::for_each(workers.begin(), workers.end(), [](std::thread& t)
	{
		t.join();
	});*/

	std::vector<animtypes::ClipBlendInfo> finalRes;
	// take out all non-useful weights
	// calculate sum
	float sum = 0.0f;
	for (auto w : res)
	{
		if (w.weight > 0.0f)
		{
			sum += w.weight;
			finalRes.push_back(w);
		}
	}

	// normalize weights
	for (int i = 0; i < finalRes.size(); i++)
	{
		finalRes[i].weight /= sum;
	}

	return finalRes;
}
