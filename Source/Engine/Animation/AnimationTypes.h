#pragma once

#include "Animation.h"
#include "AnimationClip.h"

namespace animtypes
{
	// used for mesh construction
	struct JointData
	{
		uint8_t id;
		float weight;

		JointData(const uint8_t _id, const float _weight) :
			id{ _id }, weight{ _weight }
		{ }

		JointData()
			: id{ UINT8_MAX }, weight{ 0.0f }
		{ }
	};

	struct JointInfo
	{
		XMMATRIX offset;
		XMMATRIX finalTransform;

		JointInfo(const XMMATRIX& _offset, const XMMATRIX& _finalTransform) :
			offset{ _offset }, finalTransform{ _finalTransform }
		{ }

		JointInfo() :
			offset{ XMMatrixIdentity() }, finalTransform{ XMMatrixIdentity() }
		{ }
	};

	struct Skeleton
	{
		// bone name -> index
		std::unordered_map<std::string, uint8_t> jointMapping;
		
		// bone index -> transform	
		std::unordered_map<uint8_t, JointInfo> jointProps;

		XMMATRIX invRootTransform = XMMatrixIdentity();

		uint8_t numBones;
	};

	struct AnimationData
	{
		// bind pose of the animation
		Animation bindPose;
		// collections of keyframes
		std::unordered_map<std::string, AnimationClip> clips;
		// the skeleton containing joint infos
		Skeleton skeleton;
	};

	struct ClipBlendInfo
	{
		std::string name = "";
		float weight = 0.0f;

		ClipBlendInfo(const std::string& _name, const float _weight)
		{
			name = _name;
			weight = _weight;
		}

		ClipBlendInfo()
		{
			name = "";
			weight = 0.0f;
		}
	};

	static uint8_t MAX_JOINTS = 128;
}
