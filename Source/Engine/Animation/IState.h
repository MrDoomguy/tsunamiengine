#ifndef _ISTATE_H_
#define _ISTATE_H_

#include "AnimationTypes.h"
#include <cstdint>

class IState
{
public:
	IState() {}
	virtual ~IState() {}

	void SetLoop(const bool loop) { mLoop = true; }
	const bool IsLoop() const { return mLoop; }

	virtual std::vector<animtypes::ClipBlendInfo> GetUpdatedBlendInfo() = 0;

protected:
	uint8_t mID;
	bool mLoop;
};

#endif