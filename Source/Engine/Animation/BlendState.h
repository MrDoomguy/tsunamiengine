#ifndef _BLENDSTATE_H_
#define _BLENDSTATE_H_

#include "IState.h"
#include "BlendTree.h"

class BlendState : public IState
{
public:
	BlendState(BlendTree* pTree);
	BlendState(BlendTreeNode& node);
	BlendState(const uint8_t blendTreeDim);
	~BlendState();

	//
	// Blend Tree Interface
	//
	// Build Tree from here
	void AddTreeNode(const std::string& name, const std::vector<float>& pos);
	// Call this to update interpolation weights
	// Called in scripts to update on-screen behaviour
	void SetFramePos(const std::vector<float>& framePos);
	// Get the names/weights of the clips to be interpolated for the given weights
	// Called in AnimationComponent to fire traverse
	virtual std::vector<animtypes::ClipBlendInfo> GetUpdatedBlendInfo() override;

private:
	BlendTree* mTree;
};

#endif
