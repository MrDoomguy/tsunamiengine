#ifndef _MOTIONSTATE_H_
#define _MOTIONSTATE_H_

#include "IState.h"
#include <string>

class MotionState : public IState
{
public:
	MotionState();
	~MotionState();

	std::vector<animtypes::ClipBlendInfo> GetUpdatedBlendInfo() override {}

private:
	std::string mClipName;
};

#endif
