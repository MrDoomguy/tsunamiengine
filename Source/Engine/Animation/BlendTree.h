#ifndef _BLENDTREE_H_
#define _BLENDTREE_H_

#include "AnimationTypes.h"
#include "BlendTreeNode.h"
//#include <thread>

class BlendTree
{
public:
	BlendTree(const uint8_t dimensions);
	BlendTree(BlendTreeNode& head);
	BlendTree() = delete;
	~BlendTree();

	void AddChild(const std::string& name, const std::vector<float>& params);
	void SetPos(const std::vector<float>& pos);
	// Returns the normalized weights list without outliers
	std::vector<animtypes::ClipBlendInfo> GetBlendInfo();

private:
	//BlendTreeNode* mRoot;
	std::vector<BlendTreeNode> mNodes;
	uint8_t mDim;
	std::vector<float> mCurrPos;
};

#endif
