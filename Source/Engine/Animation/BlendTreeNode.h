#ifndef _BLENDTREENODE_H_
#define _BLENDTREENODE_H_

#include "AnimationTypes.h"

class BlendTreeNode
{
public:
	explicit BlendTreeNode(const std::string& clipName, const std::vector<float>& pos)
	{
		mName = clipName;
		mPos = pos;
	}
	BlendTreeNode() = delete;

	~BlendTreeNode() {}

	std::string& Name() { return mName; }
	std::vector<float>& Pos() { return mPos; }

	// returns a non-normalized weight!
	animtypes::ClipBlendInfo GetBlendInfo(const std::vector<float>& framePos)
	{
		float w = 0.0f;
		float d_max = 0.0f;
		for (uint8_t i = 0; i < mPos.size(); i++)
		{
			//w += (mPos[static_cast<size_t>(i)] - framePos[static_cast<size_t>(i)])*(mPos[static_cast<size_t>(i)] - framePos[static_cast<size_t>(i)]);
			//w += (framePos[static_cast<size_t>(i)] - mPos[static_cast<size_t>(i)])*(framePos[static_cast<size_t>(i)] - mPos[static_cast<size_t>(i)]);
			d_max = std::fmaxf(std::fabsf(framePos[i] - mPos[i]), d_max);
		}
		//w = 1.0001f - sqrtf(w);
		w = 1.0f - d_max;

		/*if (w < 0.0f)
			w = 0.0f;*/

		return animtypes::ClipBlendInfo(mName, w);
	}

private:
	std::string mName;
	std::vector<float> mPos;
};

/*class BlendTreeNode
{
public:
	explicit BlendTreeNode(const std::string& clipName, const std::vector<float>& pos)
	{
		mName = clipName;
		mPos = pos;
	}
	BlendTreeNode() = delete;

	~BlendTreeNode()
	{
		for (auto c : mChildren)
		{
			delete c;
		}
	}

	std::string& Name() { return mName; }
	std::vector<float>& Pos() { return mPos; }
	std::vector<BlendTreeNode*>& Children() { return mChildren; }

	void AddChild(const std::string& parent, const std::string& clipName, const std::vector<float>& pos)
	{
		if(mName.compare(parent) == 0)
			mChildren.push_back(new BlendTreeNode(clipName, pos));
		else
		{
			for (auto c : mChildren)
			{
				c->AddChild(parent, clipName, pos);
			}
		}
	}

	// returns a non-normalized weight!
	std::vector<animtypes::ClipBlendInfo> GetBlendInfo(const std::vector<float>& framePos)
	{
		std::vector<animtypes::ClipBlendInfo> res;
		if (mChildren.empty())
		{
			float w = 0.0f;
			for (uint8_t i = 0; i < mPos.size(); i++)
			{
				w += (mPos[static_cast<size_t>(i)] - framePos[static_cast<size_t>(i)])*(mPos[static_cast<size_t>(i)] - framePos[static_cast<size_t>(i)]);
			}
			w = 1.0f - sqrtf(w);

			if (w < 0.0f)
				w = 0.0f;

			res.push_back(animtypes::ClipBlendInfo(mName, w));
		}
		else
		{
			for (auto c : mChildren)
			{
				auto r = c->GetBlendInfo(framePos);
				res.insert(std::end(res), std::begin(r), std::end(r));
			}
		}

		return res;
	}

private:
	std::string mName;
	std::vector<float> mPos;

	std::vector<BlendTreeNode*> mChildren;
};*/

#endif
