#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include "JointBindPose.h"

class Animation
{
public:
	Animation() : mRoot{ nullptr } {}
	~Animation() { delete mRoot; }

	JointBindPose* mRoot;
};

#endif