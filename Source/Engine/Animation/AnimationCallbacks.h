#ifndef _ANIMATIONCALLBACKS_H_
#define _ANIMATIONCALLBACKS_H_

#include <string>
#include <vector>

typedef bool(*PFN_TRIGGER_CALLBACK)(std::string* pClipToPlay);

typedef struct PassivePoseInfo
{
	std::string name;
	float weight;
} PassivePoseInfo;
typedef std::vector<PassivePoseInfo>(*PFN_PASSIVE_POSE_CALLBACK)(void);

#endif