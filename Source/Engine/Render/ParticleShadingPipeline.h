#pragma once

#include "IComputePipeline.h"

class ParticleShadingPipeline : public IComputePipeline
{
public:
	struct PerFrame : public IPipeline::IPerFrame
	{
		XMUINT2 screenSize;
		XMFLOAT2 _pad;
	};

	ParticleShadingPipeline();
	~ParticleShadingPipeline();

	void Init(const uint32_t width, const uint32_t height) override;

	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;

	void Dispatch(ID3D11DeviceContext* pContext);
	void SetIrrMap(CubeMap* pIrrMap);
	void SetRaytracerDepth(Texture2DRW* raytracerDepth);
	void SetRasterDepth(RenderTarget* rasterDepth);
	void SetParticleIntensity(Texture2DRW* particleIntensity);

	void SetNormalAndSearchRadii(RenderTarget* normalAndSearchRadii);
	void SetParticleDepth(RenderTarget* particleDepth);

	void SetShadowTarget(Texture2DRW* shadowTarget);
	void SetRadianceTarget(Texture2DRW* radianceTarget);

	RenderTarget* GetParticleDepth();

private:
	//Texture2DRW* mOutput;
	CubeMap* mIrrMap;
	ID3D11SamplerState* mColSampler;

	Texture2DRW* mRaytracerDepth;
	RenderTarget* mRasterDepth;
	RenderTarget* mNormalAndSearchRadii;
	RenderTarget* mParticleDepth;
	Texture2DRW* mParticleIntensity;

	Texture2DRW* mShadow;
	Texture2DRW* mRadiance;
};
