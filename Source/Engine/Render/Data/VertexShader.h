#ifndef _VERTEXSHADER_H_
#define _VERTEXSHADER_H_

#include "IShader.h"
#include "../Data/VertexFormat.h"

class VertexShader : public IShader
{
	friend class GPUMemoryManager;
public:
	VertexShader() : mpObject{ nullptr }, mpVertexLayout{ nullptr }, mLayout { InputLayoutDesc::Default } {}
	VertexShader(const std::vector<D3D11_INPUT_ELEMENT_DESC>& layout) : mpObject{ nullptr }, mpVertexLayout{ nullptr }, mLayout{ layout } {}
	~VertexShader() { SAFE_RELEASE(mpObject); }

	virtual HRESULT Load(ID3D11Device* pDevice, const IShader::Desc& desc) override
	{
		HRESULT hr;

		if (FAILED(hr = CompileFromFile(desc)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to compile Vertex Shader.");
			return hr;
		}

		if (FAILED(hr = pDevice->CreateVertexShader(mpBlob->GetBufferPointer(), mpBlob->GetBufferSize(), nullptr, &mpObject)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to Create Vertex Shader.");
			return hr;
		}

		if (FAILED(hr = pDevice->CreateInputLayout(mLayout.data(), static_cast<UINT>(mLayout.size()), mpBlob->GetBufferPointer(), mpBlob->GetBufferSize(), &mpVertexLayout)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create Vertex Shader Input Layout.");
			return hr;
		}

		return S_OK;
	}

	ID3D11VertexShader* GetObjectPtr() { return mpObject; }
	ID3D11InputLayout* GetVertexLayout() { return mpVertexLayout; }

private:
	ID3D11VertexShader* mpObject;
	ID3D11InputLayout* mpVertexLayout;
	std::vector<D3D11_INPUT_ELEMENT_DESC> mLayout;
};

#endif