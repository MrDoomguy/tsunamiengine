#ifndef _DIFFUSEMATERIAL_H_
#define _DIFFUSEMATERIAL_H_

#include "ColorMap.h"
#include "ConstantBuffer.h"

#include "../../General/GPUMemoryManager.h"

//[(deprecated),"use pbs instead"]
class DiffuseMaterial : public IMaterial
{
public:
	struct MaterialData
	{
		DirectX::XMFLOAT4 diffuse;
		DirectX::XMFLOAT4 specular;
		DirectX::XMFLOAT4 ambient;

		MaterialData() :
			diffuse{ DirectX::XMFLOAT4(0.75f, 0.75f, 0.75f, 1.0f) },
			specular{ DirectX::XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f) },
			ambient{ DirectX::XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f) } 
		{}
	};

	struct PerObject
	{
		MaterialData mat;
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX invWorldTranspose;

		PerObject() :
			world{ XMMatrixIdentity() },
			invWorldTranspose{ XMMatrixIdentity() }
		{}
	};

	DiffuseMaterial(const DiffuseMaterial::PerObject& data, ColorMap* texture, ID3D11SamplerState* sampler) :
		IMaterial(IMaterial::Type::DIFFUSE)
	{
		mPerObject = data;
		mColorMap = texture;
		mSampler = sampler;
	}

	DiffuseMaterial() :
		IMaterial(IMaterial::Type::DIFFUSE) 
	{
		mColorMap = GPUMemoryManager::instance().GetDefaultColorMap();
		mSampler = GPUMemoryManager::instance().GetDefaultTextureSampler();
		Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Material initialized with default texture and sampler.");
	}

	virtual HRESULT Init(ID3D11Device* pDevice) override
	{
		mBuffer = new ConstantBuffer(pDevice, sizeof(DiffuseMaterial::PerObject));

		return S_OK;
	}

	~DiffuseMaterial() { delete mBuffer; }

	void SetDiffuse(const DirectX::XMFLOAT4& diffuse) { mPerObject.mat.diffuse = diffuse; }
	DirectX::XMFLOAT4 GetDiffuse() const { return mPerObject.mat.diffuse; }

	void SetSpecular(const DirectX::XMFLOAT3& specular) 
	{ 
		mPerObject.mat.specular.x = specular.x;
		mPerObject.mat.specular.y = specular.y;
		mPerObject.mat.specular.z = specular.z;
	}
	void SetSpecular(const DirectX::XMFLOAT3& specular, const float shininess) { mPerObject.mat.specular = XMFLOAT4(specular.x, specular.y, specular.z, shininess); }
	DirectX::XMFLOAT3 GetSpecular() const { return XMFLOAT3(mPerObject.mat.specular.x, mPerObject.mat.specular.y, mPerObject.mat.specular.z); }

	void SetAmbient(const DirectX::XMFLOAT4& ambient) { mPerObject.mat.ambient = ambient; }
	DirectX::XMFLOAT4 GetAmbient() const { return mPerObject.mat.ambient; }

	void SetShininess(const float shininess) { mPerObject.mat.specular.w = shininess; }
	float GetShininess() const { return mPerObject.mat.specular.w; }

	void SetColorTexture(ColorMap* texture) { mColorMap = texture; }
	ColorMap* GetColorMap() const { return mColorMap; }

	void SetTextureSampler(ID3D11SamplerState* sampler) { mSampler = sampler; }
	ID3D11SamplerState* GetSamplerState() { return mSampler; }

	virtual void SetConstantData(void* pData) override
	{
		mPerObject = (*reinterpret_cast<DiffuseMaterial::PerObject*>(pData));
	}

	virtual void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform, const AnimationComponent* pAnimation = nullptr) override
	{
		mPerObject.world = transform->GetWorld();
		// just multiply mul(vector, matrix)
		mPerObject.invWorldTranspose = transform->GetLocal();

		pContext->UpdateSubresource(*mBuffer->GetPtr(), 0, nullptr, &mPerObject, 0, 0);
		pContext->PSSetConstantBuffers(1, 1, mBuffer->GetPtr());
		pContext->VSSetConstantBuffers(1, 1, mBuffer->GetPtr());

		pContext->PSSetSamplers(0, 1, &mSampler);

		pContext->PSSetShaderResources(0, 1, mColorMap->GetSRV());
	}
	
	virtual void Reset(ID3D11DeviceContext* pContext) override
	{
	}

	const void* GetPerObjectData() const override
	{
		return &mPerObject;
	}

private:
	DiffuseMaterial::PerObject mPerObject;
	ColorMap* mColorMap;
	ID3D11SamplerState* mSampler;
	ConstantBuffer* mBuffer;
};

#endif
