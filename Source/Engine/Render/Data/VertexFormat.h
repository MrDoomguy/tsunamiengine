#ifndef _VERTEXFORMAT_H_
#define _VERTEXFORMAT_H_

#include "../../General/TsunamiStd.h"

class InputLayoutDesc
{
public:
	static const std::vector<D3D11_INPUT_ELEMENT_DESC> Default;
	static const std::vector<D3D11_INPUT_ELEMENT_DESC> BaseParticle;
	static const std::vector<D3D11_INPUT_ELEMENT_DESC> SkinnedVertex;
	static const std::vector<D3D11_INPUT_ELEMENT_DESC> SpriteVertex;

	static const std::vector<D3D11_SO_DECLARATION_ENTRY> SpriteStreamOut;
};

#endif
