#ifndef _PARTICLEMATERIAL_H_
#define _PARTICLEMATERIAL_H_

#include "ConstantBuffer.h"
#include "PrimitiveMesh.h"

class ParticleMaterial : public IMaterial
{
private:
	struct PerObject
	{
		DirectX::XMMATRIX world;

		PerObject() :
			world{ XMMatrixIdentity() }
		{}
	};
public:
	ParticleMaterial() : IMaterial(IMaterial::Type::PARTICLE) { }

	~ParticleMaterial()
	{
		delete mBuffer;
	}

	virtual HRESULT Init(ID3D11Device* pDevice) override
	{
		mBuffer = new ConstantBuffer(pDevice, sizeof(ParticleMaterial::PerObject));
		return S_OK;
	}

	virtual void SetConstantData(void* pData) override
	{
		mPerObject = (*reinterpret_cast<ParticleMaterial::PerObject*>(pData));
	}

	virtual void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform, const AnimationComponent* anim) override
	{
		mPerObject.world = transform->GetWorld();

		pContext->UpdateSubresource(mBuffer->GetSubresource(), 0, nullptr, &mPerObject, 0, 0);
		pContext->VSSetConstantBuffers(1, 1, mBuffer->GetPtr());
	}

	virtual void Reset(ID3D11DeviceContext* pContext) override
	{
	}

	const void* GetPerObjectData() const override
	{
		return &mPerObject;
	}

private:
	ConstantBuffer* mBuffer;
	ParticleMaterial::PerObject mPerObject;
};

#endif
