#ifndef _INDEXBUFFER_H_
#define _INDEXBUFFER_H_

#include "IBuffer.h"

class IndexBuffer : public IBuffer
{
public:
	explicit IndexBuffer(ID3D11Device* pDevice, const UINT dataByteSize, const bool isStaticMesh, void* pData = nullptr);
	~IndexBuffer();
};

#endif
