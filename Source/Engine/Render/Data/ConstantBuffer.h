#ifndef _CONSTANTBUFFER_H_
#define _CONSTANTBUFFER_H_

#include "IBuffer.h"

class ConstantBuffer : public IBuffer
{
public:
	explicit ConstantBuffer(ID3D11Device* pDevice, UINT dataByteSize);
	~ConstantBuffer();
};

#endif
