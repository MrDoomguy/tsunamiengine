#ifndef _ITEXTURE_H_
#define _ITEXTURE_H_

#include <d3d11.h>
#include "../../General/TsunamiStd.h"

class ITexture2D
{
	friend class GPUMemoryManager;
public:
	ITexture2D()
		: mTexture{ nullptr }
	{
	}

	~ITexture2D()
	{
		SAFE_RELEASE(mTexture);
	}

	uint32_t GetWidth() { return mWidth; }
	uint32_t GetHeight() { return mHeight; }

	ID3D11Texture2D* Texture() { return mTexture; }
	virtual void Resize(ID3D11Device* pDevice, uint32_t width, uint32_t height) { }

protected:
	uint32_t mWidth;
	uint32_t mHeight;

	ID3D11Texture2D* mTexture;
};

class ITexture3D
{
	friend class GPUMemoryManager;
public:
	ITexture3D()
		: mTexture{ nullptr }
	{
	}

	~ITexture3D()
	{
		SAFE_RELEASE(mTexture);
	}

	uint32_t GetWidth() { return mWidth; }
	uint32_t GetHeight() { return mHeight; }
	uint32_t GetDepth() { return mDepth; }
	ID3D11Texture3D* GetTexture() { return mTexture; };

protected:
	uint32_t mWidth;
	uint32_t mHeight;
	uint32_t mDepth;

	ID3D11Texture3D* mTexture;
};

#endif
