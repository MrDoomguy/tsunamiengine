#include "ConstantBuffer.h"

ConstantBuffer::ConstantBuffer(ID3D11Device* pDevice, UINT dataByteSize)
{
	mByteSize = dataByteSize;

	HRESULT hr;

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
	bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	bd.ByteWidth = dataByteSize;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mBufferObj)))
	{
		SAFE_RELEASE(mBufferObj);
		std::wstring err = L"Constant Buffer allocation failed." + HR_to_wstr(hr);
		OutputDebugString(err.c_str());
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, err);
		exit(1);
	}
}

ConstantBuffer::~ConstantBuffer()
{

}
