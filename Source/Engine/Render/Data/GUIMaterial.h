#ifndef _GUIMATERIAL_H_
#define _GUIMATERIAL_H_

#include "IMaterial.h"
#include "ColorMap.h"

class GUIMaterial : public IMaterial
{
public:
	static const size_t MAX_LAYERS = 10;
	struct LayerInfo
	{
		// center in px
		DirectX::XMFLOAT2 pos;
		// in px
		DirectX::XMFLOAT2 size;
		float alpha;
		DirectX::XMFLOAT3 _pad0;

		LayerInfo() :
			pos{ DirectX::XMFLOAT2(0.0f, 0.0f) },
			size{ DirectX::XMFLOAT2(1.0f, 1.0f) },
			alpha{ 1.0f }
		{ }
	};

	struct PerObject
	{
		// additional slice info
		LayerInfo layerInfo[GUIMaterial::MAX_LAYERS];

		PerObject() { }
	};

	GUIMaterial(const GUIMaterial::PerObject& data, VolumeTexture* sprite, ID3D11SamplerState* sampler) :
		IMaterial(IMaterial::Type::GUI)
	{
		mPerObject = data;
		mSprite = sprite;
		mSampler = sampler;
	}

	GUIMaterial() :
		IMaterial(IMaterial::Type::GUI) { }

	~GUIMaterial() { SAFE_RELEASE(mSampler); SAFE_RELEASE(mBuffer); }

	HRESULT Init(ID3D11Device* pDevice) override
	{
		HRESULT hr;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
		bd.ByteWidth = sizeof(GUIMaterial::PerObject);
		bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		bd.StructureByteStride = 0;
		if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mBuffer)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate material buffer.");
			return hr;
		}

		return S_OK;
	}

	void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform = nullptr, const AnimationComponent* pAnimation = nullptr) override
	{
		pContext->CSSetConstantBuffers(1, 1, &mBuffer);
		pContext->UpdateSubresource(mBuffer, 0, nullptr, &mPerObject, 0, 0);

		pContext->CSSetSamplers(0, 1, &mSampler);

		pContext->CSSetShaderResources(0, 1, mSprite->GetSRV());
	}

	void Reset(ID3D11DeviceContext* pContext) override
	{
	}

	void SetSprite(VolumeTexture* pSprite) { mSprite = pSprite; }
	VolumeTexture* GetSprite() { return mSprite; }

	void SetLayerPosition(const DirectX::XMFLOAT2& pos, const size_t layer) 
	{
		if(layer < MAX_LAYERS)
		{
			mPerObject.layerInfo[layer].pos = pos;
			return;
		}

		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Failed to set layer position, index out of bound.");
	}
	DirectX::XMFLOAT2 GetLayerPosition(const size_t layer) const
	{
		if(layer < MAX_LAYERS)
			return mPerObject.layerInfo[layer].pos;

		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Failed to get layer position, index out of bound.");

		return XMFLOAT2(0.0f, 0.0f);
	}

	void SetSampler(ID3D11SamplerState* sampler) { mSampler = sampler; }
	ID3D11SamplerState* GetSampler() { return mSampler; }

	void SetLayerSize(const DirectX::XMFLOAT2& size, const size_t layer)
	{
		if (layer < MAX_LAYERS)
		{
			mPerObject.layerInfo[layer].size = size;
			return;
		}
		
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Failed to set layer size, index out of bound.");
	}
	DirectX::XMFLOAT2 GetSize(const size_t layer) const
	{
		if(layer < MAX_LAYERS)
			return mPerObject.layerInfo[layer].size;

		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Failed to get layer size, index out of bound.");

		return XMFLOAT2(0.0f, 0.0f);
	}

	void SetLayerTransparency(const float alpha, const size_t layer)
	{
		if (layer < MAX_LAYERS)
		{
			mPerObject.layerInfo[layer].alpha = alpha;
			return;
		}

		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Failed to set layer alpha, index out of bound.");
	}
	float GetLayerTransparency(const size_t layer) const
	{
		if(layer < MAX_LAYERS)
			return mPerObject.layerInfo[layer].alpha;

		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Failed to get layer alpha, index out of bound.");
		
		return 0.0f;
	}

	void SetConstantData(void* pData) override
	{
		mPerObject = (*reinterpret_cast<GUIMaterial::PerObject*>(pData));
	}

	const void* GetPerObjectData() const override
	{
		return &mPerObject;
	}

private:
	VolumeTexture* mSprite;
	ID3D11SamplerState* mSampler;
	GUIMaterial::PerObject mPerObject;
	ID3D11Buffer* mBuffer;
};

#endif
