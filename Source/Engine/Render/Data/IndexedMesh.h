#ifndef _INDEXEDMESH_H_
#define _INDEXEDMESH_H_

#include "IMesh.h"
#include "IndexBuffer.h"

class IndexedMesh : public IMesh
{
	friend class GPUMemoryManager;
public:
	IndexedMesh() : mIB{ nullptr }, mIndexCount{ 0 }
	{
		mVertexSize = sizeof(geotypes::Vertex);
		mIsSkinned = false;
	}

	~IndexedMesh()
	{
		delete mIB;
	}

	ID3D11Buffer* GetIB() { return mIB->GetSubresource(); }
	geotypes::MeshData* GetMesh() { return &mMesh; }

	virtual void Draw(ID3D11DeviceContext* context) override
	{
		uint32_t stride = static_cast<uint32_t>(mVertexSize);
		uint32_t offset = 0;
		context->IASetVertexBuffers(0, 1, mVB->GetPtr(), &stride, &offset);
		context->IASetIndexBuffer(mIB->GetSubresource(), INDEX_FORMAT, 0);

		context->DrawIndexed(mIndexCount, 0, 0);
	}

protected:
	IndexBuffer* mIB;
	uint32_t mIndexCount;

private:
	geotypes::MeshData mMesh;
};

#endif
