#pragma once
#include "PBSVolumetric.h"
#include "AnimatedVolumeTexture.h"
#include "../../General/Application.h"

class PBSAnimatedVolumetricMaterial : public PBSVolumetricMaterial
{

public:
	PBSAnimatedVolumetricMaterial(AnimatedVolumeTexture* pTexture, VolumeTextureRW* pObstacleTex, ID3D11SamplerState* pSampler) :
		PBSVolumetricMaterial()
	{
		mPerObject.dim = pTexture->GetSize();
		mPerObject.extent = XMUINT3(pTexture->GetWidth(), pTexture->GetHeight(), pTexture->GetDepth());
		mVolumeTexture = pTexture;
		mSampler = pSampler;
		mObstacleTexture = pObstacleTex;
	}

	PBSAnimatedVolumetricMaterial() :
		PBSVolumetricMaterial()
	{}

	~PBSAnimatedVolumetricMaterial() {}

	void SetVolumeTexture(AnimatedVolumeTexture* pTexture) 
	{
		mPerObject.dim = pTexture->GetSize();
		mPerObject.extent = XMUINT3(pTexture->GetWidth(), pTexture->GetHeight(), pTexture->GetDepth());
		mVolumeTexture = pTexture;
	}

	void SetObstacleTexture(VolumeTextureRW* pObstacleTex)
	{
		mObstacleTexture = pObstacleTex;
	}

	virtual AnimatedVolumeTexture* GetVolumeTexture() const override { return mVolumeTexture; }

	virtual void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform, const AnimationComponent* pAnimation = nullptr) override
	{
		mPerObject.sdfToWorld = transform->GetWorld();
		mPerObject.worldToSDF = transform->GetLocal();
		float t = Application::instance().GetTotalTime() * mVolumeTexture->GetSpeed();
		mPerObject.offset = mVolumeTexture->GetOffsetOfTime((int)t);
		mPerObject.nextOffset = mVolumeTexture->GetOffsetOfTime((int)t + 1);
		mPerObject.dt = t - (int)t;

		// Bind buffer to cs
		pContext->CSSetConstantBuffers(1, 1, &mBuffer);
		// update data
		pContext->UpdateSubresource(mBuffer, 0, nullptr, &mPerObject, 0, 0);

		// set volume sampler
		pContext->CSSetSamplers(0, 1, &mSampler);

		// set volume resource
		ID3D11ShaderResourceView* srv[] = { *mVolumeTexture->GetSRV(), *mObstacleTexture->GetSRV() };
		pContext->CSSetShaderResources(0, 2, srv);
	}

	virtual void Reset(ID3D11DeviceContext* pContext) override
	{
		ID3D11ShaderResourceView* srv[] = { nullptr, nullptr };
		pContext->CSSetShaderResources(0, 2, srv);
	}
	
private:
	float mSpeed = 1.f;
	virtual void SetVolumeTexture(VolumeTexture* pTexture) override {  }
	AnimatedVolumeTexture* mVolumeTexture;
	VolumeTextureRW* mObstacleTexture;
};
