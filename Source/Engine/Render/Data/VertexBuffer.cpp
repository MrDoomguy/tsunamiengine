#include "VertexBuffer.h"

VertexBuffer::VertexBuffer(ID3D11Device* pDevice, UINT dataByteSize, bool isStaticMesh, void* data)
{
	mByteSize = dataByteSize;

	HRESULT hr;

	D3D11_BUFFER_DESC vd;
	ZeroMemory(&vd, sizeof(D3D11_BUFFER_DESC));
	vd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_VERTEX_BUFFER;
	vd.ByteWidth = dataByteSize;
	vd.Usage = (isStaticMesh) ? D3D11_USAGE::D3D11_USAGE_IMMUTABLE : D3D11_USAGE::D3D11_USAGE_DYNAMIC;
	vd.CPUAccessFlags = (isStaticMesh) ? 0 : D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
	vd.MiscFlags = 0;
	vd.StructureByteStride = 0;

	if (data != nullptr)
	{
		D3D11_SUBRESOURCE_DATA vdata;
		ZeroMemory(&vdata, sizeof(D3D11_SUBRESOURCE_DATA));
		vdata.pSysMem = data;
		vdata.SysMemPitch = 0;
		vdata.SysMemSlicePitch = 0;

		if (FAILED(hr = pDevice->CreateBuffer(&vd, &vdata, &mBufferObj)))
		{
			SAFE_RELEASE(mBufferObj);
			std::wstring err = L"Vertex Buffer allocation failed." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}
	}
	else if (FAILED(hr = pDevice->CreateBuffer(&vd, nullptr, &mBufferObj)))
	{
		SAFE_RELEASE(mBufferObj);
		std::wstring err = L"Vertex Buffer allocation failed." + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}
}

VertexBuffer::~VertexBuffer()
{

}
