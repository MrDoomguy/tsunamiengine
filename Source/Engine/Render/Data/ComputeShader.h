#ifndef _COMPUTESHADER_H_
#define _COMPUTESHADER_H_

#include "IShader.h"

class ComputeShader : public IShader
{
	friend class GPUMemoryManager;
public:
	ComputeShader() : mpObject{ nullptr } {}
	~ComputeShader() { SAFE_RELEASE(mpObject); }

	virtual HRESULT Load(ID3D11Device* pDevice, const IShader::Desc& desc)
	{
		HRESULT hr;

		if (FAILED(hr = CompileFromFile(desc)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to compile Compute Shader.");
			return hr;
		}

		LPVOID b = mpBlob->GetBufferPointer();
		SIZE_T s = mpBlob->GetBufferSize();

		if (FAILED(hr = pDevice->CreateComputeShader(b, s, nullptr, &mpObject)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to Create Compute Shader.");
			return hr;
		}

		return S_OK;
	}

	ID3D11ComputeShader* GetObjectPtr() { return mpObject; }

private:
	ID3D11ComputeShader* mpObject;
};

#endif
