#ifndef _DEPTHSTENCILBUFFER_H_
#define _DEPTHSTENCILBUFFER_H_

#include "ITexture.h"

class DepthStencilBuffer : public ITexture2D
{
	friend class GPUMemoryManager;
public:
	DepthStencilBuffer() : mDSV{ nullptr }, mSRV{ nullptr } { }
	~DepthStencilBuffer()
	{
		SAFE_RELEASE(mSRV);
		SAFE_RELEASE(mDSV);
	}

	ID3D11DepthStencilView* GetDSV() const { return mDSV; }
	ID3D11ShaderResourceView* const* GetSRV() const { return &mSRV; }

	virtual void Resize(ID3D11Device* pDevice, uint32_t width, uint32_t height) override
	{
		mWidth = width;
		mHeight = height;

		D3D11_TEXTURE2D_DESC td;
		mTexture->GetDesc(&td);
		SAFE_RELEASE(mTexture);
		td.Width = width;
		td.Height = height;
		ThrowIfFailed(pDevice->CreateTexture2D(&td, nullptr, &mTexture));

		D3D11_DEPTH_STENCIL_VIEW_DESC dd;
		mDSV->GetDesc(&dd);
		SAFE_RELEASE(mDSV);
		ThrowIfFailed(pDevice->CreateDepthStencilView(mTexture, &dd, &mDSV));

		if (mSRV)
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC sd;
			mSRV->GetDesc(&sd);
			SAFE_RELEASE(mSRV);
			ThrowIfFailed(pDevice->CreateShaderResourceView(mTexture, &sd, &mSRV));
		}
	}

private:
	ID3D11DepthStencilView* mDSV;
	ID3D11ShaderResourceView* mSRV;
};

#endif
