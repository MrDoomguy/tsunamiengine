#include "MeshHelper.h"

using namespace geotypes;

void MeshHelper::CreateBox(float width, float height, float depth, MeshData& meshData)
{
	//
	// Vertex Creation
	//

	Vertex v[24];

	float w2 = 0.5f*width;
	float h2 = 0.5f*height;
	float d2 = 0.5f*depth;

	// Front face
	v[0] = Vertex(-w2, -h2, -d2, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, w2 * h2);
	v[1] = Vertex(-w2, +h2, -d2, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, w2 * h2);
	v[2] = Vertex(+w2, +h2, -d2, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, w2 * h2);
	v[3] = Vertex(+w2, -h2, -d2, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, w2 * h2);

	// Back face
	v[4] = Vertex(-w2, -h2, +d2, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, w2 * h2);
	v[5] = Vertex(+w2, -h2, +d2, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, w2 * h2);
	v[6] = Vertex(+w2, +h2, +d2, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, w2 * h2);
	v[7] = Vertex(-w2, +h2, +d2, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, w2 * h2);

	// Top face
	v[8] = Vertex(-w2, +h2, -d2, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, w2 * d2);
	v[9] = Vertex(-w2, +h2, +d2, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, w2 * d2);
	v[10] = Vertex(+w2, +h2, +d2, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, w2 * d2);
	v[11] = Vertex(+w2, +h2, -d2, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, w2 * d2);

	// Bottom face
	v[12] = Vertex(-w2, -h2, -d2, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, w2 * d2);
	v[13] = Vertex(+w2, -h2, -d2, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, w2 * d2);
	v[14] = Vertex(+w2, -h2, +d2, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, w2 * d2);
	v[15] = Vertex(-w2, -h2, +d2, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, w2 * d2);

	// Left face
	v[16] = Vertex(-w2, -h2, +d2, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, h2 * d2);
	v[17] = Vertex(-w2, +h2, +d2, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, h2 * d2);
	v[18] = Vertex(-w2, +h2, -d2, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, h2 * d2);
	v[19] = Vertex(-w2, -h2, -d2, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, h2 * d2);

	// Right face
	v[20] = Vertex(+w2, -h2, -d2, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, h2 * d2);
	v[21] = Vertex(+w2, +h2, -d2, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, h2 * d2);
	v[22] = Vertex(+w2, +h2, +d2, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, h2 * d2);
	v[23] = Vertex(+w2, -h2, +d2, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, h2 * d2);

	// save vertices into the mesh structure
	meshData.vertices.assign(&v[0], &v[24]);
	meshData.bounding.max = XMFLOAT3(w2, h2, d2);
	meshData.bounding.min = XMFLOAT3(-w2, -h2, -d2);
	meshData.bounding.size = XMFLOAT3(width, height, depth);

	//
	// Indices Creation
	//

	INDEX_TYPE i[36];

	// Front face
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;

	// Back face
	i[6] = 4; i[7] = 5; i[8] = 6;
	i[9] = 4; i[10] = 6; i[11] = 7;

	// Top face
	i[12] = 8; i[13] = 9; i[14] = 10;
	i[15] = 8; i[16] = 10; i[17] = 11;

	// Bottom face
	i[18] = 12; i[19] = 13; i[20] = 14;
	i[21] = 12; i[22] = 14; i[23] = 15;

	// Left face
	i[24] = 16; i[25] = 17; i[26] = 18;
	i[27] = 16; i[28] = 18; i[29] = 19;

	// Right face
	i[30] = 20; i[31] = 21; i[32] = 22;
	i[33] = 20; i[34] = 22; i[35] = 23;

	// save indices into the mesh structure
	meshData.indices.assign(&i[0], &i[36]);

	/*for (uint32_t idx = 0; idx < 24; idx++) v[idx].area = 0.f;

	for (uint32_t idx = 0; idx < 12; idx += 3)
	{
		XMVECTOR p0 = XMLoadFloat3(&v[i[idx + 0]].position);
		XMVECTOR p1 = XMLoadFloat3(&v[i[idx + 1]].position);
		XMVECTOR p2 = XMLoadFloat3(&v[i[idx + 2]].position);

		float a = XMVectorGetX(XMVector3Length(XMVector3Cross(p1 - p0, p2 - p0) / 2));

		v[i[idx + 0]].area += a * XMVectorGetX(XMVector3AngleBetweenVectors(p1 - p0, p2 - p0)) / XM_PI;
		v[i[idx + 1]].area += a * XMVectorGetX(XMVector3AngleBetweenVectors(p0 - p1, p2 - p1)) / XM_PI;
		v[i[idx + 2]].area += a * XMVectorGetX(XMVector3AngleBetweenVectors(p0 - p2, p1 - p2)) / XM_PI;
	}*/
}
/*
void MeshHelper::CreateBox(float width, float height, float depth, geotypes::MeshData& meshData)
{
	//
	// Vertex Creation
	//

	Vertex v[8];

	float w2 = 0.5f*width;
	float h2 = 0.5f*height;
	float d2 = 0.5f*depth;

	float normalComp = 1.f / sqrtf(3.f);
	float a = w2 * h2 + h2 * d2 + w2 * d2;
	// Front face
	v[0] = Vertex(-w2, -h2, -d2, -normalComp, -normalComp, -normalComp, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, a);
	v[1] = Vertex(-w2, +h2, -d2, -normalComp, normalComp, -normalComp, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, a);
	v[2] = Vertex(+w2, +h2, -d2, normalComp, normalComp, -normalComp, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, a);
	v[3] = Vertex(+w2, -h2, -d2, normalComp, -normalComp, -normalComp, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, a);

	// Back face
	v[4] = Vertex(-w2, -h2, +d2, -normalComp, -normalComp, normalComp, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f, a);
	v[5] = Vertex(+w2, -h2, +d2, normalComp, -normalComp, normalComp, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, a);
	v[6] = Vertex(+w2, +h2, +d2, normalComp, normalComp, normalComp, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, a);
	v[7] = Vertex(-w2, +h2, +d2, -normalComp, normalComp, normalComp, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f, a);

	// save vertices into the mesh structure
	meshData.vertices.assign(&v[0], &v[8]);
	meshData.bounding.max = XMFLOAT3(w2, h2, d2);
	meshData.bounding.min = XMFLOAT3(-w2, -h2, -d2);
	meshData.bounding.size = XMFLOAT3(width, height, depth);

	//
	// Indices Creation
	//

	uint32_t i[36];

	// Front face
	i[0] = 0; i[1] = 1; i[2] = 2;
	i[3] = 0; i[4] = 2; i[5] = 3;

	// Back face
	i[6] = 4; i[7] = 5; i[8] = 6;
	i[9] = 4; i[10] = 6; i[11] = 7;

	// Top face
	i[12] = 1; i[13] = 7; i[14] = 6;
	i[15] = 1; i[16] = 6; i[17] = 2;

	// Bottom face
	i[18] = 0; i[19] = 3; i[20] = 5;
	i[21] = 0; i[22] = 5; i[23] = 4;

	// Left face
	i[24] = 4; i[25] = 7; i[26] = 1;
	i[27] = 4; i[28] = 1; i[29] = 0;

	// Right face
	i[30] = 3; i[31] = 2; i[32] = 6;
	i[33] = 3; i[34] = 6; i[35] = 5;

	// save indices into the mesh structure
	meshData.indices.assign(&i[0], &i[36]);
}
*/
void MeshHelper::CreateBox(const BOX_DESC& desc, geotypes::MeshData& meshData)
{
	CreateBox(desc.width, desc.height, desc.depth, meshData);
}

// TODO: check if same problem as in box?
void MeshHelper::CreateSphere(float radius, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData)
{
	meshData.vertices.clear();
	meshData.indices.clear();
	meshData.bounding.max = XMFLOAT3(radius, radius, radius);
	meshData.bounding.min = XMFLOAT3(-radius, -radius, -radius);
	meshData.bounding.size = XMFLOAT3(2 * radius, 2 * radius, 2 * radius);

	//
	// Compute the vertices stating at the top pole and moving down the stacks.
	//

	float phiStep = DirectX::XM_PI / stackCount;
	float thetaStep = 2.0f*DirectX::XM_PI / sliceCount;

	// TODO: check if correct
	float area = phiStep * thetaStep;

	// Poles: note that there will be texture coordinate distortion as there is
	// not a unique point on the texture map to assign to the pole when mapping
	// a rectangular texture onto a sphere.
	Vertex topVertex(0.0f, +radius, 0.0f, 0.0f, +1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, area);
	Vertex bottomVertex(0.0f, -radius, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, area);

	meshData.vertices.push_back(topVertex);

	// Compute vertices for each stack ring (poles are not rings).
	for (uint32_t i = 1; i <= stackCount - 1; ++i)
	{
		float phi = i*phiStep;

		// Vertices of ring.
		for (uint32_t j = 0; j <= sliceCount; ++j)
		{
			float theta = j*thetaStep;

			Vertex v;

			// Spherical to Cartesian
			v.position.x = radius*sinf(phi)*cosf(theta);
			v.position.y = radius*cosf(phi);
			v.position.z = radius*sinf(phi)*sinf(theta);

			// Partial derivative of P with respect to theta
			v.area = area;

			DirectX::XMVECTOR p = DirectX::XMLoadFloat3(&v.position);
			DirectX::XMStoreFloat3(&v.normal, DirectX::XMVector3Normalize(p));

			v.texC.x = theta / DirectX::XM_2PI;
			v.texC.y = phi / DirectX::XM_PI;

			meshData.vertices.push_back(v);
		}
	}

	// Push the pole at last.
	meshData.vertices.push_back(bottomVertex);

	//
	// Compute indices for top stack. The top stack was written first to the vertex buffer
	// and connects the top pole to the first ring
	//
	for (uint32_t i = 1; i <= sliceCount; ++i)
	{
		meshData.indices.push_back(0);
		meshData.indices.push_back(i + 1);
		meshData.indices.push_back(i);
	}

	//
	// Compute indices for the inner stacks (not connected to poles).
	//
	// Offset the indices to the index of the first vertex in the first ring.
	// This is just skipping the top pole vertex.
	INDEX_TYPE baseIndex = 1;
	INDEX_TYPE ringVertexCount = sliceCount + 1;
	for (uint32_t i = 0; i < stackCount - 2; ++i)
	{
		for (uint32_t j = 0; j < sliceCount; ++j)
		{
			meshData.indices.push_back(baseIndex + i*ringVertexCount + j);
			meshData.indices.push_back(baseIndex + i*ringVertexCount + j + 1);
			meshData.indices.push_back(baseIndex + (i + 1)*ringVertexCount + j);
			
			meshData.indices.push_back(baseIndex + (i + 1)*ringVertexCount + j);
			meshData.indices.push_back(baseIndex + i*ringVertexCount + j + 1);
			meshData.indices.push_back(baseIndex + (i + 1)*ringVertexCount + j + 1);
		}
	}

	//
	// Compute indices for bottom stack.  The bottom stack was written last to the vertex buffer
	// and connects the bottom pole to the bottom ring.
	//

	// South pole was added last.
	uint32_t southPoleIndex = static_cast<uint32_t>(meshData.vertices.size()) - 1;

	// Offset the indices to the index of the first vertex in the last ring.
	baseIndex = southPoleIndex - ringVertexCount;

	for (uint32_t i = 0; i < sliceCount; ++i)
	{
		meshData.indices.push_back(southPoleIndex);
		meshData.indices.push_back(baseIndex + i);
		meshData.indices.push_back(baseIndex + i + 1);
	}
}

void MeshHelper::CreateSphere(const SPHERE_DESC& desc, geotypes::MeshData& meshData)
{
	CreateSphere(desc.radius, desc.sliceCount, desc.stackCount, meshData);
}

void MeshHelper::Subdivide(geotypes::MeshData& meshData)
{
	// Save a copy of the input geometry
	geotypes::MeshData inputCopy = meshData;

	meshData.vertices.resize(0);
	meshData.vertices.resize(0);

	// Subdivision Scheme
	//
	//       v1
	//       *
	//      / \
	//     /   \
	//  m0*-----*m1
	//   / \   / \
	//  /   \ /   \
	// *-----*-----*
	// v0    m2     v2

	uint32_t numTris = static_cast<uint32_t>(inputCopy.indices.size()) / 3;
	for (INDEX_TYPE i = 0; i < numTris; ++i)
	{
		Vertex v0 = inputCopy.vertices[inputCopy.indices[i * 3 + 0]];
		Vertex v1 = inputCopy.vertices[inputCopy.indices[i * 3 + 1]];
		Vertex v2 = inputCopy.vertices[inputCopy.indices[i * 3 + 2]];

		//
		// Midpoints generation
		//
		Vertex m0, m1, m2;

		// For the subdivision, we just care about the position component. We derive the other
		// vertex components in Create function.
		m0.position = DirectX::XMFLOAT3(
			0.5f*(v0.position.x + v1.position.x),
			0.5f*(v0.position.y + v1.position.y),
			0.5f*(v0.position.z + v1.position.z));

		m1.position = DirectX::XMFLOAT3(
			0.5f*(v1.position.x + v2.position.x),
			0.5f*(v1.position.y + v2.position.y),
			0.5f*(v1.position.z + v2.position.z));

		m2.position = DirectX::XMFLOAT3(
			0.5f*(v0.position.x + v2.position.x),
			0.5f*(v0.position.y + v2.position.y),
			0.5f*(v0.position.z + v2.position.z));

		//
		// Add new geometry
		//
		meshData.vertices.push_back(v0); // 0
		meshData.vertices.push_back(v1); // 1
		meshData.vertices.push_back(v2); // 2
		meshData.vertices.push_back(m0); // 3
		meshData.vertices.push_back(m1); // 4
		meshData.vertices.push_back(m2); // 5
		
		meshData.indices.push_back(i * 6 + 0);
		meshData.indices.push_back(i * 6 + 3);
		meshData.indices.push_back(i * 6 + 5);
		
		meshData.indices.push_back(i * 6 + 3);
		meshData.indices.push_back(i * 6 + 4);
		meshData.indices.push_back(i * 6 + 5);
		
		meshData.indices.push_back(i * 6 + 5);
		meshData.indices.push_back(i * 6 + 4);
		meshData.indices.push_back(i * 6 + 2);
		
		meshData.indices.push_back(i * 6 + 3);
		meshData.indices.push_back(i * 6 + 1);
		meshData.indices.push_back(i * 6 + 4);
	}
}

void MeshHelper::CreateGeosphere(float radius, uint32_t numSubdivisions, geotypes::MeshData& meshData)
{
	meshData.bounding.max = XMFLOAT3(radius, radius, radius);
	meshData.bounding.min = XMFLOAT3(-radius, -radius, -radius);
	meshData.bounding.size = XMFLOAT3(2 * radius, 2 * radius, 2 * radius);

	// Put a cap on subdivions number.
	numSubdivisions = MathHelper::Min(numSubdivisions, 5u);

	// Approximate a sphere by tassellating an icosahedron.
	const float X = 0.525731f;
	const float Z = 0.850651f;

	//TODO: check if correct
	float area = 4 * DirectX::XM_PI * radius * radius / numSubdivisions;

	DirectX::XMFLOAT3 pos[12] =
	{
		DirectX::XMFLOAT3(-X, 0.0f, Z), DirectX::XMFLOAT3(X, 0.0f, Z),
		DirectX::XMFLOAT3(-X, 0.0f, -Z), DirectX::XMFLOAT3(X, 0.0f, -Z),
		DirectX::XMFLOAT3(0.0f, Z, X), DirectX::XMFLOAT3(0.0f, Z, -X),
		DirectX::XMFLOAT3(0.0f, -Z, X), DirectX::XMFLOAT3(0.0f, -Z, -X),
		DirectX::XMFLOAT3(Z, X, 0.0f), DirectX::XMFLOAT3(-Z, X, 0.0f),
		DirectX::XMFLOAT3(Z, -X, 0.0f), DirectX::XMFLOAT3(-Z, -X, 0.0f)
	};

	INDEX_TYPE k[60] = 
	{
		1, 4, 0, 4, 9, 0, 4, 5, 9, 8, 5, 4, 1, 8, 4,
		1, 10, 8, 10, 3, 8, 8, 3, 5, 3, 2, 5, 3, 7, 2,
		3, 10, 7, 10, 6, 7, 6, 11, 7, 6, 0, 11, 6, 1, 0,
		10, 1, 6, 11, 0, 9, 2, 11, 9, 5, 2, 9, 11, 2, 7
	};

	meshData.vertices.resize(12);
	meshData.indices.reserve(60);

	for (uint32_t i = 0; i < 12; ++i)
	{
		meshData.vertices[i].position = pos[i];
	}

	for (INDEX_TYPE i = 0; i < 60; ++i)
	{
		meshData.indices[i] = k[i];
	}

	for (uint32_t i = 0; i < numSubdivisions; ++i)
	{
		Subdivide(meshData);
	}

	// Project vertices onto sphere and scale.
	for (uint32_t i = 0; i < meshData.vertices.size(); ++i)
	{
		// Project onto unit sphere.
		DirectX::XMVECTOR n = DirectX::XMVector3Normalize(DirectX::XMLoadFloat3(&meshData.vertices[i].position));

		// Project onto sphere.
		DirectX::XMVECTOR p = radius*n;

		DirectX::XMStoreFloat3(&meshData.vertices[i].position, p);
		DirectX::XMStoreFloat3(&meshData.vertices[i].normal, n);

		// Derive texture coordinates from spherical coordinates.
		float theta = MathHelper::AngleFromXY(
			meshData.vertices[i].position.x,
			meshData.vertices[i].position.z);

		float phi = acosf(meshData.vertices[i].position.y / radius);

		meshData.vertices[i].texC.x = theta / DirectX::XM_2PI;
		meshData.vertices[i].texC.y = phi / XM_PI;
		
		meshData.vertices[i].area = area;
	}
}

void MeshHelper::CreateGeosphere(const GEOSPHERE_DESC& desc, geotypes::MeshData& meshData)
{
	CreateGeosphere(desc.radius, desc.numSubdivisions, meshData);
}

void MeshHelper::CreateCylinder(float bottomRadius, float topRadius, float height, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData)
{
	meshData.vertices.clear();
	meshData.indices.clear();

	float r = bottomRadius > topRadius ? bottomRadius : topRadius;

	meshData.bounding.max = XMFLOAT3(r, height/2, r);
	meshData.bounding.min = XMFLOAT3(-r, -height/2, -r);
	meshData.bounding.size = XMFLOAT3(2 * r, height, 2 * r);

	//
	// Build Stacks.
	//
	float stackHeight = height / stackCount;

	// Amount to increment radius as we move up each stack level from bottom to top.
	float radiusStep = (topRadius - bottomRadius) / stackCount;

	uint32_t ringCount = stackCount + 1;

	// Compute vertices for each stack ring starting at the bottom and moving up.
	for (uint32_t i = 0; i < ringCount; ++i)
	{
		float y = -0.5f*height + i*stackHeight;
		float r = bottomRadius + i*radiusStep;

		// Vertices of ring
		float dTheta = 2.0f*DirectX::XM_PI / sliceCount;
		for (uint32_t j = 0; j <= sliceCount; ++j)
		{
			Vertex vertex;

			float c = cosf(j*dTheta);
			float s = sinf(j*dTheta);

			vertex.position = DirectX::XMFLOAT3(r*c, y, r*s);

			vertex.texC.x = static_cast<float>(j) / sliceCount;
			vertex.texC.y = 1.0f - static_cast<float>(i) / stackCount;

			// Cylinder can be parameterized as follows, where we introduce v
			// parameter that goes in the same direction as the v tex-coord
			// so that the bitangent goes in the same direction as the v tex-coord.
			//   Let r0 be the bottom radius and let r1 be the top radius.
			//   y(v) = h - hv for v in [0,1].
			//   r(v) = r1 + (r0-r1)v
			//
			//   x(t, v) = r(v)*cos(t)
			//   y(t, v) = h - hv
			//   z(t, v) = r(v)*sin(t)
			// 
			//  dx/dt = -r(v)*sin(t)
			//  dy/dt = 0
			//  dz/dt = +r(v)*cos(t)
			//
			//  dx/dv = (r0-r1)*cos(t)
			//  dy/dv = -h
			//  dz/dv = (r0-r1)*sin(t)

			// TODO: check if correct
			vertex.area = s * c;

			float dr = bottomRadius - topRadius;
			DirectX::XMFLOAT3 bitangent(dr*c, -height, dr*s);

			DirectX::XMVECTOR T = DirectX::XMLoadFloat3(&DirectX::XMFLOAT3(-s, 0.0f, c));
			DirectX::XMVECTOR B = DirectX::XMLoadFloat3(&bitangent);
			DirectX::XMVECTOR N = DirectX::XMVector3Normalize(DirectX::XMVector3Cross(T, B));
			DirectX::XMStoreFloat3(&vertex.normal, N);

			meshData.vertices.push_back(vertex);
		}
	}

	// Add one because we duplicate the first and the last vertex per ring
	// since the texture coordinates are different.
	INDEX_TYPE ringVertexCount = sliceCount + 1;

	// Compute indices for each stack.
	for (INDEX_TYPE i = 0; i < stackCount; ++i)
	{
		for (INDEX_TYPE j = 0; j < sliceCount; ++j)
		{
			meshData.indices.push_back(i*ringVertexCount + j);
			meshData.indices.push_back((i + 1)*ringVertexCount + j);
			meshData.indices.push_back((i + 1)*ringVertexCount + j + 1);
			
			meshData.indices.push_back(i*ringVertexCount + j);
			meshData.indices.push_back((i + 1)*ringVertexCount + j + 1);
			meshData.indices.push_back(i*ringVertexCount + j + 1);
		}
	}

	BuildCylinderTopCap(bottomRadius, topRadius, height, sliceCount, stackCount, meshData);
	BuildCylinderBottomCap(bottomRadius, topRadius, height, sliceCount, stackCount, meshData);
}

void MeshHelper::CreateCylinder(const CYLINDER_DESC& desc, geotypes::MeshData& meshData)
{
	CreateCylinder(desc.bottomRadius, desc.topRadius, desc.height, desc.sliceCount, desc.stackCount, meshData);
}

void MeshHelper::BuildCylinderTopCap(float bottomRadius, float topRadius, float height, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData)
{
	uint32_t baseIndex = static_cast<uint32_t>(meshData.vertices.size());

	float y = 0.5f*height;
	float dTheta = 2.0f*DirectX::XM_PI / sliceCount;

	// Duplicate cap ring vertices because the texture coordinates and normals differ.
	for (uint32_t i = 0; i <= sliceCount; ++i)
	{
		float x = topRadius*cosf(i*dTheta);
		float z = topRadius*sinf(i*dTheta);

		// Scale down by the height to try and make the cap texture coord area
		// proportional to base.
		float u = x / height + 0.5f;
		float v = z / height + 0.5f;

		meshData.vertices.push_back(Vertex(x, y, z, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, u, v, dTheta));
	}

	// Cap center vertex.
	meshData.vertices.push_back(Vertex(0.0f, y, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.5f, dTheta));

	// Index center vertex.
	INDEX_TYPE centerIndex = static_cast<INDEX_TYPE>(meshData.vertices.size()) - 1;
	for (INDEX_TYPE i = 0; i < sliceCount; ++i)
	{
		meshData.indices.push_back(centerIndex);
		meshData.indices.push_back(baseIndex + i + 1);
		meshData.indices.push_back(baseIndex + i);
	}
}

void MeshHelper::BuildCylinderBottomCap(float bottomRadius, float topRadius, float height, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData)
{
	uint32_t baseIndex = static_cast<uint32_t>(meshData.vertices.size());
	float y = -0.5f*height;

	// vertices of ring.
	float dTheta = 2.0f*DirectX::XM_PI / sliceCount;
	for (uint32_t i = 0; i <= sliceCount; ++i)
	{
		float x = bottomRadius*cosf(i*dTheta);
		float z = bottomRadius*sinf(i*dTheta);

		// Scale down by the height to try and make top cap texture coord area
		// proportional to base.
		float u = x / height + 0.5f;
		float v = z / height + 0.5f;

		meshData.vertices.push_back(Vertex(x, y, z, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, u, v, dTheta));
	}

	// Cap center vertex.
	meshData.vertices.push_back(Vertex(0.0f, y, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.5f, dTheta));

	// Cache the index of the center vextex
	INDEX_TYPE centerIndex = static_cast<INDEX_TYPE>(meshData.vertices.size()) - 1;
	for (INDEX_TYPE i = 0; i < sliceCount; ++i)
	{
		meshData.indices.push_back(centerIndex);
		meshData.indices.push_back(baseIndex + i);
		meshData.indices.push_back(baseIndex + i + 1);
	}
}

void MeshHelper::CreateGrid(float width, float depth, uint32_t m, uint32_t n, geotypes::MeshData& meshData)
{
	uint32_t vertexCount = m*n;
	uint32_t faceCount = (m - 1)*(n - 1) * 2;

	//
	// Vertices creation.
	//

	float w2 = 0.5f*width;
	float d2 = 0.5f*depth;

	float dx = width / (n - 1);
	float dz = depth / (m - 1);

	float du = 1.0f / (n - 1);
	float dv = 1.0f / (m - 1);

	meshData.vertices.resize(vertexCount);
	for (uint32_t i = 0; i < m; ++i)
	{
		float z = d2 - i*dz;
		for (uint32_t j = 0; j < n; ++j)
		{
			float x = -w2 + j*dx;

			meshData.vertices[i*n + j].position = DirectX::XMFLOAT3(x, 0.0f, z);
			meshData.vertices[i*n + j].normal = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
			meshData.vertices[i*n + j].area = dx * dz;

			// Stretch texture over grid.
			meshData.vertices[i*n + j].texC.x = j*du;
			meshData.vertices[i*n + j].texC.y = i*dv;
		}
	}

	//
	// Indices creation.
	//

	meshData.indices.resize(faceCount * 3);	// 3 indices per face

	//Iterate over each quad and compute indices.
	INDEX_TYPE k = 0;
	for (INDEX_TYPE i = 0; i < m - 1; ++i)
	{
		for (INDEX_TYPE j = 0; j < n - 1; ++j)
		{
			meshData.indices[k] = i*n + j;
			meshData.indices[k + 1] = i*n + j + 1;
			meshData.indices[k + 2] = (i + 1)*n + j;

			meshData.indices[k + 3] = (i + 1)*n + j;
			meshData.indices[k + 4] = i*n + j + 1;
			meshData.indices[k + 5] = (i + 1)*n + j + 1;

			k += 6; // next quad
		}
	}
}

void MeshHelper::CreateGrid(const GRID_DESC& desc, geotypes::MeshData& meshData)
{
	CreateGrid(desc.width, desc.depth, desc.m, desc.n, meshData);
}

void MeshHelper::CreateFullscreenQuad(geotypes::MeshData& meshData)
{
	meshData.vertices.resize(4);
	meshData.indices.resize(6);

	// Position coordinates specified in NDC space.
	meshData.vertices[0] = Vertex(
		-1.0f, -1.0f, 0.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f,
		0.f);

	meshData.vertices[1] = Vertex(
		-1.0f, +1.0f, 0.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 0.0f,
		0.f);

	meshData.vertices[2] = Vertex(
		+1.0f, +1.0f, 0.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, 0.0f,
		1.0f, 0.0f,
		0.f);

	meshData.vertices[3] = Vertex(
		+1.0f, -1.0f, 0.0f,
		0.0f, 0.0f, -1.0f,
		0.0f, 0.0f, 0.0f,
		1.0f, 1.0f,
		0.f);

	/*geotypes::MeshData.vertices[0] = Vertex(
		-.5f, -.5f, 0.0f,
		0.0f, 0.0f, -1.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f);

	geotypes::MeshData.vertices[1] = Vertex(
		-.5f, +.5f, 0.0f,
		0.0f, 0.0f, -1.0f,
		1.0f, 0.0f, 0.0f,
		0.0f, 0.0f);

	geotypes::MeshData.vertices[2] = Vertex(
		+.5f, +.5f, 0.0f,
		0.0f, 0.0f, -1.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 0.0f);

	geotypes::MeshData.vertices[3] = Vertex(
		+.5f, -.5f, 0.0f,
		0.0f, 0.0f, -1.0f,
		1.0f, 0.0f, 0.0f,
		1.0f, 1.0f);*/

	meshData.indices[0] = 0;
	meshData.indices[1] = 1;
	meshData.indices[2] = 2;
	
	meshData.indices[3] = 0;
	meshData.indices[4] = 2;
	meshData.indices[5] = 3;
}