#ifndef _VOLUMETEXTURERW_H_
#define _VOLUMETEXTURERW_H_

#include "ITexture.h"

class VolumeTextureRW : public ITexture3D
{
	friend class GPUMemoryManager;
public:
	VolumeTextureRW() : mSRV(nullptr) {}
	~VolumeTextureRW() { SAFE_RELEASE(mSRV); SAFE_RELEASE(mUAV); }

	ID3D11ShaderResourceView* const* GetSRV() const { return &mSRV; }
	ID3D11UnorderedAccessView* const* GetUAV() const { return &mUAV; }

private:
	ID3D11ShaderResourceView* mSRV;
	ID3D11UnorderedAccessView* mUAV;
};

#endif
