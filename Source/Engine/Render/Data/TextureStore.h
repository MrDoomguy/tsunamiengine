#ifndef _TEXTURESTORE_H_
#define _TEXTURESTORE_H_

#include "../../General/TsunamiStd.h"
#include "../../General/Filesystem.h"

#include "../../3rdParty/Include/ImageTools/DDSTextureLoader.h"

#include "../Data/ColorMap.h"
#include "../Data/VolumeTexture.h"
#include "../Data/DepthStencilBuffer.h"
#include "../Data/RenderTarget.h"

class TextureStore
{
public:
	TextureStore();
	~TextureStore();

	void LoadDataFromDisk(ID3D11Device* pDevice, const std::wstring& pathToFolder);

	void AddVolumeTexture(ID3D11Device* pDevice, const int3& size);

	ColorMap* GetColorTexture(const std::string& name);
	VolumeTexture* GetVolumeTexture(const std::string& name);

private:
	std::unordered_map<std::string, ColorMap*> mColor;
	std::unordered_map<std::string, VolumeTexture*> mVolume;
};

#endif
