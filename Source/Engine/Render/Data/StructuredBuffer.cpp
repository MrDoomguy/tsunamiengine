#include "StructuredBuffer.h"

StructuredBuffer::StructuredBuffer(ID3D11Device * pDevice, UINT dataByteSize, UINT byteStride, const bool isRW, const bool isStaged, const UINT cpuAccessFlag)
{
	mByteSize = dataByteSize;

	HRESULT hr;

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
	if (isRW)
	{
		bd.BindFlags |= D3D11_BIND_FLAG::D3D11_BIND_UNORDERED_ACCESS;
	}
	bd.ByteWidth = dataByteSize;
	bd.CPUAccessFlags = isStaged ? 0 : cpuAccessFlag;
	bd.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	bd.StructureByteStride = byteStride;
	if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mBufferObj)))
	{
		SAFE_RELEASE(mBufferObj);
		std::wstring err = L"Structured Buffer allocation failed." + HR_to_wstr(hr);
		OutputDebugString(err.c_str());
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, err);
		exit(1);
	}

	if (isStaged)
	{
		bd.Usage = D3D11_USAGE_STAGING;
		bd.BindFlags = 0;
		bd.CPUAccessFlags = cpuAccessFlag;

		if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mStagingBuffer)))
		{
			SAFE_RELEASE(mBufferObj);
			std::wstring err = L"Staging Buffer allocation failed." + HR_to_wstr(hr);
			OutputDebugString(err.c_str());
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, err);
			exit(1);
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC shaderDesc;
	ZeroMemory(&shaderDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));

	shaderDesc.Format = DXGI_FORMAT_UNKNOWN;
	shaderDesc.ViewDimension = D3D11_SRV_DIMENSION::D3D11_SRV_DIMENSION_BUFFER;
	shaderDesc.Buffer.NumElements = dataByteSize / byteStride;
	shaderDesc.Buffer.FirstElement = 0;

	if (FAILED(hr = pDevice->CreateShaderResourceView(mBufferObj, &shaderDesc, &mSRV)))
	{
		SAFE_RELEASE(mBufferObj);
		std::wstring err = L"SRV allocation failed." + HR_to_wstr(hr);
		OutputDebugString(err.c_str());
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, err);
		exit(1);
	}

	if (isRW)
	{
		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		ZeroMemory(&uavDesc, sizeof(D3D11_UNORDERED_ACCESS_VIEW_DESC));

		uavDesc.Format = DXGI_FORMAT_UNKNOWN;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION::D3D11_UAV_DIMENSION_BUFFER;
		uavDesc.Buffer.NumElements = dataByteSize / byteStride;
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.Flags = 0;
		
		if (FAILED(hr = pDevice->CreateUnorderedAccessView(mBufferObj, &uavDesc, &mUAV))) 
		{
			SAFE_RELEASE(mBufferObj);
			std::wstring err = L"UAV Buffer allocation failed." + HR_to_wstr(hr);
			OutputDebugString(err.c_str());
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, err);
			exit(1);
		}
	}
}

StructuredBuffer::~StructuredBuffer()
{
	SAFE_RELEASE(mSRV);
	SAFE_RELEASE(mUAV);
}

void StructuredBuffer::Map(ID3D11DeviceContext * context, D3D11_MAP mapType, D3D11_MAPPED_SUBRESOURCE * mappedSubresource, UINT subresourceIndex)
{
	HRESULT hr;
	if (mStagingBuffer != nullptr)
	{
		context->CopyResource(mStagingBuffer, mBufferObj);
		if (FAILED(hr = context->Map(mStagingBuffer, subresourceIndex, mapType, 0, mappedSubresource)))
		{
			OutputDebugString(HR_to_wstr(hr).c_str());
			exit(1);
		}
	}
	else
	{
		if (FAILED(hr = context->Map(mBufferObj, subresourceIndex, mapType, 0, mappedSubresource)))
		{
			OutputDebugString(HR_to_wstr(hr).c_str());
			exit(1);
		}
	}
}

void StructuredBuffer::Unmap(ID3D11DeviceContext * context, UINT subresourceIndex)
{
	if (mStagingBuffer != nullptr)
	{
		context->Unmap(mStagingBuffer, subresourceIndex);
	}
	else
	{
		context->Unmap(mBufferObj, subresourceIndex);
	}
}
