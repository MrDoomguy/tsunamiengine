#ifndef _PBSSOLIDMATERIAL_H_
#define _PBSSOLIDMATERIAL_H_

#include "IMaterial.h"
#include "../../General/GPUMemoryManager.h"

class PBSSolidMaterial : public IMaterial
{
public:
	struct MaterialData
	{
		DirectX::XMFLOAT3 baseColor;
		float metallic;
		float roughness;
		float ior;
		DirectX::XMFLOAT2 _pad0;

		MaterialData() :
			baseColor{ DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f) },
			metallic{ 0.01f },
			roughness{ 0.9f },
			ior{ 1.180f }
		{}
	};

	struct PerObject
	{
		PBSSolidMaterial::MaterialData mat;
		DirectX::XMMATRIX world;
		DirectX::XMMATRIX invWorldTranspose;

		PerObject() :
			world{ XMMatrixIdentity() },
			invWorldTranspose{ XMMatrixIdentity() }
		{}
	};

	PBSSolidMaterial(const PBSSolidMaterial::PerObject& data, ColorMap* pBaseColor, ColorMap* pNormalMap, ID3D11SamplerState* pSampler) :
		IMaterial(IMaterial::Type::PBS_SOLID)
	{
		mPerObject = data;
		mBaseColor = pBaseColor;
		mNormalMap = pNormalMap;
		mSampler = pSampler;
	}

	PBSSolidMaterial() :
		IMaterial(IMaterial::Type::PBS_SOLID)
	{
		mBaseColor = GPUMemoryManager::instance().GetDefaultColorMap();
		mNormalMap = GPUMemoryManager::instance().GetDefaultNormalMap();
		mSampler = GPUMemoryManager::instance().GetDefaultTextureSampler();
	}

	~PBSSolidMaterial() { SAFE_RELEASE(mBuffer); }

	HRESULT Init(ID3D11Device* pDevice) override
	{
		HRESULT hr;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
		bd.ByteWidth = sizeof(PBSSolidMaterial::PerObject);
		bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		bd.StructureByteStride = 0;
		if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mBuffer)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate material buffer.");
			return hr;
		}

		return S_OK;
	}

	void SetBaseColor(const DirectX::XMFLOAT3& baseColor) { mPerObject.mat.baseColor = baseColor; }
	DirectX::XMFLOAT3 GetBaseColor() const { return mPerObject.mat.baseColor; }

	void SetMetallic(const float metallic) { mPerObject.mat.metallic = metallic; }
	float GetMetallic() const { return mPerObject.mat.metallic; }

	void SetRoughness(const float roughness) { mPerObject.mat.roughness = roughness; }
	float GetRoughness() const { return mPerObject.mat.roughness; }

	void SetColorTexture(ColorMap* pColorTexture) { mBaseColor = pColorTexture; }
	ColorMap* GetColorMap() const { return mBaseColor; }

	void SetNormalMap(ColorMap* pNormalMap) { mNormalMap = pNormalMap; }
	ColorMap* GetNormalMap() const { return mNormalMap; }

	void SetTextureSampler(ID3D11SamplerState* pSampler) { mSampler = pSampler; }
	ID3D11SamplerState* GetTextureSampler() { return mSampler; }

	void SetConstantData(void* pData) override
	{
		mPerObject = (*reinterpret_cast<PBSSolidMaterial::PerObject*>(pData));
	}

	const void* GetPerObjectData() const override
	{
		return &mPerObject;
	}

	void Set(ID3D11DeviceContext* pContext, const TransformComponent* pTransform, const AnimationComponent* pAnimation=nullptr) override
	{
		mPerObject.world = pTransform->GetWorld();
		mPerObject.invWorldTranspose = pTransform->GetLocal();

		if (pAnimation != nullptr)
		{
			pContext->VSSetConstantBuffers(11, 1, pAnimation->GetTransformBuffer()->GetPtr());
		}

		pContext->UpdateSubresource(mBuffer, 0, nullptr, &mPerObject, 0, 0);
		pContext->PSSetConstantBuffers(1, 1, &mBuffer);
		pContext->VSSetConstantBuffers(1, 1, &mBuffer);

		pContext->PSSetSamplers(0, 1, &mSampler);

		pContext->PSSetShaderResources(0, 1, mBaseColor->GetSRV());
		pContext->PSSetShaderResources(1, 1, mNormalMap->GetSRV());
	}

	void Reset(ID3D11DeviceContext* pContext) override
	{
	}

private:
	ColorMap* mBaseColor;
	ColorMap* mNormalMap;
	ID3D11SamplerState* mSampler;
	PBSSolidMaterial::PerObject mPerObject;
	ID3D11Buffer* mBuffer;
};

#endif

