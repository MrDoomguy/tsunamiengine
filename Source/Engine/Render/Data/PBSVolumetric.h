#ifndef _PBSVOLUMETRICMATERIAL_H_
#define _PBSVOLUMETRICMATERIAL_H_

#include "IMaterial.h"
#include "VolumeTexture.h"

class PBSVolumetricMaterial : public IMaterial
{
public:

	PBSVolumetricMaterial(VolumeTexture* pTexture, ID3D11SamplerState* pSampler) :
		IMaterial(IMaterial::Type::PBS_VOLUMETRIC)
	{
		mPerObject.extent = XMUINT3(pTexture->GetWidth(), pTexture->GetHeight(), pTexture->GetDepth());
		mPerObject.dim = XMUINT4(mPerObject.extent.x, mPerObject.extent.y, mPerObject.extent.z, 1);
		mVolumeTexture = pTexture;
		mSampler = pSampler;
	}

	PBSVolumetricMaterial() :
		IMaterial(IMaterial::Type::PBS_VOLUMETRIC) 
	{}

	~PBSVolumetricMaterial() { SAFE_RELEASE(mBuffer); }

	virtual HRESULT Init(ID3D11Device* pDevice) override
	{
		HRESULT hr;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
		bd.ByteWidth = sizeof(PBSVolumetricMaterial::PerObject);
		bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		bd.StructureByteStride = 0;
		if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mBuffer)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate material buffer.");
			return hr;
		}

		return S_OK;
	}

	void SetBaseColor(const DirectX::XMFLOAT3& baseColor) { mPerObject.mat.baseColor = baseColor; }
	DirectX::XMFLOAT3 GetBaseColor() const { return mPerObject.mat.baseColor; }

	void SetMetallic(const float metallic) { mPerObject.mat.metallic = metallic; }
	float GetMetallic() const { return mPerObject.mat.metallic; }

	void SetRoughness(const float roughness) { mPerObject.mat.roughness = roughness; }
	float GetRoughness() const { return mPerObject.mat.roughness; }

	void SetIndexOfRefraction(const float ior) { mPerObject.mat.ior = ior; }
	float GetIndexOfRefraction() const { return mPerObject.mat.ior; }

	virtual void SetVolumeTexture(VolumeTexture* pTexture) 
	{
		mPerObject.extent = XMUINT3(pTexture->GetWidth(), pTexture->GetHeight(), pTexture->GetDepth());
		mPerObject.dim = XMUINT4(mPerObject.extent.x, mPerObject.extent.y, mPerObject.extent.z, 1);
		mVolumeTexture = pTexture;
	}
	virtual VolumeTexture* GetVolumeTexture() const { return mVolumeTexture; }

	void SetTextureSampler(ID3D11SamplerState* pSampler) { mSampler = pSampler; }
	ID3D11SamplerState* GetTextureSampler() { return mSampler; }

	virtual void SetConstantData(void* pData) override
	{
		mPerObject = (*reinterpret_cast<PBSVolumetricMaterial::PerObject*>(pData));
	}

	virtual void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform, const AnimationComponent* pAnimation=nullptr) override
	{
		mPerObject.sdfToWorld = transform->GetWorld();
		mPerObject.worldToSDF = transform->GetLocal();

		// Bind buffer to cs
		pContext->CSSetConstantBuffers(1, 1, &mBuffer);
		// update data
		pContext->UpdateSubresource(mBuffer, 0, nullptr, &mPerObject, 0, 0);

		// set volume sampler
		pContext->CSSetSamplers(0, 1, &mSampler);

		// set volume resource
		pContext->CSSetShaderResources(0, 1, mVolumeTexture->GetSRV());
	}

	virtual void Reset(ID3D11DeviceContext* pContext) override
	{
	}

	virtual const void* GetPerObjectData() const override
	{
		return &mPerObject;
	}

protected:

	struct MaterialData
	{
		DirectX::XMFLOAT3 baseColor;
		float metallic;
		float roughness;
		float ior;
		DirectX::XMFLOAT2 _pad0;

		MaterialData() :
			baseColor{ DirectX::XMFLOAT3(0.75f, 0.75f, 0.75f) },
			metallic{ 0.1f },
			roughness{ 0.5f },
			ior{ 1.0f }
		{}
	};

	struct PerObject
	{
		PBSVolumetricMaterial::MaterialData mat;
		DirectX::XMMATRIX sdfToWorld;
		DirectX::XMMATRIX worldToSDF;
		DirectX::XMUINT4 dim;
		DirectX::XMUINT3 extent;
		float _pad0;
		XMUINT3 offset;
		float _pad1;
		XMUINT3 nextOffset;
		float dt;

		PerObject() :
			sdfToWorld{ XMMatrixIdentity() },
			worldToSDF{ XMMatrixIdentity() },
			dim{DirectX::XMUINT4(0, 0, 0, 0)},
			extent{ DirectX::XMUINT3(0, 0, 0) },
			offset{DirectX::XMUINT3(0, 0, 0)},
			nextOffset{DirectX::XMUINT3(0, 0, 0)},
			dt{0}
		{}
	};

	ID3D11SamplerState* mSampler;
	ID3D11Buffer* mBuffer;
	PBSVolumetricMaterial::PerObject mPerObject;

private:


	VolumeTexture* mVolumeTexture;

};

#endif
