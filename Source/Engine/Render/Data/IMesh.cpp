#include "IMesh.h"
#include "../../General/GPUMemoryManager.h"

void IMesh::UpdateVB()
{
	GPUMemoryManager::instance().CopyResource(*mVB->GetPtr(), *mSB->GetPtr());
}

void IMesh::UpdateSB()
{
	GPUMemoryManager::instance().CopyResource(*mSB->GetPtr(), *mVB->GetPtr());
}

void IMesh::Update(void* data)
{
	GPUMemoryManager::instance().BufferData(*mVB->GetPtr(), data, mVertexCount * mVertexSize, 0, D3D11_MAP::D3D11_MAP_WRITE_DISCARD, 0);
	UpdateSB();
}
