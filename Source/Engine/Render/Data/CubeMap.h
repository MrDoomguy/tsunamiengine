#ifndef _CUBEMAP_H_
#define _CUBEMAP_H_

#include "ITexture.h"

class CubeMap : public ITexture2D
{
	friend class GPUMemoryManager;
public:
	CubeMap() :
		mSRV{ nullptr }
	{
		for (uint8_t i = 0; i < 6; i++)
		{
			mRTV[i] = nullptr;
		}
	}
	~CubeMap()
	{
		SAFE_RELEASE(mSRV);
		for (uint8_t i = 0; i < 6; i++)
		{
			SAFE_RELEASE(mRTV[i]);
		}
	}

	ID3D11ShaderResourceView* const* GetSRV() const { return &mSRV; }
	ID3D11RenderTargetView* GetRTV(const uint8_t index) const { return mRTV[index]; }
	D3D11_VIEWPORT* GetViewport() { return &mViewport; }

private:
	ID3D11RenderTargetView* mRTV[6];
	ID3D11ShaderResourceView* mSRV;
	D3D11_VIEWPORT mViewport;
};

#endif