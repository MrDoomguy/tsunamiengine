#ifndef _IBUFFER_H_
#define _IBUFFER_H_

#include "../../General/TsunamiStd.h"

class IBuffer
{
public:
	IBuffer() { }
	~IBuffer() { SAFE_RELEASE(mBufferObj); }

	ID3D11Buffer* const* GetPtr() const { return &mBufferObj; }
	ID3D11Resource* GetResource() { return mBufferObj; }
	ID3D11Buffer* GetSubresource() const { return mBufferObj; }
	UINT GetDataByteSize() const { return mByteSize; }

protected:
	ID3D11Buffer* mBufferObj;
	UINT mByteSize;
};

#endif
