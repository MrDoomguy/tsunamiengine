#ifndef _MESHHELPER_H_
#define _MESHHELPER_H_

#include "../../General/MathHelper.h"
#include "MeshTypes.h"

using namespace DirectX;

class MeshHelper
{
public:
	struct BOX_DESC
	{
		float width = 1.f;
		float height = 1.f;
		float depth = 1.f;
	};

	struct SPHERE_DESC
	{
		float radius;
		uint32_t sliceCount;
		uint32_t stackCount;

		SPHERE_DESC()
		{
			radius = 1.0f;
			sliceCount = 16;
			stackCount = 16;
		}
	};

	struct GEOSPHERE_DESC
	{
		float radius;
		uint32_t numSubdivisions;
	};

	struct CYLINDER_DESC
	{
		float bottomRadius;
		float topRadius;
		float height;
		uint32_t sliceCount;
		uint32_t stackCount;
	};

	struct GRID_DESC
	{
		float width;
		float depth;
		uint32_t m;
		uint32_t n;
	};

	void CreateBox(float width, float height, float depth, geotypes::MeshData& meshData);
	void CreateBox(const BOX_DESC& desc, geotypes::MeshData& meshData);

	void CreateSphere(float radius, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData);
	void CreateSphere(const SPHERE_DESC& desc, geotypes::MeshData& meshData);

	void CreateGeosphere(float radius, uint32_t numSubdivisions, geotypes::MeshData& meshData);
	void CreateGeosphere(const GEOSPHERE_DESC& desc, geotypes::MeshData& meshData);

	void CreateCylinder(float bottomRadius, float topRadius, float height, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData);
	void CreateCylinder(const CYLINDER_DESC& desc, geotypes::MeshData& meshData);

	void CreateGrid(float width, float depth, uint32_t m, uint32_t n, geotypes::MeshData& meshData);
	void CreateGrid(const GRID_DESC& desc, geotypes::MeshData& meshData);

	void CreateFullscreenQuad(geotypes::MeshData& meshData);

private:
	// I only provide a single subdivision scheme, later would be great to have different schemes
	void Subdivide(geotypes::MeshData& meshData);

	void BuildCylinderTopCap(float bottomRadius, float topRadius, float height, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData);

	void BuildCylinderBottomCap(float bottomRadius, float topRadius, float height, uint32_t sliceCount, uint32_t stackCount, geotypes::MeshData& meshData);
};

#endif
