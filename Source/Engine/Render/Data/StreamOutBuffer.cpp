#include "StreamOutBuffer.h"

StreamOutBuffer::StreamOutBuffer(ID3D11Device* pDevice, int bufferSize)
{
	HRESULT hr;

	D3D11_BUFFER_DESC vd;
	ZeroMemory(&vd, sizeof(D3D11_BUFFER_DESC));
	vd.BindFlags = D3D11_BIND_STREAM_OUTPUT | D3D11_BIND_VERTEX_BUFFER;
	vd.ByteWidth = bufferSize;
	vd.Usage = D3D11_USAGE_DEFAULT;
	vd.CPUAccessFlags = 0;
	vd.MiscFlags = 0;
	vd.StructureByteStride = 0;

	if(FAILED(hr = pDevice->CreateBuffer(&vd, nullptr, &mBufferObj)))
	{
		SAFE_RELEASE(mBufferObj);
		std::wstring err = L"Stream Out Buffer allocation failed." + HR_to_wstr(hr);
		OutputDebugString(err.c_str());
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, err);
		exit(1);
	}
}

StreamOutBuffer::~StreamOutBuffer()
{

}
