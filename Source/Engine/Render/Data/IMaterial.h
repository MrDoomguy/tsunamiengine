#ifndef _IMATERIAL_H_
#define _IMATERIAL_H_

#include "../../General/TsunamiStd.h"
#include "../../Scene/TransformComponent.h"
#include "../../Scene/AnimationComponent.h"

// TODO: could be template class!
class IMaterial
{
public:
	enum class Type
	{
		DIFFUSE,
		PBS_SOLID,
		PBS_SOLID_SKINNED,
		PBS_VOLUMETRIC,
		TRANSPARENCY,
		GUI,
		FONT,
		SKY,
		COMPUTE,
		PARTICLE,
		INVALID
	};

	explicit IMaterial(const Type type) : mType{ type } {}
	~IMaterial() {}

	virtual HRESULT Init(ID3D11Device* pDevice) { return E_NOTIMPL; }
	virtual HRESULT Init(IDWriteFactory* pWriteFactory) { return E_NOTIMPL; }

	Type GetType() const { return mType; }

	virtual void SetConstantData(void* pData) {}

	virtual void Set(IDWriteFactory* pWriteFactory) {}
	virtual void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform, const AnimationComponent* pAnimation=nullptr) {}
	virtual void Reset(ID3D11DeviceContext* pContext) {}

	virtual const void* GetPerObjectData() const { return nullptr; }

protected:
	Type mType;
};

#endif
