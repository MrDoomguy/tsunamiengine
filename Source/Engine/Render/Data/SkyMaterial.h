#ifndef _SKYMATERIAL_H_
#define _SKYMATERIAL_H_

#include "../../General/GPUMemoryManager.h"

class SkyMaterial : public IMaterial
{
public:
	struct PerObject
	{
		DirectX::XMMATRIX world;
	};

	SkyMaterial(CubeMap* pSkyProbeTexture, ID3D11SamplerState* pSampler) :
		IMaterial(IMaterial::Type::SKY)
	{
		mSkyProbe = pSkyProbeTexture;
		mSampler = pSampler;
	}

	SkyMaterial() :
		IMaterial(IMaterial::Type::SKY)
	{
		mSkyProbe = GPUMemoryManager::instance().GetDefaultCubeMap();
		mSampler = GPUMemoryManager::instance().GetDefaultTextureSampler();
	}

	~SkyMaterial() { SAFE_RELEASE(mBuffer); }

	HRESULT Init(ID3D11Device* pDevice) override
	{
		HRESULT hr;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
		bd.ByteWidth = sizeof(SkyMaterial::PerObject);
		bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		bd.StructureByteStride = 0;
		if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mBuffer)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate material buffer.");
			return hr;
		}

		return S_OK;
	}

	void SetSkyProbe(CubeMap* pSkyProbe) { mSkyProbe = pSkyProbe; }
	CubeMap* GetSkyProbe() const { return mSkyProbe; }
	void SetSampler(ID3D11SamplerState* pSampler) { mSampler = pSampler; }

	void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform, const AnimationComponent* pAnimation=nullptr) override
	{
		// link to camera transform
		mPerObject.world = transform->GetWorld();

		pContext->UpdateSubresource(mBuffer, 0, nullptr, &mPerObject, 0, 0);
		pContext->VSSetConstantBuffers(1, 1, &mBuffer);

		pContext->PSSetSamplers(0, 1, &mSampler);

		pContext->PSSetShaderResources(0, 1, mSkyProbe->GetSRV());
	}

	void Reset(ID3D11DeviceContext* pContext) override
	{
	}

private:
	CubeMap* mSkyProbe;
	ID3D11SamplerState* mSampler;
	SkyMaterial::PerObject mPerObject;
	ID3D11Buffer* mBuffer;
};

#endif
