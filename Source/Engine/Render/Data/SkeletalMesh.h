#ifndef _SKELETALMESH_H_
#define _SKELETALMESH_H_

#include "IndexedMesh.h"

class SkeletalMesh : public IndexedMesh
{
	friend class GPUMemoryManager;
public:
	SkeletalMesh()
	{
		mVertexSize = sizeof(geotypes::SkinnedVertex);
		mIsSkinned = true;
	}

	~SkeletalMesh() { }
	geotypes::SkinnedMeshData* GetMesh() { return &mMesh; }

private:
	geotypes::SkinnedMeshData mMesh;
};

#endif