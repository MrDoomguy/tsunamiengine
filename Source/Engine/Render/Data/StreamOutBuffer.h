#pragma once

#include "IBuffer.h"

class StreamOutBuffer : public IBuffer
{
public:
	explicit StreamOutBuffer(ID3D11Device* pDevice, int bufferSize);
	~StreamOutBuffer();
};

