#pragma once

#include "IShader.h"
#include "VertexFormat.h"

class GeometryShader : public IShader
{
	friend class GPUMemoryManager;
public:
	GeometryShader() : mpObject{ nullptr } {}
	~GeometryShader() { SAFE_RELEASE(mpObject); }

	virtual HRESULT Load(ID3D11Device* pDevice, const IShader::Desc& desc) override
	{
		HRESULT hr;

		if (FAILED(hr = CompileFromFile(desc)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to compile Geometry Shader.");
			return hr;
		}

		if (FAILED(hr = pDevice->CreateGeometryShader(mpBlob->GetBufferPointer(), mpBlob->GetBufferSize(), nullptr, &mpObject)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to Create Geometry Shader.");
			return hr;
		}

		return S_OK;
	}

	ID3D11GeometryShader* GetObjectPtr() { return mpObject; }

private:
	ID3D11GeometryShader* mpObject;
};


