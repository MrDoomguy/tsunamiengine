#ifndef _SHADERSTORE_H_
#define _SHADERSTORE_H_

#include "../../General/TsunamiStd.h"
#include "../../General/Filesystem.h"

#include "PixelShader.h"
#include "VertexShader.h"
#include "ComputeShader.h"

class ShaderStore
{
public:
	/*enum ShaderModel
	{
		SM_5,
		SM_4
	};*/

	//explicit ShaderStore(const ShaderModel sm);
	ShaderStore();
	~ShaderStore();

	void LoadDataFromDisk(ID3D11Device* pDevice, const std::wstring& pathToFolder);

	PixelShader* GetPixelShader(const std::string& name);
	VertexShader* GetVertexShader(const std::string& name);
	ComputeShader* GetComputeShader(const std::string& name);

private:
	/*enum ShaderIdx
	{
		VERTEX = 0,
		PIXEL = 1,
		COMPUTE = 2,
		HULL = 3,
		TESSELLATION = 4,
		GEOMETRY = 5
	};*/

	//ShaderModel mSM;

	std::unordered_map<std::string, VertexShader*> mVertex;
	std::unordered_map<std::string, PixelShader*> mPixel;
	std::unordered_map<std::string, ComputeShader*> mCompute;
};

#endif
