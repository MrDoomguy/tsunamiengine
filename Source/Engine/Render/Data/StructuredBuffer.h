#pragma once

#include "IBuffer.h"

class StructuredBuffer 
	: public IBuffer
{
public:
	explicit StructuredBuffer(ID3D11Device* pDevice, UINT dataByteSize, UINT byteStride, const bool isRW, const bool isStaged, const UINT cpuAccessFlag);
	~StructuredBuffer();

	void Map(ID3D11DeviceContext* context, D3D11_MAP mapType, D3D11_MAPPED_SUBRESOURCE* mappedSubresource, UINT subresourceIndex = 0);
	void Unmap(ID3D11DeviceContext* context, UINT subresourceIndex = 0);

	ID3D11UnorderedAccessView* const * GetUAV() const { return &mUAV; }
	ID3D11ShaderResourceView* const * GetSRV() const { return &mSRV; }

private:
	ID3D11Buffer *mStagingBuffer = nullptr;
	ID3D11ShaderResourceView* mSRV = nullptr;
	ID3D11UnorderedAccessView* mUAV;
};