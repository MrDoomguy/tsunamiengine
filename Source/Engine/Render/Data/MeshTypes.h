#ifndef _MESHTYPES_H_
#define _MESHTYPES_H_

#include "../../General/TsunamiStd.h"

namespace geotypes
{
	namespace vertices
	{
		struct SkinnedVertex
		{
			SkinnedVertex() {}
			SkinnedVertex(const DirectX::XMFLOAT3& p,
				const DirectX::XMFLOAT3& n,
				const DirectX::XMFLOAT3& t,
				const DirectX::XMFLOAT2& uv,
				const float a,
				const XMUBYTE4& ids,
				const DirectX::XMFLOAT4& w)
				: position(p), normal(n), tangent(t), texC(uv), area(a), boneIDs(ids), weights(w) {}
			SkinnedVertex(const float px, const float py, const float pz,
				const float nx, const float ny, const float nz,
				const float tx, const float ty, const float tz,
				const float u, const float v,
				const float a,
				const uint8_t id0, const uint8_t id1, const uint8_t id2, const uint8_t id3,
				const float w0, const float w1, const float w2, const float w3)
				: position(px, py, pz), normal(nx, ny, nz), tangent(tx, ty, tz), texC(u, v), area(a), boneIDs(id0, id1, id2, id3), weights(w0, w1, w2, w3) {}

			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			DirectX::XMFLOAT3 tangent;
			DirectX::XMFLOAT2 texC;
			float area;
			// one channel for each bone
			XMUBYTE4 boneIDs;
			DirectX::XMFLOAT4 weights;
		};

		struct BaseParticleVertex
		{
			BaseParticleVertex() {}
			BaseParticleVertex(const DirectX::XMFLOAT3& p, const float& s, const uint32_t& st, const float& time) : position(p), scale(s), state(st), ttl(time) {}
			DirectX::XMFLOAT3 position;
			float scale;
			uint32_t state;
			float ttl;

		};
		// This data structure is used to keep all vertices information
		struct Vertex
		{
			Vertex() {}
			Vertex(const DirectX::XMFLOAT3& p,
				const DirectX::XMFLOAT3& n,
				const DirectX::XMFLOAT3& t,
				const DirectX::XMFLOAT2& uv,
				const float& a)
				: position(p), normal(n), tangent(t), texC(uv), area(a) {}
			Vertex(const float px, const float py, const float pz,
				const float nx, const float ny, const float nz,
				const float tx, const float ty, const float tz,
				const float u, const float v,
				const float a)
				: position(px, py, pz), normal(nx, ny, nz), tangent(tx, ty, tz), texC(u, v), area(a) {}

			DirectX::XMFLOAT3 position;
			DirectX::XMFLOAT3 normal;
			//float _pad1;
			DirectX::XMFLOAT3 tangent;
			DirectX::XMFLOAT2 texC;
			float area;
		};

		struct SpriteVertex
		{
			SpriteVertex() {}
			SpriteVertex(const DirectX::XMFLOAT4& p, const DirectX::XMFLOAT2& t, const uint32_t& st, const float& time, const float& r) : position(p), tex(t), state(st), ttl(time), radius(r) {}
			DirectX::XMFLOAT4 position;
			DirectX::XMFLOAT2 tex;
			uint32_t state;
			float ttl;
			float radius;
		};

	}
	using namespace vertices;

	namespace bounding
	{
		// AABB generated on mesh load
		struct BoundingVolume
		{
			DirectX::XMFLOAT3 min;
			DirectX::XMFLOAT3 max;
			DirectX::XMFLOAT3 size;

			BoundingVolume() :
				min{ DirectX::XMFLOAT3(FLT_MAX, FLT_MAX, FLT_MAX) },
				max{ DirectX::XMFLOAT3(-FLT_MAX, -FLT_MAX, -FLT_MAX) },
				size{ DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f) }
			{ }
		};
	}
	using namespace bounding;

	namespace meshes
	{
		// MeshData is a point of access to the mesh data, I'll make sure to load each mesh a single time.
		struct MeshData
		{
			MeshData() {}
			// Preferred to avoid runtime memory resize
			MeshData(uint32_t vertexCount, uint32_t indexCount)
			{
				vertices.resize(vertexCount);
				indices.resize(indexCount);
			}

			std::vector<Vertex> vertices;
			std::vector<INDEX_TYPE> indices;
			BoundingVolume bounding;
		};

		struct SkinnedMeshData
		{
			SkinnedMeshData() {}
			SkinnedMeshData(const uint32_t vertexCount, const uint32_t indexCount)
			{
				vertices.resize(vertexCount);
				indices.resize(indexCount);
			}

			std::vector<SkinnedVertex> vertices;
			std::vector<INDEX_TYPE> indices;
			BoundingVolume bounding;
		};
	}
	using namespace meshes;
}

#endif
