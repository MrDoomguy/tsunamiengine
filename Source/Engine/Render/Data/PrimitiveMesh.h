#pragma once

#include "IMesh.h"
#include "StreamOutBuffer.h"
#include "MeshTypes.h"


class PrimitiveMesh : public IMesh
{
	friend class GPUMemoryManager;
public:
	PrimitiveMesh()
	{
		mVertexSize = sizeof(geotypes::BaseParticleVertex);
	}

	~PrimitiveMesh(){}

	void Draw(ID3D11DeviceContext* context) override
	{
		uint32_t stride = static_cast<uint32_t>(mVertexSize);
		uint32_t offset = 0;
		context->IASetVertexBuffers(0, 1, mVB->GetPtr(), &stride, &offset);
		context->SOSetTargets(1, mStreamBuffer->GetPtr(), &offset);
		context->Draw(mVertexCount, 0);
		ID3D11Buffer* bfr[] = { nullptr };
		context->SOSetTargets(1, bfr, &offset);
	}
	void DrawAuto(ID3D11DeviceContext* context)
	{
		uint32_t stride = sizeof(geotypes::SpriteVertex);
		uint32_t offset = 0;
		context->IASetVertexBuffers(0, 1, mStreamBuffer->GetPtr(), &stride, &offset);

		context->DrawAuto();
	}
private:
	StreamOutBuffer* mStreamBuffer;
};

