#pragma once

#include "../../Render/Data/MeshHelper.h"
#include "VertexBuffer.h"
#include "StructuredBuffer.h"

class IMesh
{
public:
	IMesh() : mVB{ nullptr }, mSB{ nullptr } {}
	~IMesh()
	{
		delete mVB;
		delete mSB;
	}

	ID3D11Buffer* const* GetVB() const { return mVB->GetPtr(); }
	ID3D11ShaderResourceView* const* GetSRV() { return mSB->GetSRV(); }
	ID3D11UnorderedAccessView* const* GetUAV() { return mSB->GetUAV(); }
	ID3D11Buffer* const* GetStructuredBuffer() { return mSB->GetPtr(); }
	bool IsStaticMesh() const { return mIsStatic; }
	bool IsSkinnedMesh() const { return mIsSkinned; }

	void UpdateVB();
	void UpdateSB();
	void Update(void* data);

	virtual void Draw(ID3D11DeviceContext* context) = 0;

	int GetVertexCount() { return mVertexCount; }

protected:
	VertexBuffer* mVB;
	StructuredBuffer* mSB;

	bool mIsStatic;
	bool mIsSkinned;

	uint32_t mVertexCount;
	size_t mVertexSize;
};

