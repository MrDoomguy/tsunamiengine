#ifndef _ISHADER_H_
#define _ISHADER_H_

#include "../../General/TsunamiStd.h"

class IShader
{
	friend class GPUMemoryManager;
public:
	enum ShaderType
	{
		VERTEX = 0x01,
		PIXEL = 0x02,
		COMPUTE = 0x04
	};

	IShader() : mpBlob{ nullptr } {}
	~IShader() { SAFE_RELEASE(mpBlob); }

	struct Desc
	{
		const wchar_t* filename;
		const char* entryPoint;
		const char* profile;
		DWORD dwFlags;
	};

	virtual HRESULT Load(ID3D11Device* pDevice, const IShader::Desc& desc) = 0;

	ID3DBlob* GetBlob() { return mpBlob; }

protected:
	HRESULT CompileFromFile(const IShader::Desc& desc)
	{
		HRESULT hr;
		mpBlob = nullptr;
		ID3DBlob* pErrorBlob = nullptr;
		if (FAILED(hr = D3DCompileFromFile(desc.filename, nullptr, nullptr, desc.entryPoint, desc.profile, desc.dwFlags, 0, &mpBlob, &pErrorBlob)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, std::string((char*)pErrorBlob->GetBufferPointer()));

			SAFE_RELEASE(pErrorBlob);
			SAFE_RELEASE(mpBlob);

			return hr;
		}

		return S_OK;
	}

	ID3DBlob* mpBlob;
};

#endif