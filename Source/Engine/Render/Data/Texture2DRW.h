#ifndef _TEXTURE2DRW_H_
#define _TEXTURE2DRW_H_

#include "ITexture.h"

class Texture2DRW : public ITexture2D
{
	friend class GPUMemoryManager;
public:
	Texture2DRW() : mSRV{ nullptr }, mUAV{ nullptr } { }
	~Texture2DRW() { SAFE_RELEASE(mSRV); SAFE_RELEASE(mUAV); }

	virtual void Resize(ID3D11Device* pDevice, uint32_t width, uint32_t height) override
	{
		mWidth = width;
		mHeight = height;

		D3D11_TEXTURE2D_DESC td;
		mTexture->GetDesc(&td);
		SAFE_RELEASE(mTexture);
		td.Width = width;
		td.Height = height;
		ThrowIfFailed(pDevice->CreateTexture2D(&td, nullptr, &mTexture));

		D3D11_UNORDERED_ACCESS_VIEW_DESC ud;
		mUAV->GetDesc(&ud);
		SAFE_RELEASE(mUAV);
		ThrowIfFailed(pDevice->CreateUnorderedAccessView(mTexture, &ud, &mUAV));

		if (mSRV)
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC sd;
			mSRV->GetDesc(&sd);
			SAFE_RELEASE(mSRV);
			ThrowIfFailed(pDevice->CreateShaderResourceView(mTexture, &sd, &mSRV));
		}
	}

	ID3D11ShaderResourceView* const* GetSRV() const { return &mSRV; }
	ID3D11UnorderedAccessView* const* GetUAV() const { return &mUAV; }

private:
	ID3D11ShaderResourceView* mSRV;
	ID3D11UnorderedAccessView* mUAV;
};

#endif
