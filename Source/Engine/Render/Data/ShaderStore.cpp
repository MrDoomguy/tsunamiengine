#include "ShaderStore.h"

/*ShaderStore::ShaderStore(const ShaderModel sm)
	: mSM{ sm }
{
}*/

ShaderStore::ShaderStore()
{
}

ShaderStore::~ShaderStore()
{
}

void ShaderStore::LoadDataFromDisk(ID3D11Device* pDevice, const std::wstring& pathToFolder)
{
	std::vector<std::wstring> filenames = Filesystem::GetFilenames(pathToFolder);

	IShader::Desc desc;
	ZeroMemory(&desc, sizeof(IShader::Desc));
	desc.entryPoint = "main";
	desc.dwFlags = 0;

	/*std::vector<std::string> profiles(5);
	if (mSM == ShaderModel::SM_5)
	{
		profiles[ShaderIdx::VERTEX] = "vs_5_0";
	}*/

	for (auto& f : filenames)
	{
		desc.filename = f.c_str();
		std::string fName = wstr_to_str(f);
		// extract name from "filename_Stage_.hlsl"
		fName = fName.substr(fName.find_first_of("."));

		switch (Filesystem::GetShaderType(f))
		{
		case ShaderType::VERTEX:
		{
			desc.profile = "vs_5_0";
			mVertex[fName] = new VertexShader();
			mVertex[fName]->Load(pDevice, desc);
			break;
		}
		
		case ShaderType::PIXEL:
		{
			desc.profile = "ps_5_0";
			mPixel[fName] = new PixelShader();
			mPixel[fName]->Load(pDevice, desc);
			break;
		}

		case ShaderType::COMPUTE:
		{
			desc.profile = "cs_5_0";
			mCompute[fName] = new ComputeShader();
			mCompute[fName]->Load(pDevice, desc);
			break;
		}

		default:
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, L"Unknown shader type reported.");
			break;
		}
	}
}

PixelShader* ShaderStore::GetPixelShader(const std::string& name)
{
	auto shader = mPixel.find(name);
	if (shader != mPixel.end())
		return shader->second;
	else
		return nullptr;
}

VertexShader* ShaderStore::GetVertexShader(const std::string& name)
{
	auto shader = mVertex.find(name);
	if (shader != mVertex.end())
		return shader->second;
	else
		return nullptr;
}

ComputeShader* ShaderStore::GetComputeShader(const std::string& name)
{
	auto shader = mCompute.find(name);
	if (shader != mCompute.end())
		return shader->second;
	else
		return nullptr;
}
