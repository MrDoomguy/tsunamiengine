#include "TextureStore.h"

TextureStore::TextureStore()
{
}

TextureStore::~TextureStore()
{
}

void TextureStore::LoadDataFromDisk(ID3D11Device* pDevice, const std::wstring& pathToFolder)
{
	std::vector<std::wstring> filenames = Filesystem::GetFilenames(pathToFolder);

	for (auto& f : filenames)
	{
		std::string fName = wstr_to_str(f);
		fName = fName.substr(fName.find_first_of("."));

		mColor[fName] = new ColorMap();
		mColor[fName]->LoadFromDisk(pDevice, f);
	}
}

void TextureStore::AddVolumeTexture(ID3D11Device* pDevice, const int3& size)
{
}

ColorMap* TextureStore::GetColorTexture(const std::string& name)
{
	auto texture = mColor.find(name);
	if (texture != mColor.end())
		return texture->second;
	else
		return nullptr;
}

VolumeTexture* TextureStore::GetVolumeTexture(const std::string& name)
{
	auto texture = mVolume.find(name);
	if (texture != mVolume.end())
		return texture->second;
	else
		return nullptr;
}
