#ifndef _RENDERTARGET_H_
#define _RENDERTARGET_H_

#include "ITexture.h"

class RenderTarget : public ITexture2D
{
	friend class GPUMemoryManager;
public:
	RenderTarget() : mRTV{ nullptr }, mSRV{ nullptr } { }
	~RenderTarget()
	{
		SAFE_RELEASE(mSRV);
		SAFE_RELEASE(mRTV);
	}

	ID3D11RenderTargetView* GetRTV() const { return mRTV; }
	ID3D11ShaderResourceView* const* GetSRV() const { return &mSRV; }

	virtual void Resize(ID3D11Device* pDevice, uint32_t width, uint32_t height) override
	{
		mWidth = width;
		mHeight = height;

		D3D11_TEXTURE2D_DESC td;
		mTexture->GetDesc(&td);
		SAFE_RELEASE(mTexture);
		td.Width = width;
		td.Height = height;
		ThrowIfFailed(pDevice->CreateTexture2D(&td, nullptr, &mTexture));

		D3D11_RENDER_TARGET_VIEW_DESC rd;
		mRTV->GetDesc(&rd);
		SAFE_RELEASE(mRTV);
		ThrowIfFailed(pDevice->CreateRenderTargetView(mTexture, &rd, &mRTV));

		if (mSRV)
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC sd;
			mSRV->GetDesc(&sd);
			SAFE_RELEASE(mSRV);
			ThrowIfFailed(pDevice->CreateShaderResourceView(mTexture, &sd, &mSRV));
		}
	}

	void Release()
	{
		mWidth = 0;
		mHeight = 0;
		SAFE_RELEASE(mSRV);
		SAFE_RELEASE(mRTV);
		SAFE_RELEASE(mTexture);
	}

private:
	ID3D11RenderTargetView* mRTV;
	ID3D11ShaderResourceView* mSRV;
};

#endif
