#ifndef _PIXELSHADER_H_
#define _PIXELSHADER_H_

#include "IShader.h"

class PixelShader : public IShader
{
	friend class GPUMemoryManager;
public:
	PixelShader() : mpObject{ nullptr } {}
	~PixelShader() { SAFE_RELEASE(mpObject); }

	virtual HRESULT Load(ID3D11Device* pDevice, const IShader::Desc& desc) override
	{
		HRESULT hr;
		if (FAILED(hr = CompileFromFile(desc)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to compile Pixel Shader.");
			return hr;
		}

		if (FAILED(hr = pDevice->CreatePixelShader(mpBlob->GetBufferPointer(), mpBlob->GetBufferSize(), nullptr, &mpObject)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to Create Pixel Shader.");
			return hr;
		}

		return S_OK;
	}

	ID3D11PixelShader* GetObjectPtr() { return mpObject; }

private:
	ID3D11PixelShader* mpObject;
};

#endif
