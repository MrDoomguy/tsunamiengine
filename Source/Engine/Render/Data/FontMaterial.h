#ifndef _FONTMATERIAL_H_
#define _FONTMATERIAL_H_

#include "IMaterial.h"

class FontMaterial : public IMaterial
{
public:
	struct PerObject
	{
		std::wstring text;
		std::wstring fontName;
		DWRITE_FONT_WEIGHT weight;
		DWRITE_FONT_STYLE style;
		DWRITE_FONT_STRETCH stretch;
		DWRITE_TEXT_ALIGNMENT textAlignment;
		DWRITE_PARAGRAPH_ALIGNMENT paragraphAlignment;
		D2D1_RECT_F rect;
		D2D1_COLOR_F color;
		float size;

		PerObject() :
			text{ L"New Font Material" },
			fontName{ L"Verdana" },
			weight{ DWRITE_FONT_WEIGHT::DWRITE_FONT_WEIGHT_NORMAL },
			style{ DWRITE_FONT_STYLE::DWRITE_FONT_STYLE_NORMAL },
			stretch{ DWRITE_FONT_STRETCH::DWRITE_FONT_STRETCH_NORMAL },
			textAlignment{ DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_CENTER },
			paragraphAlignment{ DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_CENTER },
			color{ D2D1::ColorF::ForestGreen},
			size{ 12.0f }
		{
			rect = D2D1::RectF(0.0f, 0.0f, 100.0f, 100.0f);
		}
	};

	FontMaterial(const FontMaterial::PerObject& data) :
		IMaterial(IMaterial::Type::FONT)
	{
		mTextFormat = nullptr;

		mPerObject = data; 
	}
	FontMaterial() :
		IMaterial(IMaterial::Type::FONT) { }
	~FontMaterial() 
	{
		SAFE_RELEASE(mTextFormat);
	}

	using IMaterial::Set;
	HRESULT Init(IDWriteFactory* pWriteFactory) override
	{
		HRESULT hr;

		if (FAILED(hr = pWriteFactory->CreateTextFormat(
			mPerObject.fontName.c_str(),
			nullptr,
			mPerObject.weight,
			mPerObject.style,
			mPerObject.stretch,
			mPerObject.size,
			L"",
			&mTextFormat)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create Text Format." + HR_to_wstr(hr));
			return hr;
		}

		mTextFormat->SetTextAlignment(mPerObject.textAlignment);
		mTextFormat->SetParagraphAlignment(mPerObject.paragraphAlignment);

		return S_OK;
	}

	using IMaterial::Init;
	void Set(IDWriteFactory* pWriteFactory) override
	{
		if (mDirty)
		{
			this->Init(pWriteFactory);
			mDirty = false;
		}
	}

	void SetConstantData(void* pData) override
	{
		mPerObject = (*reinterpret_cast<FontMaterial::PerObject*>(pData));
	}

	const void* GetPerObjectData() const override
	{
		return &mPerObject;
	}

	void SetText(const wchar_t* text)
	{ 
		mPerObject.text = text;
	}
	std::wstring GetText() const { return mPerObject.text; }

	void SetRect(const float left, const float top, const float right, const float bottom)
	{ 
		mPerObject.rect = D2D1::RectF(left, top, right, bottom);
	}
	D2D1_RECT_F GetRect() const { return mPerObject.rect; }

	void SetSolidColor(const D2D1_COLOR_F& color) { mPerObject.color = color; }
	D2D1_COLOR_F GetColor() const { return mPerObject.color; }

	void SetFontSize(const float size)
	{
		mPerObject.size = size;
		mDirty = true;
	}
	float GetFontSize() const { return mPerObject.size; }

	void SetFontName(const wchar_t* fontName)
	{
		mPerObject.fontName = fontName;
		mDirty = true;
	}
	std::wstring GetFontName() const { return std::wstring(mPerObject.fontName); }

	void SetTextAlignment(const DWRITE_TEXT_ALIGNMENT alignment)
	{
		mPerObject.textAlignment = alignment;
		mTextFormat->SetTextAlignment(alignment);
	}
	DWRITE_TEXT_ALIGNMENT GetTextAlignment() const { return mPerObject.textAlignment; }

	void SetParagraphAlignment(const DWRITE_PARAGRAPH_ALIGNMENT alignment)
	{
		mPerObject.paragraphAlignment = alignment;
		mTextFormat->SetParagraphAlignment(alignment);
	}
	DWRITE_PARAGRAPH_ALIGNMENT GetParagraphAlignment() const { return mPerObject.paragraphAlignment; }

	IDWriteTextFormat* GetTextFormat() { return mTextFormat; }

private:
	FontMaterial::PerObject mPerObject;

	bool mDirty = false;

	IDWriteTextFormat* mTextFormat;
};

#endif
