#ifndef _VOLUMETEXTURE_H_
#define _VOLUMETEXTURE_H_

#include "ITexture.h"

class VolumeTexture : public ITexture3D
{
	friend class GPUMemoryManager;
public:
	VolumeTexture() : mSRV(nullptr) {}
	~VolumeTexture() { SAFE_RELEASE(mSRV); }

	ID3D11ShaderResourceView* const* GetSRV() const { return &mSRV; }

private:
	ID3D11ShaderResourceView* mSRV;
};

#endif
