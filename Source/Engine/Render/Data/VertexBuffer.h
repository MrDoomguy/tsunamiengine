#pragma once

#include "IBuffer.h"

class VertexBuffer : public IBuffer
{
public:
	explicit VertexBuffer(ID3D11Device* pDevice, UINT dataByteSize, bool isStatic, void* data = nullptr);
	~VertexBuffer();
};

