#ifndef _COLORMAP_H_
#define _COLORMAP_H_

#include "ITexture.h"

class ColorMap : public ITexture2D
{
	friend class GPUMemoryManager;
public:
	ColorMap() : mSRV{ nullptr } { }
	~ColorMap()
	{
		SAFE_RELEASE(mSRV);
	}

	/*HRESULT LoadFromDisk(ID3D11Device* pDevice, const std::wstring& filename)
	{
		HRESULT hr;

		ID3D11Resource* pTex;

		if (FAILED(hr = DirectX::CreateDDSTextureFromFile(pDevice, filename.c_str(), &pTex, &mSRV)))
		{
			SAFE_RELEASE(mSRV);
			return hr;
		}

		mTexture = reinterpret_cast<ID3D11Texture2D*>(pTex);

		D3D11_TEXTURE2D_DESC td;
		mTexture->GetDesc(&td);

		mWidth = td.Width;
		mHeight = td.Height;

		return S_OK;
	}*/

	ID3D11ShaderResourceView* const* GetSRV() const
	{
		return &mSRV;
	}

private:
	ID3D11ShaderResourceView* mSRV;
};

#endif
