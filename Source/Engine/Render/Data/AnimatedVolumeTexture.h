#pragma once
#include "VolumeTexture.h"

class AnimatedVolumeTexture : public VolumeTexture
{
public:

	AnimatedVolumeTexture() : VolumeTexture() {}
	AnimatedVolumeTexture(const DirectX::XMUINT4& dim, float speed, float factor) : VolumeTexture(), mDim(dim), mSpeed(speed), mFactor(factor) {}

	void SetDim(const DirectX::XMUINT4& dim) { mDim = dim; }
	const DirectX::XMUINT4& GetSize() const { return mDim; }

	// returns offset in texture depending on time
	DirectX::XMUINT3 GetOffsetOfTime(int t)
	{
		t = t % mDim.w;

		DirectX::XMUINT3 texArraySize = DirectX::XMUINT3(mWidth / mDim.x, mHeight / mDim.y, mDepth / mDim.z);

		DirectX::XMUINT3 offset;
		offset.x = t % texArraySize.x * mDim.x;
		offset.y = (t / texArraySize.x) % texArraySize.y * mDim.y;
		offset.z = t / (texArraySize.x * texArraySize.y) * mDim.z;

		return offset;
	}

	float GetSpeed() { return mSpeed; }
	void SetSpeed(float speed) { mSpeed = speed; }
	float GetFactor() { return mFactor; }
	void SetFactor(float factor) { mFactor = factor; }

private:
	DirectX::XMUINT4 mDim;
	float mSpeed = 1.f;
	float mFactor = 1.f;
};