#ifndef _VOLUMETRICMATERIAL_H_
#define _VOLUMETRICMATERIAL_H_

#include "IMaterial.h"
#include "VolumeTexture.h"

class VolumetricMaterial : public IMaterial
{
public:
	struct PerObject
	{
		DirectX::XMMATRIX sdfToWorld;
		DirectX::XMMATRIX worldToSDF;
		DirectX::XMUINT4 dim;
		DirectX::XMUINT3 extent;
		float _pad0;

		PerObject() :
			sdfToWorld{ XMMatrixIdentity() },
			worldToSDF{ XMMatrixIdentity() }, 
			dim{ DirectX::XMUINT4(0, 0, 0, 0) },
			extent{ DirectX::XMUINT3(0, 0, 0) }
		{}
	};

	VolumetricMaterial(const VolumetricMaterial::PerObject& data, ID3D11ShaderResourceView* pTexture, ID3D11SamplerState* pSampler) :
		IMaterial(IMaterial::Type::COMPUTE)
	{
		mPerObject = data;
		mSRV = pTexture;
		mSampler = pSampler;
	}

	VolumetricMaterial() :
		IMaterial(IMaterial::Type::COMPUTE) {}

	~VolumetricMaterial() { SAFE_RELEASE(mBuffer); }

	virtual HRESULT Init(ID3D11Device* pDevice) override
	{
		HRESULT hr;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
		bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
		bd.ByteWidth = sizeof(VolumetricMaterial::PerObject);
		bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;
		bd.StructureByteStride = 0;
		if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mBuffer)))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate material buffer.");
			return hr;
		}

		return S_OK;
	}

	void SetVolumeDim(const XMUINT4& dim) { mPerObject.dim = dim; }
	XMUINT4 GetVolumeDim() const { return mPerObject.dim; }

	void SetVolumeExtent(const DirectX::XMUINT3& extent) { mPerObject.extent = extent; }
	DirectX::XMUINT3 GetVolumeExtent() const { return mPerObject.extent; }

	void SetVolumeTexture(ID3D11ShaderResourceView* pTexture) { mSRV = pTexture; }
	ID3D11ShaderResourceView* GetVolumeTexture() const { return mSRV; }

	void SetTextureSampler(ID3D11SamplerState* pSampler) { mSampler = pSampler; }
	ID3D11SamplerState* GetTextureSampler() { return mSampler; }

	virtual void SetConstantData(void* pData) override
	{
		mPerObject = (*reinterpret_cast<VolumetricMaterial::PerObject*>(pData));
	}

	virtual void Set(ID3D11DeviceContext* pContext, const TransformComponent* transform, const AnimationComponent* pAnimation = nullptr) override
	{
		mPerObject.sdfToWorld = transform->GetWorld();
		mPerObject.worldToSDF = transform->GetLocal();

		// Bind buffer to cs
		pContext->CSSetConstantBuffers(1, 1, &mBuffer);
		// update data
		pContext->UpdateSubresource(mBuffer, 0, nullptr, &mPerObject, 0, 0);

		// set volume sampler
		pContext->CSSetSamplers(0, 1, &mSampler);

		// set volume resource
		pContext->CSSetShaderResources(0, 1, &mSRV);
	}

	virtual void Reset(ID3D11DeviceContext* pContext) override
	{
		ID3D11ShaderResourceView* nullSRV[] = { nullptr };
		pContext->CSSetShaderResources(0, 1, nullSRV);
	}

	const void* GetPerObjectData() const override
	{
		return &mPerObject;
	}

private:
	VolumetricMaterial::PerObject mPerObject;
	ID3D11ShaderResourceView* mSRV;
	ID3D11SamplerState* mSampler;
	ID3D11Buffer* mBuffer;
};

#endif
