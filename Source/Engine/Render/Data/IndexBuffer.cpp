#include "IndexBuffer.h"

IndexBuffer::IndexBuffer(ID3D11Device* pDevice, const UINT dataByteSize, const bool isStaticMesh, void* pData)
{
	mByteSize = dataByteSize;

	HRESULT hr;

	D3D11_BUFFER_DESC id;
	ZeroMemory(&id, sizeof(D3D11_BUFFER_DESC));
	id.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_INDEX_BUFFER;
	id.ByteWidth = dataByteSize;
	id.Usage = (isStaticMesh) ? D3D11_USAGE::D3D11_USAGE_IMMUTABLE : D3D11_USAGE::D3D11_USAGE_DYNAMIC;
	id.CPUAccessFlags = (isStaticMesh) ? 0 : D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
	id.MiscFlags = 0;
	id.StructureByteStride = 0;

	if (pData != nullptr)
	{
		D3D11_SUBRESOURCE_DATA idata;
		ZeroMemory(&idata, sizeof(D3D11_SUBRESOURCE_DATA));
		idata.pSysMem = pData;
		idata.SysMemPitch = 0;
		idata.SysMemSlicePitch = 0;

		if (FAILED(hr = pDevice->CreateBuffer(&id, &idata, &mBufferObj)))
		{
			SAFE_RELEASE(mBufferObj);
			std::wstring err = L"Index Buffer allocation failed." + HR_to_wstr(hr);
			core::TsunamiFatalError(err);
		}
	}
	else if (FAILED(hr = pDevice->CreateBuffer(&id, nullptr, &mBufferObj)))
	{
		SAFE_RELEASE(mBufferObj);
		std::wstring err = L"Index Buffer allocation failed." + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}
}

IndexBuffer::~IndexBuffer()
{
}
