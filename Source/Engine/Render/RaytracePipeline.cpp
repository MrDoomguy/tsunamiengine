#include "RaytracePipeline.h"

RaytracePipeline::RaytracePipeline()
{
}

RaytracePipeline::~RaytracePipeline()
{
	delete mColorTarget;
	delete mDepthTarget;
}

void RaytracePipeline::Init(const uint32_t width, const uint32_t height)
{
	mColorTarget = new Texture2DRW();
	GPUMemoryManager::instance().MakeTexture2DRW(width, height, true, HDR_RT_FORMAT, mColorTarget);

	mDepthTarget = new Texture2DRW();
	GPUMemoryManager::instance().MakeTexture2DRW(width, height, true, DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT, mDepthTarget);

	mEnvMap = GPUMemoryManager::instance().GetDefaultCubeMap();
	mIrrMap = GPUMemoryManager::instance().GetDefaultCubeMap();
	mEnvSampler = GPUMemoryManager::instance().GetDefaultTextureSampler();
}

HRESULT RaytracePipeline::Create(ID3D11Device* pDevice, IPipelineDesc* pDesc)
{
	HRESULT hr;

	mDesc = (*reinterpret_cast<IComputePipeline::Desc*>(pDesc));

	if (mDesc.computeStage == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Raytrace Pipeline, failed to validate Compute Shader.");
		return E_INVALIDARG;
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
	bd.ByteWidth = sizeof(RaytracePipeline::PerFrame);
	bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mPerFrameBuffer)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate Per Frame Raytrace Pipeline Data." + HR_to_wstr(hr));
		return hr;
	}

	return S_OK;
}

void RaytracePipeline::_Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData)
{
	pContext->CSSetShader(mDesc.computeStage->GetObjectPtr(), nullptr, 0);

	float clearuav[] = { FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX };
	pContext->ClearUnorderedAccessViewFloat(*mDepthTarget->GetUAV(), clearuav);

	ID3D11UnorderedAccessView* uav[] = { *mColorTarget->GetUAV(), *mDepthTarget->GetUAV(), *mDisplacement->GetUAV() };
	pContext->CSSetUnorderedAccessViews(0, 3, uav, nullptr);

	/*std::vector<ID3D11ShaderResourceView*> srv;// = { *mEnvMap->GetSRV(), *mIrrMap->GetSRV() };
	for (uint8_t i = 0; i < mCubeMaps.size(); i++)
	{
		srv.push_back(*mCubeMaps[i]->GetSRV());
	}
	srv.push_back(*mIrrMap->GetSRV());*/
	ID3D11ShaderResourceView* srv[] = { *mEnvMap->GetSRV(), *mIrrMap->GetSRV() };
	pContext->CSSetShaderResources(11, 2, srv);

	pContext->CSSetSamplers(11, 1, &mEnvSampler);

	pContext->CSSetConstantBuffers(0, 1, &mPerFrameBuffer);
	pContext->UpdateSubresource(mPerFrameBuffer, 0, nullptr, pData, 0, 0);
}

void RaytracePipeline::_Reset(ID3D11DeviceContext* pContext)
{
	ID3D11UnorderedAccessView* nullView[] = { nullptr, nullptr, nullptr };
	pContext->CSSetUnorderedAccessViews(0, 3, nullView, nullptr);

	ID3D11ShaderResourceView* srv[1] = { nullptr };
	pContext->PSSetShaderResources(11, 1, srv);
}

void RaytracePipeline::SetColorTarget(Texture2DRW* pColorTarget)
{
	mColorTarget = pColorTarget;
}

void RaytracePipeline::SetDepthTarget(Texture2DRW* pDepthTarget)
{
	mDepthTarget = pDepthTarget;
}

/*void RaytracePipeline::SetEnvMap(ColorMap* pEnvMap)
{
	mEnvMap = pEnvMap;
}*/

void RaytracePipeline::SetEnvMap(CubeMap* pEnvMaps)
{
	mEnvMap = pEnvMaps;
}

void RaytracePipeline::SetIrrMap(CubeMap* pIrrMap)
{
	mIrrMap = pIrrMap;
}

void RaytracePipeline::SetEnvSampler(ID3D11SamplerState* pSampler)
{
	mEnvSampler = pSampler;
}

void RaytracePipeline::SetDisplace(Texture2DRW * pDisplacement)
{
	mDisplacement = pDisplacement;
}

Texture2DRW* RaytracePipeline::GetColorTarget() const
{
	return mColorTarget;
}

Texture2DRW* RaytracePipeline::GetDepthTarget() const
{
	return mDepthTarget;
}

/*ColorMap* RaytracePipeline::GetEnvMap() const
{
	return mEnvMap;
}*/
