#include "FontEngine.h"

FontEngine::FontEngine() : 
	mD2DFactory{ nullptr },
	mDWriteFactory{ nullptr },
	mRenderTarget{ nullptr },
	mTextFormat{ nullptr },
	mSolidBrush{ nullptr }
{
}

FontEngine::~FontEngine()
{
	SAFE_RELEASE(mDWriteFactory);
	SAFE_RELEASE(mD2DFactory);
	SAFE_RELEASE(mRenderTarget);
	SAFE_RELEASE(mSolidBrush);
	SAFE_RELEASE(mTextFormat);
}

HRESULT FontEngine::Init(Device* pDevice)
{
	HRESULT hr;

	auto options = D2D1_FACTORY_OPTIONS();
#if defined(_DEBUG) || defined(DEBUG)
	options.debugLevel = D2D1_DEBUG_LEVEL::D2D1_DEBUG_LEVEL_INFORMATION;
#else
	options.debugLevel = D2D1_DEBUG_LEVEL::D2D1_DEBUG_LEVEL_NONE;
#endif
	if (FAILED(hr = D2D1CreateFactory(D2D1_FACTORY_TYPE::D2D1_FACTORY_TYPE_MULTI_THREADED, options, &mD2DFactory)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create D2D Factory." + HR_to_wstr(hr));
		return hr;
	}

	if (FAILED(hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE::DWRITE_FACTORY_TYPE_SHARED, __uuidof(mDWriteFactory), reinterpret_cast<IUnknown**>(&mDWriteFactory))))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create DirectWrite Factory." + HR_to_wstr(hr));
		return hr;
	}

	if (FAILED(hr = mDWriteFactory->CreateTextFormat(
		L"Gabriola",
		nullptr,
		DWRITE_FONT_WEIGHT::DWRITE_FONT_WEIGHT_NORMAL,
		DWRITE_FONT_STYLE::DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH::DWRITE_FONT_STRETCH_NORMAL,
		24.0f,
		L"en-US",
		&mTextFormat)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create Default Text Format." + HR_to_wstr(hr));
		return hr;
	}

	// Make default alignment
	mTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_JUSTIFIED);
	mTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_NEAR);


	this->CreateResources(pDevice);

	return S_OK;
}

void FontEngine::PreResize()
{
	SAFE_RELEASE(mRenderTarget);
}

HRESULT FontEngine::PostResize(Device* pDevice)
{
	return this->CreateResources(pDevice);
}

void FontEngine::Set()
{
	mRenderTarget->BeginDraw();
}

void FontEngine::Reset()
{
	HRESULT hr = mRenderTarget->EndDraw();
	if (FAILED(hr))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to Flush D2D Draw Context");
	}
}

void FontEngine::DebugDraw(const std::wstring& text)
{
	mSolidBrush->SetColor(D2D1::ColorF(D2D1::ColorF::Crimson));
	mRenderTarget->DrawTextW(text.c_str(), static_cast<uint32_t>(text.size()), mTextFormat, D2D1::RectF(0.0f, 0.0f, 200.0f, 200.0f), mSolidBrush);
	mSolidBrush->SetColor(D2D1::ColorF(D2D1::ColorF::ForestGreen));
	mRenderTarget->DrawTextW(text.c_str(), static_cast<uint32_t>(text.size()), mTextFormat, D2D1::RectF(200.0f, 0.0f, 400.0f, 200.0f), mSolidBrush);
}

ID2D1Factory* FontEngine::GetD2DFactory()
{
	return mD2DFactory;
}

IDWriteFactory* FontEngine::GetWriteFactory()
{
	return mDWriteFactory;
}

ID2D1RenderTarget* FontEngine::GetRenderTarget()
{
	return mRenderTarget;
}

HRESULT FontEngine::CreateResources(Device* pDevice)
{
	HRESULT hr;
	//
	// Create render resources
	//
	IDXGISurface* fbSurface;
	if (FAILED(hr = pDevice->GetSwapchain()->GetBuffer(0, IID_PPV_ARGS(&fbSurface))))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to retrieve framebuffer surface." + HR_to_wstr(hr));
		return hr;
	}

	//
	// High DPI support
	//
	float dpiX;
	float dpiY;
	mD2DFactory->GetDesktopDpi(&dpiX, &dpiY);
	// DXGI_FORMAT_UNKNOWN will cause it to use the same format as the back buffer (R8G8B8A8_UNORM)
	auto d2dRTProps = D2D1::RenderTargetProperties(
		D2D1_RENDER_TARGET_TYPE::D2D1_RENDER_TARGET_TYPE_DEFAULT,
		D2D1::PixelFormat(DXGI_FORMAT::DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE::D2D1_ALPHA_MODE_PREMULTIPLIED),
		96, 96);

	// Make render target
	if (FAILED(hr = mD2DFactory->CreateDxgiSurfaceRenderTarget(fbSurface, &d2dRTProps, &mRenderTarget)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create D2D Render Target." + HR_to_wstr(hr));
		return hr;
	}

	// Create brush to paint text
	if (FAILED(hr = mRenderTarget->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Crimson), &mSolidBrush)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create D2D Solid Brush." + HR_to_wstr(hr));
		return hr;
	}

	SAFE_RELEASE(fbSurface);

	return S_OK;
}
