#include "RenderStates.h"

ID3D11RasterizerState* RenderStates::DefaultRS = nullptr;
ID3D11RasterizerState* RenderStates::NoCullRS = nullptr;
ID3D11RasterizerState* RenderStates::WireframeRS = nullptr;
ID3D11DepthStencilState* RenderStates::NoDepthWriteDS = nullptr;
ID3D11BlendState* RenderStates::AdditiveBlendState = nullptr;
ID3D11BlendState* RenderStates::AdditiveAccumState = nullptr;

void RenderStates::Init(ID3D11Device* pDevice)
{
	HRESULT hr;

	// Default
	D3D11_RASTERIZER_DESC defaultDesc;
	ZeroMemory(&defaultDesc, sizeof(D3D11_RASTERIZER_DESC));
	defaultDesc.FillMode = D3D11_FILL_MODE::D3D11_FILL_SOLID;
	defaultDesc.CullMode = D3D11_CULL_MODE::D3D11_CULL_BACK;
	defaultDesc.FrontCounterClockwise = false;
	defaultDesc.DepthClipEnable = true;
	if (FAILED(hr = pDevice->CreateRasterizerState(&defaultDesc, &DefaultRS)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create Default RS");
	}

	// No Cull
	D3D11_RASTERIZER_DESC noCullDesc;
	ZeroMemory(&noCullDesc, sizeof(D3D11_RASTERIZER_DESC));
	noCullDesc.FillMode = D3D11_FILL_SOLID;
	noCullDesc.CullMode = D3D11_CULL_NONE;
	noCullDesc.FrontCounterClockwise = false;
	noCullDesc.DepthClipEnable = true;
	if (FAILED(hr = pDevice->CreateRasterizerState(&noCullDesc, &NoCullRS)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create No Cull RS");
	}

	// Wireframe
	D3D11_RASTERIZER_DESC wireframeDesc;
	ZeroMemory(&wireframeDesc, sizeof(D3D11_RASTERIZER_DESC));
	wireframeDesc.FillMode = D3D11_FILL_WIREFRAME;
	wireframeDesc.CullMode = D3D11_CULL_BACK;
	wireframeDesc.FrontCounterClockwise = false;
	wireframeDesc.DepthClipEnable = true;
	if (FAILED(hr = pDevice->CreateRasterizerState(&wireframeDesc, &WireframeRS)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create Wireframe RS");
	}

	// No Depth Write
	D3D11_DEPTH_STENCIL_DESC noDepthWriteDesc;
	ZeroMemory(&noDepthWriteDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	noDepthWriteDesc.StencilEnable = false;
	noDepthWriteDesc.DepthEnable = true;
	noDepthWriteDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK::D3D11_DEPTH_WRITE_MASK_ZERO;
	noDepthWriteDesc.DepthFunc = D3D11_COMPARISON_FUNC::D3D11_COMPARISON_LESS;
	if (FAILED(hr = pDevice->CreateDepthStencilState(&noDepthWriteDesc, &NoDepthWriteDS)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create no depth stencil");
	}

	// Additive Blending
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;	
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;
	blendDesc.IndependentBlendEnable = true;
	if (FAILED(hr = pDevice->CreateBlendState(&blendDesc, &AdditiveBlendState)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create no additive blend state");
	}

	// Accum Blending
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	if (FAILED(hr = pDevice->CreateBlendState(&blendDesc, &AdditiveAccumState)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to create no additive blend state");
	}

}

void RenderStates::Release()
{
	SAFE_RELEASE(DefaultRS);
	SAFE_RELEASE(NoCullRS);
	SAFE_RELEASE(WireframeRS);
	SAFE_RELEASE(NoDepthWriteDS);
	SAFE_RELEASE(AdditiveBlendState);
}