#ifndef _RENDERER_H_
#define _RENDERER_H_

#include "../General/TsunamiStd.h"
#include "../Scene/Scene.h"
#include "Data/Texture2DRW.h"
#include "Data/SkyMaterial.h"
#include "Device.h"
#include "RenderStates.h"
#include "FontEngine.h"

#include "SolidPipeline.h"
#include "ParticlePipeline.h"
#include "RaytracePipeline.h"
#include "ParticleIntensityPipeline.h"
#include "ParticleShadingPipeline.h"
#include "MergerPipeline.h"
#include "GUIPipeline.h"
#include "HDRPostProcessPipeline.h"

class Renderer
{
public:
	Renderer();
	~Renderer();

	void Init(HWND hWnd);
	void Resize(uint32_t width, uint32_t height, bool fullscreen);

	void Render();
	void Present();

	void BakeStaticEnvironment(Scene* pScene, const XMFLOAT3& cameraPos);
	void BakeStaticEnvironment(const XMFLOAT3& cameraPos);

	Scene* GetCurrentScene() { return mCurrScene; }
	void SetCurrentScene(Scene* scene);

	Device* GetDevice() { return mDevice; }
	FontEngine* GetFontEngine() { return mFontEngine; }

	void InitPipelines();
private:

	void PrepareRender();
	void FinalizeRender();

	// renders solid objects in the scene
	SolidPipeline mSolid;
	// render skinned animated objects
	SolidPipeline mSkinned;
	// renders particles
	ParticlePipeline mParticles;
	ParticlePipeline mParticleProps;
	ParticleIntensityPipeline mParticleIntensity;
	ParticleShadingPipeline mParticleShading;
	// skybox rendering
	SolidPipeline mSky;
	// renders water volume
	RaytracePipeline mVolumeRaytracer;
	// merge solid with water doing depth test and blending
	MergerPipeline mMerger;
	// GUI pass
	GUIPipeline mGUI;
	// Tone Mapping
	HDRPostProcessPipeline mHDRpp;

	ParticleIntensityPipeline::PerFrame mParticleIntensityData;
	MergerPipeline::PerFrame mMergerTextures;

	// Set and reset resources for font rendering
	FontEngine* mFontEngine;

	Device* mDevice;
	Scene* mCurrScene;
};

#endif