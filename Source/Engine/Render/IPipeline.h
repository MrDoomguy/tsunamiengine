#ifndef _IPIPELINE_H_
#define _IPIPELINE_H_

#include "../General/TsunamiStd.h"
#include "../Render/Data/RenderTarget.h"
#include "../Render/Data/DepthStencilBuffer.h"
#include "../Render/Data/Texture2DRW.h"
#include "../Render/Data/VertexShader.h"
#include "../Render/Data/GeometryShader.h"
#include "../Render/Data/PixelShader.h"
#include "../Render/Data/ComputeShader.h"

#include "../Scene/DirectionalLightComponent.h"

#include "../Render/Device.h"

class IPipeline
{
public:
	struct DirectionalLight
	{
		XMFLOAT4 ambient;
		XMFLOAT4 diffuse;
		XMFLOAT4 specular;
		XMFLOAT3 direction;
		float _pad0;
	};

	static const size_t MAX_DIR_LIGHTS = 10;

	struct IPerFrame
	{
	};

	struct IPipelineDesc
	{
	};

	virtual HRESULT Create(ID3D11Device* pDevice, IPipelineDesc* pDesc) { return E_NOTIMPL; }
	virtual HRESULT Create(Device* pDevice, ID2D1Factory* pFactory) { return E_NOTIMPL; }
	void Set(Device* pDevice, IPerFrame* pData) { pDevice->LockContext(); _Set(pDevice->GetContext(), pData); }
	virtual void Set() { };
	void Reset(Device* pDevice) { _Reset(pDevice->GetContext()); pDevice->UnlockContext(); }
	virtual void Reset() { };

protected:
	virtual void _Set(ID3D11DeviceContext* pContext, IPerFrame* pData) {};
	virtual void _Reset(ID3D11DeviceContext* pContext) {};
};
#endif
