#include "HDRPostProcessPipeline.h"

HDRPostProcessPipeline::HDRPostProcessPipeline() :
	mInput{ nullptr },
	mOutput{ nullptr }
{
}

HDRPostProcessPipeline::~HDRPostProcessPipeline()
{
}

void HDRPostProcessPipeline::Init(const uint32_t width, const uint32_t height)
{
	mInput = GPUMemoryManager::instance().GetDefaultTexture2DRW();
	mOutput = GPUMemoryManager::instance().GetDefaultTexture2DRW();
}

void HDRPostProcessPipeline::Init(Texture2DRW* pInputImage, Texture2DRW* pOutputImage)
{
	mInput = pInputImage;
	mOutput = pOutputImage;
}

HRESULT HDRPostProcessPipeline::Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc * pDesc)
{
	mDesc = (*reinterpret_cast<IComputePipeline::Desc*>(pDesc));

	if (mDesc.computeStage == nullptr)
	{
		core::TsunamiFatalError(L"Failed to validate HDR Post Processing Pipeline, shader program is nullptr.");
	}

	return S_OK;
}

void HDRPostProcessPipeline::_Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData)
{
	pContext->CSSetShader(mDesc.computeStage->GetObjectPtr(), nullptr, 0);

	pContext->CSSetShaderResources(0, 1, mInput->GetSRV());

	pContext->CSSetUnorderedAccessViews(0, 1, mOutput->GetUAV(), nullptr);
}

void HDRPostProcessPipeline::_Reset(ID3D11DeviceContext* pContext)
{
	ID3D11UnorderedAccessView* nullUAV = nullptr;
	pContext->CSSetUnorderedAccessViews(0, 1, &nullUAV, nullptr);
}

void HDRPostProcessPipeline::ResizeBuffers(ID3D11Device* pDevice, const uint32_t width, const uint32_t height)
{
	mInput->Resize(pDevice, width, height);
	mOutput->Resize(pDevice, width, height);
}

Texture2DRW* HDRPostProcessPipeline::GetRenderTarget()
{
	return mOutput;
}

void HDRPostProcessPipeline::SetInputFrame(Texture2DRW* pFrame)
{
	mInput = pFrame;
}

void HDRPostProcessPipeline::Dispatch(ID3D11DeviceContext* pContext)
{
	pContext->Dispatch((UINT)std::ceil(mOutput->GetWidth() / (float)THREAD_COUNT_2D), (UINT)std::ceil(mOutput->GetHeight() / (float)THREAD_COUNT_2D), 1);
}
