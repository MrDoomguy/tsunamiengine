#ifndef _DEVICE_H_
#define _DEVICE_H_

#include "../General/TsunamiStd.h"

#include "../General/GPUMemoryManager.h"

class Device
{
public:
	Device();
	~Device();

	void Init(HWND hWnd);
	void Resize(uint32_t width, uint32_t height, bool fullscreen);

	ID3D11RenderTargetView* GetBackBuffer() const { return mRenderTarget->GetRTV(); }
	ID3D11Texture2D* GetBackBufferTexture() { return mRenderTarget->Texture(); }
	ID3D11DepthStencilView* GetDepthStencil() const { return mDepthBuffer->GetDSV(); }
	ID3D11Texture2D* GetDepthStencilTexture() const { return mDepthBuffer->Texture(); }
	D3D11_VIEWPORT* GetViewport() { return &mViewport; }

	ID3D11Device* GetDevice() { return mDevice.pDevice; }
	ID3D11DeviceContext* GetContext() { return mDevice.pContext; }
	IDXGISwapChain* GetSwapchain() { return mDevice.pSwapchain; }

	void LockContext() { mGraphicsPhysicsInteropMutex.lock(); }
	void UnlockContext() { mGraphicsPhysicsInteropMutex.unlock(); }

	UINT GetWidth() const { return mScreenProperties.width; }
	UINT GetHeight() const { return mScreenProperties.height; }

	bool UseVSYNC() { return mScreenProperties.useVsync; }

private:

	std::mutex mGraphicsPhysicsInteropMutex;
	struct DevicePtr
	{
		ID3D11Device* pDevice;
		ID3D11DeviceContext* pContext;
		IDXGISwapChain* pSwapchain;

		DevicePtr();
	};

	struct ScreenProperties
	{
		UINT width;
		UINT height;
		bool fullscreen;
		bool useMSAA;
		UINT msaaQuality;
		bool useVsync;

		ScreenProperties();
	};

	void CreateDeviceAndSwapchain(HWND hWnd);
	void CreateDeviceAndContext();
	void CreateSwapchain(HWND hWnd);

	void SetupSwapchain();
	void SetupRenderTarget();
	void SetupDepthStencil();
	
	void CreateViewport();

	DevicePtr mDevice;
	ScreenProperties mScreenProperties;
	
	RenderTarget* mRenderTarget;
	DepthStencilBuffer* mDepthBuffer;
	D3D11_VIEWPORT mViewport;
};

#endif
