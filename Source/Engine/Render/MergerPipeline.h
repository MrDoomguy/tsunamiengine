#ifndef _MERGERPIPELINE_H_
#define _MERGERPIPELINE_H_

#include "IComputePipeline.h"

class MergerPipeline : public IComputePipeline
{
public:
	struct PerFrame : public IPipeline::IPerFrame
	{
		Texture2DRW* mRaytraceColor;
		Texture2DRW* mRaytracerDepth;
		RenderTarget* mRasterColor;
		RenderTarget* mRasterDepth;
		Texture2DRW* mParticleIntensity;
		Texture2DRW* mParticleShadow;
		Texture2DRW* mParticleRadiance;
		Texture2DRW* mDisplacement;
		RenderTarget* mParticleNormalAndRadii;
		PerFrame()
		{
			mRaytraceColor = new Texture2DRW();
			mRaytracerDepth = new Texture2DRW();
			mRasterColor = new RenderTarget();
			mRasterDepth = new RenderTarget();
			mParticleIntensity = new Texture2DRW();
			mParticleShadow = new Texture2DRW();
			mParticleRadiance = new Texture2DRW();
			mDisplacement = new Texture2DRW();
			mParticleNormalAndRadii = new RenderTarget();
		}
	};

	MergerPipeline();
	~MergerPipeline();

	void Init(const uint32_t width, const uint32_t height) override;

	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;

	void Dispatch(ID3D11DeviceContext* pContext);
	Texture2DRW* GetRenderTarget();
	void SetRenderTarget(Texture2DRW* pRenderTarget);

	UINT GetRenderTargetWidth() const;
	UINT GetRenderTargetHeight() const;

private:
	ID3D11SamplerState* mSampler;
	Texture2DRW* mOutput;
};

#endif