#ifndef _RENDERSTATES_H_
#define _RENDERSTATES_H_

#include "../General/TsunamiStd.h"

class RenderStates
{
public:
	static void Init(ID3D11Device* pDevice);
	static void Release();

	static ID3D11RasterizerState* DefaultRS;
	static ID3D11RasterizerState* NoCullRS;
	static ID3D11RasterizerState* WireframeRS;

	static ID3D11DepthStencilState* NoDepthWriteDS;

	static ID3D11BlendState* AdditiveBlendState;
	static ID3D11BlendState* AdditiveAccumState;
};

#endif