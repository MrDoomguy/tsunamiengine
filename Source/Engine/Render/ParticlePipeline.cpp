#include "ParticlePipeline.h"

ParticlePipeline::ParticlePipeline()
{
}

ParticlePipeline::~ParticlePipeline()
{
	SAFE_RELEASE(mSampler);
}

HRESULT ParticlePipeline::Create(ID3D11Device * pDevice, IPipeline::IPipelineDesc * pDesc)
{
	HRESULT hr;

	// Set pipeline private data
	mDesc = (*reinterpret_cast<IGraphicsPipeline::Desc*>(pDesc));

	if (mDesc.viewport == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Solid Pipeline, no viewport attached." + HR_to_wstr(E_INVALIDARG));
		return E_INVALIDARG;
	}

	if (mDesc.topology == D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Solid Pipeline, undefined topology." + HR_to_wstr(E_INVALIDARG));
		return E_INVALIDARG;
	}

	if (mDesc.rasterState == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Solid Pipeline, no rasterizer state attached." + HR_to_wstr(E_INVALIDARG));
		return E_INVALIDARG;
	}

	if (mDesc.vertexStage == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Solid Pipeline, no vertex stage." + HR_to_wstr(E_INVALIDARG));
		return E_INVALIDARG;
	}

	if (mDesc.pixelStage == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Solid Pipeline, no pixel stage." + HR_to_wstr(E_INVALIDARG));
		return E_INVALIDARG;
	}

	// Allocate Per Frame Buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
	bd.ByteWidth = sizeof(ParticlePipeline::PerFrame);
	bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mPerFrameBuffer)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate Per Frame Graphics Pipeline Data." + HR_to_wstr(hr));
		return hr;
	}

	return S_OK;
}

void ParticlePipeline::_Set(ID3D11DeviceContext * pContext, IPipeline::IPerFrame * pData)
{
	ID3D11RenderTargetView* rtv[2] = { nullptr, nullptr };
	ID3D11DepthStencilView* dsv[1] = { nullptr };

	if (mDesc.renderTarget)
	{
		if (mDesc.clearFramebuffer)
		{
			const float clearColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
			pContext->ClearRenderTargetView(mDesc.renderTarget, clearColor);
		}

		rtv[0] = mDesc.renderTarget;
	}

	if (mDesc.depthTarget)
	{
		if (mDesc.clearFramebuffer)
		{
			const float clearColor[] = { FLT_MAX };
			pContext->ClearRenderTargetView(mDesc.depthTarget, clearColor);
		}

		rtv[1] = mDesc.depthTarget;
	}

	if (mDesc.depthStencil)
	{
		if (mDesc.clearDepthStencil)
		{
			pContext->ClearDepthStencilView(mDesc.depthStencil, D3D11_CLEAR_FLAG::D3D11_CLEAR_DEPTH | D3D11_CLEAR_FLAG::D3D11_CLEAR_STENCIL, 1.0f, 0);
		}

		dsv[0] = mDesc.depthStencil;
	}

	pContext->OMSetRenderTargets(2, rtv, *dsv);

	if (mDesc.blendState)
	{
		const float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
		pContext->OMSetBlendState(mDesc.blendState, blendFactors, 0xffffffff);
	}

	if (mDesc.depthState)
	{
		pContext->OMSetDepthStencilState(mDesc.depthState, 1);
	}

	pContext->RSSetState(mDesc.rasterState);

	pContext->RSSetViewports(1, mDesc.viewport);

	// Input Assembler
	pContext->IASetPrimitiveTopology(mDesc.topology);
	pContext->IASetInputLayout(mDesc.vertexStage->GetVertexLayout());

	// Update per frame buffer
 	pContext->UpdateSubresource(mPerFrameBuffer, 0, nullptr, pData, 0, 0);

	// Geometry shader
	if (mDesc.geometryStage != nullptr)
	{
		pContext->GSSetShader(mDesc.geometryStage->GetObjectPtr(), nullptr, 0);
		pContext->GSSetConstantBuffers(0, 1, &mPerFrameBuffer);
	}

	// Vertex shader
	pContext->VSSetShader(mDesc.vertexStage->GetObjectPtr(), nullptr, 0);
	pContext->VSSetConstantBuffers(0, 1, &mPerFrameBuffer);

	// Pixel shader
	pContext->PSSetShader(mDesc.pixelStage->GetObjectPtr(), nullptr, 0);
	pContext->PSSetConstantBuffers(0, 1, &mPerFrameBuffer);

	if (mDesc.geometryStage != nullptr)
	{
		ID3D11ShaderResourceView* srv[] = { *mSolidDepth->GetSRV(), *mRaytraceDepth->GetSRV() };
		pContext->PSSetShaderResources(0, 2, srv);
		pContext->PSSetSamplers(0, 0, &mSampler);
	}
}

void ParticlePipeline::_Reset(ID3D11DeviceContext * pContext)
{
	// release render targets
	ID3D11RenderTargetView* rtv[2] = { nullptr, nullptr };
	pContext->OMSetRenderTargets(2, rtv, nullptr);
	const float blendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	pContext->OMSetBlendState(nullptr, blendFactors, 0xffffffff);
	pContext->GSSetShader(nullptr, nullptr, 0);

	pContext->OMSetDepthStencilState(nullptr, 0);

	// TODO: leave out
	ID3D11ShaderResourceView* srv[] = { nullptr, nullptr };
	pContext->PSSetShaderResources(0, 2, srv);
	pContext->RSSetState(0);
}

void ParticlePipeline::Init()
{
	mSampler = GPUMemoryManager::instance().GetDefaultNNSampler();
}
