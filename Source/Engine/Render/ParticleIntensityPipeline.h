#pragma once

#include "IComputePipeline.h"

class ParticleIntensityPipeline : public IComputePipeline
{
public:
	struct PerFrame : public IPipeline::IPerFrame
	{
		RenderTarget* mParticleDensity;

		PerFrame()
		{
			mParticleDensity = new RenderTarget();
		}
	};

	ParticleIntensityPipeline();
	~ParticleIntensityPipeline();

	void Init(const uint32_t width, const uint32_t height) override;

	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;

	void Dispatch(ID3D11DeviceContext* pContext);
	//Texture2DRW* GetRenderTarget();
	void SetRenderTarget(Texture2DRW* pRenderTarget);

private:
	Texture2DRW* mOutput;
};
