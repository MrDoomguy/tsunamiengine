#include "MergerPipeline.h"

MergerPipeline::MergerPipeline()
{
}

MergerPipeline::~MergerPipeline()
{
	delete mOutput;
}

void MergerPipeline::Init(const uint32_t width, const uint32_t height)
{
	mOutput = new Texture2DRW();
	GPUMemoryManager::instance().MakeTexture2DRW(width, height, true, HDR_RT_FORMAT, mOutput);
	mSampler = GPUMemoryManager::instance().GetDefaultTextureSampler();
}

HRESULT MergerPipeline::Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc)
{
	mDesc = (*reinterpret_cast<IComputePipeline::Desc*>(pDesc));

	if (mDesc.computeStage == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Merger Pipeline, failed to validate Compute Shader.");
		return E_INVALIDARG;
	}

	mPerFrameBuffer = nullptr;

	return S_OK;
}

void MergerPipeline::_Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData)
{
	MergerPipeline::PerFrame* targets = reinterpret_cast<MergerPipeline::PerFrame*>(pData);

	pContext->CSSetShader(mDesc.computeStage->GetObjectPtr(), nullptr, 0);

	ID3D11ShaderResourceView* srv[] = { *targets->mRaytraceColor->GetSRV(), *targets->mRaytracerDepth->GetSRV(), *targets->mRasterColor->GetSRV(), *targets->mRasterDepth->GetSRV(), *targets->mDisplacement->GetSRV(), *targets->mParticleIntensity->GetSRV(), *targets->mParticleShadow->GetSRV(), *targets->mParticleRadiance->GetSRV(), *targets->mParticleNormalAndRadii->GetSRV() };
	// set all textures
	pContext->CSSetShaderResources(0, 9, srv);

	pContext->CSSetSamplers(0, 1, &mSampler);

	// set output target
	pContext->CSSetUnorderedAccessViews(0, 1, mOutput->GetUAV(), nullptr);
}

void MergerPipeline::_Reset(ID3D11DeviceContext* pContext)
{
	ID3D11ShaderResourceView* srv[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
	ID3D11UnorderedAccessView* nullUAV = nullptr;
	pContext->CSSetShaderResources(0, 9, srv);

	pContext->CSSetUnorderedAccessViews(0, 1, &nullUAV, nullptr);
}

void MergerPipeline::Dispatch(ID3D11DeviceContext* pContext)
{
	pContext->Dispatch((UINT)std::ceil(mOutput->GetWidth() / (float)THREAD_COUNT_2D), (UINT)std::ceil(mOutput->GetHeight() / (float)THREAD_COUNT_2D), 1);
}

Texture2DRW* MergerPipeline::GetRenderTarget()
{
	return mOutput;
}

void MergerPipeline::SetRenderTarget(Texture2DRW* pRenderTarget)
{
	mOutput = pRenderTarget;
}

UINT MergerPipeline::GetRenderTargetWidth() const
{
	return mOutput->GetWidth();
}

UINT MergerPipeline::GetRenderTargetHeight() const
{
	return mOutput->GetHeight();
}
