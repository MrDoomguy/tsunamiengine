#include "ParticleShadingPipeline.h"

ParticleShadingPipeline::ParticleShadingPipeline()
{
}

ParticleShadingPipeline::~ParticleShadingPipeline()
{
	delete mParticleDepth;
}

void ParticleShadingPipeline::Init(const uint32_t width, const uint32_t height)
{
	mColSampler = GPUMemoryManager::instance().GetDefaultTextureSampler();

	mParticleDepth = new RenderTarget();
	GPUMemoryManager::instance().SingleFramebuffer(width, height, true, false, mParticleDepth);
}

HRESULT ParticleShadingPipeline::Create(ID3D11Device * pDevice, IPipeline::IPipelineDesc * pDesc)
{
	HRESULT hr;
	mDesc = (*reinterpret_cast<IComputePipeline::Desc*>(pDesc));
	if (mDesc.computeStage == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Particle Shading Pipeline, failed to validate Compute Shader.");
		return E_INVALIDARG;
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
	bd.ByteWidth = sizeof(ParticleShadingPipeline::PerFrame);
	bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mPerFrameBuffer)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate Per Frame Particle Shading Pipeline Data." + HR_to_wstr(hr));
		return hr;
	}

	return S_OK;
}

void ParticleShadingPipeline::_Set(ID3D11DeviceContext * pContext, IPipeline::IPerFrame * pData)
{
	pContext->CSSetShader(mDesc.computeStage->GetObjectPtr(), nullptr, 0);

	ID3D11ShaderResourceView* srv[] = { *mParticleIntensity->GetSRV(), *mNormalAndSearchRadii->GetSRV(), *mParticleDepth->GetSRV(), *mRaytracerDepth->GetSRV(), *mRasterDepth->GetSRV(), *mIrrMap->GetSRV() };
	// set all textures
	pContext->CSSetShaderResources(0, 6, srv);

	ID3D11SamplerState* sampler[] = { mColSampler };

	pContext->CSSetSamplers(0, 1, sampler);

	ID3D11UnorderedAccessView* uav[] = { *mShadow->GetUAV(), *mRadiance->GetUAV() };
	// set output target
	pContext->CSSetUnorderedAccessViews(0, 2, uav, nullptr);

	pContext->CSSetConstantBuffers(0, 1, &mPerFrameBuffer);
	pContext->UpdateSubresource(mPerFrameBuffer, 0, nullptr, pData, 0, 0);
}

void ParticleShadingPipeline::_Reset(ID3D11DeviceContext * pContext)
{
	ID3D11ShaderResourceView* srv[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
	ID3D11UnorderedAccessView* nullUAV[] = { nullptr, nullptr };
	ID3D11SamplerState* sampler[] = { nullptr };

	pContext->CSSetShaderResources(0, 6, srv);
	pContext->CSSetUnorderedAccessViews(0, 2, nullUAV, nullptr);
	pContext->CSSetSamplers(0, 1, sampler);
}

void ParticleShadingPipeline::Dispatch(ID3D11DeviceContext * pContext)
{
	pContext->Dispatch((UINT)std::ceil(mRadiance->GetWidth() / (float)THREAD_COUNT_2D), (UINT)std::ceil(mRadiance->GetHeight() / (float)THREAD_COUNT_2D), 1);
}

void ParticleShadingPipeline::SetIrrMap(CubeMap* pIrrMap)
{
	mIrrMap = pIrrMap;
}

void ParticleShadingPipeline::SetRaytracerDepth(Texture2DRW * raytracerDepth)
{
	mRaytracerDepth = raytracerDepth;
}

void ParticleShadingPipeline::SetRasterDepth(RenderTarget * rasterDepth)
{
	mRasterDepth = rasterDepth;
}

void ParticleShadingPipeline::SetParticleIntensity(Texture2DRW * particleIntensity)
{
	mParticleIntensity = particleIntensity;
}

void ParticleShadingPipeline::SetNormalAndSearchRadii(RenderTarget * normalAndSearchRadii)
{
	mNormalAndSearchRadii = normalAndSearchRadii;
}

void ParticleShadingPipeline::SetParticleDepth(RenderTarget * particleDepth)
{
	mParticleDepth = particleDepth;
}

void ParticleShadingPipeline::SetShadowTarget(Texture2DRW * shadowTarget)
{
	mShadow = shadowTarget;
}

void ParticleShadingPipeline::SetRadianceTarget(Texture2DRW * radianceTarget)
{
	mRadiance = radianceTarget;
}

RenderTarget * ParticleShadingPipeline::GetParticleDepth()
{
	return mParticleDepth;
}
