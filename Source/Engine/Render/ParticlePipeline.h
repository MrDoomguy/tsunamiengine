#pragma once
#include "IGraphicsPipeline.h"

class ParticlePipeline : public IGraphicsPipeline
{
public:
	struct PerFrame : public IPipeline::IPerFrame
	{
		XMMATRIX viewM = XMMatrixIdentity();
		XMMATRIX projM = XMMatrixIdentity();
		XMMATRIX normalTrans = XMMatrixIdentity();
		XMUINT2 screenSize;
		float viewZ;
		float _pad;
	};

	ParticlePipeline();
	~ParticlePipeline();

	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;
	void Init();

	void SetSolidDepth(RenderTarget* solidDepth) { mSolidDepth = solidDepth; }
	void SetRaytraceDepth(Texture2DRW* raytraceDepth) { mRaytraceDepth = raytraceDepth; }

private:
	RenderTarget* mSolidDepth;
	Texture2DRW* mRaytraceDepth;
	ID3D11SamplerState* mSampler;
};