#ifndef _HDRPOSTPROCESSPIPELINE_H_
#define _HDRPOSTPROCESSPIPELINE_H_

#include "IComputePipeline.h"

#include "../General/GPUMemoryManager.h"

class HDRPostProcessPipeline : public IComputePipeline
{
public:
	struct PerFrame : public IPipeline::IPerFrame
	{

	};

	HDRPostProcessPipeline();
	~HDRPostProcessPipeline();

	void Init(const uint32_t width, const uint32_t height) override;
	void Init(Texture2DRW* pInputImage, Texture2DRW* pOutputImage);

	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;

	void ResizeBuffers(ID3D11Device* pDevice, const uint32_t width, const uint32_t height);
	
	Texture2DRW* GetRenderTarget();
	void SetInputFrame(Texture2DRW* pFrame);

	void Dispatch(ID3D11DeviceContext* pContext);

private:
	Texture2DRW* mInput;
	Texture2DRW* mOutput;
};

#endif
