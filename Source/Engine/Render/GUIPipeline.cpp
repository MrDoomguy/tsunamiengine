#include "GUIPipeline.h"

GUIPipeline::GUIPipeline()
{
}

GUIPipeline::~GUIPipeline()
{
}

void GUIPipeline::Init(const uint32_t width, const uint32_t height)
{
	mOutput = new Texture2DRW();
	GPUMemoryManager::instance().MakeTexture2DRW(width, height, true, HDR_RT_FORMAT, mOutput);

	mInput = new Texture2DRW();
	GPUMemoryManager::instance().MakeTexture2DRW(width, height, true, HDR_RT_FORMAT, mInput);
	//mOutput = GPUMemoryManager::instance().GetTexture2DRW("GUIRenderTarget", width, height, true, HDR_RT_FORMAT);
	//mInput = GPUMemoryManager::instance().GetDefaultTexture2DRW();
}

HRESULT GUIPipeline::Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc)
{
	HRESULT hr;

	mDesc = (*reinterpret_cast<IComputePipeline::Desc*>(pDesc));

	if (mDesc.computeStage == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate GUI Pipeline, failed to validate Compute Shader.");
		return E_INVALIDARG;
	}

	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.BindFlags = D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
	bd.ByteWidth = sizeof(GUIPipeline::PerFrame);
	bd.Usage = D3D11_USAGE::D3D11_USAGE_DEFAULT;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	if (FAILED(hr = pDevice->CreateBuffer(&bd, nullptr, &mPerFrameBuffer)))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to allocate Per Frame Compute Pipeline Data." + HR_to_wstr(hr));
		return hr;
	}

	return S_OK;
}

void GUIPipeline::_Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData)
{
	GUIPipeline::PerFrame data;
	data.imgPlaneSize.x = static_cast<float>(mOutput->GetWidth());
	data.imgPlaneSize.y = static_cast<float>(mOutput->GetHeight());

	pContext->CSSetShader(mDesc.computeStage->GetObjectPtr(), nullptr, 0);

	pContext->CSSetUnorderedAccessViews(0, 1, mOutput->GetUAV(), nullptr);
	pContext->CSSetShaderResources(1, 1, mInput->GetSRV());

	pContext->CSSetConstantBuffers(0, 1, &mPerFrameBuffer);
	pContext->UpdateSubresource(mPerFrameBuffer, 0, nullptr, &data, 0, 0);
}

void GUIPipeline::_Reset(ID3D11DeviceContext* pContext)
{
	ID3D11UnorderedAccessView* nullUAV = nullptr;
	pContext->CSSetUnorderedAccessViews(0, 1, &nullUAV, nullptr);
}

Texture2DRW* GUIPipeline::GetRenderTarget()
{
	return mOutput;
}

void GUIPipeline::SetRenderTarget(Texture2DRW* pRenderTarget)
{
	if (mInput)
		delete mInput;
	
	mInput = pRenderTarget;
}

UINT GUIPipeline::GetRenderTargetWidth() const
{
	return mOutput->GetWidth();
}

UINT GUIPipeline::GetRenderTargetHeight() const
{
	return mOutput->GetHeight();
}
