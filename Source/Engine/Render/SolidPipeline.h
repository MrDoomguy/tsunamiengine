#ifndef _SOLIDPIPELINE_H_
#define _SOLIDPIPELINE_H_

#include "IGraphicsPipeline.h"

#include "../General/GPUMemoryManager.h"

class SolidPipeline : public IGraphicsPipeline
{
public:
	struct PerFrame : public IPipeline::IPerFrame
	{
		IPipeline::DirectionalLight dirLights[IPipeline::MAX_DIR_LIGHTS];
		uint32_t nDirLights;
		XMFLOAT3 viewDir;
		XMMATRIX viewProj = XMMatrixIdentity();
		XMFLOAT3 eyePosW;
		float _pad0;
	};

	SolidPipeline();
	~SolidPipeline();

	void Init();
	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;

	void SetEnvMap(CubeMap* pEnvMap);
	void SetIrrMap(CubeMap* pIrrMap);
	void SetEnvSampler(ID3D11SamplerState* pSampler);

private:
	CubeMap* mEnvMap;
	CubeMap* mIrrMap;
	ID3D11SamplerState* mEnvSampler;
};

#endif
