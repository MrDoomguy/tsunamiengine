#ifndef _GUIPIPELINE_H_
#define _GUIPIPELINE_H_

#include "IComputePipeline.h"

class GUIPipeline : public IComputePipeline
{
public:
	struct PerFrame : public IPipeline::IPerFrame
	{
		DirectX::XMFLOAT2 imgPlaneSize;
		DirectX::XMFLOAT2 _pad0;
	};

	GUIPipeline();
	~GUIPipeline();

	void Init(const uint32_t width, const uint32_t height) override;

	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;

	Texture2DRW* GetRenderTarget();
	void SetRenderTarget(Texture2DRW* pRenderTarget);

	UINT GetRenderTargetWidth() const;
	UINT GetRenderTargetHeight() const;

private:
	Texture2DRW* mOutput;
	Texture2DRW* mInput;
};

#endif