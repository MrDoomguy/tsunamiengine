#include "ParticleIntensityPipeline.h"

ParticleIntensityPipeline::ParticleIntensityPipeline()
{
}

ParticleIntensityPipeline::~ParticleIntensityPipeline()
{
}

void ParticleIntensityPipeline::Init(const uint32_t width, const uint32_t height)
{
}

HRESULT ParticleIntensityPipeline::Create(ID3D11Device * pDevice, IPipeline::IPipelineDesc * pDesc)
{
	mDesc = (*reinterpret_cast<IComputePipeline::Desc*>(pDesc));
	if (mDesc.computeStage == nullptr)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to validate Particle Intensity Pipeline, failed to validate Compute Shader.");
		return E_INVALIDARG;
	}

	mPerFrameBuffer = nullptr;

	return S_OK;
}

void ParticleIntensityPipeline::_Set(ID3D11DeviceContext * pContext, IPipeline::IPerFrame * pData)
{
	ParticleIntensityPipeline::PerFrame* targets = static_cast<ParticleIntensityPipeline::PerFrame*>(pData);

	pContext->CSSetShader(mDesc.computeStage->GetObjectPtr(), nullptr, 0);

	ID3D11ShaderResourceView* srv[] = { *targets->mParticleDensity->GetSRV() };
	// set all textures
	pContext->CSSetShaderResources(0, 1, srv);

	// set output target
	pContext->CSSetUnorderedAccessViews(0, 1, mOutput->GetUAV(), nullptr);
}

void ParticleIntensityPipeline::_Reset(ID3D11DeviceContext * pContext)
{
	ID3D11ShaderResourceView* srv[] = { nullptr };
	ID3D11UnorderedAccessView* nullUAV = nullptr;
	pContext->CSSetShaderResources(0, 1, srv);

	pContext->CSSetUnorderedAccessViews(0, 1, &nullUAV, nullptr);
}

void ParticleIntensityPipeline::Dispatch(ID3D11DeviceContext * pContext)
{
	pContext->Dispatch((UINT)std::ceil(mOutput->GetWidth() / (float)THREAD_COUNT_2D), (UINT)std::ceil(mOutput->GetHeight() / (float)THREAD_COUNT_2D), 1);
}

void ParticleIntensityPipeline::SetRenderTarget(Texture2DRW * pRenderTarget)
{
	mOutput = pRenderTarget;
}
