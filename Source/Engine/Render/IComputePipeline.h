#ifndef _ICOMPUTEPIPELINE_H_
#define _ICOMPUTEPIPELINE_H_

#include "IPipeline.h"

#include "../General/GPUMemoryManager.h"

class IComputePipeline : public IPipeline
{
public:
	struct Desc : public IPipeline::IPipelineDesc
	{
		ComputeShader* computeStage;

		Desc() :
			computeStage{ nullptr }
		{
			computeStage = new ComputeShader();
		}
	};

	IComputePipeline() { }
	~IComputePipeline()
	{
		SAFE_RELEASE(mPerFrameBuffer);
	}

	virtual void Init(const uint32_t width, const uint32_t height) = 0;

	virtual HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override = 0;
	virtual void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override = 0;
	virtual void _Reset(ID3D11DeviceContext* pContext) override = 0;

protected:
	IComputePipeline::Desc mDesc;
	ID3D11Buffer* mPerFrameBuffer;
};

#endif