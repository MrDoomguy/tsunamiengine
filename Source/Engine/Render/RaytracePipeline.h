#ifndef _RAYTRACEPIPELINE_H_
#define _RAYTRACEPIPELINE_H_

#include "IComputePipeline.h"

#include "Data\ColorMap.h"

class RaytracePipeline : public IComputePipeline
{
public:
	/*struct Sphere
	{
		XMFLOAT3 pos;
		float radius;
	};*/
	struct PerFrame : public IPipeline::IPerFrame
	{
		IPipeline::DirectionalLight dirLights[IPipeline::MAX_DIR_LIGHTS];
		uint32_t nDirLights;
		float viewAngle;
		float imgPlaneWidth;
		float imgPlaneHeight;
		float viewZ;
		XMMATRIX view;
		XMMATRIX viewLocal;
		XMMATRIX viewProj;
		float nearPlane;
		float farPlane;
		XMUINT3 offset;
		float dt;
		float _pad;

		/*Sphere spheres[IPipeline::MAX_DIR_LIGHTS];
		uint32_t nSpheres;*/
	};

	RaytracePipeline();
	~RaytracePipeline();

	void Init(const uint32_t width, const uint32_t height) override;

	HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override;
	void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override;
	void _Reset(ID3D11DeviceContext* pContext) override;

	void SetColorTarget(Texture2DRW* pColorTarget);
	void SetDepthTarget(Texture2DRW* pDepthTarget);
	//void SetEnvMap(ColorMap* pEnvMap);
	void SetEnvMap(CubeMap* pEnvMaps);
	void SetIrrMap(CubeMap* pIrrMap);
	void SetEnvSampler(ID3D11SamplerState* pSampler);
	void SetDisplace(Texture2DRW* pDisplacement);
	Texture2DRW* GetColorTarget() const;
	Texture2DRW* GetDepthTarget() const;
	//ColorMap* GetEnvMap() const;

private:
	Texture2DRW* mColorTarget;
	Texture2DRW* mDepthTarget;
	Texture2DRW* mDisplacement;
	//ColorMap* mEnvMap;
	//std::vector<CubeMap*> mCubeMaps;
	CubeMap* mEnvMap;
	CubeMap* mIrrMap;
	ID3D11SamplerState* mEnvSampler;
};

#endif