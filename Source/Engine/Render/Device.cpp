#include "Device.h"

#include "../General/Logger.h"

Device::DevicePtr::DevicePtr() :
	pDevice{ nullptr },
	pContext{ nullptr },
	pSwapchain{ nullptr }
{
}

Device::ScreenProperties::ScreenProperties() :
	width{ 640 },
	height{ 480 },
	fullscreen{ false },
	useMSAA{ false },
	msaaQuality{ 0 },
	useVsync{ false }
{
}

Device::Device()
{
}

Device::~Device()
{
#if defined(DEBUG) || defined(_DEBUG)
	ID3D11Debug* debugDevice = nullptr;
	HRESULT hr = mDevice.pDevice->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&debugDevice));
#endif

	ID3D11RenderTargetView* nullViews[] = { nullptr };
	mDevice.pContext->OMSetRenderTargets(1, nullViews, nullptr);
	
	delete mRenderTarget;
	delete mDepthBuffer;

	mDevice.pSwapchain->SetFullscreenState(false, nullptr);
	SAFE_RELEASE(mDevice.pSwapchain);
	
	mDevice.pContext->Flush();
	SAFE_RELEASE(mDevice.pContext);

#if defined(DEBUG) || defined(_DEBUG)
	debugDevice->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	SAFE_RELEASE(debugDevice);
#endif

	SAFE_RELEASE(mDevice.pDevice);
}

void Device::Init(HWND hWnd)
{
	mRenderTarget = new RenderTarget();
	mDepthBuffer = new DepthStencilBuffer();

	CreateDeviceAndSwapchain(hWnd);
	CreateViewport();

	Logger::instance().Msg(LOG_LAYER::LOG_RENDER, L"Device: Initialization Complete.");
}

void Device::Resize(uint32_t width, uint32_t height, bool fullscreen)
{
	if (!mDevice.pSwapchain)
		return;

	mScreenProperties.width = width;
	mScreenProperties.height = height;
	mScreenProperties.fullscreen = fullscreen;

	ID3D11RenderTargetView* nullViews[] = { nullptr };
	mDevice.pContext->OMSetRenderTargets(1, nullViews, nullptr);

	mRenderTarget->Release();

	mDevice.pContext->Flush();
	SetupSwapchain();
	SetupRenderTarget();
	SetupDepthStencil();
	CreateViewport();

	Logger::instance().Msg(LOG_LAYER::LOG_RENDER, L"Device: Resize Complete.");
}

void Device::CreateDeviceAndSwapchain(HWND hWnd)
{
	CreateDeviceAndContext();

	CreateSwapchain(hWnd);

	Logger::instance().Msg(LOG_LAYER::LOG_RENDER, L"Device: Device and Swapchain Created.");
}

void Device::CreateDeviceAndContext()
{
	UINT createDeviceFlags = D3D11_CREATE_DEVICE_FLAG::D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#if defined(DEBUG) || defined(_DEBUG)
	createDeviceFlags |= D3D11_CREATE_DEVICE_FLAG::D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL featureLevel;
	ThrowIfFailed(D3D11CreateDevice(
		0,
		D3D_DRIVER_TYPE::D3D_DRIVER_TYPE_HARDWARE,
		0,
		createDeviceFlags,
		0, 0,
		D3D11_SDK_VERSION,
		&mDevice.pDevice,
		&featureLevel,
		&mDevice.pContext
	));

	if (featureLevel != D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_11_0)
		throw std::runtime_error("DirectX 11 not supported :(.");
}

void Device::CreateSwapchain(HWND hWnd)
{
	mDevice.pDevice->CheckMultisampleQualityLevels(
		LDR_RT_FORMAT,
		4,
		&mScreenProperties.msaaQuality
	);

	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = mScreenProperties.width;
	sd.BufferDesc.Height = mScreenProperties.height;
	if (mScreenProperties.useVsync)
	{
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
	}
	else
	{
		sd.BufferDesc.RefreshRate.Numerator = 0;
		sd.BufferDesc.RefreshRate.Denominator = 1;
	}
	sd.BufferDesc.Format = LDR_RT_FORMAT;
	sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER::DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	sd.BufferDesc.Scaling = DXGI_MODE_SCALING::DXGI_MODE_SCALING_UNSPECIFIED;
	if (mScreenProperties.useMSAA)
	{
		sd.SampleDesc.Count = 4;
		sd.SampleDesc.Quality = mScreenProperties.msaaQuality - 1;
	}
	else
	{
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
	}
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	// number of back buffers so 1 means double buffer, 1 front and 1 back
	sd.BufferCount = 1;
	sd.SwapEffect = DXGI_SWAP_EFFECT::DXGI_SWAP_EFFECT_DISCARD;
	sd.OutputWindow = hWnd;
	sd.Windowed = !mScreenProperties.fullscreen;
	sd.Flags = DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	if (mScreenProperties.fullscreen)
		sd.Flags |= DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_FULLSCREEN_VIDEO;

	Microsoft::WRL::ComPtr<IDXGIDevice> dxgiDevice = nullptr;
	mDevice.pDevice->QueryInterface(__uuidof(IDXGIDevice), (void**)&dxgiDevice);
	Microsoft::WRL::ComPtr<IDXGIAdapter> dxgiAdapter = nullptr;
	dxgiDevice->GetParent(__uuidof(IDXGIAdapter), (void**)&dxgiAdapter);
	Microsoft::WRL::ComPtr<IDXGIFactory> dxgiFactory = nullptr;
	dxgiAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&dxgiFactory);

	ThrowIfFailed(dxgiFactory->CreateSwapChain(mDevice.pDevice, &sd, &mDevice.pSwapchain));

	dxgiFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_ALT_ENTER);
}

void Device::SetupSwapchain()
{
	HRESULT hr = mDevice.pSwapchain->ResizeBuffers(0, mScreenProperties.width, mScreenProperties.height, LDR_RT_FORMAT, 0);
	mDevice.pSwapchain->SetFullscreenState(mScreenProperties.fullscreen, nullptr);
}

void Device::SetupRenderTarget()
{
	ID3D11Texture2D* backbuffer = nullptr;
	mDevice.pSwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backbuffer));

	GPUMemoryManager::instance().Framebuffer(backbuffer, false, mRenderTarget);
}

void Device::SetupDepthStencil()
{
	if (!mDepthBuffer->Texture())
	{
		GPUMemoryManager::instance().DepthStencil(mScreenProperties.width, mScreenProperties.height, false, mScreenProperties.useMSAA, mDepthBuffer);
	}
	else
	{
		mDepthBuffer->Resize(mDevice.pDevice, mScreenProperties.width, mScreenProperties.height);
	}
}

void Device::CreateViewport()
{
	mViewport.TopLeftX = 0;
	mViewport.TopLeftY = 0;
	mViewport.Width = static_cast<float>(mScreenProperties.width);
	mViewport.Height = static_cast<float>(mScreenProperties.height);
	mViewport.MinDepth = 0.0f;
	mViewport.MaxDepth = 1.0f;
}
