#ifndef _FONTENGINE_H_
#define _FONTENGINE_H_

#include "../General/TsunamiStd.h"
#include "Device.h"

class FontEngine
{
public:
	FontEngine();
	~FontEngine();

	HRESULT Init(Device* pDevice);

	void PreResize();
	HRESULT PostResize(Device* pDevice);

	void Set();
	void Reset();

	void DebugDraw(const std::wstring& text);

	ID2D1Factory* GetD2DFactory();
	IDWriteFactory* GetWriteFactory();
	ID2D1RenderTarget* GetRenderTarget();
	ID2D1SolidColorBrush* GetBrush() { return mSolidBrush; }

private:
	HRESULT CreateResources(Device* pDevice);

	ID2D1Factory* mD2DFactory;
	IDWriteFactory* mDWriteFactory;
	ID2D1RenderTarget* mRenderTarget;

	ID2D1SolidColorBrush* mSolidBrush;
	IDWriteTextFormat* mTextFormat;
};

#endif