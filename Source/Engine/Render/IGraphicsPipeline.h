#ifndef _IGRAPHICSPIPELINE_H_
#define _IGRAPHICSPIPELINE_H_

#include "IPipeline.h"
#include "RenderStates.h"

class IGraphicsPipeline : public IPipeline
{
public:
	struct Desc : public IPipelineDesc
	{
		ID3D11RenderTargetView* renderTarget;
		bool clearFramebuffer;
		ID3D11RenderTargetView* depthTarget;
		ID3D11DepthStencilView* depthStencil;
		bool clearDepthStencil;
		ID3D11BlendState* blendState;
		ID3D11DepthStencilState* depthState;
		ID3D11RasterizerState* rasterState;
		D3D11_VIEWPORT* viewport;
		GeometryShader* geometryStage;
		VertexShader* vertexStage;
		PixelShader* pixelStage;
		D3D11_PRIMITIVE_TOPOLOGY topology;

		Desc() :
			renderTarget{ nullptr },
			depthTarget{ nullptr },
			clearFramebuffer{ false },
			depthStencil{ nullptr },
			clearDepthStencil{ false },
			blendState{ nullptr },
			depthState{ nullptr },
			rasterState{ nullptr },
			viewport{ nullptr },
			geometryStage{nullptr},
			vertexStage{ nullptr },
			pixelStage{ nullptr }
		{
			geometryStage = new GeometryShader();
			vertexStage = new VertexShader();
			pixelStage = new PixelShader();
			topology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
		}
	};

	IGraphicsPipeline() { }
	~IGraphicsPipeline()
	{
		SAFE_RELEASE(mDesc.blendState);
		SAFE_RELEASE(mDesc.depthState);
		SAFE_RELEASE(mDesc.rasterState);
		SAFE_RELEASE(mPerFrameBuffer);
	}

	virtual HRESULT Create(ID3D11Device* pDevice, IPipeline::IPipelineDesc* pDesc) override = 0;
	virtual void _Set(ID3D11DeviceContext* pContext, IPipeline::IPerFrame* pData) override = 0;
	virtual void _Reset(ID3D11DeviceContext* pContext) override = 0;

	IGraphicsPipeline::Desc GetDesc() { return mDesc; }
	// Set Pipeline description without validating structure,
	// use only to write a description which is already validated
	void SetDesc(const IGraphicsPipeline::Desc& desc) { mDesc = desc; }

protected:

	IGraphicsPipeline::Desc mDesc;
	ID3D11Buffer* mPerFrameBuffer;
};

#endif
