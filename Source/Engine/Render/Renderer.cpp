#include "Renderer.h"

Renderer::Renderer()
{
}

Renderer::~Renderer()
{
	delete mDevice;
	delete mFontEngine;
}

void Renderer::Init(HWND hWnd)
{
	mDevice = new Device();
	mDevice->Init(hWnd);

	mFontEngine = new FontEngine();
	mFontEngine->Init(mDevice);

	RenderStates::Init(mDevice->GetDevice());

	// HACK : would be better to init the resource factory into the device
	// as this singleton is basically an allocator interface for GPU resources
	// for now I'll do it here as it almost make sense
	GPUMemoryManager::instance().Init(mDevice);

	mVolumeRaytracer.Init(mDevice->GetWidth(), mDevice->GetHeight());
	mVolumeRaytracer.SetEnvMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	mVolumeRaytracer.SetIrrMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_i.dds"));

	// TODO : set env maps for all pipelines that uses that in the graphics pipeline maybe
	mSolid.Init();
	mSolid.SetEnvMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	mSolid.SetIrrMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_i.dds"));

	mSkinned.Init();
	mSkinned.SetEnvMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	mSkinned.SetIrrMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_i.dds"));

	mParticles.Init();
	
	mParticleIntensity.Init(mDevice->GetWidth(), mDevice->GetHeight());

	mParticleShading.Init(mDevice->GetWidth(), mDevice->GetHeight());
	mParticleShading.SetIrrMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_i.dds"));

	mSky.Init();

	GPUMemoryManager::instance().Framebuffer(mDevice->GetWidth(), mDevice->GetHeight(), true, false, mMergerTextures.mRasterColor);
	GPUMemoryManager::instance().SingleFramebuffer(mDevice->GetWidth(), mDevice->GetHeight(), true, false, mMergerTextures.mRasterDepth);

	GPUMemoryManager::instance().MakeTexture2DRW(mDevice->GetWidth(), mDevice->GetHeight(), true, DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT, mMergerTextures.mParticleIntensity);
	GPUMemoryManager::instance().MakeTexture2DRW(mDevice->GetWidth(), mDevice->GetHeight(), true, DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT, mMergerTextures.mParticleShadow);
	GPUMemoryManager::instance().MakeTexture2DRW(mDevice->GetWidth(), mDevice->GetHeight(), true, DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT, mMergerTextures.mParticleRadiance);
	GPUMemoryManager::instance().MakeTexture2DRW(mDevice->GetWidth(), mDevice->GetHeight(), true, DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT, mMergerTextures.mDisplacement);
	GPUMemoryManager::instance().SingleFramebuffer(mDevice->GetWidth(), mDevice->GetHeight(), true, false, mParticleIntensityData.mParticleDensity);

	GPUMemoryManager::instance().Framebuffer(mDevice->GetWidth(), mDevice->GetHeight(), true, false, mMergerTextures.mParticleNormalAndRadii);

	mMerger.Init(mDevice->GetWidth(), mDevice->GetHeight());
	
	mGUI.Init(mDevice->GetWidth(), mDevice->GetHeight());

	//
	// Do all render targets associations
	//
	mMergerTextures.mRaytraceColor = mVolumeRaytracer.GetColorTarget();
	mMergerTextures.mRaytracerDepth = mVolumeRaytracer.GetDepthTarget();

	mParticles.SetRaytraceDepth(mMergerTextures.mRaytracerDepth);
	mParticles.SetSolidDepth(mMergerTextures.mRasterDepth);

	mParticleIntensity.SetRenderTarget(mMergerTextures.mParticleIntensity);
	
	mParticleShading.SetRaytracerDepth(mMergerTextures.mRaytracerDepth);
	mParticleShading.SetRasterDepth(mMergerTextures.mRasterDepth);
	mParticleShading.SetParticleIntensity(mMergerTextures.mParticleIntensity);

	mParticleShading.SetShadowTarget(mMergerTextures.mParticleShadow);
	mParticleShading.SetRadianceTarget(mMergerTextures.mParticleRadiance);

	mParticleShading.SetNormalAndSearchRadii(mMergerTextures.mParticleNormalAndRadii);

	mVolumeRaytracer.SetDisplace(mMergerTextures.mDisplacement);

	mGUI.SetRenderTarget(mMerger.GetRenderTarget());

	Texture2DRW* hdrOut = new Texture2DRW();
	GPUMemoryManager::instance().MakeTexture2DRW(mDevice->GetWidth(), mDevice->GetHeight(), true, LDR_RT_FORMAT, hdrOut);
	mHDRpp.Init(mGUI.GetRenderTarget(), hdrOut);
}

void Renderer::Resize(uint32_t width, uint32_t height, bool fullscreen)
{
	mDevice->LockContext();

	// release backbuffer from d2d
	mFontEngine->PreResize();

	// resize backbuffer and depth stencil
	mDevice->Resize(width, height, fullscreen);

	// get back the new framebuffer from IDXGI
	mFontEngine->PostResize(mDevice);

	mDevice->UnlockContext();

	// resize raytrace render target
	mVolumeRaytracer.GetColorTarget()->Resize(mDevice->GetDevice(), width, height);
	mVolumeRaytracer.GetDepthTarget()->Resize(mDevice->GetDevice(), width, height);

	// Resizing all buffers
	mMergerTextures.mRasterColor->Resize(mDevice->GetDevice(), width, height);
	mMergerTextures.mRasterDepth->Resize(mDevice->GetDevice(), width, height);
	mMergerTextures.mParticleIntensity->Resize(mDevice->GetDevice(), width, height);
	mMergerTextures.mParticleRadiance->Resize(mDevice->GetDevice(), width, height);
	mMergerTextures.mParticleShadow->Resize(mDevice->GetDevice(), width, height);
	mMergerTextures.mDisplacement->Resize(mDevice->GetDevice(), width, height);
	mMergerTextures.mParticleNormalAndRadii->Resize(mDevice->GetDevice(), width, height);

	mParticleIntensityData.mParticleDensity->Resize(mDevice->GetDevice(), width, height);
	mParticleShading.GetParticleDepth()->Resize(mDevice->GetDevice(), width, height);

	// resize merger textures
	mMerger.GetRenderTarget()->Resize(mDevice->GetDevice(), width, height);

	// resize gui framebuffer
	mGUI.GetRenderTarget()->Resize(mDevice->GetDevice(), width, height);

	// resize hdr targets
	mHDRpp.ResizeBuffers(mDevice->GetDevice(), width, height);

	// resize camera
	mCurrScene->OnResize(mDevice->GetDevice(), width, height);

	InitPipelines();
}

void Renderer::Render()
{
	PrepareRender();

	//
	// Draw all raytraced objects
	// and accumulate on the framebuffer
	//
	mVolumeRaytracer.Set(mDevice, &mCurrScene->GetRaytracePerFrameData());
	for (auto& go : mCurrScene->GetRaytraceQueue())
	{
		if(go->IsEnabled()) go->Dispatch(mDevice->GetContext(), (UINT)std::ceil(mVolumeRaytracer.GetColorTarget()->GetWidth() / (float)THREAD_COUNT_2D), (UINT)std::ceil(mVolumeRaytracer.GetColorTarget()->GetHeight() / (float)THREAD_COUNT_2D), 1);
	}
	mVolumeRaytracer.Reset(mDevice);
	
	//
	// Draw all solids
	//
	auto solidData = mCurrScene->GetSolidPerFrameData();
	mSolid.Set(mDevice, &solidData);
	for (auto& go : mCurrScene->GetOpaqueQueue())
	{
		// not required in the alpha pipeline
		if (go->IsEnabled()) go->Draw(mDevice->GetContext());
	}
	mSolid.Reset(mDevice);

	//
	// Draw skinned meshes
	//
	mSkinned.Set(mDevice, &solidData);
	for (auto& go : mCurrScene->GetAnimationQueue())
	{
		if (go->IsEnabled()) go->Draw(mDevice->GetContext());
	}
	mSkinned.Reset(mDevice);

	//
	// Draw sky
	//
	mSky.Set(mDevice, &mCurrScene->GetSolidPerFrameData());
	// HACK : should always load default sky
	// when u have default sky, check for flag instead of nullptr
	auto s = mCurrScene->GetSky();
	if (s != nullptr) s->Draw(mDevice->GetContext());
	mSky.Reset(mDevice);

	//
	// Draw Particles
	//
	ParticlePipeline::IPerFrame* particlePerFrame = &mCurrScene->GetParticlePerFrameData();
	mParticles.Set(mDevice, particlePerFrame);
	for (auto& go : mCurrScene->GetParticleQueue())
	{
		if (go->IsEnabled()) go->Draw(mDevice->GetContext());
	}
	mParticles.Reset(mDevice);

	mParticleProps.Set(mDevice, particlePerFrame);
	for (auto& go : mCurrScene->GetParticleQueue())
	{
		if (go->IsEnabled()) go->DrawAuto(mDevice->GetContext());
	}
	mParticleProps.Reset(mDevice);

	mParticleIntensity.Set(mDevice, &mParticleIntensityData);
	mParticleIntensity.Dispatch(mDevice->GetContext());
	mParticleIntensity.Reset(mDevice);
	
	mParticleShading.Set(mDevice, &mCurrScene->GetParticleShadingPerFrameData());
	mParticleShading.Dispatch(mDevice->GetContext());
	mParticleShading.Reset(mDevice);

	//
	// Merge Raster and Raytrace
	//
	mMerger.Set(mDevice, &mMergerTextures);
	mMerger.Dispatch(mDevice->GetContext());
	mMerger.Reset(mDevice);

	//
	// Draw GUI & Tone mapping & show framebuffer
	//
	auto rtt = mDevice->GetBackBufferTexture();
	if (mCurrScene->ExecuteGUI())
	{
		mGUI.Set(mDevice, &mCurrScene->GetGUIPerFrameData());
		for (auto& go : mCurrScene->GetGUIQueue())
		{
			if (go->IsEnabled()) go->Dispatch(mDevice->GetContext(), (UINT)std::ceil(mGUI.GetRenderTargetWidth() / (float)THREAD_COUNT_2D), (UINT)std::ceil(mGUI.GetRenderTargetHeight() / (float)THREAD_COUNT_2D), 1);
		}
		mGUI.Reset(mDevice);

		// render target is correct already, this op is no use
		mHDRpp.SetInputFrame(mGUI.GetRenderTarget());
		mHDRpp.Set(mDevice, nullptr);
		mHDRpp.Dispatch(mDevice->GetContext());
		mHDRpp.Reset(mDevice);
		
		mDevice->LockContext();
		mDevice->GetContext()->CopyResource(rtt, mHDRpp.GetRenderTarget()->Texture());
		mDevice->UnlockContext();
	}
	else
	{
		mHDRpp.SetInputFrame(mMerger.GetRenderTarget());
		mHDRpp.Set(mDevice, nullptr);
		mHDRpp.Dispatch(mDevice->GetContext());
		mHDRpp.Reset(mDevice);
		
		mDevice->LockContext();
		mDevice->GetContext()->CopyResource(rtt, mHDRpp.GetRenderTarget()->Texture());
		mDevice->UnlockContext();
	}
	
	//
	// Draw Font
	//
	mDevice->LockContext();
	mFontEngine->Set();
	for (auto& go : mCurrScene->GetFontQueue())
	{
		if (go->IsEnabled()) go->Draw(mFontEngine->GetRenderTarget(), mFontEngine->GetWriteFactory(), mFontEngine->GetBrush());
	}
	mFontEngine->Reset();
	mDevice->UnlockContext();
	
	FinalizeRender();
	Present();
}

void Renderer::Present()
{
	mDevice->LockContext();
	if (mDevice->UseVSYNC())
		mDevice->GetSwapchain()->Present(1, 0);
	else
		mDevice->GetSwapchain()->Present(0, 0);
	mDevice->UnlockContext();
}

void Renderer::BakeStaticEnvironment(Scene* pScene, const XMFLOAT3& cameraPos)
{
	Logger::instance().Msg(LOG_LAYER::LOG_RENDER, "Baking Static Environment Map to CubeMap...");

	mVolumeRaytracer.SetEnvMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	mSolid.SetEnvMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));
	mSkinned.SetEnvMap(GPUMemoryManager::instance().GetCubeMap("Assets/Textures/wood_02_d.dds"));

	CubeMap* envMap = GPUMemoryManager::instance().GetCubeMap(pScene->GetName(), 256);
	DepthStencilBuffer* envDepth = new DepthStencilBuffer();
	RenderTarget* fakeDepthTarget = new RenderTarget();
	GPUMemoryManager::instance().DepthStencil(envMap->GetWidth(), envMap->GetHeight(), false, false, envDepth);
	GPUMemoryManager::instance().SingleFramebuffer(envMap->GetWidth(), envMap->GetHeight(), false, false, fakeDepthTarget);

	IGraphicsPipeline::Desc oldSolid, oldSky, envSolid, envSky;
	
	// old camera transform
	XMFLOAT3 oldCameraPos = pScene->GetCamera()->GetTransform()->GetPosition();
	XMFLOAT4 oldCameraRot = pScene->GetCamera()->GetTransform()->GetRotation();
	// new camera position
	pScene->GetCamera()->GetTransform()->SetPosition(cameraPos);
	// save old frustum info
	CameraComponent oldCam = *pScene->GetCamera()->GetCamera();
	// set new frustum to render
	pScene->GetCamera()->GetCamera()->SetFrustum(envMap->GetWidth(), envMap->GetHeight(), 90.0f, 1.0f, 0.1f, 1000.0f);

	XMFLOAT3 targets[6] =
	{
		XMFLOAT3(cameraPos.x + 1,	cameraPos.y,		cameraPos.z		),
		XMFLOAT3(cameraPos.x - 1,	cameraPos.y,		cameraPos.z		),
		XMFLOAT3(cameraPos.x,		cameraPos.y + 1,	cameraPos.z		),
		XMFLOAT3(cameraPos.x,		cameraPos.y - 1,	cameraPos.z		),
		XMFLOAT3(cameraPos.x,		cameraPos.y,		cameraPos.z + 1	),
		XMFLOAT3(cameraPos.x,		cameraPos.y,		cameraPos.z - 1	)
	};

	oldSolid = mSolid.GetDesc();
	envSolid = oldSolid;
	envSolid.viewport = envMap->GetViewport();
	envSolid.depthTarget = fakeDepthTarget->GetRTV();
	envSolid.depthStencil = envDepth->GetDSV();

	oldSky = mSky.GetDesc();
	envSky = oldSky;
	envSky.viewport = envMap->GetViewport();
	envSky.depthTarget = fakeDepthTarget->GetRTV();
	envSky.depthStencil = envDepth->GetDSV();

	for (uint8_t i = 0; i < 6; i++)
	{
		pScene->GetCamera()->GetTransform()->LookAt(targets[i]);
		
		//
		// Draw all solids
		//
		envSolid.renderTarget = envMap->GetRTV(i);
		auto solidData = pScene->GetSolidPerFrameData();
		mSolid.SetDesc(envSolid);
		mSolid.Set(mDevice, &solidData);
		for (auto& go : pScene->GetOpaqueQueue())
		{
			// not required in the alpha pipeline
			if (go->IsEnabled()) go->Draw(mDevice->GetContext());
		}
		mSolid.Reset(mDevice);

		//
		// Draw sky
		//
		envSky.renderTarget = envMap->GetRTV(i);
		mSky.SetDesc(envSky);
		mSky.Set(mDevice, &pScene->GetSolidPerFrameData());
		// HACK : should always load default sky
		// when u have default sky, check for flag instead of nullptr
		auto s = pScene->GetSky();
		if (s != nullptr) s->Draw(mDevice->GetContext());
		mSky.Reset(mDevice);
	}

	// set original pipeline data
	mSolid.SetDesc(oldSolid);
	mSky.SetDesc(oldSky);

	delete envDepth;

	// reset camera transform
	pScene->GetCamera()->GetTransform()->SetPosition(oldCameraPos);
	pScene->GetCamera()->GetTransform()->SetRotation(oldCameraRot);
	// copy back old camera
	pScene->GetCamera()->GetCamera()->SetFrustum(oldCam.GetWidth(), oldCam.GetHeight(), 60.0f, oldCam.GetWidth() / (float)oldCam.GetHeight(), 0.1f, 1000.0f);

	mVolumeRaytracer.SetEnvMap(GPUMemoryManager::instance().GetCubeMap(pScene->GetName(), 256));
	mSolid.SetEnvMap(GPUMemoryManager::instance().GetCubeMap(pScene->GetName(), 256));
	mSkinned.SetEnvMap(GPUMemoryManager::instance().GetCubeMap(pScene->GetName(), 256));

	mVolumeRaytracer.SetIrrMap(GPUMemoryManager::instance().GetCubeMap(pScene->GetName(), 256));
	Logger::instance().Msg(LOG_LAYER::LOG_RENDER, "Completed!");
}

void Renderer::BakeStaticEnvironment(const XMFLOAT3& cameraPos)
{
	this->BakeStaticEnvironment(mCurrScene, cameraPos);
}

void Renderer::SetCurrentScene(Scene* scene)
{
	mCurrScene = scene;
	
	mVolumeRaytracer.SetEnvMap(GPUMemoryManager::instance().GetCubeMap(mCurrScene->GetName(), 256));
	mSolid.SetEnvMap(GPUMemoryManager::instance().GetCubeMap(mCurrScene->GetName(), 256));
	mSkinned.SetEnvMap(GPUMemoryManager::instance().GetCubeMap(mCurrScene->GetName(), 256));

	// Get static scene information
	if (mCurrScene->GetSky() != nullptr)
	{
		mVolumeRaytracer.SetEnvMap(dynamic_cast<SkyMaterial*>(mCurrScene->GetSky()->GetMaterial())->GetSkyProbe());
	}

	mCurrScene->OnResize(mDevice->GetDevice(), mDevice->GetWidth(), mDevice->GetHeight());
}

void Renderer::InitPipelines()
{
	// 
	// Solid Pipeline
	//
	IGraphicsPipeline::Desc solidDesc;
	solidDesc.renderTarget = mMergerTextures.mRasterColor->GetRTV();
	solidDesc.depthTarget = mMergerTextures.mRasterDepth->GetRTV();
	solidDesc.clearFramebuffer = true;
	solidDesc.depthStencil = mDevice->GetDepthStencil();
	solidDesc.clearDepthStencil = true;
	solidDesc.viewport = mDevice->GetViewport();
	solidDesc.rasterState = RenderStates::DefaultRS;
	solidDesc.topology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	solidDesc.vertexStage = GPUMemoryManager::instance().GetVertexShader("Assets/Shaders/Solid/pbs_solid_vert.cso", InputLayoutDesc::Default);
	solidDesc.pixelStage = GPUMemoryManager::instance().GetPixelShader("Assets/Shaders/Solid/pbs_solid_frag.cso");
	mSolid.Create(mDevice->GetDevice(), &solidDesc);

	//
	// Skinned pipeline
	//
	// only need to change shaders as buffers are the same
	solidDesc.clearFramebuffer = false;
	solidDesc.clearDepthStencil = false;
	solidDesc.vertexStage = GPUMemoryManager::instance().GetVertexShader("Assets/Shaders/Solid/pbs_skinned_vert.cso", InputLayoutDesc::SkinnedVertex);
	solidDesc.pixelStage = GPUMemoryManager::instance().GetPixelShader("Assets/Shaders/Solid/pbs_skinned_frag.cso");
	mSkinned.Create(mDevice->GetDevice(), &solidDesc);

	//
	// Raytrace Pipeline
	//
	IComputePipeline::Desc compDesc;

	compDesc.computeStage = GPUMemoryManager::instance().GetComputeShader("Assets/Shaders/Compute/PBSRaytracer.cso");
	mVolumeRaytracer.Create(mDevice->GetDevice(), &compDesc);

	//
	// Particle Pipeline
	//
	IGraphicsPipeline::Desc particleDesc;
	particleDesc.renderTarget = mParticleIntensityData.mParticleDensity->GetRTV();
	particleDesc.viewport = mDevice->GetViewport();
	particleDesc.rasterState = RenderStates::NoCullRS;
	particleDesc.depthState = RenderStates::NoDepthWriteDS;
	particleDesc.clearFramebuffer = true;
	particleDesc.clearDepthStencil = false;
	particleDesc.topology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
	particleDesc.geometryStage = GPUMemoryManager::instance().GetGeometryShader("Assets/Shaders/Solid/particle_geom.cso", InputLayoutDesc::SpriteStreamOut);
	particleDesc.vertexStage = GPUMemoryManager::instance().GetVertexShader("Assets/Shaders/Solid/particle_vert.cso", InputLayoutDesc::BaseParticle);
	particleDesc.pixelStage = GPUMemoryManager::instance().GetPixelShader("Assets/Shaders/Solid/particle_frag.cso");
	particleDesc.blendState = RenderStates::AdditiveAccumState;
	mParticles.Create(mDevice->GetDevice(), &particleDesc);

	particleDesc.depthStencil = mDevice->GetDepthStencil();
	//particleDesc.clearDepthStencil = true;
	particleDesc.topology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST; 
	particleDesc.depthState = nullptr;
	particleDesc.geometryStage = nullptr;
	particleDesc.renderTarget = mMergerTextures.mParticleNormalAndRadii->GetRTV();
	particleDesc.depthTarget = mParticleShading.GetParticleDepth()->GetRTV();
	particleDesc.vertexStage = GPUMemoryManager::instance().GetVertexShader("Assets/Shaders/Solid/particle_nop_vert.cso", InputLayoutDesc::SpriteVertex);
	particleDesc.pixelStage = GPUMemoryManager::instance().GetPixelShader("Assets/Shaders/Solid/particle_depth_frag.cso");
	particleDesc.blendState = nullptr;
	mParticleProps.Create(mDevice->GetDevice(), &particleDesc);

	IComputePipeline::Desc particlePostProcess;
  	particlePostProcess.computeStage = GPUMemoryManager::instance().GetComputeShader("Assets/Shaders/Compute/particle_intensity.cso");
	mParticleIntensity.Create(mDevice->GetDevice(), &particlePostProcess);

	particlePostProcess.computeStage = GPUMemoryManager::instance().GetComputeShader("Assets/Shaders/Compute/particle_radiance.cso");
	mParticleShading.Create(mDevice->GetDevice(), &particlePostProcess);

	//
	// Skybox Pipeline
	//
	IGraphicsPipeline::Desc skyDesc;
	skyDesc.renderTarget = mMergerTextures.mRasterColor->GetRTV();
	skyDesc.clearFramebuffer = false;
	skyDesc.depthStencil = mDevice->GetDepthStencil();
	skyDesc.clearDepthStencil = false;
	skyDesc.viewport = mDevice->GetViewport();
	skyDesc.rasterState = RenderStates::NoCullRS;
	skyDesc.depthState = RenderStates::NoDepthWriteDS;
	skyDesc.topology = D3D11_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	skyDesc.vertexStage = GPUMemoryManager::instance().GetVertexShader("Assets/Shaders/Solid/sky_vert.cso", InputLayoutDesc::Default);
	skyDesc.pixelStage = GPUMemoryManager::instance().GetPixelShader("Assets/Shaders/Solid/sky_frag.cso");
	mSky.Create(mDevice->GetDevice(), &skyDesc);

	//
	// Merge Pipeline
	//
	IComputePipeline::Desc mergeDesc;
	mergeDesc.computeStage = GPUMemoryManager::instance().GetComputeShader("Assets/Shaders/Compute/Merger.cso");
	mMerger.Create(mDevice->GetDevice(), &mergeDesc);

	//
	// GUI Pipeline
	//
	IComputePipeline::Desc guiDesc;
	guiDesc.computeStage = GPUMemoryManager::instance().GetComputeShader("Assets/Shaders/Compute/GUI.cso");
	mGUI.Create(mDevice->GetDevice(), &guiDesc);

	//
	// HDR Tone Mapping
	//
	IComputePipeline::Desc hdrDesc;
	hdrDesc.computeStage = GPUMemoryManager::instance().GetComputeShader("Assets/Shaders/Compute/ToneMapping.cso");
	mHDRpp.Create(mDevice->GetDevice(), &hdrDesc);
}

void Renderer::PrepareRender()
{
	if (!mCurrScene)
		return;

	/*ID3D11RenderTargetView* lrtv = mDevice->GetBackBuffer();
	ID3D11DepthStencilView* ldsv = mDevice->GetDepthStencil();

	// should be done in a render pass, OK for now
	static const float clearColor[] = { 0.0f, 0.65f, 0.95f, 1.0f };
	mDevice->GetContext()->ClearRenderTargetView(lrtv, clearColor);
	mDevice->GetContext()->ClearDepthStencilView(ldsv, D3D11_CLEAR_FLAG::D3D11_CLEAR_DEPTH | D3D11_CLEAR_FLAG::D3D11_CLEAR_STENCIL, 1.0f, 0);

	ID3D11RenderTargetView* rtv[] = { lrtv };
	ID3D11DepthStencilView* dsv[] = { ldsv };
	mDevice->GetContext()->OMSetRenderTargets(1, rtv, *dsv);
	mDevice->GetContext()->RSSetViewports(1, mDevice->GetViewport());*/
}

void Renderer::FinalizeRender()
{
}
