#include "DeviceManager.h"

#include "InputManager.h"

#ifdef _WIN64
typedef unsigned __int64 QWORD;
#endif


DeviceManager::DeviceManager()
{
}


DeviceManager::~DeviceManager()
{
}

bool DeviceManager::Init()
{
	return true;
}

// Adds a new Device Class with proper Usage Pages and Usages for Registering Raw Input Devices.
bool DeviceManager::AddNewDevice(HWND hWnd, InputDevice deviceType, IInputDevice** device)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	if (deviceType != Input::InputDevice::SMARTPHONE)
	{
		RAWINPUTDEVICE rid;
		rid.usUsagePage = DeviceRawUsagePages[deviceType].first;
		rid.usUsage = DeviceRawUsagePages[deviceType].second;
		rid.dwFlags = /*RIDEV_NOLEGACY |*/ RIDEV_EXINPUTSINK;
		rid.hwndTarget = hWnd;
		if (RegisterRawInputDevices(&rid, 1, sizeof(RAWINPUTDEVICE)) == FALSE)
		{
			InputManager::instance().HandleError("ErrorCode for Adding new Raw Input Device:", GetLastError());
			return false;
		}
	}

	switch (deviceType)
	{
	case InputDevice::KEYBOARD:
		*device = new Keyboard();
		break;

	case InputDevice::MOUSE:
		*device = new Mouse();
		break;

	case InputDevice::SMARTPHONE:
		*device = new Smartphone();
		break;
	}

	// RegisterNewDevice here
	if (RegisterNewDevice(hWnd, deviceType, *device) == false)
	{
		//HandleError("Error with registering new device", GetLastError());
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "New device registration didn't work.");
		delete *device;
		return false;
	}

	mRegisteredDevices.insert(std::make_pair(deviceType, *device));
	return true;
}

bool DeviceManager::RegisterNewDevice(HWND hWnd, InputDevice deviceType, IInputDevice* device)
{
	// TODO: GUI Transparent Text.
	// Print out a message to the user to press anything on the device.
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	PRAWINPUTDEVICE pRID;
	UINT numDevices;

	if (PrintAllRegisteredTLC(&pRID, &numDevices) == false)
	{
		InputManager::instance().HandleError("Error printing registered TLCs", GetLastError());
		free(pRID);
		return false;
	}

	PrintAllRegisteredDevices();

	/*	Possibly integrate Network here
	If DeviceType for network, then network, otherwise RawInput */
	switch (deviceType)
	{
	case InputDevice::SMARTPHONE:
		{
			Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Connect your supported device with the server to register.");
			printf("\tDeviceType to register: %s\n", InputDeviceString[deviceType].c_str());
			if (InputManager::instance().InitNetwork())
			{
				int *idArray = nullptr;
				int length = 0;
				if (InputManager::instance().AcceptNetworkConnection(1, &idArray, &length))
				{
					device->Init((HANDLE)static_cast<int64_t>(idArray[length - 1]));
				}
				else
				{
					printf("No new device found.\n");
					free(pRID);
					return false;
				}
			}
			else
			{
				printf("Could not initialize network!\n");
				free(pRID);
				return false;
			}
			break;
		}
	case InputDevice::KEYBOARD:
	case InputDevice::MOUSE:
		{
			Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Press any key on your supported device to register.");
			printf("\tDeviceType to register: %s\n", InputDeviceString[deviceType].c_str());

			PRAWINPUTHEADER pRIH = nullptr;
			bool found;
			double startTime = GetTickCount();
			double currentTime;
			while (true)
			{
				pRIH = nullptr;
				currentTime = GetTickCount() - startTime;
				if (currentTime > 5000)
				{
					break;
				}

				found = false;
				if (GetInput(hWnd, &pRIH) == false)
				{
					free(pRID);
					return false;
				}

				for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
				{
					if (it->second->GetDeviceID() == pRIH->hDevice)
					{
						found = true;
						//printf("Device already registered! Stopping Registration.\n");
						printf("Device already registered!\n");
						free(pRIH);
						//return false;
						break;
					}
					if (found)
						break;
				}
				if (found)
					continue;

				if (((pRIH->dwType == RIM_TYPEKEYBOARD) && (deviceType == Input::InputDevice::KEYBOARD)) ||
					((pRIH->dwType == RIM_TYPEMOUSE) && (deviceType == Input::InputDevice::MOUSE)) ||
					((pRIH->dwType == RIM_TYPEHID) && true /*Placeholder*/))
				{
					// TODO: ErrorHandling for wrong InputDeviceMapping between IInputDevice and RealHardware missing (probably only a border case)
					device->Init(pRIH->hDevice);
					break;
				}
				else
				{
					//printf("DeviceType %i doesn't match Type %s! Stopping Registration.\n", pRIH->dwType, Input::InputDeviceString[deviceType].c_str());
					printf("DeviceType %i doesn't match Type %s!\n", pRIH->dwType, Input::InputDeviceString[deviceType].c_str());

					free(pRIH);
					//free(pRID);
					//return false;
				}
			}

			if (pRIH == nullptr)
			{
				free(pRID);
				return false;
			}

			free(pRIH);
			break;
		}
	}

	free(pRID);

	return true;
}

bool DeviceManager::RemoveRegisteredDevice(IInputDevice* device, bool *lastDevice, Input::InputDevice *ofType)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	int i = 0;
	bool found = false;
	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		if (it->second == device)
		{
			/*if ((it->first == Input::InputDevice::KEYBOARD) && (mRegisteredDevices.count(it->first) == 1))
			{
				Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Last Keyboard will not be removed!");
				break;
			}*/

			Input::InputDevice inputDevice = it->first;
			printf("Removed Device %i: Device %s\tHandle %p\n", i, InputDeviceString[it->first].c_str(), it->second->GetDeviceID());

			delete device;
			mRegisteredDevices.erase(it);

			if (mRegisteredDevices.count(inputDevice) == 0)
			{
				if (inputDevice != Input::InputDevice::SMARTPHONE)
				{
					RAWINPUTDEVICE rid;
					rid.usUsagePage = DeviceRawUsagePages[inputDevice].first;
					rid.usUsage = DeviceRawUsagePages[inputDevice].second;
					rid.dwFlags = RIDEV_REMOVE;
					rid.hwndTarget = NULL;

					if (RegisterRawInputDevices(&rid, 1, sizeof(RAWINPUTDEVICE)) == FALSE)
					{
						InputManager::instance().HandleError("Error for removing device TLC", GetLastError());
						return false;
					}
				}

				if (lastDevice != nullptr)
					*lastDevice = true;
				if (ofType != nullptr)
					*ofType = inputDevice;
			}
			else
			{
				if (lastDevice != nullptr)
					*lastDevice = false;
				if (ofType != nullptr)
					ofType = nullptr;
			}

			found = true;
			break;
		}
		if (found)
			break;
	}
	return found;
}

void DeviceManager::FlushAllDevices()
{
	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		delete it->second;
	}
	mRegisteredDevices.clear();
	inputDeviceState = InputState::NONE;
	Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Nullifying the InputState.");
}

bool DeviceManager::UseAllDevices(HWND hWnd)
{
	if (inputDeviceState == InputState::PLAYER)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in PLAYER InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::RAW;
	}

	const int numDevices = (int)Input::InputDevice::NUM_SIZE;
	RAWINPUTDEVICE rid[numDevices];
	int arrayShift = 0;
	for (int i = 0; i < numDevices; i++)
	{
		if (i == (int)Input::InputDevice::SMARTPHONE)
		{
			arrayShift++;
			continue;
		}
		rid[i-arrayShift].usUsagePage = DeviceRawUsagePages[(Input::InputDevice)i].first;
		rid[i-arrayShift].usUsage = DeviceRawUsagePages[(Input::InputDevice)i].second;
		rid[i-arrayShift].dwFlags = /*RIDEV_NOLEGACY | */RIDEV_EXINPUTSINK;
		rid[i-arrayShift].hwndTarget = hWnd;
	}
	if (RegisterRawInputDevices(rid, numDevices-arrayShift, sizeof(RAWINPUTDEVICE)) == FALSE)
	{
		InputManager::instance().HandleError("ErrorCode for Adding new Raw Input Device in UseAllDevices:", GetLastError());
		return false;
	}

	return true;
}

bool DeviceManager::RemoveDevicesOfType(InputDevice inputType, IInputDevice *** deviceList, int * numOfDevicesToRemove)
{
	auto pair = mRegisteredDevices.equal_range(inputType);
	*numOfDevicesToRemove = 0;
	IInputDevice **tmpList = (IInputDevice **)malloc(mRegisteredDevices.size() * sizeof(IInputDevice*));
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		tmpList[*numOfDevicesToRemove] = valueIt->second;
		*numOfDevicesToRemove++;
	}
	*deviceList = (IInputDevice **)malloc(*numOfDevicesToRemove * sizeof(IInputDevice*));
	memcpy(*deviceList, tmpList, *numOfDevicesToRemove * sizeof(IInputDevice*));
	free(tmpList);
	mRegisteredDevices.erase(inputType);

	if (*numOfDevicesToRemove == 0)
		return false;
	else
		return true;
}

void DeviceManager::GetAllKnownDevices()
{
	PRAWINPUTDEVICELIST pridl;
	UINT numDevices;

	if (GetRawInputDeviceList(NULL, &numDevices, sizeof(RAWINPUTDEVICELIST)) != 0)
	{
		InputManager::instance().HandleError("ErrorCode for counting number of Input Devices on the system:", GetLastError());
	}

	if ((pridl = (PRAWINPUTDEVICELIST)malloc(sizeof(RAWINPUTDEVICELIST) * numDevices)) == NULL)
	{
		InputManager::instance().HandleError("ErrorCode for allocating memory of RAWINPUTDEVICELIST:", GetLastError());
	}

	if (GetRawInputDeviceList(pridl, &numDevices, sizeof(RAWINPUTDEVICELIST)) == -1)
	{
		InputManager::instance().HandleError("ErrorCode for getting Raw Input Devices:", GetLastError());
	}

	for (UINT i = 0; i < numDevices; i++)
	{
		UINT uiCommand = RIDI_DEVICEINFO;
		UINT bufSize;
		if (GetRawInputDeviceInfo(pridl[i].hDevice, uiCommand, NULL, &bufSize) != 0)
		{
			InputManager::instance().HandleError("ErrorCode for getting size of Raw Input Device Info:", GetLastError());
		}

		PRID_DEVICE_INFO priddi;
		if ((priddi = (PRID_DEVICE_INFO)malloc(bufSize)) == NULL)
		{
			InputManager::instance().HandleError("ErrorCode for allocating memory of Device Info:", GetLastError());
		}

		priddi->cbSize = sizeof(RID_DEVICE_INFO);

		if (GetRawInputDeviceInfo(pridl[i].hDevice, uiCommand, priddi, &bufSize) <= 0)
		{
			InputManager::instance().HandleError("ErrorCode for getting Raw Input Device Info:", GetLastError());
		}

		if (priddi->dwType == RIM_TYPEMOUSE)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Mouse ID: " + std::to_string(priddi->mouse.dwId) + "\n\tUsage Page 0x01, Usage 0x02");
			printf("\tHandle: %p\n", pridl[i].hDevice);
		}
		else if (priddi->dwType == RIM_TYPEKEYBOARD)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Keyboard Type & SubType: " + std::to_string(priddi->keyboard.dwType) + " & " +
				std::to_string(priddi->keyboard.dwSubType) + "\n\tUsage Page 0x01, Usage 0x06");
			printf("\tHandle: %p\n", pridl[i].hDevice);
		}
		else if (priddi->dwType == RIM_TYPEHID)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "HID Vendor & Product ID: " + std::to_string(priddi->hid.dwVendorId) + " & " +
				std::to_string(priddi->hid.dwProductId) + "\n\tUsage Page " + std::to_string(priddi->hid.usUsagePage) + 
				", Usage " + std::to_string(priddi->hid.usUsage));
			printf("\tHandle: %p\n", pridl[i].hDevice);
		}

		free(priddi);
	}

	PRAWINPUTDEVICE pRID;

	if (PrintAllRegisteredTLC(&pRID, &numDevices) == false)
	{
		InputManager::instance().HandleError("Error printing registered TLCs", GetLastError());
	}

	free(pRID);
	free(pridl);
}

bool DeviceManager::PrintAllRegisteredTLC(PRAWINPUTDEVICE* pRID, UINT* numDevices)
{
	if (GetRegisteredRawInputDevices(NULL, numDevices, sizeof(RAWINPUTDEVICE)) == -1)
	{
		if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
		{
			InputManager::instance().HandleError("ErrorCode for counting number of Registered Raw Input Devices:", GetLastError());
			return false;
		}
	}

	if ((*pRID = (PRAWINPUTDEVICE)malloc(sizeof(RAWINPUTDEVICE) * *numDevices)) == NULL)
	{
		InputManager::instance().HandleError("ErrorCode for allocating memory:", GetLastError());
		return false;
	}

	if (*numDevices > 0)
	{
		if (GetRegisteredRawInputDevices(*pRID, numDevices, sizeof(RAWINPUTDEVICE)) == -1)
		{
			InputManager::instance().HandleError("ErrorCode for getting Registered Raw Input Devices:", GetLastError());
		}
	}
	else
	{
		Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "No Registered Raw Input Devices found. Call AddNewDevice first.");
	}

	for (UINT i = 0; i < *numDevices; i++)
	{
		printf("Registered Raw Input %i:\n\tUsagePage 0x%02X, Usage 0x%02X\n\tFlags 0x%08X, WindowHandle %p\n",
			i, (*pRID)[i].usUsagePage, (*pRID)[i].usUsage, (*pRID)[i].dwFlags, (*pRID)[i].hwndTarget);
	}
	
	return true;
}

void DeviceManager::PrintAllRegisteredDevices()
{
	int i = 0;
	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		printf("Registered Device %i: Device %s\tHandle %p\n", i, InputDeviceString[it->first].c_str(), it->second->GetDeviceID());
	}
}

bool DeviceManager::GetInput(HWND hWnd, PRAWINPUTHEADER *pRIH)
{
	MSG deviceMessage;
	BOOL bRet;
	
	UINT byteSize;
	while ((bRet = GetMessage(&deviceMessage, hWnd, WM_INPUT, WM_INPUT)) != 0)
	{
		if (bRet == -1)
		{
			InputManager::instance().HandleError("Error receiving a message with GetMessage", GetLastError());
			return false;
		}
		else
		{
			if (GetRawInputData((HRAWINPUT)deviceMessage.lParam, RID_HEADER, NULL, &byteSize, sizeof(RAWINPUTHEADER)) != 0)
			{
				InputManager::instance().HandleError("Error with getting the right size of the message", GetLastError());
				return false;
			}

			if ((*pRIH = (PRAWINPUTHEADER)malloc(byteSize)) == NULL)
			{
				InputManager::instance().HandleError("Error with allocating memory for RAWINPUTHEADER", GetLastError());
				return false;
			}

			if (GetRawInputData((HRAWINPUT)deviceMessage.lParam, RID_HEADER, *pRIH, &byteSize, sizeof(RAWINPUTHEADER)) == -1)
			{
				InputManager::instance().HandleError("Error copying the data", GetLastError());
				free(*pRIH);
				return false;
			}

			printf("Received data from device with handle %p.\n", (*pRIH)->hDevice);

			

			TranslateMessage(&deviceMessage);
			DispatchMessage(&deviceMessage);

			break;
		}
	}
	return true;
}

int DeviceManager::HandleRawInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// ---------------------------------- Non-buffered Read ---------------------------------- //
	UINT byteSize;
	
	if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &byteSize, sizeof(RAWINPUTHEADER)) != 0)
	{
		InputManager::instance().HandleError("Error with getting the right size of the message", GetLastError());
		//free(pRI);
		return false;
	}
	
	byte* dataBuffer = new byte[byteSize];
	
	if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, dataBuffer, &byteSize, sizeof(RAWINPUTHEADER)) == -1)
	{
		InputManager::instance().HandleError("Error copying the data", GetLastError());
		//free(pRI);
		delete dataBuffer;
		return false;
	}
	
	RAWINPUT* pRI = reinterpret_cast<RAWINPUT*>(dataBuffer);
	
	//printf("Received data from device with handle %p.\n", pRI->header.hDevice);
	
	int processed = false;
	int found = false;

	if (inputDeviceState == InputState::PLAYER)
	{
		for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
		{
			if (it->second->GetDeviceID() == pRI->header.hDevice)
			{
				processed = it->second->HandleRawInput(pRI);
				found = true;
				break;
			}
		}
	}
	else if (inputDeviceState == InputState::RAW)
	{
		for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
		{
			if (it->second->GetDeviceID() == pRI->header.hDevice)
			{
				processed = it->second->HandleRawInput(pRI);
				found = true;
				break;
			}
		}

		if (!found)
		{
			switch (pRI->header.dwType)
			{
			case RIM_TYPEMOUSE:
			{
				Mouse* newMouse = new Mouse();
				newMouse->Init(pRI->header.hDevice);
				mRegisteredDevices.insert(std::make_pair(Input::InputDevice::MOUSE, newMouse));
				processed = newMouse->HandleRawInput(pRI);
				break;
			}
			case RIM_TYPEKEYBOARD:
			{
				Keyboard* newKeyboard = new Keyboard();
				newKeyboard->Init(pRI->header.hDevice);
				mRegisteredDevices.insert(std::make_pair(Input::InputDevice::KEYBOARD, newKeyboard));
				processed = newKeyboard->HandleRawInput(pRI);
				break;
			}
			case RIM_TYPEHID:
				Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "HID Devices are not handled yet.");
				break;
			}
		}
	}
	
	delete dataBuffer;
	//free(pRI);
	return processed;
}

void DeviceManager::UpdatePressedState()
{
	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		it->second->UpdatePressedState();	
	}
}

bool DeviceManager::KeyDown(InputKey button)
{
	if (inputDeviceState == InputState::PLAYER)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in PLAYER InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::RAW;
	}

	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		if (it->second->KeyDown(button))
			return true;
	}

	return false;
}

bool DeviceManager::KeyDown(IInputDevice * device, InputKey button)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	return (device != nullptr) ? device->KeyDown(button) : false;
}
					
bool DeviceManager::KeyPressed(InputKey button)
{
	if (inputDeviceState == InputState::PLAYER)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in PLAYER InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::RAW;
	}

	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		if (it->second->KeyPressed(button))
			return true;
	}

	return false;	
}

bool DeviceManager::KeyPressed(IInputDevice * device, InputKey button)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	return (device != nullptr) ? device->KeyPressed(button) : false;
}
					
bool DeviceManager::KeyUp(InputKey button)
{
	if (inputDeviceState == InputState::PLAYER)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in PLAYER InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::RAW;
	}

	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		if (it->second->KeyUp(button))
			return true;
	}

	return false;
}

bool DeviceManager::KeyUp(IInputDevice * device, InputKey button)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	return (device != nullptr) ? device->KeyUp(button) : false;
}

int DeviceManager::GetRawValue(IInputDevice * device)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	return (device != nullptr) ? device->GetRawValue() : 0x00;
}

bool DeviceManager::GetXYPosWheel(int * x, int * y, short *deltaX, short *deltaY, int * w, bool *insideX, bool *insideY)
{
	if (inputDeviceState == InputState::PLAYER)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in PLAYER InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::RAW;
	}

	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		if (it->second->GetXYPosWheel(x, y, deltaX, deltaY, w, insideX, insideY))
			return true;
	}

	return false;
}

bool DeviceManager::GetXYPosWheel(IInputDevice * device, int * x, int * y, short *deltaX, short *deltaY, int * w, bool *insideX, bool *insideY)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	return (device != nullptr) ? device->GetXYPosWheel(x, y, deltaX, deltaY, w, insideX, insideY) : false;
}

bool DeviceManager::GetRange(float * up, float * right)
{
	if (inputDeviceState == InputState::PLAYER)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in PLAYER InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::RAW;
	}

	for (auto it = mRegisteredDevices.begin(); it != mRegisteredDevices.end(); it++)
	{
		if (it->second->GetRange(up, right))
			return true;
	}

	return false;
}

bool DeviceManager::GetRange(IInputDevice * device, float * up, float * right)
{
	if (inputDeviceState == InputState::RAW)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Not allowed to do that! You are in RAW InputState.");
		return false;
	}
	else if (inputDeviceState == InputState::NONE)
	{
		inputDeviceState = InputState::PLAYER;
	}

	return (device != nullptr) ? device->GetRange(up, right) : false;
}


