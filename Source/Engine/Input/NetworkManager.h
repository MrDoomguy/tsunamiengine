#pragma once

//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <sys/types.h>
//#include <WinSock2.h>
//#include <time.h>
//#include <fcntl.h>
//#include <thread>
//
//#pragma comment(lib, "Ws2_32.lib")

#include "../General/TsunamiStd.h"


#define PORT 5555
#define MAXCLIENTS 10
#define BUFFERSIZE 256
#define TIMEOUTCLIENTLOGIN 10
#define REUSEADDR 1
#define DEBUG 1
#define LOOP 1

struct GamePacket
{
	char    packetType;
	short   payloadSize;
	char*   payload;
};

class InputManager;


typedef void(*funcToWF)(float*, float*);

class NetworkManager
{
public:
	NetworkManager();
	~NetworkManager();

	int Init();
	int ResetState();
	int AcceptConnection(int numOfClients, int **clientIDs, int *numIDsInClientIDs);
	int PrepareForGame();
	//int RunGame();


	/*template<class ClassObject>
	int RunGame(ClassObject *thisClass, void(ClassObject::*funcToGetWF)(float*,float*));*/

	template<class ClassObject>
	int RunGame(ClassObject *const thisClass, void(ClassObject::*funcToGetWF)(float*, float*))
	{
		running = true;

		recvMethod = new std::thread(&NetworkManager::RecvThreaded);

		sendMethod = new std::thread(&NetworkManager::SendThreaded<ClassObject>, thisClass, funcToGetWF);

		return true;
	}

	int RunGame();


	int SendData(float up, float right);
	int RecvData();
	int EndGame();

	void RecvThreaded();
	//void SendThreaded(funcToWF getValues);



	/*template<class ClassObject>
	void SendThreaded(ClassObject *thisClass, void(ClassObject::*funcToGetWF)(float*, float*));*/

	template<class ClassObject>
	void SendThreaded(ClassObject *thisClass, void(ClassObject::*funcToGetWF)(float*, float*))
	{
		float up, right;
		while (running)
		{
			((thisClass)->*(funcToGetWF))(&up, &right);
			//func(&up, &right);
			SendData(up, right);
		}
	}

	int gettimeofday(struct timeval * tp, struct timezone * tzp);
	/* Subtract the �struct timeval� values X and Y,
	storing the result in RESULT.
	Return 1 if the difference is negative, otherwise 0. */
	int TimevalSubtract(struct timeval *result, struct timeval *x, struct timeval *y);

private:
	uint8_t running;
	bool initialized = false;

	std::thread* recvMethod = nullptr;
	std::thread* sendMethod = nullptr;

	WSADATA wsaData;
	int iResult;
	SOCKET ServerSocket = INVALID_SOCKET;

	char *buf = nullptr;
	char *bufptr = nullptr;
	int buflen;
	int rcv, snd;

	fd_set readset, tmpset;
	struct timeval timetocheck;
	time_t timeout;
	int maxfd;

	struct sockaddr_in saddr, caddr;
	struct sockaddr_in caddrlst[MAXCLIENTS];
	int saddrlen = sizeof(saddr);
	int caddrlen = sizeof(caddr);

	int clnum;

	
	bool ClientCheck(struct sockaddr_in *__list, int __numelements, struct sockaddr_in *__searchfor, int *__listnumber);
};

