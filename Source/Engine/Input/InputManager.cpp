#include "InputManager.h"



InputManager::InputManager()
{
}


InputManager::~InputManager()
{
}

void InputManager::Init(HWND hWnd)
{
	mDeviceManager = new DeviceManager();
	mInputMapper = new InputMapper();
	mContextManager = new ContextManager();
	mNetworkManager = new NetworkManager();

	SetHandleWindow(hWnd);
	//mInitialized = mDeviceManager->Init(hWnd);
}

void InputManager::Destroy()
{
	delete mDeviceManager;
	delete mInputMapper;
	delete mContextManager;
	delete mNetworkManager;
}

void InputManager::SetHandleWindow(HWND hWnd)
{
	mHandleWindow = hWnd;
}

void InputManager::GetHandleWindow(HWND * hWnd)
{
	if (hWnd != nullptr)
	{
		*hWnd = mHandleWindow;
	}
}

void InputManager::SetWindowSize(UINT width, UINT height)
{
	mWidth = width;
	mHeight = height;
}

void InputManager::GetWindowSize(UINT * width, UINT * height)
{
	if (width != nullptr)
	{
		*width = mWidth;
	}

	if (height != nullptr)
	{
		*height = mHeight;
	}
}

bool InputManager::AddPlayer(UINT8 playerID, InputDevice deviceType)
{
	if (mPlayerToDevice.find(playerID) != mPlayerToDevice.end())
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Player already available.");
		return false;
	}

	IInputDevice* device = nullptr;
	if (AddNewDevice(deviceType, &device) == false)
	{
		return false;
	}

	mPlayerToDevice.insert(std::make_pair(playerID, device));

	return true;
}

bool InputManager::AddDevice(UINT8 playerID, InputDevice deviceType)
{
	if (mPlayerToDevice.find(playerID) == mPlayerToDevice.end())
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Player not found.");
		return false;
	}

	IInputDevice* device = nullptr;
	if (AddNewDevice(deviceType, &device) == false)
	{
		return false;
	}

	mPlayerToDevice.insert(std::make_pair(playerID, device));

	return true;
}

bool InputManager::RemoveDevice(UINT8 playerID, InputDevice deviceType)
{
	if (mPlayerToDevice.find(playerID) == mPlayerToDevice.end())
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Player not found. Cannot remove device.");
		return false;
	}

	switch (deviceType)
	{
	case InputDevice::SMARTPHONE:
		/*Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Disonnect your supported device from the server to remove it.");
		printf("\tDeviceType to register: %s\n", InputDeviceString[deviceType].c_str());*/
		
		//InputManager::instance().ResetNetworkState();
		RemoveDevicesOfType(Input::InputDevice::SMARTPHONE);

		break;
	case InputDevice::KEYBOARD:
	case InputDevice::MOUSE:
		Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Press any key on the supported device to remove it.");
		printf("\tDeviceType to remove: %s\n", InputDeviceString[deviceType].c_str());

		PRAWINPUTHEADER pRIH = nullptr;
		if (mDeviceManager->GetInput(mHandleWindow, &pRIH) == false)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Problem with receiving Input. No device removed.");
			return false;
		}

		switch (pRIH->dwType)
		{
		case RIM_TYPEMOUSE:
			if (deviceType != InputDevice::MOUSE)
			{
				Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Registered device is not of intended DeviceType to remove.");
				return false;
			}
			break;
		case RIM_TYPEKEYBOARD:
			if (deviceType != InputDevice::KEYBOARD)
			{
				Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Registered device is not of intended DeviceType to remove.");
				return false;
			}
		}

		bool lastDevice;
		InputDevice ofType;
		auto pair = mPlayerToDevice.equal_range(playerID);
		for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
		{
			if (valueIt->second->GetDeviceID() == pRIH->hDevice)
			{
				if (RemoveRegisteredDevice(valueIt->second, &lastDevice, &ofType) == false)
				{
					Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Problem removing device.");
					free(pRIH);
					return false;
				}
				mPlayerToDevice.erase(valueIt);
				if (lastDevice)
				{
					printf("Removed last device of type %s\n", Input::InputDeviceString[ofType].c_str());
				}
				break;
			}
		}

		free(pRIH);
		break;
	}
	return true;
}

bool InputManager::RemovePlayer(UINT8 playerID)
{
	bool lastDevice;
	InputDevice ofType;
	auto pair = mPlayerToDevice.equal_range(playerID);
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		if (RemoveRegisteredDevice(valueIt->second, &lastDevice, &ofType) == false)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Problem removing device.");
			return false;
		}
		mPlayerToDevice.erase(valueIt);
		if (lastDevice)
		{
			printf("Removed last device of type %s\n", Input::InputDeviceString[ofType].c_str());
		}
	}

	Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Removed Player " + std::to_string(playerID) + ".");
	return true;
}

bool InputManager::RemoveDevicesOfType(InputDevice inputType)
{
	IInputDevice** deviceList = nullptr;
	int numOfDevicesToRemove = 0;
	bool result = mDeviceManager->RemoveDevicesOfType(inputType, &deviceList, &numOfDevicesToRemove);
	if (inputType == Input::InputDevice::SMARTPHONE)
	{
		mNetworkManager->ResetState();
	}
	
	for (int i = numOfDevicesToRemove - 1; i >= 0; i--)
	{
		for (auto it = mPlayerToDevice.begin(); it != mPlayerToDevice.end();)
		{
			if (it->second == deviceList[i])
			{
				free(it->second);
				mPlayerToDevice.erase(it++);	// or "it = m.erase(it)" since C++11
				break;
			}
			else
			{
				++it;
			}
		}
	}

	free(deviceList);

	return result;
}

void InputManager::FlushAllDevices()
{
	mDeviceManager->FlushAllDevices();
	mPlayerToDevice.clear();
	mInitialized = false;
}

bool InputManager::UseAllDevices()
{
	if (!mDeviceManager->UseAllDevices(mHandleWindow))
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Couldn't successfully open all devices for Raw Input.");
		return false;
	}

	if(!mInitialized)
		mInitialized = true;

	return true;
}

int InputManager::InitNetwork()
{
	return mNetworkManager->Init();
}

int InputManager::ResetNetworkState()
{
	return mNetworkManager->ResetState();
}

int InputManager::AcceptNetworkConnection(int numOfClients, int ** clientIDs, int * numIDsInClientIDs)
{
	return mNetworkManager->AcceptConnection(numOfClients, clientIDs, numIDsInClientIDs);
}

int InputManager::PrepareNetworkForGame()
{
	return mNetworkManager->PrepareForGame();
}

//template<class ClassObject>
//int InputManager::RunNetworkGame(ClassObject *thisClass, void(ClassObject::*funcToGetWF)(float*, float*))
//{
//	return mNetworkManager->RunGame<ClassObject>(thisClass, funcToGetWF);
//}

int InputManager::RunNetworkGame()
{
	return mNetworkManager->RunGame();
}

int InputManager::EndNetworkGame()
{
	return mNetworkManager->EndGame();
}

bool InputManager::KeyDown(UINT8 playerID, InputKey button)
{
	bool success = false;
	auto pair = mPlayerToDevice.equal_range(playerID);
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		if (mDeviceManager->KeyDown(valueIt->second, button))
		{
			success = true;
			break;
		}
	}
	return success;
}

bool InputManager::KeyPressed(UINT8 playerID, InputKey button)
{
	bool success = false;
	auto pair = mPlayerToDevice.equal_range(playerID);
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		if (mDeviceManager->KeyPressed(valueIt->second, button))
		{
			success = true;
			break;
		}
	}
	return success;
}

bool InputManager::KeyUp(UINT8 playerID, InputKey button)
{
	bool success = false;
	auto pair = mPlayerToDevice.equal_range(playerID);
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		if (mDeviceManager->KeyUp(valueIt->second, button))
		{
			success = true;
			break;
		}
	}
	return success;
}

int InputManager::GetRawValue(UINT8 playerID)
{
	int res = 0x00;
	auto pair = mPlayerToDevice.equal_range(playerID);
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		if ((res = mDeviceManager->GetRawValue(valueIt->second)) != 0x00) break;
	}
	return res;
}

bool InputManager::GetXYPosWheel(UINT8 playerID, int * x, int * y, short *deltaX, short *deltaY, int * w, bool * insideX, bool * insideY)
{
	bool success = false;
	auto pair = mPlayerToDevice.equal_range(playerID);
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		if (mDeviceManager->GetXYPosWheel(valueIt->second, x, y, deltaX, deltaY, w, insideX, insideY))
		{
			success = true;
			break;
		}
	}
	return success;
}

bool InputManager::GetRange(UINT8 playerID, float * up, float * right)
{
	bool success = false;
	auto pair = mPlayerToDevice.equal_range(playerID);
	for (auto valueIt = pair.first; valueIt != pair.second; valueIt++)
	{
		if (mDeviceManager->GetRange(valueIt->second, up, right))
		{
			success = true;
			break;
		}
	}
	return success;
}

void InputManager::SetRange(UINT8 deviceID, float up, float right)
{
	for (auto it = mPlayerToDevice.begin(); it != mPlayerToDevice.end(); it++)
	{
		if (it->second->GetDeviceID() == (HANDLE)deviceID)
		{
			it->second->SetRange(up, right);
			return;
		}
	}
}

void InputManager::SetKey(UINT8 deviceID, char keyValues)
{
	for (auto it = mPlayerToDevice.begin(); it != mPlayerToDevice.end(); it++)
	{
		if (it->second->GetDeviceID() == (HANDLE)deviceID)
		{
			it->second->SetKey(keyValues);
			return;
		}
	}
}

int InputManager::SendData(float up, float right)
{
	return mNetworkManager->SendData(up, right);
}

int InputManager::gettimeofday(timeval * tp, timezone * tzp)
{
	return mNetworkManager->gettimeofday(tp, tzp);
}

int InputManager::TimevalSubtract(timeval * result, timeval * x, timeval * y)
{
	return mNetworkManager->TimevalSubtract(result, x, y);
}

bool InputManager::AddNewDevice(InputDevice deviceType, IInputDevice ** device)
{
	bool success = false;
	success = mDeviceManager->AddNewDevice(mHandleWindow, deviceType, device);
	if (mDeviceManager->AnyRegisteredDevices())
		mInitialized = true;
	return success;
}

bool InputManager::RemoveRegisteredDevice(IInputDevice * device, bool *lastDevice, Input::InputDevice *ofType)
{
	bool success = false;
	success = mDeviceManager->RemoveRegisteredDevice(device, lastDevice, ofType);
	if (!mDeviceManager->AnyRegisteredDevices())
		mInitialized = false;

	return success;
}

void InputManager::GetAllKnownDevices()
{
	mDeviceManager->GetAllKnownDevices();
}

void InputManager::PrintAllRegisteredDevices()
{
	mDeviceManager->PrintAllRegisteredDevices();
}

int InputManager::HandleInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (mInitialized)
	{
		if (message == WM_INPUT)
		{
			//SetHandleWindow(hWnd);
			return mDeviceManager->HandleRawInput(mHandleWindow, message, wParam, lParam);
		}
	}

	return -1;
}

void InputManager::UpdatePressedState()
{
	mDeviceManager->UpdatePressedState();
}

bool InputManager::KeyDown(InputKey button)
{
	return mDeviceManager->KeyDown(button);
}

bool InputManager::KeyDown(IInputDevice * device, InputKey button)
{
	return mDeviceManager->KeyDown(device, button);
}

bool InputManager::KeyPressed(InputKey button)
{
	return mDeviceManager->KeyPressed(button);
}

bool InputManager::KeyPressed(IInputDevice * device, InputKey button)
{
	return mDeviceManager->KeyPressed(device, button);
}

bool InputManager::KeyUp(InputKey button)
{
	return mDeviceManager->KeyUp(button);
}

bool InputManager::KeyUp(IInputDevice * device, InputKey button)
{
	return mDeviceManager->KeyUp(device, button);
}

bool InputManager::GetXYPosWheel(int * x, int * y, short *deltaX, short *deltaY, int * w, bool *insideX, bool *insideY)
{
	return mDeviceManager->GetXYPosWheel(x, y, deltaX, deltaY, w, insideX, insideY);
}

bool InputManager::GetXYPosWheel(IInputDevice * device, int * x, int * y, short *deltaX, short *deltaY, int * w, bool *insideX, bool *insideY)
{
	return mDeviceManager->GetXYPosWheel(device, x, y, deltaX, deltaY, w, insideX, insideY);
}

bool InputManager::GetRange(float * up, float * right)
{
	return mDeviceManager->GetRange(up, right);
}

bool InputManager::GetRange(IInputDevice * device, float * up, float * right)
{
	return mDeviceManager->GetRange(device, up, right);
}
