#pragma once

#include "DeviceManager.h"
#include "InputMapper.h"
#include "ContextManager.h"
#include "NetworkManager.h"


/*
Combine the three layers together to get the raw input,
mapping them to contexts and execute them in order.
*/

class InputManager
{
public:
	static InputManager& instance()
	{
		static InputManager _instance;
		return _instance;
	}
	~InputManager();

	void Init(HWND hWnd);
	void Destroy();
	void SetHandleWindow(HWND hWnd);
	void GetHandleWindow(HWND *hWnd);
	void SetWindowSize(UINT width, UINT height);
	void GetWindowSize(UINT *width, UINT *height);

	bool AddPlayer(UINT8 playerID, InputDevice deviceType);
	bool AddDevice(UINT8 playerID, InputDevice deviceType);
	bool RemoveDevice(UINT8 playerID, InputDevice deviceType);
	bool RemovePlayer(UINT8 playerID);
	bool RemoveDevicesOfType(InputDevice inputType);

	void FlushAllDevices();
	bool UseAllDevices();

	// additional

	// ----------------- Methods of NetworkManager -------------------

	int InitNetwork();
	int ResetNetworkState();
	int AcceptNetworkConnection(int numOfClients, int **clientIDs, int *numIDsInClientIDs);
	int PrepareNetworkForGame();


	/*template<class ClassObject>
	int RunNetworkGame(ClassObject *thisClass, void(ClassObject::*funcToGetWF)(float*, float*));*/

	template<class ClassObject>
	int RunNetworkGame(ClassObject *thisClass, void(ClassObject::*funcToGetWF)(float*, float*))
	{
		return mNetworkManager->RunGame<ClassObject>(thisClass, funcToGetWF);
	}

	int RunNetworkGame();

	int EndNetworkGame();

	// -------------- End of Methods of NetworkManager ---------------


	// ------------------ Methods of DeviceManager --------------------

	// InputKey button was pressed this frame, but not last frame.
	bool KeyDown(UINT8 playerID, InputKey button);
	// InputKey button is kept pressed this frame, was also registered in last frame.
	bool KeyPressed(UINT8 playerID, InputKey button);
	// InputKey button was just released this frame, but pressed in last frame.
	bool KeyUp(UINT8 playerID, InputKey button);
	int GetRawValue(UINT8 playerID);
	// * Returns the values for the x and y position and delta of the mouse.
	// * The w parameter will be written with the delta of the mouse wheel change.
	// * Parameters can be NULL. If no value has been written back, the function returns false.
	bool GetXYPosWheel(UINT8 playerID, int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY);
	// Returns the axis values in the range -1 to 1
	bool GetRange(UINT8 playerID, float* up, float* right);
	void SetRange(UINT8 deviceID, float up, float right);

	void SetKey(UINT8 deviceID, char keyValues);

	int HandleInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void UpdatePressedState();

	void GetAllKnownDevices();
	void PrintAllRegisteredDevices();

	// --------------- End of Methods of DeviceManager ----------------

	// ------------------ Internal Methods --------------------

	// NetworkManager
	int SendData(float up, float right);

	int gettimeofday(struct timeval * tp, struct timezone * tzp);
	/* Subtract the �struct timeval� values X and Y,
	storing the result in RESULT.
	Return 1 if the difference is negative, otherwise 0. */
	int TimevalSubtract(struct timeval *result, struct timeval *x, struct timeval *y);

	// DeviceManager
	bool AddNewDevice(InputDevice deviceType, IInputDevice** device);
	// * Removes the IInputDevice device, if it has been registered before.
	// * If the device was the last device of its kind, lastDevice will be true and the InputDevice saved in ofType.
	// * Returns true, if the device could be removed.
	bool RemoveRegisteredDevice(IInputDevice* device, bool *lastDevice, Input::InputDevice *ofType);

	/*void GetAllKnownDevices();
	void PrintAllRegisteredDevices();*/

	//bool GetInput(HWND hWnd, PRAWINPUTHEADER pRIH);

	/*int HandleInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void UpdatePressedState();*/

	bool KeyDown(InputKey button);
	bool KeyDown(IInputDevice * device, InputKey button);
	bool KeyPressed(InputKey button);
	bool KeyPressed(IInputDevice * device, InputKey button);
	bool KeyUp(InputKey button);
	bool KeyUp(IInputDevice * device, InputKey button);

	bool GetXYPosWheel(int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY);
	bool GetXYPosWheel(IInputDevice * device, int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY);

	bool GetRange(float* up, float* right);
	bool GetRange(IInputDevice* device, float* up, float* right);

	// --------------- End of Internal Methods -----------------
	void HandleError(std::string errorMessage, DWORD error)
	{
		LPSTR messageBuffer = nullptr;
		size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

		std::string message(messageBuffer, size);

		//Free the buffer.
		LocalFree(messageBuffer);

		Logger::instance().Msg(LOG_LAYER::LOG_ERROR, errorMessage + "\t" + std::to_string(error) + " " + message);
	};

private:
	InputManager();
	InputManager(const InputManager&);
	InputManager &operator = (const InputManager &);

	DeviceManager* mDeviceManager;
	InputMapper* mInputMapper;
	ContextManager* mContextManager;
	NetworkManager* mNetworkManager;

	HWND mHandleWindow;
	UINT mWidth, mHeight;

	

	std::unordered_multimap<UINT8, IInputDevice*> mPlayerToDevice;

	float ClampFloat(float n, float lower, float upper) {
		return std::fmaxf(lower, std::fminf(n, upper));
	}

	template<typename type>
	type ClampFloat(type n, type lower, type upper) {
		return max(lower, min(n, upper));
	}

	bool mInitialized = false;
};

