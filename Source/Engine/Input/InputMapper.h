#pragma once

/*
From the DeviceManager, take the known input IDs and map them to
known actions, states or ranges.
*/

class InputMapper
{
public:
	// Singleton approach
	/*static InputMapper& instance()
	{
		static InputMapper _instance;
		return _instance;
	}
	~InputMapper();*/

	// No singleton approach
	InputMapper();
	~InputMapper();

	void Init();

private:
	// Singleton approach
	/*InputMapper();
	InputMapper(const InputMapper&);
	InputMapper &operator = (const InputMapper &);*/


};

