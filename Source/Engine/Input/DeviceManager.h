#pragma once

#include "Devices/IInputDevice.h"
#include "Devices/Keyboard.h"
#include "Devices/Mouse.h"
#include "Devices/Smartphone.h"

/*
Get the raw input of a device registered on the active window
and change it to one of the known input IDs.
*/

// Forward declarations and namespaces
class InputManager;
using namespace Input;

class DeviceManager
{
public:
	// Singleton approach
	/*static DeviceManager& instance()
	{
		static DeviceManager _instance;
		return _instance;
	}
	~DeviceManager();*/

	// No singleton approach
	DeviceManager();
	~DeviceManager();

	bool Init();

	bool AddNewDevice(HWND hWnd, InputDevice deviceType, IInputDevice** device);
	bool RegisterNewDevice(HWND hWnd, InputDevice deviceType, IInputDevice* device);
	bool RemoveRegisteredDevice(IInputDevice* device, bool *lastDevice, Input::InputDevice *ofType);
	void FlushAllDevices();
	bool UseAllDevices(HWND hWnd);
	bool RemoveDevicesOfType(InputDevice inputType, IInputDevice ***deviceList, int *numOfDevicesToRemove);

	void GetAllKnownDevices();
	bool PrintAllRegisteredTLC(PRAWINPUTDEVICE* pRID, UINT* numDevices);
	void PrintAllRegisteredDevices();

	bool GetInput(HWND hWnd, PRAWINPUTHEADER *pRIH);

	int HandleRawInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam/*, IInputDevice* &device*/);
	void UpdatePressedState();

	bool KeyDown(InputKey button);
	bool KeyDown(IInputDevice * device, InputKey button);
	bool KeyPressed(InputKey button);
	bool KeyPressed(IInputDevice * device, InputKey button);
	bool KeyUp(InputKey button);
	bool KeyUp(IInputDevice * device, InputKey button);
	int GetRawValue(IInputDevice * device);

	bool GetXYPosWheel(int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY);
	bool GetXYPosWheel(IInputDevice * device, int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY);

	bool GetRange(float* up, float* right);
	bool GetRange(IInputDevice* device, float* up, float* right);

	bool AnyRegisteredDevices()
	{
		return !mRegisteredDevices.empty();
	}

private:
	// Singleton approach
	/*DeviceManager();
	DeviceManager(const DeviceManager&);
	DeviceManager &operator = (const DeviceManager &);*/

	std::unordered_multimap<InputDevice, IInputDevice*> mRegisteredDevices;

	enum class InputState
	{
		NONE,
		RAW,
		PLAYER,
	};

	InputState inputDeviceState = InputState::NONE;

	//void HandleError(std::string errorMessage, DWORD error)
	//{
	//	LPSTR messageBuffer = nullptr;
	//	size_t size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
	//		NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);

	//	std::string message(messageBuffer, size);

	//	//Free the buffer.
	//	LocalFree(messageBuffer);

	//	Logger::instance().Msg(LOG_LAYER::LOG_ERROR, errorMessage + "\t" + std::to_string(error) + " " + message);
	//};
	
};

