#pragma once

#include "IInputDevice.h"

class InputManager;

class Mouse : public IInputDevice
{
public:
	Mouse();
	~Mouse();

	void Init(HANDLE uniqueID) override;

	int HandleRawInput(PRAWINPUT pRI) override;
	//int HandleRawInput(PRAWINPUT pRI, UINT width, UINT height) override;

	bool GetXYPosWheel(int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY) override;

	bool GetRange(float* up, float* right)
	{
		return false;
	}

	void UpdatePressedState() override;

	struct MouseDevice
	{
		DeviceAttributes* mAttr;
		int posX;
		int posY;
		short deltaX;
		short deltaY;
		signed short wheel;
		POINT posRel;
		POINT posAbs;
		POINT posAbsOld;
		struct WindowBool
		{
			bool x;
			bool y;
		} inWindow;
	};
	MouseDevice mMouseAttributes;
	
};

