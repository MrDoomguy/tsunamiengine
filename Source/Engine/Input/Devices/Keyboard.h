#pragma once

#include "IInputDevice.h"

class Keyboard : public IInputDevice
{
public:
	Keyboard();
	~Keyboard();

	void Init(HANDLE uniqueID) override;
	//void MapRawToVirtual(int raw, Input::InputKey vir) override;

	//void HandleRawInput(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	int HandleRawInput(PRAWINPUT pRI) override;
	//int HandleRawInput(PRAWINPUT pRI, UINT width, UINT height) override { return false; };

	bool GetXYPosWheel(int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY)
	{
		return false;
	}

	bool GetRange(float* up, float* right)
	{
		return false;
	}
	
	/*bool KeyDown(Input::InputKey button);
	bool KeyPressed(Input::InputKey button);
	bool KeyUp(Input::InputKey button);
	void UpdatePressedState();
	HANDLE GetDeviceID();*/
};

