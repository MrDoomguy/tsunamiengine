#include "Mouse.h"
#include "../InputManager.h"

Mouse::Mouse()
{
}


Mouse::~Mouse()
{
}

void Mouse::Init(HANDLE uniqueID)
{
	mAttributes.pDeviceID = uniqueID;

	// MapRawToVirtual
	MapRawToVirtual(MK_LBUTTON, Input::InputKey::MOUSE_LEFT);
	MapRawToVirtual(MK_RBUTTON, Input::InputKey::MOUSE_RIGHT);
	MapRawToVirtual(MK_MBUTTON, Input::InputKey::MOUSE_MIDDLE);
	MapRawToVirtual(MK_XBUTTON1, Input::InputKey::MOUSE_X1);
	MapRawToVirtual(MK_XBUTTON2, Input::InputKey::MOUSE_X2);

	if (mAttributes.pPreviouslyPressedState.empty())
	{
		for (auto it = mAttributes.pMapping.begin(); it != mAttributes.pMapping.end(); it++)
		{
			mAttributes.pPreviouslyPressedState[it->first] = false;
			mAttributes.pCurrentlyPressedState[it->first] = false;
		}
	}

	mMouseAttributes.mAttr = &mAttributes;
	mMouseAttributes.posX = 0;
	mMouseAttributes.posY = 0;
	mMouseAttributes.wheel = 0;
}

int Mouse::HandleRawInput(PRAWINPUT pRI)
{
	RAWMOUSE & mouse = pRI->data.mouse;
	if ((mouse.usFlags & MOUSE_ATTRIBUTES_CHANGED) == MOUSE_ATTRIBUTES_CHANGED)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "MOUSE_ATTRIBUTES_CHANGED!");
	}

	if ((mouse.usFlags & MOUSE_MOVE_ABSOLUTE) == MOUSE_MOVE_ABSOLUTE) 
	{
		bool virtualDesktop = (mouse.usFlags & MOUSE_VIRTUAL_DESKTOP) == MOUSE_VIRTUAL_DESKTOP;
		int width = GetSystemMetrics(
			virtualDesktop ? SM_CXVIRTUALSCREEN : SM_CXSCREEN
		);
		int height = GetSystemMetrics(
			virtualDesktop ? SM_CYVIRTUALSCREEN : SM_CYSCREEN
		);
		
		mMouseAttributes.posX = (mouse.lLastX / USHRT_MAX) * width; // float(USHRT_MAX) == 65535.0f
		mMouseAttributes.posY = (mouse.lLastY / USHRT_MAX) * height;
	}
	else 
	{
		mMouseAttributes.posX += mouse.lLastX;
		mMouseAttributes.posY += mouse.lLastY;
	}

	UINT w, h;
	InputManager::instance().GetWindowSize(&w, &h);
	//mMouseAttributes.posX = ClampValue<int>(mMouseAttributes.posX, 0, w);
	//mMouseAttributes.posY = ClampValue<int>(mMouseAttributes.posY, 0, h);*/

	if (GetCursorPos(&mMouseAttributes.posAbs))
	{
		mMouseAttributes.posRel.x = mMouseAttributes.posAbs.x;
		mMouseAttributes.posRel.y = mMouseAttributes.posAbs.y;
		mMouseAttributes.inWindow.x = mMouseAttributes.inWindow.y = true;
		HWND hWnd;
		InputManager::instance().GetHandleWindow(&hWnd);
		ScreenToClient(hWnd, &mMouseAttributes.posRel);

		mMouseAttributes.deltaX = (short)(mMouseAttributes.posAbs.x - mMouseAttributes.posAbsOld.x);
		mMouseAttributes.deltaY = (short)(mMouseAttributes.posAbs.y - mMouseAttributes.posAbsOld.y);
		//mMouseAttributes.posAbsOld = mMouseAttributes.posAbs;

		if (mMouseAttributes.posRel.x < 0) 
		{
			mMouseAttributes.inWindow.x = false;
		} 
		else if (mMouseAttributes.posRel.x > (int)w)
		{
			mMouseAttributes.posRel.x -= w;
			mMouseAttributes.inWindow.x = false;
		}

		if (mMouseAttributes.posRel.y < 0)
		{
			mMouseAttributes.inWindow.y = false;
		}
		else if (mMouseAttributes.posRel.y > (int)h)
		{
			mMouseAttributes.posRel.y -= h;
			mMouseAttributes.inWindow.y = false;
		}
	}
	
	if ((mouse.usButtonFlags & RI_MOUSE_WHEEL) == RI_MOUSE_WHEEL)
	{
		mMouseAttributes.wheel = mouse.usButtonData;
	}
	else
	{
		mMouseAttributes.wheel = 0;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN) == RI_MOUSE_BUTTON_1_DOWN)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_LEFT] = true;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP) == RI_MOUSE_BUTTON_1_UP)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_LEFT] = false;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN) == RI_MOUSE_BUTTON_2_DOWN)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_RIGHT] = true;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP) == RI_MOUSE_BUTTON_2_UP)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_RIGHT] = false;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN) == RI_MOUSE_BUTTON_3_DOWN)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_MIDDLE] = true;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP) == RI_MOUSE_BUTTON_3_UP)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_MIDDLE] = false;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_BUTTON_4_DOWN) == RI_MOUSE_BUTTON_4_DOWN)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_X1] = true;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_BUTTON_4_UP) == RI_MOUSE_BUTTON_4_UP)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_X1] = false;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_BUTTON_5_DOWN) == RI_MOUSE_BUTTON_5_DOWN)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_X2] = true;
	}

	if ((mouse.usButtonFlags & RI_MOUSE_BUTTON_5_UP) == RI_MOUSE_BUTTON_5_UP)
	{
		mMouseAttributes.mAttr->pCurrentlyPressedState[Input::InputKey::MOUSE_X2] = false;
	}

	return true;
}

bool Mouse::GetXYPosWheel(int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY)
{
	bool processed = false;

	if ((x != nullptr) /*&& (*x != mMouseAttributes.posRel.x)*/)
	{
		//*x = mMouseAttributes.posX;
		*x = mMouseAttributes.posRel.x;
		processed = true;
	}

	if ((y != nullptr) /*&& (*y != mMouseAttributes.posRel.y)*/)
	{
		//*y = mMouseAttributes.posY;
		*y = mMouseAttributes.posRel.y;
		processed = true;
	}

	if ((deltaX != nullptr))
	{
		*deltaX = mMouseAttributes.deltaX;
		processed = true;
	}

	if ((deltaY != nullptr))
	{
		*deltaY = mMouseAttributes.deltaY;
		processed = true;
	}

	if ((w != nullptr) /*&& (*w != mMouseAttributes.wheel)*/)
	{
		*w = mMouseAttributes.wheel;
		processed = true;
	}

	if ((insideX != nullptr) /*&& (*insideX != mMouseAttributes.inWindow.x)*/)
	{
		*insideX = mMouseAttributes.inWindow.x;
		processed = true;
	}

	if ((insideY != nullptr) /*&& (*insideY != mMouseAttributes.inWindow.y)*/)
	{
		*insideY = mMouseAttributes.inWindow.y;
		processed = true;
	}

	return processed;
}

void Mouse::UpdatePressedState()
{
	IInputDevice::UpdatePressedState();
	mMouseAttributes.posAbsOld = mMouseAttributes.posAbs;

	mMouseAttributes.posX = mMouseAttributes.posY = mMouseAttributes.deltaX = mMouseAttributes.deltaY = mMouseAttributes.wheel = 0;
	//mMouseAttributes.posRel.x = mMouseAttributes.posRel.y = mMouseAttributes.posAbs.x = mMouseAttributes.posAbs.y = 0;
}
