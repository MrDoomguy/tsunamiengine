#include "Smartphone.h"



Smartphone::Smartphone()
{
}


Smartphone::~Smartphone()
{
}

void Smartphone::Init(HANDLE uniqueID)
{
	// Be sure to know what you are doing here. This is no handle for the system to access a connected device!
	mAttributes.pDeviceID = uniqueID;

	MapRawToVirtual(1, Input::InputKey::ARROW_LEFT);
	MapRawToVirtual(2, Input::InputKey::ARROW_RIGHT);
	MapRawToVirtual(VK_RETURN, Input::InputKey::KEY_RETURN);

	if (mAttributes.pPreviouslyPressedState.empty())
	{
		for (auto it = mAttributes.pMapping.begin(); it != mAttributes.pMapping.end(); it++)
		{
			mAttributes.pPreviouslyPressedState[it->first] = false;
			mAttributes.pCurrentlyPressedState[it->first] = false;
		}
	}
}

void Smartphone::SetRange(float up, float right)
{
	mUp = up;
	mRight = right;
}

bool Smartphone::GetRange(float * up, float * right)
{
	bool processed = false;
	if (up != nullptr)
	{
		*up = mUp;
		processed = true;
	}

	if (right != nullptr)
	{
		*right = mRight;
		processed = true;
	}

	return processed;
}

void Smartphone::SetKey(char keyValues)
{
	if (keyValues == 0)
	{
		/*mAttributes.pCurrentlyPressedState[Input::InputKey::ARROW_LEFT] = false;
		mAttributes.pCurrentlyPressedState[Input::InputKey::ARROW_RIGHT] = false;*/
		mAttributes.pCurrentlyPressedState[Input::InputKey::KEY_RETURN] = false;
		return;
	}

	if ((keyValues & 1) == 1)
	{
		mAttributes.pCurrentlyPressedState[Input::InputKey::KEY_RETURN] = true;
		//mAttributes.pCurrentlyPressedState[Input::InputKey::ARROW_LEFT] = true;
	}

	if ((keyValues & 2) == 2)
	{
		mAttributes.pCurrentlyPressedState[Input::InputKey::KEY_RETURN] = true;
		//mAttributes.pCurrentlyPressedState[Input::InputKey::ARROW_RIGHT] = true;
	}
}
