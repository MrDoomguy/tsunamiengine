#pragma once

#include "../../General/TsunamiStd.h"
//#include <Windows.h>
#include <map>
//#include <unordered_map>
//#include "../../General/Timer.h"
//#include "../../General/Logger.h"


namespace Input
{
	enum class InputDevice
	{
		KEYBOARD = 0,
		MOUSE,
		SMARTPHONE,
		NUM_SIZE,
	};

	static std::map<InputDevice, std::string> InputDeviceString =
	{
		{ InputDevice::KEYBOARD, "KEYBOARD" },
		{ InputDevice::MOUSE, "MOUSE" },
		{ InputDevice::SMARTPHONE, "SMARTPHONE" },
	};

	static std::map<InputDevice, std::pair<int, int>> DeviceRawUsagePages =
	{
		{ InputDevice::KEYBOARD, { 0x01, 0x06 } },
		{ InputDevice::MOUSE, { 0x01, 0x02 } },
		{ InputDevice::SMARTPHONE, { 0x00, 0x00 } }, /* Don't use it, it won't register correctly. */
	};

	enum class InputKey
	{
		// Keyboard
		ARROW_UP,
		ARROW_DOWN,
		ARROW_LEFT,
		ARROW_RIGHT,
		KEY_PLUS,
		KEY_MINUS,
		KEY_0,
		KEY_1,
		KEY_2,
		KEY_3,
		KEY_4,
		KEY_5,
		KEY_6,
		KEY_7,
		KEY_8,
		KEY_9,
		KEY_Q,
		KEY_W,
		KEY_E,
		KEY_R,
		KEY_T,
		KEY_Z,
		KEY_U,
		KEY_I,
		KEY_O,
		KEY_P,
		KEY_A,
		KEY_S,
		KEY_D,
		KEY_F,
		KEY_G,
		KEY_H,
		KEY_J,
		KEY_K,
		KEY_L,
		KEY_Y,
		KEY_X,
		KEY_C,
		KEY_V,
		KEY_B,
		KEY_N,
		KEY_M,
		KEY_RCTRL,
		KEY_RSHIFT,
		KEY_ESC,
		KEY_DEL,
		KEY_BS,
		KEY_RETURN,
		KEY_TAB,

		// Mouse
		MOUSE_LEFT,
		MOUSE_RIGHT,
		MOUSE_MIDDLE,
		MOUSE_X1,
		MOUSE_X2,

		// Smartphone
		//RANGE_X,
		//RANGE_Y,
	};
}




class IInputDevice
{
public:
	virtual ~IInputDevice() {};

	virtual void Init(HANDLE uniqueID) = 0;

	virtual void MapRawToVirtual(int raw, Input::InputKey vir)
	{
		int oldRaw = mAttributes.pMapping[vir];
		for (auto it = mAttributes.pMapping.begin(); it != mAttributes.pMapping.end(); it++)
		{
			if (it->second == raw && it->first != vir)
			{
				it->second = NULL;
				mAttributes.pPreviouslyPressedState[it->first] = false;
				mAttributes.pCurrentlyPressedState[it->first] = false;
			}
		}
		mAttributes.pMapping[vir] = raw;
	}

	virtual int HandleRawInput(PRAWINPUT pRI) = 0;
	//virtual int HandleRawInput(PRAWINPUT pRI, UINT width, UINT height) = 0;

	// InputKey button was pressed this frame, but not last frame.
	virtual bool KeyDown(Input::InputKey button)
	{
		if (this->FindMapping<Input::InputKey, int>(mAttributes.pMapping, button))
		{
			if (mAttributes.pCurrentlyPressedState[button] && !mAttributes.pPreviouslyPressedState[button])
			{
				return true;
			}
		}

		return false;
	}

	// InputKey button is kept pressed this frame, was also registered in last frame.
	virtual bool KeyPressed(Input::InputKey button)
	{
		if (this->FindMapping<Input::InputKey, int>(mAttributes.pMapping, button))
		{
			if (mAttributes.pCurrentlyPressedState[button] && mAttributes.pPreviouslyPressedState[button])
			{
				return true;
			}
		}

		return false;
	}

	// InputKey button was just released this frame, but pressed in last frame.
	virtual bool KeyUp(Input::InputKey button)
	{
		if (this->FindMapping<Input::InputKey, int>(mAttributes.pMapping, button))
		{
			if (!mAttributes.pCurrentlyPressedState[button] && mAttributes.pPreviouslyPressedState[button])
			{
				return true;
			}
		}

		return false;
	}

	// Only for the end of current frame!
	// Update the state list with the current pressed states of mapped keys for next frame.
	virtual void UpdatePressedState()
	{
		mRawValue = 0x00;
		for (auto it = mAttributes.pPreviouslyPressedState.begin(); it != mAttributes.pPreviouslyPressedState.end(); it++)
		{
			it->second = mAttributes.pCurrentlyPressedState[it->first];
			//mAttributes.pCurrentlyPressedState[it->first] = false;
		}
	}

	virtual int GetRawValue()
	{
		return mRawValue;
	}

	virtual HANDLE GetDeviceID()
	{
		return mAttributes.pDeviceID;
	}

	virtual bool GetXYPosWheel(int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY) = 0;

	virtual bool GetRange(float* up, float* right) = 0;
	virtual void SetRange(float up, float right) {};

	virtual void SetKey(char keyValues) {};

protected:
	struct DeviceAttributes
	{
		HANDLE pDeviceID;
		std::map<Input::InputKey, int> pMapping;
		std::map<Input::InputKey, bool> pCurrentlyPressedState;
		std::map<Input::InputKey, bool> pPreviouslyPressedState;
	};
	DeviceAttributes mAttributes;
	int mRawValue = 0x00;

	//[deprecated("Old version! Use with template instead.")]

	bool FindMapping(const Input::InputKey ik) const
	{
		if (mAttributes.pMapping.find(ik) != mAttributes.pMapping.end())
			return true;

		return false;
	}

	template<typename Key, typename Value>
	bool FindMapping(const std::map<Key, Value> &map, Key ik) const
	{
		if (map.find(ik) != map.end())
			return true;

		return false;
	}

	template<typename type>
	type ClampValue(type n, type lower, type upper) {
		return max(lower, min(n, upper));
	}
};

//IInputDevice::~IInputDevice(){}
