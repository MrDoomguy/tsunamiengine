#pragma once

#include "IInputDevice.h"

class Smartphone : public IInputDevice
{
public:
	Smartphone();
	~Smartphone();

	void Init(HANDLE uniqueID) override;

	int HandleRawInput(PRAWINPUT pRI)
	{
		Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "This should not have happened. I'm a Smartphone, I have my network to care for!");
		return 0;
	}

	bool GetXYPosWheel(int *x, int *y, short *deltaX, short *deltaY, int *w, bool *insideX, bool *insideY)
	{
		return false;
	}

	void SetRange(float up, float right) override;
	bool GetRange(float* up, float* right);

	void SetKey(char keyValues) override;

private:
	float mUp = 0;
	float mRight = 0;
};

