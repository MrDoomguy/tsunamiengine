#include "Keyboard.h"



Keyboard::Keyboard()
{
}


Keyboard::~Keyboard()
{
}

void Keyboard::Init(HANDLE uniqueID)
{
	mAttributes.pDeviceID = uniqueID;

	MapRawToVirtual(VK_UP, Input::InputKey::ARROW_UP);
	MapRawToVirtual(VK_DOWN, Input::InputKey::ARROW_DOWN);
	MapRawToVirtual(VK_LEFT, Input::InputKey::ARROW_LEFT);
	MapRawToVirtual(VK_RIGHT, Input::InputKey::ARROW_RIGHT);
	MapRawToVirtual(VK_OEM_PLUS, Input::InputKey::KEY_PLUS);
	MapRawToVirtual(VK_OEM_MINUS, Input::InputKey::KEY_MINUS);
	MapRawToVirtual(0x30, Input::InputKey::KEY_0);
	MapRawToVirtual(0x31, Input::InputKey::KEY_1);
	MapRawToVirtual(0x32, Input::InputKey::KEY_2);
	MapRawToVirtual(0x33, Input::InputKey::KEY_3);
	MapRawToVirtual(0x34, Input::InputKey::KEY_4);
	MapRawToVirtual(0x35, Input::InputKey::KEY_5);
	MapRawToVirtual(0x36, Input::InputKey::KEY_6);
	MapRawToVirtual(0x37, Input::InputKey::KEY_7);
	MapRawToVirtual(0x38, Input::InputKey::KEY_8);
	MapRawToVirtual(0x39, Input::InputKey::KEY_9);
	MapRawToVirtual(0x41, Input::InputKey::KEY_A);
	MapRawToVirtual(0x42, Input::InputKey::KEY_B);
	MapRawToVirtual(0x43, Input::InputKey::KEY_C);
	MapRawToVirtual(0x44, Input::InputKey::KEY_D);
	MapRawToVirtual(0x45, Input::InputKey::KEY_E);
	MapRawToVirtual(0x46, Input::InputKey::KEY_F);
	MapRawToVirtual(0x47, Input::InputKey::KEY_G);
	MapRawToVirtual(0x48, Input::InputKey::KEY_H);
	MapRawToVirtual(0x49, Input::InputKey::KEY_I);
	MapRawToVirtual(0x4A, Input::InputKey::KEY_J);
	MapRawToVirtual(0x4B, Input::InputKey::KEY_K);
	MapRawToVirtual(0x4C, Input::InputKey::KEY_L);
	MapRawToVirtual(0x4D, Input::InputKey::KEY_M);
	MapRawToVirtual(0x4E, Input::InputKey::KEY_N);
	MapRawToVirtual(0x4F, Input::InputKey::KEY_O);
	MapRawToVirtual(0x50, Input::InputKey::KEY_P);
	MapRawToVirtual(0x51, Input::InputKey::KEY_Q);
	MapRawToVirtual(0x52, Input::InputKey::KEY_R);
	MapRawToVirtual(0x53, Input::InputKey::KEY_S);
	MapRawToVirtual(0x54, Input::InputKey::KEY_T);
	MapRawToVirtual(0x55, Input::InputKey::KEY_U);
	MapRawToVirtual(0x56, Input::InputKey::KEY_V);
	MapRawToVirtual(0x57, Input::InputKey::KEY_W);
	MapRawToVirtual(0x58, Input::InputKey::KEY_X);
	MapRawToVirtual(0x59, Input::InputKey::KEY_Y);
	MapRawToVirtual(0x5A, Input::InputKey::KEY_Z);
	MapRawToVirtual(VK_RCONTROL, Input::InputKey::KEY_RCTRL);
	MapRawToVirtual(VK_RSHIFT, Input::InputKey::KEY_RSHIFT);
	MapRawToVirtual(VK_ESCAPE, Input::InputKey::KEY_ESC);
	MapRawToVirtual(VK_DELETE, Input::InputKey::KEY_DEL);
	MapRawToVirtual(VK_BACK, Input::InputKey::KEY_BS);
	MapRawToVirtual(VK_RETURN, Input::InputKey::KEY_RETURN);
	MapRawToVirtual(VK_TAB, Input::InputKey::KEY_TAB);

	if (mAttributes.pPreviouslyPressedState.empty())
	{
		for (auto it = mAttributes.pMapping.begin(); it != mAttributes.pMapping.end(); it++)
		{
			mAttributes.pPreviouslyPressedState[it->first] = false;
			mAttributes.pCurrentlyPressedState[it->first] = false;
		}
	}
}

int Keyboard::HandleRawInput(PRAWINPUT pRI)
{
	/*
	begin source: https://blog.molecular-matters.com/2011/09/05/properly-handling-keyboard-input/
	*/

	UINT virtualKey = pRI->data.keyboard.VKey;
	UINT scanCode = pRI->data.keyboard.MakeCode;
	UINT flags = pRI->data.keyboard.Flags;

	if (virtualKey == 255)
	{
		// discard "fake keys" which are part of an escaped sequence
		virtualKey = MapVirtualKey(scanCode, MAPVK_VSC_TO_VK);
		return false;
	}
	else if (virtualKey == VK_SHIFT)
	{
		// correct left-hand / right-hand SHIFT
		virtualKey = MapVirtualKey(scanCode, MAPVK_VSC_TO_VK_EX);
	}
	else if (virtualKey == VK_NUMLOCK)
	{
		// correct PAUSE/BREAK and NUM LOCK silliness, and set the extended bit
		scanCode = (MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC) | 0x100);
	}

	// e0 and e1 are escape sequences used for certain special keys, such as PRINT and PAUSE/BREAK.
	// see http://www.win.tue.nl/~aeb/linux/kbd/scancodes-1.html
	const bool isE0 = ((flags & RI_KEY_E0) != 0);
	const bool isE1 = ((flags & RI_KEY_E1) != 0);

	if (isE1)
	{
		// for escaped sequences, turn the virtual key into the correct scan code using MapVirtualKey.
		// however, MapVirtualKey is unable to map VK_PAUSE (this is a known bug), hence we map that by hand.
		if (virtualKey == VK_PAUSE)
			scanCode = 0x45;
		else
			scanCode = MapVirtualKey(virtualKey, MAPVK_VK_TO_VSC);
	}

	switch (virtualKey)
	{
		// right-hand CONTROL and ALT have their e0 bit set
	case VK_CONTROL:
		if (isE0)
			virtualKey = VK_RCONTROL;
		else
			virtualKey = VK_LCONTROL;
		break;

	case VK_MENU:
		if (isE0)
			virtualKey = VK_RMENU;
		else
			virtualKey = VK_LMENU;
		break;

		// NUMPAD ENTER has its e0 bit set
	case VK_RETURN:
		if (isE0)
			virtualKey = VK_RETURN;
		break;

		// the standard INSERT, DELETE, HOME, END, PRIOR and NEXT keys will always have their e0 bit set, but the
		// corresponding keys on the NUMPAD will not.
	case VK_INSERT:
		if (!isE0)
			virtualKey = VK_NUMPAD0;
		break;

	case VK_DELETE:
		if (!isE0)
			virtualKey = VK_DECIMAL;
		break;

	case VK_HOME:
		if (!isE0)
			virtualKey = VK_NUMPAD7;
		break;

	case VK_END:
		if (!isE0)
			virtualKey = VK_NUMPAD1;
		break;

	case VK_PRIOR:
		if (!isE0)
			virtualKey = VK_NUMPAD9;
		break;

	case VK_NEXT:
		if (!isE0)
			virtualKey = VK_NUMPAD3;
		break;

		// the standard arrow keys will always have their e0 bit set, but the
		// corresponding keys on the NUMPAD will not.
	case VK_LEFT:
		if (!isE0)
			virtualKey = VK_NUMPAD4;
		break;

	case VK_RIGHT:
		if (!isE0)
			virtualKey = VK_NUMPAD6;
		break;

	case VK_UP:
		if (!isE0)
			virtualKey = VK_NUMPAD8;
		break;

	case VK_DOWN:
		if (!isE0)
			virtualKey = VK_NUMPAD2;
		break;

		// NUMPAD 5 doesn't have its e0 bit set
	case VK_CLEAR:
		if (!isE0)
			virtualKey = VK_NUMPAD5;
		break;
	}

	/*
	end source
	*/

	for (auto& k : mAttributes.pMapping)
	{
		//UINT virtualKey = MapVirtualKey(pRI->data.keyboard.MakeCode, MAPVK_VSC_TO_VK);
		if (k.second == virtualKey)
		{
			if ((pRI->data.keyboard.Flags & RI_KEY_BREAK) == RI_KEY_BREAK)
			{
				mAttributes.pCurrentlyPressedState[k.first] = false;
			}
			else //if ((pRI->data.keyboard.Flags & RI_KEY_MAKE) == 0)
			{
				mAttributes.pCurrentlyPressedState[k.first] = true;
				mRawValue = virtualKey;
			}
			return true;
		}
	}
	return false;
}
