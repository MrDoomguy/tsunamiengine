#include "NetworkManager.h"

#include "InputManager.h"


NetworkManager::NetworkManager()
{
}


NetworkManager::~NetworkManager()
{
}

int NetworkManager::Init()
{
	if (!initialized)
	{
		ResetState();
		// Initialize Winsock
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != NO_ERROR) {
			printf("WSAStartup failed: %d\n", iResult);
			InputManager::instance().HandleError("Error:", iResult);
			return false; //EXIT_FAILURE;
		}
		printf("WSAStartup: %d\n", iResult);

		ServerSocket = socket(AF_INET, SOCK_DGRAM, 0);
		if (ServerSocket == INVALID_SOCKET)
		{
			iResult = WSAGetLastError();
			strerror_s(bufptr, BUFFERSIZE, iResult);
			fprintf(stderr, "server-socket() unsuccessful: %d\n%s\n", iResult, bufptr);
			InputManager::instance().HandleError("Error:", iResult);
			WSACleanup();
			return false;
		}
		if (DEBUG > 1)
			printf("server-socket() successful\n");


		int reuse = REUSEADDR;

		memset(&saddr, 0, sizeof(struct sockaddr_in));

		saddr.sin_family = AF_INET;
		saddr.sin_port = htons(PORT);
		saddr.sin_addr.s_addr = htonl(INADDR_ANY);

		iResult = bind(ServerSocket, (struct sockaddr*)&saddr, saddrlen);
		if (iResult == SOCKET_ERROR)
		{
			iResult = WSAGetLastError();
			if (setsockopt(ServerSocket, SOL_SOCKET, SO_REUSEADDR, (const char *)&reuse, sizeof(int)) < 0)			// Maybe before the bind
			{
				strerror_s(bufptr, BUFFERSIZE, iResult);
				fprintf(stderr, "server-bind() unsuccessful: %d\n%s\n", iResult, bufptr);
				InputManager::instance().HandleError("Error:", iResult);
				closesocket(ServerSocket);
				WSACleanup();
				return false;
			}
			if (DEBUG > 1)
				printf("server reusing address\n");
		}
		if (DEBUG > 0)
		{
			printf("server-bind() successful\n");
			printf("UDP server using IP %s and port %d\n", inet_ntop(AF_INET, &saddr.sin_addr, bufptr, BUFFERSIZE), PORT);
		}
		initialized = true;
	}
	return true; //EXIT_SUCCESS;
}

int NetworkManager::ResetState()
{
	if (buf != nullptr)
	{
		free(buf);
	}
	buf = (char *)malloc(BUFFERSIZE);
	bufptr = buf;
	buflen = BUFFERSIZE;
	clnum = 0;
	timeout = TIMEOUTCLIENTLOGIN;
	rcv = snd = 0;
	running = false;
	if (sendMethod != nullptr)
	{
		sendMethod->join();
		free(sendMethod);
	}

	if (recvMethod != nullptr)
	{
		recvMethod->join();
		free(recvMethod);
	}

	return true;
}

int NetworkManager::AcceptConnection(int numOfClients, int **clientIDs, int *numIDsInClientIDs)
{
	FD_ZERO(&readset);
	FD_SET(ServerSocket, &readset);
	maxfd = (int)ServerSocket + 1;
	timetocheck.tv_sec = (long)timeout;
	timetocheck.tv_usec = 0;
	int startingNumberOfClients = clnum;
	*numIDsInClientIDs = 0;
	int *tmpIDs = (int *)malloc((MAXCLIENTS - clnum) * sizeof(int));
	while (clnum-startingNumberOfClients < numOfClients && (timetocheck.tv_sec > 0 || timetocheck.tv_usec > 0))
	{
		if (DEBUG > 0)
		{
			printf("Timeout: %ld.%06ld\n", (long)timetocheck.tv_sec, timetocheck.tv_usec);
		}

		tmpset = readset;
		if (DEBUG > 1 && FD_ISSET(ServerSocket, &tmpset))
		{
			printf("socket %d is in set\n", (int)ServerSocket);
		}
		int s;
		if (DEBUG > 1)
			printf("select()ing\n");

		if ((s = select(maxfd, &tmpset, NULL, NULL, &timetocheck)) == SOCKET_ERROR)
		{
			iResult = WSAGetLastError();
			strerror_s(bufptr, BUFFERSIZE, iResult);
			fprintf(stderr, "select() unsuccessful: %d\n%s\n", iResult, bufptr);
			InputManager::instance().HandleError("Error:", iResult);
			closesocket(ServerSocket);
			WSACleanup();
			free(buf);
			return false;
		}
		if (DEBUG > 1)
			printf("...done select()ing\n");

		if (DEBUG > 1 && s == 0)
		{
			printf("received nothing\n");
		}

		if (!FD_ISSET(ServerSocket, &tmpset))
		{
			if (DEBUG > 1)
				printf("socket %d is not in set\n", (int)ServerSocket);
			break;
		}

		if ((rcv = recvfrom(ServerSocket, buf, buflen, 0, (struct sockaddr *)&caddr, &caddrlen)) < 0)
		{
			iResult = WSAGetLastError();
			strerror_s(bufptr, BUFFERSIZE, iResult);
			fprintf(stderr, "server-recvfrom() unsuccessful: %d\n%s\n", iResult, bufptr);
			InputManager::instance().HandleError("Error:", iResult);
			closesocket(ServerSocket);
			WSACleanup();
			free(buf);
			return false;
		}
		if (DEBUG > 0)
			printf("server-recvfrom() successful: %d byte\n", rcv);

		if ((strncmp(bufptr, "Dark Luminance", 15) == 0) && (ClientCheck(caddrlst, clnum, &caddr, NULL) == false))
		{
			if (DEBUG > 0)
			{
				printf("%s", bufptr);
				printf(" #%d\n", clnum + 1);
			}

			bufptr = buf;
			memcpy(&caddrlst[clnum], &caddr, caddrlen);
			tmpIDs[*numIDsInClientIDs] = clnum;
			(*numIDsInClientIDs)++;
			clnum++;
		}
	}

	*clientIDs = (int *)malloc(*numIDsInClientIDs * sizeof(int));
	memcpy(*clientIDs, tmpIDs, *numIDsInClientIDs * sizeof(int));
	free(tmpIDs);

	if (DEBUG > 0)
	{
		if (clnum - startingNumberOfClients == 0)
		{
			printf("Server received no Client at all... no one want's to play. :(\n");
			return false;
		}
		if (clnum-startingNumberOfClients < numOfClients)
		{
			printf("Server just received %d different clients instead of %d.\n",
				clnum-startingNumberOfClients, numOfClients);
		}
		else
		{
			int s = (int) timeout - timetocheck.tv_sec - 1;
			long us = (timetocheck.tv_usec == 0) ? 0 : 1000000 - timetocheck.tv_usec;
			if (s < 0) s = 0;
			printf("Server got %d clients after %d.%06ld seconds\n", clnum-startingNumberOfClients, s, us);
		}
	}

	return true;
}

/*-----------------------------
  Starting of the game loop.
  First load the initial game
  state to every client.
-----------------------------*/
int NetworkManager::PrepareForGame()
{
	time_t timer;
	struct tm timeinfo;
	struct timeval tv;

	time(&timer);
	localtime_s(&timeinfo, &timer);
	gettimeofday(&tv, nullptr);

	//memset(buf, 0, BUFFERSIZE);

	if (DEBUG > 0)
	{
		strftime(bufptr, 20, "%Y-%m-%d%t%T", &timeinfo);
		sprintf_s(bufptr, BUFFERSIZE, "%s.%d", bufptr, (int)tv.tv_usec);
	}

	int *clst = (int *)malloc(clnum * sizeof(int));
	int lstnmbr;

	for (int i = 0; i < clnum; i++)
	{
		if ((snd = sendto(ServerSocket, bufptr, 27, 0, (struct sockaddr *)&(caddrlst[i]), caddrlen)) < 0)
		{
			iResult = WSAGetLastError();
			strerror_s(bufptr, BUFFERSIZE, iResult);
			fprintf(stderr, "server-sendto() unsuccessful: %d\n%s\n", iResult, bufptr);
			InputManager::instance().HandleError("Error:", iResult);
			closesocket(ServerSocket);
			WSACleanup();
			free(buf);
			free(clst);
			return false;
		}
		if (DEBUG > 1)
			printf("server-sendto() successful: %d byte\n", snd);
		clst[i] = 0;
	}

	FD_ZERO(&readset);
	FD_SET(ServerSocket, &readset);
	timetocheck.tv_sec = (long)timeout;

	for (int i = 0; i < clnum; i++)
	{
		tmpset = readset;
		if (select(maxfd, &tmpset, NULL, NULL, &timetocheck) == SOCKET_ERROR)
		{
			iResult = WSAGetLastError();
			strerror_s(bufptr, BUFFERSIZE, iResult);
			fprintf(stderr, "game-select() unsuccessful: %d\n%s\n", iResult, bufptr);
			InputManager::instance().HandleError("Error:", iResult);
			closesocket(ServerSocket);
			WSACleanup();
			free(buf);
			free(clst);
			return false;
		}

		if (!FD_ISSET(ServerSocket, &tmpset))
		{
			break;
		}

		if ((rcv = recvfrom(ServerSocket, buf, buflen, 0, (struct sockaddr *)&caddr, &caddrlen)) < 0)
		{
			iResult = WSAGetLastError();
			strerror_s(bufptr, BUFFERSIZE, iResult);
			fprintf(stderr, "server-recvfrom() unsuccessful: %d\n%s\n", iResult, bufptr);
			InputManager::instance().HandleError("Error:", iResult);
			closesocket(ServerSocket);
			WSACleanup();
			free(buf);
			free(clst);
			return false;
		}
		if (DEBUG > 0)
		{
			printf("server received from client %s:%d %d bytes\n", inet_ntop(AF_INET, &caddr.sin_addr, bufptr, BUFFERSIZE), ntohs(caddr.sin_port), rcv);
		}

		if (rcv == 4)
		{
			ClientCheck(caddrlst, clnum, &caddr, &lstnmbr);
			clst[lstnmbr] = 1;
			if (DEBUG > 0)
				printf("client #%d answered\n", lstnmbr + 1);
		}
		else
			i--;
	}
	if (DEBUG > 0)
	{
		for (int i = 0; i < clnum; i++)
		{
			printf("client #%d: %d\n", i + 1, clst[i]);
		}
	}

	free(clst);
	return true;
}

// Add a Parameter with a function call to get the newest Water Force Data.
//int NetworkManager::RunGame()
//{
//	memset(&buf, 0, buflen);
//	GamePacket* gp;
//	//gp.payload = "I've got alot of data."; 
//
//	int sndbytes; //to be specified
//
//	int cID = -1;
//
//	while (LOOP && clnum) // need Threading! For Send and Recv!
//	{
//		cID = -1;
//		if ((rcv = recvfrom(ServerSocket, buf, buflen, 0, (struct sockaddr *)&caddr, &caddrlen)) < 0)
//		{
//			iResult = WSAGetLastError();
//			strerror_s(bufptr, BUFFERSIZE, iResult);
//			fprintf(stderr, "server-recvfrom() unsuccessful: %d\n%s\n", iResult, bufptr);
//			closesocket(ServerSocket);
//			WSACleanup();
//			free(buf);
//			return false;
//		}
//
//		if (ClientCheck(caddrlst, clnum, &caddr, &cID) == true)
//		{
//			if (DEBUG > 1)
//			{
//				printf("Received from client #%i %s:%d %d bytes\n", cID, inet_ntop(AF_INET, &caddr.sin_addr, bufptr, BUFFERSIZE), ntohs(caddr.sin_port), rcv);
//			}
//			gp = (GamePacket*) buf;
//		}
//		else
//		{
//			if (DEBUG > 1)
//			{
//				printf("Received from external %s:%d %d bytes\n", inet_ntop(AF_INET, &caddr.sin_addr, bufptr, BUFFERSIZE), ntohs(caddr.sin_port), rcv);
//			}
//			continue;
//		}
//
//		//seqnmbr = gp.serversequence;
//		//memcpy(&buf, &gp, sizeof(gp));
//
//		if ((snd = sendto(ServerSocket, bufptr, sndbytes, 0, (struct sockaddr *)&caddr, caddrlen)) < 0)
//		{
//			iResult = WSAGetLastError();
//			strerror_s(bufptr, BUFFERSIZE, iResult);
//			fprintf(stderr, "server-sendto() unsuccessful: %d\n%s\n", iResult, bufptr);
//			closesocket(ServerSocket);
//			WSACleanup();
//			free(buf);
//			return false;
//		}
//		if (DEBUG > 1)
//		{
//			printf("Sent to client #%i %s:%d %d bytes\n", cID, inet_ntop(AF_INET, &caddr.sin_addr, bufptr, BUFFERSIZE), ntohs(caddr.sin_port), snd);
//		}
//	}
//
//	return true;
//}

//template<class ClassObject>
//int NetworkManager::RunGame(ClassObject *const thisClass, void(ClassObject::*funcToGetWF)(float*, float*))
//{
//	running = true;
//
//	recvMethod = new std::thread(NetworkManager::RecvThreaded);
//
//	sendMethod = new std::thread(NetworkManager::SendThreaded<ClassObject>, thisClass, funcToGetWF);
//
//	return true;
//}

int NetworkManager::RunGame()
{
	running = true;

	recvMethod = new std::thread(&NetworkManager::RecvThreaded, this);

	return true;
}

int NetworkManager::SendData(float up, float right)
{
	GamePacket gp;
	char* bytestream = (char*)malloc(BUFFERSIZE);												//GamePacket* gp = (GamePacket*)malloc(BUFFERSIZE);
	memset(bytestream, 0, BUFFERSIZE);															//memset(gp, 0, BUFFERSIZE);
	bytestream[0] = 's';																		//gp->packetType = 's';
	short* shortfield = (short*)&bytestream[sizeof(gp.packetType)]; 
	shortfield[0] = /*htons*/(sizeof(float) * 2);													//gp->payloadSize = htons(sizeof(float) * 2);
	float* floatfield = (float*)&bytestream[sizeof(gp.packetType) + sizeof(gp.payloadSize) + 1];	//float* floatPayload = (float*)&gp->payload;
	floatfield[0] = /*htonl*/(up);																	//floatPayload[0] = htonl(up); //htonf(up);
	floatfield[1] = /*htonl*/(right);																//floatPayload[1] = htonl(right);
	int sndSize = sizeof(gp.packetType) + sizeof(gp.payloadSize) + 1 + sizeof(float) * 2;

	for (int i = 0; i < clnum; i++)
	{
		if ((snd = sendto(ServerSocket, bytestream, sndSize, 0, (struct sockaddr *)&caddrlst[i], caddrlen)) < 0)
		{
			iResult = WSAGetLastError();
			strerror_s(bufptr, BUFFERSIZE, iResult);
			fprintf(stderr, "server-sendto() unsuccessful: %d\n%s\n", iResult, bufptr);
			InputManager::instance().HandleError("Error:", iResult);
			closesocket(ServerSocket);
			WSACleanup();
			free(bytestream);
			return false;
		}
		if (DEBUG > 1)
		{
			printf("Sent to client #%i %s:%d %d bytes\n", i, inet_ntop(AF_INET, &caddr.sin_addr, bufptr, BUFFERSIZE), ntohs(caddr.sin_port), snd);
		}
	}

	free(bytestream);
	return true;
}

int NetworkManager::RecvData()
{
	GamePacket gp;
	int cID = -1;
	char* bytestream = (char*)malloc(BUFFERSIZE);
	memset(bytestream, 0, BUFFERSIZE);

	if ((rcv = recvfrom(ServerSocket, bytestream, BUFFERSIZE, 0, (struct sockaddr *)&caddr, &caddrlen)) < 0)
	{
		iResult = WSAGetLastError();
		strerror_s(bufptr, BUFFERSIZE, iResult);
		fprintf(stderr, "server-recvfrom() unsuccessful: %d\n%s\n", iResult, bufptr);
		InputManager::instance().HandleError("Error:", iResult);
		closesocket(ServerSocket);
		WSACleanup();
		free(bytestream);
		return false;
	}

	if (ClientCheck(caddrlst, clnum, &caddr, &cID) == true)
	{
		if (DEBUG > 1)
		{
			printf("Received from client #%i %s:%d %d bytes\n", cID, inet_ntop(AF_INET, &caddr.sin_addr, bufptr, BUFFERSIZE), ntohs(caddr.sin_port), rcv);
		}

		if (bytestream[0] == 'c' && rcv == sizeof(gp.packetType) + sizeof(gp.payloadSize) + sizeof(float) * 2 + sizeof(char)
			&& /*ntohs*/(((short*)&bytestream[sizeof(gp.packetType)])[0]) == sizeof(char) + sizeof(float) * 2)
		{
			float mUp, mRight;
			char mSlide;

			mSlide = bytestream[sizeof(gp.packetType) + sizeof(gp.payloadSize)];
			float *floatPayload = (float *)&bytestream[sizeof(gp.packetType) + sizeof(gp.payloadSize) + sizeof(char)];
			mUp = /*ntohl*/(floatPayload[0]); //ntohf(thePayload)
			mRight = /*ntohl*/(floatPayload[1]);
			InputManager::instance().SetKey(cID, mSlide);
			InputManager::instance().SetRange(cID, mUp, mRight);
		}
	}
	else
	{
		if (DEBUG > 1)
		{
			printf("Received from external %s:%d %d bytes\n", inet_ntop(AF_INET, &caddr.sin_addr, bufptr, BUFFERSIZE), ntohs(caddr.sin_port), rcv);
		}
	}

	free(bytestream);
	return true;
}

int NetworkManager::EndGame()
{
	running = false;

	if (sendMethod != nullptr)
	{
		sendMethod->join();
		free(sendMethod);
	}

	if (recvMethod != nullptr)
	{
		recvMethod->join();
		free(recvMethod);
	}

	closesocket(ServerSocket);
	WSACleanup();
	free(buf);
	initialized = false;
	return true;
}

void NetworkManager::RecvThreaded()
{
	while (running)
	{
		RecvData();
	}
}

//void NetworkManager::SendThreaded(funcToWF getValues)
//{
//	float up, right;
//	while (running)
//	{
//		getValues(&up, &right);
//		SendData(up, right);
//	}
//}

//template<class ClassObject>
//void NetworkManager::SendThreaded(ClassObject *thisClass, void(ClassObject::*funcToGetWF)(float*, float*))
//{
//	float up, right;
//	while (running)
//	{
//		((thisClass)->*(funcToGetWF))(&up, &right);
//		//func(&up, &right);
//		SendData(up, right);
//	}
//}

int NetworkManager::gettimeofday(struct timeval * tp, struct timezone * tzp)
{
	// Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
	static const UINT64 EPOCH = ((UINT64)116444736000000000ULL);

	SYSTEMTIME  system_time;
	FILETIME    file_time;
	UINT64    time;

	GetSystemTime(&system_time);
	SystemTimeToFileTime(&system_time, &file_time);
	time = ((UINT64)file_time.dwLowDateTime);
	time += ((UINT64)file_time.dwHighDateTime) << 32;

	tp->tv_sec = (long)((time - EPOCH) / 10000000L);
	tp->tv_usec = (long)(system_time.wMilliseconds * 1000);
	return true;
}

int NetworkManager::TimevalSubtract(struct timeval *result,	struct timeval *x, struct timeval *y)
{
	/* Perform the carry for the later subtraction by updating y. */
	if (x->tv_usec < y->tv_usec)
	{
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000)
	{
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait.
	tv_usec is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}

bool NetworkManager::ClientCheck(struct sockaddr_in *__list, int __numelements, struct sockaddr_in *__searchfor, int *__listnumber)
{
	int i;
	for (i = 0; i < __numelements; i++)
	{
		if (memcmp(&(__list[i].sin_port), &(__searchfor->sin_port),
			sizeof(__searchfor->sin_port) +
			sizeof(__searchfor->sin_addr)) == 0)
		{
			if (__listnumber)
				*__listnumber = i;
			return true;
		}
	}
	return false;
}

