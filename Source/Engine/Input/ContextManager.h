#pragma once

/*
With an input map, take the actions, states and ranges and 
test them with the active contexts and consume them, if wanted.
*/

class ContextManager
{
public:
	// Singleton approach
	/*static ContextManager& instance()
	{
		static ContextManager _instance;
		return _instance;
	}
	~ContextManager();*/

	// No singleton approach
	ContextManager();
	~ContextManager();

	void Init();

private:
	// Singleton approach
	/*ContextManager();
	ContextManager(const ContextManager&);
	ContextManager &operator = (const ContextManager &);*/
};

