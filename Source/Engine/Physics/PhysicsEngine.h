#ifndef _PHYSICSENGINE_H_
#define _PHYSICSENGINE_H_

#include <set>
#include <thread>
#include <chrono>
#include "../General/Logger.h"
#include "../Scene/Scene.h"

#include "../General/Application.h"

#define GRAVITY -9.81f

class IPhysicComponent;
class ICollider;

// manages all the physic, each physic-component will be registered here and makes then part of the physics
class PhysicsEngine
{
public:
	static PhysicsEngine& instance()
	{
		static PhysicsEngine _instance;
		return _instance;
	}
	~PhysicsEngine() {};

	void Start(Scene* scene, float fixedDt, float slowDown = 1.0f);

	// runs seperate loop for simulation with fixed timestep, with EvaluateSimulation the data can be copied to the device
	void Run();

	void Pause(bool pause);
	void Stop();

	// copies actual data from sim to device (can block!)
	void EvaluateSimulation(float dt);

	void AddPhysicObject(IPhysicComponent *po);
	void RemovePhysicObject(IPhysicComponent *po);
	
	float GetActualTime();
	std::thread::id GetThreadId();
	
private:
	PhysicsEngine() {};
	PhysicsEngine(const PhysicsEngine&);
	PhysicsEngine &operator = (const PhysicsEngine &);
	std::thread *mPhysicThread;
};

#endif
