#include "PhysicsEngine.h"
#include "../Scene/IPhysicComponent.h"
#include "../Scene/GameObject.h"
#include <queue>


// global variables and functions

bool gRun = false;
bool gPause = false;

std::set<IPhysicComponent*> gPhysicObjects = std::set<IPhysicComponent*>();
float gFixedDt;
float gSlowDown;

float gActualTime = 0;
int gTimeSamples = 5;
Scene* gScene;

void gHandleCollisions()
{
	// TODO: naive collision detection! afterwards maybe kd-tree or uniform-grid collision handling?!
	std::vector<std::pair<ICollider*, ICollider*>> collisions = std::vector<std::pair<ICollider*, ICollider*>>();
	std::queue<std::pair<ICollider*, ICollider*>> collisionPairs = std::queue<std::pair<ICollider*, ICollider*>>();

	// collision detection
	
	// TODO: replace by a broadphase data structure (kd-tree or similar)
	for (auto po : gPhysicObjects)
	{
		if (po->IsEnabled() && po->GetGameObject()->IsEnabled() && po->GetCollider() != nullptr)
		{
			auto other = gPhysicObjects.find(po);
			for (++other; other != gPhysicObjects.end(); other++)
			{
				if ((*other)->IsEnabled() && po->GetGameObject()->IsEnabled() && (*other)->GetCollider() != nullptr)
				{
					collisionPairs.push(std::pair<ICollider*, ICollider*>(po->GetCollider(), (*other)->GetCollider()));
				}
			}
		}
	}

	while (!collisionPairs.empty())
	{
		std::pair<ICollider*, ICollider*> collPair = collisionPairs.front();
		collisionPairs.pop();

		Collision tmp = Collision();
		// TODO: static CollisionDetection function?!
		if (collPair.first->CollisionDetection(collPair.second))
		{
			if (!collPair.first->HasChildren() && !collPair.second->HasChildren())
			{
				collisions.push_back(std::pair<ICollider*, ICollider*>(collPair.first, collPair.second));
			}
			else
			{
				for (auto po : collPair.first->GetChildren())
				{
					for (auto other : collPair.second->GetChildren())
					{
						collisionPairs.push(std::pair<ICollider*, ICollider*>(po, other));
					}
				}
			}
		}
	}

	// collision handling
	for (auto col : collisions)
	{ 		
 		// TODO: static HandleCollision function?!
		col.first->GetPhysicComponent()->HandleCollision(*col.first, *col.second);
	}
}

void gFixedUpdate()
{
	double accumTime = 0;
	int count = 0;
	// TODO: more threads for fixedUpdate calls/ collision handling
	while (gRun)
	{
		auto start = std::chrono::high_resolution_clock::now();
		auto nextStartTime = start + std::chrono::duration<double, std::milli>(gFixedDt * gSlowDown * 1000);
		if (!gPause)
		{
			for (auto po : gPhysicObjects)
			{
				if (po->IsEnabled() && po->GetGameObject()->IsEnabled())
				{
					po->FixedUpdate(gFixedDt);
				}
			}

			gHandleCollisions(); 

			for (auto po : gPhysicObjects)
			{
				if (po->IsEnabled() && po->GetGameObject()->IsEnabled())
				{
					po->SwapTransforms();
				}
			}
			gScene->FixedUpdate(gFixedDt);
			for (auto po : gPhysicObjects)
			{
				if (po->IsEnabled() && po->GetGameObject()->IsEnabled())
				{
					po->UpdateForces(gFixedDt);
				}
			}
		}
		auto stop = std::chrono::high_resolution_clock::now();

		accumTime += std::chrono::duration<double, std::milli>(stop - start).count();
		count++;

		if (++count >= gTimeSamples)
		{
			gActualTime = (float)accumTime / count;
			accumTime = 0;
			count = 0;
		}

		if (gRun)
		{
			std::this_thread::sleep_until(nextStartTime);
		}
	}
}

// PhysicsEngine methods

void PhysicsEngine::Start(Scene* scene, float fixedDt, float slowDown)
{
	gScene = scene;
	gFixedDt = fixedDt;
	gSlowDown = slowDown;
	gActualTime = 25;

	for (auto po : gPhysicObjects)
	{
		po->CallStart();
	}
}

void PhysicsEngine::EvaluateSimulation(float dt)
{
	for (auto po : gPhysicObjects)
	{
		if (po->IsEnabled() && po->GetGameObject()->IsEnabled())
		{
			po->EvaluateSimulation(dt);
		}
	}
}

void PhysicsEngine::Run()
{
	if (!gRun)
	{
		gRun = true;
		mPhysicThread = new std::thread(gFixedUpdate);
	}
}

void PhysicsEngine::Pause(bool pause)
{
	gPause = pause;
}

void PhysicsEngine::Stop()
{
	gRun = false;
	mPhysicThread->join();
	delete mPhysicThread;
}

void PhysicsEngine::AddPhysicObject(IPhysicComponent * po)
{
	if (gRun && !gPause)
	{
		Logger::instance().Msg(LOG_WARNING, L"You are adding a PhysicObject during simulation! Stop or pause first!");
	}
	gPhysicObjects.insert(po);
}

void PhysicsEngine::RemovePhysicObject(IPhysicComponent * po)
{
	if (gRun && !gPause)
	{
		Logger::instance().Msg(LOG_WARNING, L"You are removing a PhysicObject during simulation! Stop or pause first!");
	}
	gPhysicObjects.erase(po);
}

float PhysicsEngine::GetActualTime()
{
	return gActualTime;
}

std::thread::id PhysicsEngine::GetThreadId()
{
	if(mPhysicThread != nullptr) return mPhysicThread->get_id();
	return std::thread::id();
}

