#include "SDFGrid.h"

SDFGrid::SDFGrid(Device* pDevice, const SDFProperties& sdfProps, AnimatedVolumeTexture* SDFResource, ID3D11SamplerState* sampler, VolumeTextureRW* obstacleGrid, AnimatedVolumeTexture* velResource)
	: mDevice(pDevice),
	mSDFPropCB(pDevice->GetDevice(), sizeof(SDFProperties)),
	mPerFrameDataCB(pDevice->GetDevice(), sizeof(PerFrameData)),
	mPerObjectDataCB(pDevice->GetDevice(), sizeof(PerObjectData)),
	mBuoyancyPerObjectDataCB(pDevice->GetDevice(), sizeof(BuoyancyPerObjectData)),
	mSDFProps(sdfProps),
	mDim(sdfProps.dim),
	mSDFResource(SDFResource),
	mSampler(sampler),
	mVelResource(velResource),
	mObstacleGrid(obstacleGrid)
{
	mBuoyancyCS = GPUMemoryManager::instance().GetComputeShader("./Assets/Shaders/Compute/Physics/Buoyancy.cso");
	mMeshToSDFCS = GPUMemoryManager::instance().GetComputeShader("./Assets/Shaders/Compute/Physics/MeshToSDF.cso");

	mDevice->GetContext()->UpdateSubresource(mSDFPropCB.GetSubresource(), 0, nullptr, &sdfProps, 0, 0);
}
SDFGrid::~SDFGrid()
{
	// TODO: in some other place release!
	delete mObstacleGrid;
	delete mVelResource;
	delete mSDFResource;
}
void SDFGrid::MeshToSDF(const ID3D11Buffer* mesh, const XMMATRIX& objToSDFGrid, const PerObjectData& perFrameData)
{

}
bool SDFGrid::GetBuoyancyForce(XMFLOAT3* force, XMFLOAT3* torque, MeshCollider* mesh, const BuoyancyPerObjectData& perObject)
{
	int blockSize = (int)std::ceil(perObject.meshData.count / (float)THREAD_COUNT_1D);

	StructuredBuffer buoyancyResult(mDevice->GetDevice(), sizeof(Buoyancy) * blockSize, sizeof(Buoyancy), true, true, D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_READ);

	float time = Application::instance().GetTotalTime();
	float t = time * mSDFResource->GetSpeed();

	PerFrameData perFrame;
	perFrame.offset = mSDFResource->GetOffsetOfTime((int)t);
	perFrame.nextOffset = mSDFResource->GetOffsetOfTime((int)t + 1);
	perFrame.blend = t - (int)t;
	perFrame.sdfFactor = mSDFResource->GetFactor();

	if (mVelResource != nullptr)
	{
		t = time * mVelResource->GetSpeed();

		perFrame.velOffset = mVelResource->GetOffsetOfTime((int)t);
		perFrame.velNextOffset = mVelResource->GetOffsetOfTime((int)t + 1);
		perFrame.velBlend = t - (int)t;
		perFrame.velFactor = mVelResource->GetFactor() * mVelResource->GetSpeed() * mSDFResource->GetSize().x / mVelResource->GetSize().x;
	}

	mDevice->LockContext();

	ID3D11DeviceContext* context = mDevice->GetContext();

	// set shader and init buffers + textures
	context->CSSetShader(mBuoyancyCS->GetObjectPtr(), nullptr, 0);

	// copy per frame data into constant buffer
	context->UpdateSubresource(mPerFrameDataCB.GetSubresource(), 0, nullptr, &perFrame, 0, 0);

	context->UpdateSubresource(mBuoyancyPerObjectDataCB.GetSubresource(), 0, nullptr, &perObject, 0, 0);

	float clearuav[] = {-2,-2,-2,-2};
	context->ClearUnorderedAccessViewFloat(*mObstacleGrid->GetUAV(), clearuav);

	// set constant buffers
	ID3D11Buffer* buffer[] = { *mSDFPropCB.GetPtr(), *mPerFrameDataCB.GetPtr(), *mBuoyancyPerObjectDataCB.GetPtr() };
	context->CSSetConstantBuffers(0, 3, buffer);

	// set output (buoyancy + obstacle grid)
	ID3D11UnorderedAccessView* uav[] = { *buoyancyResult.GetUAV(), *mObstacleGrid->GetUAV() };
	context->CSSetUnorderedAccessViews(0, 2, uav, nullptr);

	// set sampler
	context->CSSetSamplers(0, 1, &mSampler);

	// set read-only input values
	ID3D11ShaderResourceView* srv[] = { *mSDFResource->GetSRV(), mVelResource == nullptr ? nullptr : *mVelResource->GetSRV(), *mesh->GetMesh() };
	context->CSSetShaderResources(0, 3, srv);

	context->Dispatch(blockSize, 1, 1);

	Buoyancy* result = new Buoyancy[blockSize];
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	buoyancyResult.Map(context, D3D11_MAP::D3D11_MAP_READ, &mappedSubresource);

	memcpy(result, mappedSubresource.pData, blockSize * sizeof(Buoyancy));

	buoyancyResult.Unmap(context);

	ID3D11ShaderResourceView* nullSRV[] = { nullptr, nullptr, nullptr };
	ID3D11UnorderedAccessView* nullUAV[] = { nullptr, nullptr };
	context->CSSetShaderResources(0, 3, nullSRV);
	context->CSSetUnorderedAccessViews(0, 2, nullUAV, nullptr);

	mDevice->UnlockContext();

	XMVECTOR forceSum = XMVectorReplicate(0.f);
	XMVECTOR torqueSum = XMVectorReplicate(0.f);

	for (int i = 0; i < blockSize; i++)
	{
		forceSum = forceSum + XMLoadFloat3(&result[i].force);
		torqueSum = torqueSum + XMLoadFloat3(&result[i].torque);
	}

	delete result;

	if (XMVectorGetX(XMVector3LengthSq(forceSum)) > 1e-10f || XMVectorGetX(XMVector3LengthSq(torqueSum)) > 1e-10f)
	{
		XMStoreFloat3(force, forceSum);
		XMStoreFloat3(torque, torqueSum);
		return true;
	}
	return false;
}
