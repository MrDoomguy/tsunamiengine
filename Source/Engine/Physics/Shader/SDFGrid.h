#pragma once

#include "../../Render/Data/AnimatedVolumeTexture.h"
#include "../../Render/Data/VolumeTextureRW.h"
#include "../../Render/Data/ConstantBuffer.h"
#include "../../Render/Data/StructuredBuffer.h"
#include "../../Render/Data/ComputeShader.h"

#include "../../Render/Device.h"

#include "../../Scene/MeshCollider.h"

#include "../../General/Application.h"

/*
TODO:
struct for creating buffer to pass
take pDeviceContext
buffer is immutable

make mobstacle as map with rendercomponent?! or material?

map values with meshtosdf to the obstaclegrids
take a obstacle grid and calc buoyancy for a specific object
getobstaclegrids and use it for water reflections (with material?), particle generation and particle collision handling
*/


class SDFGrid
{
public:
	struct SDFProperties
	{
		XMUINT4 dim;
		XMUINT3 extent;
		float _pad0;
		XMUINT4 velDim = XMUINT4(-1,-1,-1,-1);
		XMUINT3 velExtent;
		float _pad1;
		float dampingFactor;
		float buoyancyFactor;
		float flowFactor;
		float frictionFactor;
	};

	// offset gives directly the position of the actual frame in the texture, dt is the interpolation factor between offset and offset + frame.dim
	struct PerFrameData
	{
		XMUINT3 offset;
		float sdfFactor;
		XMUINT3 nextOffset;
		float blend;
		XMUINT3 velOffset;
		float velFactor;
		XMUINT3 velNextOffset;
		float velBlend;
	};

	struct PerObjectData
	{
		XMMATRIX objToSDF;
		XMMATRIX SDFToObj;
		unsigned int count;
		XMFLOAT3 _pad4;
	};

	struct BuoyancyPerObjectData
	{
		PerObjectData meshData;
		XMFLOAT3 sdfScale;
		float _pad5;
		XMFLOAT3 scale;
		float _pad6;
		XMFLOAT3 com;
		float _pad7;
		XMFLOAT3 vel;
		float _pad8;
		XMFLOAT3 rotVel;
		float _pad9;
	};

	struct Buoyancy
	{
		XMFLOAT3 force;
		float _pad10;
		XMFLOAT3 torque;
		float _pad11;
	};

	SDFGrid(Device* pDevice, const SDFProperties& buoyancyProps, AnimatedVolumeTexture* SDFResource, ID3D11SamplerState* sampler, VolumeTextureRW* obstacleGrid, AnimatedVolumeTexture* velResource = nullptr);
	~SDFGrid();

	// TODO: clear obstaclegrid beforehand!
	// TODO: remove volumetexture if out of collider
	void MeshToSDF(const ID3D11Buffer* mesh, const XMMATRIX& objToSDFGrid, const PerObjectData& perObject);

	// mappes also mesh to sdf!
	bool GetBuoyancyForce(XMFLOAT3* force, XMFLOAT3* torque, MeshCollider* mesh, const BuoyancyPerObjectData& perObject);

private:
	XMUINT4 mDim;

	Device* mDevice;
	AnimatedVolumeTexture* mSDFResource;
	AnimatedVolumeTexture* mVelResource;
	VolumeTextureRW* mObstacleGrid;

	SDFProperties mSDFProps;

	ConstantBuffer mSDFPropCB;

	ConstantBuffer mPerFrameDataCB;

	ConstantBuffer mPerObjectDataCB;
	ConstantBuffer mBuoyancyPerObjectDataCB;

	ComputeShader* mBuoyancyCS;
	ComputeShader* mMeshToSDFCS;

	ID3D11SamplerState* mSampler;
};
