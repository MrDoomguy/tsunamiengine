#ifndef _AUDIOENGINE_H_
#define _AUDIOENGINE_H_

#include "../General/TsunamiStd.h"
#include "AudioTypes.h"

#include "../Scene/AudioSourceComponent.h"

class AudioEngine
{
public:
	AudioEngine();
	~AudioEngine();

	void CreateSourceVoice(IXAudio2SourceVoice** ppSource, const WAVEFORMATEX& wf);

	IXAudio2* mEngine;
	IXAudio2MasteringVoice* mMaster;
};

#endif
