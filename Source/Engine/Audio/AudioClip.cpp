#include "AudioClip.h"

//#include "../General/GPUMemoryManager.h"

AudioClip::AudioClip(const bool loop)
{
	// force default values for empty clip
	// more on default values:
	// https://msdn.microsoft.com/en-us/library/windows/desktop/microsoft.directx_sdk.xaudio2.xaudio2_buffer(v=vs.85).aspx
	mBuffer.PlayBegin = 0;
	mBuffer.PlayLength = 0;
	mBuffer.LoopCount = loop ? XAUDIO2_LOOP_INFINITE : 0;
	mBuffer.LoopBegin = 0;
	mBuffer.LoopLength = 0;
	mBuffer.pContext = nullptr;
	mBuffer.Flags = 0;
	// start with an empty buffer
	mBuffer.AudioBytes = 0;
	mBuffer.pAudioData = nullptr;

	ZeroMemory(&mWaveFormat, sizeof(WAVEFORMATEX));
	mWaveFormat.cbSize = sizeof(WAVEFORMATEX);

	mState = audiotypes::ClipState::STOPPED;
	mVolume = 1.0f;
}

AudioClip::~AudioClip()
{
	mSource->DestroyVoice();
}

void AudioClip::Play()
{
	if (mState == audiotypes::ClipState::PLAYING)
	{
		mSource->Stop();
	}

	HRESULT hr;
	if (FAILED(hr = mSource->Start()))
	{
		core::TsunamiFatalError(L"Failed to Play AudioClip." + HR_to_wstr(hr));
	}
	mState = audiotypes::ClipState::PLAYING;
}

void AudioClip::Stop()
{
	mState = audiotypes::ClipState::STOPPED;
	mSource->Stop();
}

bool AudioClip::IsPlaying() const
{
	if (mState == audiotypes::ClipState::PLAYING)
		return true;
	
	return false;
}

void AudioClip::SetVolume(const float volume)
{
	mVolume = volume;
	mSource->SetVolume(mVolume);
}
