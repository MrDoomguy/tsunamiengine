#include "AudioEngine.h"

AudioEngine::AudioEngine() :
	mEngine{ nullptr },
	mMaster{ nullptr }
{
	HRESULT hr;
	CoInitializeEx(NULL, COINIT_MULTITHREADED);

	if (FAILED(hr = XAudio2Create(&mEngine, 0, XAUDIO2_DEFAULT_PROCESSOR)))
	{
		CoUninitialize();
		std::wstring err = L"Failed to initialize Audio Engine." + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}

	if (FAILED(hr = mEngine->CreateMasteringVoice(&mMaster)))
	{
		CoUninitialize();
		std::wstring err = L"Failed to initialize Audio Master." + HR_to_wstr(hr);
		core::TsunamiFatalError(err);
	}
}

AudioEngine::~AudioEngine()
{
	SAFE_RELEASE(mEngine);
	mMaster->DestroyVoice();
}

void AudioEngine::CreateSourceVoice(IXAudio2SourceVoice** ppSource, const WAVEFORMATEX& wf)
{
	HRESULT hr;
	if (FAILED(hr = mEngine->CreateSourceVoice(ppSource, &wf)))
	{
		core::TsunamiFatalError(L"Failed to create source voice." + HR_to_wstr(hr));
	}
}
