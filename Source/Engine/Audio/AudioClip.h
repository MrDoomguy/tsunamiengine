#ifndef _AUDIOCLIP_H_
#define _AUDIOCLIP_H_

#include "../General/TsunamiStd.h"
#include "AudioTypes.h"

class AudioClip
{
	friend class GPUMemoryManager;
public:
	explicit AudioClip(const bool loop);
	~AudioClip();

	void Play();
	void Stop();
	bool IsPlaying() const;

	void SetVolume(const float volume);

private:
	XAUDIO2_BUFFER mBuffer;
	WAVEFORMATEX mWaveFormat;
	IXAudio2SourceVoice* mSource;
	
	audiotypes::ClipState mState;
	std::vector<char> mRawData;
	float mVolume;
};

#endif