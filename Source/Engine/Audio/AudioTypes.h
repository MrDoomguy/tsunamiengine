#ifndef _AUDIOTYPES_H_
#define _AUDIOTYPES_H_

namespace audiotypes
{

	enum class MixerName : std::uint8_t
	{
		AMBIENT = 0x1,
		MUSIC = 0x2,
		SFX = 0x3,
		NAMES_END = 0x4
	};

	enum class Channels : std::uint8_t
	{
		MONO = 0x1,
		STEREO = 0x2,
		SURROUND_1D_L_C_R = 0x3,
		SURROUND_QUADMORPHIC_FL_C_RL_RR = 0x4,
		SURROUND_FIVE_CHANNELS_FL_C_FR_RL_RR = 0x5,
		SURROUND_51_FL_C_FR_RL_RR_LFE = 0x6,
		SURROUND_61_FL_C_FR_SL_SR_RC_LFE = 0x7,
		SURROUND_71_FL_C_FR_SL_SR_RL_RR_LFE = 0x8
	};

	enum class ClipState : std::uint8_t
	{
		PLAYING = 0x0,
		PAUSED = 0x1,
		STOPPED = 0x2
	};
}

#endif
