///////////////////////////////////////////////////////////////////////////////////
//
// IceWave Game entry point
//
///////////////////////////////////////////////////////////////////////////////////

#include "../../Engine/General/Application.h"

#include "../../Engine/General/TsunamiStd.h"

#include "../../Engine/General/Logger.h"

#include "../../Engine/General/Timer.h"
#include "../../Engine/General/ConfigManager.h"
#include "../../Engine/Scene/SceneBuilder.h"

#include "../../Engine/Render/Renderer.h"
#include "../../Engine/Scene/Scene.h"
#include "../Scripts/TestScript.hpp"
#include "../Scripts/TestScript2.hpp"
#include "../../Engine/Scene/SDFPhysic.h"
#include "../../Engine/Scene/Rigidbody.h"
#include "../../Engine/Scene/SphereCollider.h"

#include "../../Engine/Physics/PhysicsEngine.h"

#include "../../Engine/Render/Data/VolumetricMaterial.h"

#include "../../Engine/Input/InputManager.h"

#include <ShellScalingApi.h>
#pragma comment(lib, "Shcore.lib")

// Window vars
HINSTANCE		g_hInst = nullptr;
HWND			g_hWnd = nullptr;

// global app data
unsigned int g_width;
unsigned int g_height;
unsigned int g_resX;
unsigned int g_resY;
const UINT g_dpiDefaultX = 96;
const UINT g_dpiDefaultY = 96;
UINT g_dpiX = 96;
UINT g_dpiY = 96;
bool g_fullscreen;
std::wstring g_wTitle(L"IceWave Game");

// Engine goodies
Renderer* g_Renderer = nullptr;
Timer g_Timer;

// Static classes
ConfigManager& g_ConfigManager = ConfigManager::instance();
Logger& g_Logger = Logger::instance();
InputManager& g_InputManager = InputManager::instance();

HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow/*, unsigned int width, unsigned int height*/);
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void CalculateFrameCount();

void Resize(UINT width, UINT height, bool fullscreen)
{
	g_resX = g_width * g_dpiDefaultX / g_dpiX;
	g_resY = g_height * g_dpiDefaultY / g_dpiY;

	g_Renderer->Resize(g_resX, g_resY, g_fullscreen);

	Application::instance().SetScreenSize(g_resX, g_resY);

	g_InputManager.SetWindowSize(g_width, g_height);

	RECT rc = { 0, 0, static_cast<LONG>(width), static_cast<LONG>(height) };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
	SetProcessDpiAwareness(PROCESS_DPI_AWARENESS::PROCESS_PER_MONITOR_DPI_AWARE);

	g_Logger.Init(L"Tsunami Debug Console");

	std::setlocale(LC_ALL, "en_US.utf8");

	// Logger example usage
	// Remove in production code
	g_Logger.Msg(LOG_LAYER::LOG_GENERAL, L"Current Working Directory: " + Filesystem::GetCurrentPathW());

	// Configuration Manager
	if (FAILED(g_ConfigManager.Init(L"config.alk")))
		return 0;

	// Framebuffer resolution
	g_resX = g_ConfigManager.GetValueByKey<unsigned int>("width");
	g_resY = g_ConfigManager.GetValueByKey<unsigned int>("height");
	g_fullscreen = g_ConfigManager.GetValueByKey<bool>("fullscreen");

	if (FAILED(InitWindow(hInstance, nCmdShow)))
		return 0;

	// enable/disable cursor
	ShowCursor(g_ConfigManager.GetValueByKey<bool>("cursor"));

	g_Timer.Reset();

	g_Renderer = new Renderer();
	g_Renderer->Init(g_hWnd);

	//
	// Test for giving out all connected devices
	// and initializing the InputManager
	//
	g_InputManager.GetAllKnownDevices();
	g_InputManager.Init(g_hWnd);
	g_InputManager.SetWindowSize(g_width, g_height);
	
	Application::instance().Init(g_resX, g_resY, g_fullscreen, g_wTitle);
	Application::instance().SetRenderer(g_Renderer);
	Application::instance().SetGlobalTimer(&g_Timer);

	Scene* scene = new Scene();
	SceneBuilder builder;
#if EDITOR
	builder.MakeEditorScene(g_Renderer->GetDevice(), g_width, g_height, scene);
#else
	//builder.MakeDevelopmentDemo(g_Renderer->GetDevice(), g_width, g_height, scene);
	builder.MakePhysicsDemo(g_Renderer->GetDevice(), g_Renderer->GetFontEngine(), g_resX, g_resY, scene);
	//builder.MakeCtrlDemo(g_Renderer->GetDevice(), g_width, g_height, scene);
	//builder.MakeAnimationDemo(g_Renderer->GetDevice()->GetDevice(), g_Renderer->GetFontEngine(), g_width, g_height, scene);
	//builder.MakeRenderingDemo(g_Renderer->GetDevice()->GetDevice(), g_Renderer->GetFontEngine(), g_width, g_height, scene);
#endif

	g_Renderer->SetCurrentScene(scene);

	g_Renderer->Resize(g_resX, g_resY, g_fullscreen);

	//
	// Bake Static Environment Map
	//
	g_Renderer->BakeStaticEnvironment(scene->GetEnvProbePosition());

	scene->Awake();
	scene->Start();

	// After setup of scene and start call!!
	PhysicsEngine::instance().Start(scene, 0.025f, 1);
	PhysicsEngine::instance().Run();

	//
	// Windows Application Loop
	//
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			// Render and app logic update
			g_Timer.Tick();
			
			PhysicsEngine::instance().EvaluateSimulation(static_cast<float>(g_Timer.DeltaTime()));
			scene->Update(static_cast<float>(g_Timer.DeltaTime()));

			g_Renderer->Render();

			g_InputManager.UpdatePressedState();

			CalculateFrameCount();
		}
	}

	PhysicsEngine::instance().Stop();

	g_Logger.Destroy();
	GPUMemoryManager::instance().Release();
	Application::instance().Destroy();
	delete scene;

	return (int)msg.wParam;
}

HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow/*, unsigned int width, unsigned int height*/)
{

	HMONITOR monitor = MonitorFromPoint({ 1,1 }, MONITOR_DEFAULTTOPRIMARY);
	LPMONITORINFO monInfo = (LPMONITORINFO)malloc(sizeof(LPMONITORINFO));
	GetMonitorInfoW(MonitorFromWindow(g_hWnd, MONITOR_DEFAULTTOPRIMARY), monInfo);
	DEVICE_SCALE_FACTOR scaleFactor;
	GetScaleFactorForMonitor(MonitorFromWindow(g_hWnd, MONITOR_DEFAULTTOPRIMARY), &scaleFactor);
	GetDpiForMonitor(monitor, MONITOR_DPI_TYPE::MDT_EFFECTIVE_DPI, &g_dpiX, &g_dpiY);
	//GetDpiForMonitor(monitor, MONITOR_DPI_TYPE::MDT_ANGULAR_DPI, &g_dpiDefaultX, &g_dpiDefaultY);

	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_APPLICATION);
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);//(HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = L"TsunamiApp";
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_APPLICATION);
	if (!RegisterClassEx(&wcex))
		return E_FAIL;

	// Create window
	g_hInst = hInstance;
	g_width = g_resX * g_dpiX / g_dpiDefaultX;
	g_height = g_resY * g_dpiY / g_dpiDefaultY;
	RECT rc = { 0, 0, static_cast<LONG>(g_width), static_cast<LONG>(g_height) };
	// init the drag area of the input
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	g_hWnd = CreateWindow(
		L"TsunamiApp",
		g_wTitle.c_str(),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		rc.right - rc.left,
		rc.bottom - rc.top,
		nullptr, nullptr,
		hInstance,
		nullptr);

	if (!g_hWnd)
		return E_FAIL;

	ShowWindow(g_hWnd, nCmdShow);

	return S_OK;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	bool currentResizing = false;

	switch (message)
	{
	case WM_ACTIVATE:
		if (LOWORD(wParam) == WA_INACTIVE)
		{
			g_Timer.Stop();
		}
		else
		{
			g_Timer.Start();
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_SIZE:
	{
		g_width = LOWORD(lParam);
		g_height = HIWORD(lParam);

		if (wParam == SIZE_MAXIMIZED)
		{
			Resize(g_width, g_height, false);
		}
		break;
	}
	case WM_EXITSIZEMOVE:
	{
		Resize(g_width, g_height, false);
		break;
	}

	case WM_CLOSE:
		DestroyWindow(g_hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_INPUT:
		// Testing to include InputManager
		g_InputManager.HandleInput(hWnd, message, wParam, lParam);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

void CalculateFrameCount()
{
	// Code computes the average frames per second, and also the 
	// average time it takes to render one frame.  These stats 
	// are appended to the window caption bar.
	static int frameCnt = 0;
	static float timeElapsed = 0.0f;

	frameCnt++;

	// Compute averages over one second period.
	if ((g_Timer.TotalTime() - timeElapsed) >= 1.0f)
	{
		float fps = (float)frameCnt;
		float mspf = 1000.0f / fps;

		float pT = PhysicsEngine::instance().GetActualTime();
		float pFps = 1000.f / pT;


		std::wostringstream outs;
		outs.precision(6);
		outs << g_wTitle << L"    "
			<< L"FPS: " << fps << L"    "
			<< L"Frame Time: " << mspf << L" (ms)    "
			<< L"Physics FPS: " << pFps << L"    "
			<< L"Physics Time: " << pT << L" (ms)";
		SetWindowText(g_hWnd, outs.str().c_str());

		// Reset for next average.
		frameCnt = 0;
		timeElapsed += 1.0f;
	}
}
