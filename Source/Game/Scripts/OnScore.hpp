#ifndef _OnScore_H_
#define _OnScore_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Render/Data/FontMaterial.h"
#include "../../Engine/General/Application.h"

class OnScore : public IScript
{
public:
	OnScore(std::string& scriptName, float padding)
		: IScript(scriptName) 
	{
		mPadding = padding;
	}

	virtual void Awake() override
	{
		mat = dynamic_cast<FontMaterial*>(mGo->GetMaterial());
		mat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_CENTER);
		mat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
		mat->SetText(text.c_str());
		mat->SetFontName(L"Another X Display tfb");
		mat->SetFontSize(48.0f);
		mat->SetRect(mPadding, mPadding, (float)Application::instance().GetScreenWidth() * (1.f - mPadding), (float)Application::instance().GetScreenHeight() * (1.f - mPadding));
		mat->SetSolidColor(D2D1::ColorF(.97f, 1.f, .14f));
	}

	virtual void Start() override 
	{
		mScore = 0;
	}

	virtual void Update(float dt) override
	{
		if (!isPaused)
		{
			std::wstringstream ss;
			ss << (int)mScore;

			std::wstring tmp = text;
			tmp.append(ss.str());

			mat->SetText(tmp.c_str());
		}

		mat->SetRect(0.0f, 0.0f, (float)Application::instance().GetScreenWidth(), (float)Application::instance().GetScreenHeight());
	}

	void SetScore(float score)
	{
		mScore = score;
	}

	void SetPaused(const bool paused)
	{
		isPaused = paused;
	}

	void SetText(const std::wstring& txt)
	{
		text = txt;
		mat->SetText(text.c_str());
	}

private:
	FontMaterial* mat;
	float mScore;
	std::wstring text;
	bool isPaused = false;
	float mPadding;
};

#endif
