#ifndef _EditorScript_H_
#define _EditorScript_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Render/Data/DiffuseMaterial.h"
#include "../../Engine/Render/Data/FontMaterial.h"
#include "../../Engine/General/Application.h"
#include "../../Engine/Input/InputManager.h"

class EditorScript : public IScript
{
public:
	EditorScript(std::string& scriptName)
		: IScript(scriptName) { }

	virtual void Start() override
	{
		InputManager::instance().AddPlayer(0, Input::InputDevice::KEYBOARD);
		InputManager::instance().AddDevice(0, Input::InputDevice::MOUSE);

		lastMaterial = new DiffuseMaterial();

		FontMaterial* overlay = new FontMaterial();
		overlay->Init(Application::instance().GetFontRenderDevice());
		overlay->SetFontName(L"Gabriola");
		overlay->SetRect(0.0f, 0.0f, (float)Application::instance().GetScreenWidth(), (float)Application::instance().GetScreenWidth());
		overlay->SetFontSize(24.0f);
		overlay->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_LEADING);
		overlay->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
		overlay->SetSolidColor(D2D1::ColorF(D2D1::ColorF::ForestGreen));

		mGo->AddComponent(new RenderComponent(overlay, nullptr));
		Application::instance().GetCurrentScene()->NotifyFontQueue(mGo);
	}

	virtual void Update(float dt) override
	{
		float sp = tSpeed;
		if (!snap)
			sp *= dt;

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_TAB))
		{
			SelectNext();
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_0))
		{
			SaveScene();
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_B))
		{
			currTransform = Transform::TRANSLATE;
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_N))
		{
			currTransform = Transform::ROTATE;
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_M))
		{
			currTransform = Transform::SCALE;
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_X))
		{
			snap = !snap;
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_1))
		{
			if (Application::instance().GetCurrentScene()->GetCamera()->GetCamera()->GetLensMode() == CameraComponent::LensMode::PERSPECTIVE)
			{
				Application::instance().GetCurrentScene()->GetCamera()->GetCamera()->SetOrtho();
			}
			else
			{
				Application::instance().GetCurrentScene()->GetCamera()->GetCamera()->SetPerspective();
			}
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_PLUS))
		{
			tSpeed += 1.0f;
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_MINUS))
		{
			tSpeed -= 1.0f;

			if (tSpeed < 0.0f)
				tSpeed = 0.0f;
		}

		// T +z | R +x | S +z
		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::ARROW_UP) && selected != nullptr)
		{
			switch (currTransform)
			{
			case Transform::TRANSLATE:
				selected->GetTransform()->Translate(XMFLOAT3(0.0f, 0.0f, sp));
				break;
			case Transform::ROTATE:
				selected->GetTransform()->Rotate(XMFLOAT3(1, 0, 0), sp);
				break;
			case Transform::SCALE:
				XMFLOAT3 s = selected->GetTransform()->GetScale();
				selected->GetTransform()->SetScale(XMFLOAT3(s.x, s.y, s.z + sp / 1000.0f));
				break;
			}
		}

		// T -z | R -x | S -z
		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::ARROW_DOWN) && selected != nullptr)
		{
			switch (currTransform)
			{
			case Transform::TRANSLATE:
				selected->GetTransform()->Translate(XMFLOAT3(0.0f, 0.0f, -sp));
				break;
			case Transform::ROTATE:
				selected->GetTransform()->Rotate(XMFLOAT3(1, 0, 0), -sp);
				break;
			case Transform::SCALE:
				XMFLOAT3 s = selected->GetTransform()->GetScale();
				selected->GetTransform()->SetScale(XMFLOAT3(s.x, s.y, s.z - sp / 1000.0f));
				break;
			}
		}

		// T -x | R -y | S -z
		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::ARROW_LEFT) && selected != nullptr)
		{
			switch (currTransform)
			{
			case Transform::TRANSLATE:
				selected->GetTransform()->Translate(XMFLOAT3(-sp, 0.0f, 0.0f));
				break;
			case Transform::ROTATE:
				selected->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), -sp);
				break;
			case Transform::SCALE:
				XMFLOAT3 s = selected->GetTransform()->GetScale();
				selected->GetTransform()->SetScale(XMFLOAT3(s.x - sp / 1000.0f, s.y, s.z));
				break;
			}
		}

		// T +x | R +y | S +z
		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::ARROW_RIGHT) && selected != nullptr)
		{
			switch (currTransform)
			{
			case Transform::TRANSLATE:
				selected->GetTransform()->Translate(XMFLOAT3(sp, 0.0f, 0.0f));
				break;
			case Transform::ROTATE:
				selected->GetTransform()->Rotate(XMFLOAT3(0, 1, 0), sp);
				break;
			case Transform::SCALE:
				XMFLOAT3 s = selected->GetTransform()->GetScale();
				selected->GetTransform()->SetScale(XMFLOAT3(s.x + sp / 1000.0f, s.y, s.z));
				break;
			}
		}

		// T -y | R -z | S -y
		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_RCTRL) && selected != nullptr)
		{
			switch (currTransform)
			{
			case Transform::TRANSLATE:
				selected->GetTransform()->Translate(XMFLOAT3(0.0f, -sp, 0.0f));
				break;
			case Transform::ROTATE:
				selected->GetTransform()->Rotate(XMFLOAT3(0, 0, 1), -sp);
				break;
			case Transform::SCALE:
				XMFLOAT3 s = selected->GetTransform()->GetScale();
				selected->GetTransform()->SetScale(XMFLOAT3(s.x, s.y - sp / 1000.0f, s.z));
				break;
			}
		}

		// T +y | R +z | S +y
		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::KEY_RSHIFT) && selected != nullptr)
		{
			switch (currTransform)
			{
			case Transform::TRANSLATE:
				selected->GetTransform()->Translate(XMFLOAT3(0.0f, sp, 0.0f));
				break;
			case Transform::ROTATE:
				selected->GetTransform()->Rotate(XMFLOAT3(0, 0, 1), sp);
				break;
			case Transform::SCALE:
				XMFLOAT3 s = selected->GetTransform()->GetScale();
				selected->GetTransform()->SetScale(XMFLOAT3(s.x, s.y + sp / 1000.0f, s.z));
				break;
			}
		}

		// All done for the frame
		// Print overlay
		std::wstring txt = L"Current Transformation: " + this->EnumToString(currTransform);
		std::wstringstream ss;
		ss << std::fixed << std::setprecision(2) << tSpeed;
		txt.append(L"\nTransform Speed: " + ss.str());
		ss.str(std::wstring());
		ss << std::boolalpha << snap;
		txt.append(L"\nConstant Step: " + ss.str());
		if (selected)
		{
			std::wstringstream s;
			selected->GetTransform()->Print(s);
			txt.append(s.str());
		}
		dynamic_cast<FontMaterial*>(mGo->GetMaterial())->SetText(txt.c_str());
	}

	void SelectNext()
	{
		objects = Application::instance().GetCurrentScene()->GetOpaqueQueue();

		if (objects.size() < 0) return;

		// reset last material before overwrite
		lastMaterial->SetDiffuse(lastColor);

		// get next object
		selected = objects[currObj % objects.size()];
		currObj++;

		if (selected)
		{
			// overwrite cached material
			lastMaterial = dynamic_cast<DiffuseMaterial*>(selected->GetMaterial());
			lastColor = lastMaterial->GetDiffuse();
			// set selected color
			lastMaterial->SetDiffuse(selColor);
		}
	}

	void SaveScene()
	{
		Logger::instance().Msg(LOG_GENERAL, L"Saving Scene...");
		try
		{
			std::ofstream file("./Assets/Scenes/scene.land", std::ios::out);

			for (auto go : objects)
			{
				file << go->GetName();
				go->GetTransform()->Print(file);
			}

			file.close();
		}
		catch (std::ofstream::failure &writeFail)
		{
			Logger::instance().Exception(writeFail.code(), writeFail.what(), L"Failed to open file on Scene Save.");
			return;
		}
		Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, L"Succesfully saved Scene File @ \"Assets/Scenes/scene.land\"");
	}

private:
	enum class Transform
	{
		TRANSLATE,
		ROTATE,
		SCALE
	};

	std::wstring EnumToString(const Transform t) const
	{
		switch (t)
		{
		case Transform::TRANSLATE:
			return std::wstring(L"Translate");
		case Transform::ROTATE:
			return std::wstring(L"Rotate");
		case Transform::SCALE:
			return std::wstring(L"Scale");
		default:
			return L"";
		}
	}

	Transform currTransform = Transform::TRANSLATE;
	float tSpeed = 10.0f;

	bool snap = true;

	GameObject* selected = nullptr;

	std::vector<GameObject*> objects;
	size_t currObj = 0;
	
	const XMFLOAT4 selColor = XMFLOAT4(0.0f, 0.3f, 0.9f, 1.0f);
	XMFLOAT4 lastColor;

	DiffuseMaterial* lastMaterial;
};

#endif
