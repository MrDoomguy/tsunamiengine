#pragma once

#include "../../Engine/Input/InputManager.h"
#include "../../Engine/Scene/AnimationComponent.h"
#include "../../Engine/Scene/GameObject.h"

class AnimationTest : public IScript
{
public:
	AnimationTest(std::string& scriptName)
		: IScript(scriptName)
	{
		fwd = 0;
		swd = 0;
		sld = 0;
	}
	virtual void Start() override
	{
		InputManager::instance().AddPlayer(0, Input::InputDevice::KEYBOARD);
		mAnimator = dynamic_cast<AnimationComponent*>(mGo->GetComponent(IComponent::Type::ANIMATION))->GetAnimator();
	}

	virtual void Update(float dt) override
	{
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_W))
		{
			fwd += dt;
			if (fwd > 1) fwd = 1;
		}

		// left
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_A))
		{
			swd -= dt;
			if (swd < -1) swd = -1;
		}

		// back
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_S))
		{
			fwd -= dt;
			if (fwd < -1) fwd = -1;
		}

		// right
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_D))
		{
			swd += dt;
			if (swd > 1) swd = 1;
		}
		std::cout << fwd << " " << swd << std::endl;
		mAnimator->SetBlendParams({ swd, fwd });
	}

protected:
	Animator* mAnimator;
	float fwd;
	float swd;
	float sld;
};