#ifndef _BalanceScript_H_
#define _BalanceScript_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Render/Data/GUIMaterial.h"
#include "../../Engine/General/Application.h"
#include "../../Engine/Scene/Rigidbody.h"

class BalanceScript : public IScript
{
public:
	explicit BalanceScript(std::string& scriptName, GUIMaterial* guiMaterial, SurfboardControl* sbctrl)
		: IScript(scriptName)
	{
		mat = guiMaterial;
		surfboardCtrl = sbctrl;

		waterForceCenter = XMFLOAT2(0.f, 0.f);
		centerOfMass = XMFLOAT2(0.f, 0.f);
	}

	virtual void Start() override
	{
		if ((rb = dynamic_cast<Rigidbody*>(mGo->GetComponent(IComponent::Type::PHYSIC))) == nullptr)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_ERROR, L"Failed to get rigidbody.");
		}
	}

	virtual void Update(float dt) override
	{
		uint32_t width = Application::instance().GetScreenWidth();
		uint32_t height = Application::instance().GetScreenHeight();
		 
		guiCenter = XMFLOAT2(width - (width/18.0f), height/2.0f);
		guiMin = XMFLOAT2(guiCenter.x - (width / 18.0f), guiCenter.y - (height / 2.0f));
		guiMax = XMFLOAT2(guiCenter.x + (width / 18.0f), guiCenter.y + (height / 2.0f));

		mat->SetLayerPosition(guiCenter, surfIdx);
		mat->SetLayerSize(XMFLOAT2(width/5.0f, height/2.75f), surfIdx);

		XMVECTOR force = XMLoadFloat2(&guiCenter);// +XMLoadFloat2(&surfboardCtrl->GetForce()) * 20;

		XMStoreFloat2(&centerOfMass, XMVectorLerp(XMLoadFloat2(&centerOfMass), force, dt * 10));

		centerOfMass.x = Clamp(centerOfMass.x, guiMin.x, guiMax.x);
		centerOfMass.y = Clamp(centerOfMass.y, guiMin.y, guiMax.y);

		mat->SetLayerTransparency(0.5f, centerOfMassIdx);
		mat->SetLayerPosition(centerOfMass, centerOfMassIdx);
		mat->SetLayerSize(XMFLOAT2(width/40.0f, width/40.0f), centerOfMassIdx);

		if (mat->GetLayerTransparency(rangeIdx) > alphaMax || mat->GetLayerTransparency(rangeIdx) < alphaMin)
		{
			rangeAlpha = Clamp(rangeAlpha, alphaMin, alphaMax);
			alphaSpeed = -alphaSpeed;
		}
		rangeAlpha += alphaSpeed*dt;
		mat->SetLayerTransparency(rangeAlpha, rangeIdx);
		mat->SetLayerPosition(guiCenter, rangeIdx);
		mat->SetLayerSize(XMFLOAT2(width/20.0f, width/20.0f), rangeIdx);

		// Do buoyancy shit
		/*XMVECTOR locBuoyancy = XMVector3Rotate(XMLoadFloat3(&rb->GetBuoyancyPoint()), XMQuaternionInverse(XMLoadFloat4(&mGo->GetTransform()->GetRotation()))) * 100;
		XMStoreFloat2(&waterForceCenter, XMVectorLerp(XMLoadFloat2(&waterForceCenter), XMLoadFloat2(&XMFLOAT2(guiCenter.x + XMVectorGetX(locBuoyancy), guiCenter.y + XMVectorGetZ(locBuoyancy))), dt * 10));

		waterForceCenter.x = Clamp(waterForceCenter.x, guiMin.x, guiMax.x);
		waterForceCenter.y = Clamp(waterForceCenter.y, guiMin.y, guiMax.y);

		mat->SetLayerTransparency(0.5f, waterForceIdx);
		mat->SetLayerPosition(waterForceCenter, waterForceIdx);
		mat->SetLayerSize(XMFLOAT2(width/40.0f, width/40.0f), waterForceIdx);*/
	}

	float Saturate(const float value)
	{
		float res = value;
		if (value > 1.0f) res = 1.0f;
		if (value < 0.0f) res = 0.0f;
		return res;
	}

	float Clamp(const float value, const float min, const float max)
	{
		float res = value;
		if (value > max) res = max;
		if (value < min) res = min;
		return res;
	}

private:
	GUIMaterial* mat;
	Rigidbody* rb;
	
	const size_t surfIdx = 3;
	const size_t centerOfMassIdx = 2;
	const size_t waterForceIdx = 1;
	const size_t rangeIdx = 0;

	float rangeAlpha = 0.1f;
	float alphaSpeed = 0.3f;

	const float alphaMin = 0.0f;
	const float alphaMax = 0.3f;

	XMFLOAT2 guiCenter;
	XMFLOAT2 guiMin;
	XMFLOAT2 guiMax;

	XMFLOAT2 centerOfMass;
	XMFLOAT2 waterForceCenter;

	SurfboardControl* surfboardCtrl;
};

#endif
