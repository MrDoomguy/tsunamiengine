#ifndef _AnimateLight_H_
#define _AnimateLight_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/General/Application.h"

class AnimateLight : public IScript
{
public:
	AnimateLight(std::string& scriptName)
		: IScript(scriptName) { }

	virtual void Start() override
	{

	}

	virtual void Update(float dt) override
	{
		auto l = Application::instance().GetCurrentScene()->GetDirLights();

		if (l[0] != nullptr)
		{
			l[0]->GetTransform()->Rotate(XMFLOAT3(0.0f, 1.0f, 0.0f), speed*dt);
		}
	}

private:
	float speed = 20.0f;
};

#endif
