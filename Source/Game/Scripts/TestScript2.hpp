#ifndef _TESTSCRIPT2_H_
#define _TESTSCRIPT2_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Scene/Rigidbody.h"
#include "../../Engine/Input/InputManager.h"

#include <iostream>

class TestScript2 : public IScript
{
public:
	TestScript2(std::string& scriptName)
		: IScript(scriptName) { }

	UINT8 ID;
	Input::InputDevice deviceType;
	float COMup = 0;
	float COMright = 0;
	float WFup = 0;
	float WFright = 0;
	bool running = false;
	timeval start;
	timeval end;
	timeval diff;
	float thresholdInSeconds = 0.1f;

	int x = 0;
	int y = 0;
	short deltaX = 0;
	short deltaY = 0;
	int w = 0;
	bool inX, inY;

	virtual void Start() override
	{
		std::cout << "Hello From Script Start func!" << std::endl;

		ID = 0;
		deviceType = Input::InputDevice::SMARTPHONE;

		if (InputManager::instance().AddPlayer(ID, Input::InputDevice::KEYBOARD) == false)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Oh ouh. No additional Keyboard!\n");
			//return;
		}

		if (InputManager::instance().AddDevice(ID, deviceType) == false)
		{
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Oh ouh. No added Smartphone!\n");
			//return;
		}
		else
		{
			running = true;
		}


		if (deviceType == Input::InputDevice::SMARTPHONE)
		{
			InputManager::instance().PrepareNetworkForGame();
			InputManager::instance().RunNetworkGame();
			InputManager::instance().gettimeofday(&start, nullptr);
			InputManager::instance().gettimeofday(&end, nullptr);
		}
	}

	virtual void Update(float dt) override
	{
		if (deviceType == Input::InputDevice::SMARTPHONE)
		{
			if (InputManager::instance().GetRange(ID, &COMup, &COMright))
			{
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Got Range:");
				printf("\tup: %f\tright: %f\n", COMup, COMright);
			}

			if (running)
			{
				InputManager::instance().gettimeofday(&end, nullptr);
				InputManager::instance().TimevalSubtract(&diff, &end, &start);
				if (diff.tv_sec > thresholdInSeconds || diff.tv_usec > thresholdInSeconds * 1000000)
				{
					InputManager::instance().gettimeofday(&start, nullptr);
					fakedWaterForce(&WFup, &WFright);
					Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "WaterForce:");
					printf("\tup: %f\tright: %f\n", WFup, WFright);
					InputManager::instance().SendData(WFup, WFright);
				}
			}

			if (InputManager::instance().KeyDown(ID, Input::InputKey::KEY_ESC))
			{
				if (running)
				{
					running = false;
					InputManager::instance().ResetNetworkState();
				}
				else
				{
					running = true;
					if (InputManager::instance().AddDevice(ID, deviceType) == false)
					{
						Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Oh ouh. Couldn't add Smartphone while playing the game!\n");
						return;
					}
					InputManager::instance().PrepareNetworkForGame();
					InputManager::instance().RunNetworkGame();
					//void(TestScript2::*myFakeWFfunc)(float*, float*) = &TestScript2::fakedWaterForce;
					//InputManager::instance().RunNetworkGame<TestScript2>(this, myFakeWFfunc);
					////InputManager::instance().RunNetworkGame<TestScript2>(this, &TestScript2::fakedWaterForce);
				}
			}
		}
		else
		{
			if (InputManager::instance().KeyDown(ID, Input::InputKey::ARROW_UP))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Pressed Arrow Up!");
			if (InputManager::instance().KeyUp(ID, Input::InputKey::ARROW_UP))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Released Arrow Up!");

			if (InputManager::instance().KeyDown(ID, Input::InputKey::ARROW_DOWN))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Pressed Arrow Down!");
			if (InputManager::instance().KeyUp(ID, Input::InputKey::ARROW_DOWN))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Released Arrow Down!");

			if (InputManager::instance().KeyPressed(ID, Input::InputKey::ARROW_LEFT))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Pressing Arrow Left!");
			if (InputManager::instance().KeyPressed(ID, Input::InputKey::ARROW_RIGHT))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Arrow Right!");

			if (InputManager::instance().KeyDown(ID, Input::InputKey::MOUSE_LEFT))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Pressing Left Mouse Button!");
			if (InputManager::instance().KeyUp(ID, Input::InputKey::MOUSE_LEFT))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Releasing Left Mouse Button!");

			if (InputManager::instance().KeyDown(ID, Input::InputKey::MOUSE_RIGHT))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Pressing Right Mouse Button!");
			if (InputManager::instance().KeyUp(ID, Input::InputKey::MOUSE_RIGHT))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Releasing Right Mouse Button!");

			if (InputManager::instance().KeyDown(ID, Input::InputKey::MOUSE_MIDDLE))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Pressing Middle Mouse Button!");
			if (InputManager::instance().KeyUp(ID, Input::InputKey::MOUSE_MIDDLE))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Releasing Middle Mouse Button!");

			if (InputManager::instance().KeyPressed(ID, Input::InputKey::MOUSE_X1))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Holding X1 Mouse Button!");
			if (InputManager::instance().KeyPressed(ID, Input::InputKey::MOUSE_X2))
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Holding X2 Mouse Button!");

			if (InputManager::instance().KeyUp(ID, Input::InputKey::KEY_ESC))
			{
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Hit ESC!");
				InputManager::instance().AddDevice(ID, Input::InputDevice::MOUSE);
			}

			if (InputManager::instance().KeyUp(ID, Input::InputKey::KEY_DEL))
			{
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Hit DEL!");
				InputManager::instance().RemoveDevice(ID, Input::InputDevice::KEYBOARD);
			}

			if (InputManager::instance().KeyUp(ID, Input::InputKey::KEY_D))
			{
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Hit D!");
				InputManager::instance().RemoveDevice(ID, Input::InputDevice::MOUSE);
			}

			InputManager::instance().GetXYPosWheel(ID, &x, &y, &deltaX, &deltaY, &w, &inX, &inY);
			if ((deltaX) || (deltaY) || (w))
			{
				printf("\tMouse Input!\n\tX = %d    \tInScreen = %s  \tDelta: %d\n\tY = %d    \tInScreen = %s  \tDelta: %d\n\tWheelDelta = %d\n",
					x, inX ? "true" : "false", deltaX, y, inY ? "true" : "false", deltaY, w);
			}

			if (InputManager::instance().KeyDown(ID, Input::InputKey::KEY_RETURN))
			{
				Logger::instance().Msg(LOG_LAYER::LOG_GENERAL, "Hit Return!");
				InputManager::instance().PrintAllRegisteredDevices();
			}
		}
	}


	void fakedWaterForce(float *mUp, float* mRight)
	{
		*mUp = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 2.f - 1.f;
		*mRight = (static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 2.f - 1.f;
	}

protected:

};

#endif
#pragma once
