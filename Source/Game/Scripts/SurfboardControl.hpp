#pragma once

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Scene/Rigidbody.h"
#include "../../Engine/Input/InputManager.h"
#include "../../Engine/Scene/AnimationComponent.h"

class SurfboardControl : public IScript
{
public:
	SurfboardControl(std::string& scriptName, float force, float offset, XMFLOAT2 corrForce, float dampingForce, Animator* surferAnimator)
		: IScript(scriptName) 
	{
		mForce = force;
		mCorrForce = corrForce;
		tmpAxis = XMFLOAT2(0, 0);
		mAnimator = surferAnimator;
		animParam = XMFLOAT2(0, 0);
		mOffset = offset; 
		mDampingForce = dampingForce;
	}
	void Reset()
	{
		tmpAxis = XMFLOAT2(0, 0);
		animParam = XMFLOAT2(0, 0);
	}
	virtual void Start() override
	{
		mRigidbody = dynamic_cast<Rigidbody*>(mGo->GetComponent(IComponent::Type::PHYSIC));
		mTransform = mGo->GetTransform();

		ID = 0;
		deviceType = Input::InputDevice::SMARTPHONE;

		if (!InputManager::instance().AddPlayer(ID, Input::InputDevice::KEYBOARD))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Oh ouh. No additional Keyboard!\n");
		}

		if (!InputManager::instance().AddDevice(ID, deviceType))
		{
			Logger::instance().Msg(LOG_LAYER::LOG_WARNING, "Oh ouh. No added Smartphone!\n");
		}
		else
		{
			running = true;
		}


		if (deviceType == Input::InputDevice::SMARTPHONE)
		{
			InputManager::instance().PrepareNetworkForGame();
			InputManager::instance().RunNetworkGame();
			InputManager::instance().gettimeofday(&start, nullptr);
			InputManager::instance().gettimeofday(&end, nullptr);
		}
	}

	virtual void Update(float dt) override
	{
		//XMStoreFloat3(&animParam, XMVectorLerp(XMLoadFloat3(&animParam), XMLoadFloat3(&XMFLOAT3(asin(f.y) / 3.1415f * 6, asin(mTransform->GetRight().y) / 3.1415f * 6, (atan2(f.z, f.x) - atan2(lastForward.z, lastForward.x)) / 3.1415f * 2)), dt * 10));
		animParam = XMFLOAT2(asin(mTransform->GetForward().y) / 3.1415f * 6, asin(mTransform->GetRight().y) / 3.1415f * 6);
		mAnimator->SetBlendParams({ animParam.x, animParam.y });
	}

	virtual void FixedUpdate(float dt) override
	{
		// stabilisiation
		XMVECTOR va = XMLoadFloat3(&mRigidbody->GetAngularVelocity());
		XMVECTOR v = XMLoadFloat3(&mRigidbody->GetVelocity());

		XMFLOAT3 xA, zA;

		XMVECTOR xAxis = XMLoadFloat3(&mGo->GetTransform()->GetForward()) * XMLoadFloat3(&XMFLOAT3(1, 0, 1)) * 0.2f;
		XMVECTOR zAxis = XMLoadFloat3(&mGo->GetTransform()->GetRight()) * XMLoadFloat3(&XMFLOAT3(1, 0, 1)) * 0.5f;

		tmpAxis.x *= 1 - dt;
		tmpAxis.y *= 1 - dt;

		XMStoreFloat3(&zA, zAxis);

		if (deviceType == Input::InputDevice::SMARTPHONE && running)
		{
			if (InputManager::instance().GetRange(ID, &COMup, &COMright))
			{
				tmpAxis.x = COMright;
				tmpAxis.y = -COMup;
			}
		}
		else
		{
			// forward
			if (InputManager::instance().KeyPressed(ID, Input::InputKey::KEY_W))
			{
				tmpAxis.y -= dt;
				if (tmpAxis.y < -1) tmpAxis.y = -1;
			}

			// back
			if (InputManager::instance().KeyPressed(ID, Input::InputKey::KEY_S))
			{
				tmpAxis.y += dt;
				if (tmpAxis.y > 1) tmpAxis.y = 1;
			}
			tmpAxis.y += 0.f;

			// right
			if (InputManager::instance().KeyPressed(ID, Input::InputKey::KEY_A))
			{
				tmpAxis.x -= dt;
				if (tmpAxis.x < -1) tmpAxis.x = -1;
			}

			// left
			if (InputManager::instance().KeyPressed(ID, Input::InputKey::KEY_D))
			{
				tmpAxis.x += dt;
				if (tmpAxis.x > 1) tmpAxis.x = 1;
			}
		}

		// Add force of the surfer on the projected position of the com

		XMFLOAT2 tA = tmpAxis;

		tA.y += mOffset;

		XMStoreFloat3(&xA, xAxis * tA.x);
		XMStoreFloat3(&zA, zAxis * tA.y);

		mRigidbody->AddForce(XMFLOAT3(0, -mForce, 0), xA);
		mRigidbody->AddForce(XMFLOAT3(0, -mForce, 0), zA);


		// Calc the desired up vector
		
		xAxis = XMVector3Normalize(xAxis);
		zAxis = XMVector3Normalize(zAxis);

		XMVECTOR l = XMVector2LengthSq(XMLoadFloat2(&tA));
		if(XMVectorGetX(l) > 1) XMStoreFloat2(&tA, XMVector2Normalize(XMLoadFloat2(&tA)));

		float f = 1 - tA.x * tA.x - tA.y * tA.y;
		if (f < 0.f) f = 0.f;
		XMVECTOR upAxis = tA.x * xAxis + tA.y * zAxis + sqrtf(f) * XMLoadFloat3(&XMFLOAT3(0, 1, 0));


		// stabilisate the board to maintain the desired up vector

		XMVECTOR corrForce = XMLoadFloat3(&XMFLOAT3(mCorrForce.x, mCorrForce.y, mCorrForce.x));

		XMVECTOR yAxis = XMLoadFloat3(&mGo->GetTransform()->GetUp());
		XMFLOAT3 corrVal;

		XMStoreFloat3(&corrVal, XMVector3Cross(upAxis, (upAxis - yAxis) * corrForce));
		mRigidbody->AddTorque(corrVal);


		// Stabilizate surfer with damping

		XMStoreFloat3(&corrVal, -va * mDampingForce);
		mRigidbody->AddTorque(corrVal);
	}

protected:
	Rigidbody* mRigidbody;
	Animator* mAnimator;
	TransformComponent* mTransform;

	float mForce;
	XMFLOAT2 mCorrForce;
	float mDampingForce;
	float mOffset;

	XMFLOAT2 tmpAxis;
	XMFLOAT2 animParam;

	UINT8 ID;
	Input::InputDevice deviceType;
	float COMup = 0;
	float COMright = 0;
	bool running = false;
	timeval start;
	timeval end;
	timeval diff;
	float thresholdInSeconds = 0.01f;
};