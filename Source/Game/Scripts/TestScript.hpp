#ifndef _TESTSCRIPT_H_
#define _TESTSCRIPT_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Scene/Rigidbody.h"
#include "../../Engine/Input/InputManager.h"

class TestScript : public IScript
{
public:
	TestScript(std::string& scriptName)
		: IScript(scriptName) { }

	virtual void Start() override
	{
		mRigidbody = dynamic_cast<Rigidbody*>(mGo->GetComponent(IComponent::Type::PHYSIC));
		mTransform = dynamic_cast<TransformComponent*>(mGo->GetComponent(IComponent::Type::TRANSFORM)); 
		
		InputManager::instance().AddPlayer(0, Input::InputDevice::KEYBOARD);
	}

	virtual void Update(float dt) override
	{
		float velocity = 5.f;

		if (mRigidbody->IsKinematic())
		{
			// forward
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_W))
			{
				mTransform->Walk(velocity*dt);
			}

			// left
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_A))
			{
				mTransform->Strafe(-velocity*dt);
			}

			// back
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_S))
			{
				mTransform->Walk(-velocity*dt);
			}

			// right
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_D))
			{
				mTransform->Strafe(velocity*dt);
			}

			// down
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_Q))
			{
				mTransform->Fly(-velocity*dt);
			}

			// up
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_E))
			{
				mTransform->Fly(velocity*dt);
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_DOWN))
			{
				mTransform->Roll(-velocity*dt);
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_UP))
			{
				mTransform->Roll(velocity*dt);
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_LEFT))
			{
				mTransform->Pitch(-velocity*dt);
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_RIGHT))
			{
				mTransform->Pitch(velocity*dt);
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_PLUS))
			{
				mTransform->Yaw(velocity*dt);
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_MINUS))
			{
				mTransform->Yaw(-velocity*dt);
			}
		}
		else
		{
			// forward
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_W))
			{
				mRigidbody->AddImpulse(XMFLOAT3(0, 0, velocity*dt));
			}

			// left
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_A))
			{
				mRigidbody->AddImpulse(XMFLOAT3(-velocity*dt, 0, 0));
			}

			// back
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_S))
			{
				mRigidbody->AddImpulse(XMFLOAT3(0, 0, -velocity*dt));
			}

			// right
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_D))
			{
				mRigidbody->AddImpulse(XMFLOAT3(velocity*dt, 0, 0));
			}

			// down
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_Q))
			{
				mRigidbody->AddImpulse(XMFLOAT3(0, -velocity*dt, 0));
			}

			// up
			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_E))
			{
				mRigidbody->AddImpulse(XMFLOAT3(0, velocity*dt, 0));
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_DOWN))
			{
				XMFLOAT3 pos = mGo->GetTransform()->GetPosition();
				mRigidbody->AddImpulse(XMFLOAT3(0, velocity*dt, 0), XMFLOAT3(0, 0, 1));
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_UP))
			{
				XMFLOAT3 pos = mGo->GetTransform()->GetPosition();
				mRigidbody->AddImpulse(XMFLOAT3(0, -velocity*dt, 0), XMFLOAT3(0, 0, 1));
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_LEFT))
			{
				XMFLOAT3 pos = mGo->GetTransform()->GetPosition();
				mRigidbody->AddImpulse(XMFLOAT3(-velocity*dt, 0, 0), XMFLOAT3(0, 1, 0));
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_RIGHT))
			{
				XMFLOAT3 pos = mGo->GetTransform()->GetPosition();
				mRigidbody->AddImpulse(XMFLOAT3(velocity*dt, 0, 0), XMFLOAT3(0, 1, 0));
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_PLUS))
			{
				XMFLOAT3 pos = mGo->GetTransform()->GetPosition();
				mRigidbody->AddImpulse(XMFLOAT3(0, 0, velocity*dt), XMFLOAT3(1, 0, 0));
			}

			if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_MINUS))
			{
				XMFLOAT3 pos = mGo->GetTransform()->GetPosition();
				mRigidbody->AddImpulse(XMFLOAT3(0, 0, -velocity*dt), XMFLOAT3(1, 0, 0));
			}
		}
	}

protected:
	TransformComponent* mTransform;
	Rigidbody* mRigidbody;
};

#endif
