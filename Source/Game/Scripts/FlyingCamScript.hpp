#pragma once

#include "../../Engine/Scene/GameObject.h"

class FlyingCamScript : public IScript
{
public:
	FlyingCamScript(std::string& scriptName, TransformComponent* reference, XMFLOAT3 offset, float movementSpeed, float rotationSpeed, const std::pair<XMFLOAT3, XMFLOAT3>& rotOffset, float rotSpeed, const std::vector<std::pair<XMFLOAT3, XMFLOAT3>>& camPos = {})
		: IScript(scriptName), mReference(reference), mOffset(offset), mMovementSpeed(movementSpeed), mRotationSpeed(rotationSpeed), mCamState(camPos), mRotSpeed(rotSpeed), mRotOffset(rotOffset) { }

	virtual void Start() override
	{
		InputManager::instance().AddPlayer(0, Input::InputDevice::KEYBOARD);
		mTransform = mGo->GetTransform();
	}

	virtual void Update(float dt) override
	{

		if (dt > 1) return;

		if (InputManager::instance().KeyDown((UINT8)0, Input::InputKey::KEY_C))
		{
			mState = (mState + 1) % (mCamState.size() + 2);
			mRotT = 0.f;
		}

		if (mState == 0)
		{
			XMVECTOR target = XMLoadFloat3(&mOffset) + XMLoadFloat3(&mReference->GetWorldPosition());
			XMFLOAT3 newPos;
			XMStoreFloat3(&newPos, XMVectorLerp(XMLoadFloat3(&mTransform->GetPosition()), target, dt * mMovementSpeed));

			mTransform->SetPosition(newPos);

			XMVECTOR lookAt = XMLoadFloat4(&mTransform->GetLookAt(mReference->GetWorldPosition()));
			XMFLOAT4 newRot;
			XMStoreFloat4(&newRot, XMQuaternionSlerp(XMLoadFloat4(&mTransform->GetRotation()), lookAt, dt * mRotationSpeed));
			mTransform->SetRotation(newRot);
		}
		else if(mState <= mCamState.size())
		{
			std::pair<XMFLOAT3, XMFLOAT3> state = mCamState[mState - 1];
			mTransform->SetPosition(state.first);
			mTransform->LookAt(state.second);
		}
		else
		{
			XMFLOAT3 p;
			XMStoreFloat3(&p, XMVector3Rotate(XMLoadFloat3(&mRotOffset.first) - XMLoadFloat3(&mRotOffset.second), XMQuaternionRotationAxis(XMLoadFloat3(&XMFLOAT3(0, 1, 0)), mRotT)));
			mTransform->SetPosition(p);
			mTransform->LookAt(mRotOffset.second);
			mRotT += dt * mRotSpeed;
			if (mRotT > XM_PI) mRotT -= XM_PI * 2;
		}
	}

private:
	int mState = 0;
	float mRotT;
	std::vector<std::pair<XMFLOAT3, XMFLOAT3>> mCamState;
	TransformComponent* mTransform;
	TransformComponent* mReference;
	XMFLOAT3 mOffset;
	std::pair<XMFLOAT3, XMFLOAT3> mRotOffset;
	float mRotSpeed;
	float mMovementSpeed;
	float mRotationSpeed;
};


