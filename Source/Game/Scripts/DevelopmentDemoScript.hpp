#pragma once

#include "../../Engine/Scene/GameObject.h"
#include "TraceFlyScript.hpp"

class DevelopmentDemoScript : public IScript
{
public:
	DevelopmentDemoScript(std::string& scriptName, const std::vector<GameObject*>& ambient, TraceFlyScript* traceFlyScript, const std::vector<std::pair<XMFLOAT3, XMFLOAT3>>& path)
		: IScript(scriptName), mAmbient(ambient) 
	{
		mTraceFlyScript = traceFlyScript;
		mPath = path;
	}

	virtual void Start() override
	{
		for (auto go : mAmbient)
		{
			go->Enable(false);
		}
	}

	virtual void Update(float dt) override
	{
		if (!mInitAmbient && mTraceFlyScript->IsFinished())
		{
			mInitAmbient = true;
			mTraceFlyScript->SetPath(mPath, 15.f);
		}

		if (mInitAmbient)
		{
			int idx = min(mTraceFlyScript->GetIdx() - 2, (int)mAmbient.size() - 1);
			if (mIdx != idx && idx >= 0)
			{
				mIdx = idx;
				mAmbient[mIdx]->Enable(true);
			}
		}
	}

private:
	std::vector<GameObject*> mAmbient;
	TraceFlyScript* mTraceFlyScript;
	bool mInitAmbient = false;
	std::vector<std::pair<XMFLOAT3, XMFLOAT3>> mPath;
	int mIdx = -1;
};


#pragma once
