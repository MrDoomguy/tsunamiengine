#ifndef _OnScreenTimer_H_
#define _OnScreenTimer_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Render/Data/FontMaterial.h"
#include "../../Engine/General/Timer.h"
#include "../../Engine/General/Application.h"

class OnScreenTimer : public IScript
{
public:
	OnScreenTimer(std::string& scriptName, float padding)
		: IScript(scriptName) 
	{
		mPadding = padding;
	}

	virtual void Awake() override
	{
		mat = dynamic_cast<FontMaterial*>(mGo->GetMaterial());
		mat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_LEADING);
		mat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
		mat->SetText(text.c_str());
		mat->SetFontName(L"Another X Display tfb");
		mat->SetFontSize(36.0f);		
		mat->SetRect((float)Application::instance().GetScreenWidth() * mPadding, (float)Application::instance().GetScreenHeight() * mPadding, (float)Application::instance().GetScreenWidth() * (1.f - mPadding), (float)Application::instance().GetScreenHeight() * (1.f - mPadding));
		mat->SetSolidColor(D2D1::ColorF(.97f, 1.f, .14f));
	}

	virtual void Start() override
	{ 
		timer.Reset();
	}

	virtual void Update(float dt) override
	{
		if (!isPaused)
		{
			std::wstringstream ss;
			ss << std::fixed << std::setprecision(2) << timer.TotalTime();

			std::wstring tmp = text;
			tmp.append(ss.str());

			mat->SetText(tmp.c_str());
		}
	}

	void ResetTimer()
	{
		timer.Reset();
	}

	void StartTimer()
	{
		timer.Start();
	}

	void SetPaused(const bool paused)
	{
		isPaused = paused;
	}

private:
	FontMaterial* mat;
	Timer timer;
	std::wstring text;
	bool isPaused = false;
	float mPadding;
};

#endif
