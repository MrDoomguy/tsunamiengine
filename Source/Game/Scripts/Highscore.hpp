﻿#pragma once

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Render/Data/FontMaterial.h"
#include "../../Engine/General/Application.h"
#include "../../Engine/General/HighscoreLoader.h"
#include "../../Engine/Input/InputManager.h"

class Highscore : public IScript
{
public:
	enum class UIState : std::uint8_t
	{
		HIGHSCORE = 0x0,
		UI = 0x1,
		LIST = 0x2
	};


	Highscore(std::string& scriptName, GameObject* highscoreListObj, const std::wstring& highscorePath, int maxEntries, float padding)
		: IScript(scriptName) 
	{
		mLoader.Init(highscorePath, maxEntries);
		mHighscoreListObj = highscoreListObj;
		mPadding = padding;
	}

	virtual void Awake() override
	{
		mat = dynamic_cast<FontMaterial*>(mGo->GetMaterial());
		mat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_TRAILING);
		mat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
		mat->SetFontName(L"Another X Display tfb");
		mat->SetFontSize(36.0f);
		mat->SetRect((float)Application::instance().GetScreenWidth() * mPadding, (float)Application::instance().GetScreenHeight() * mPadding, (float)Application::instance().GetScreenWidth() * (1.f - mPadding), (float)Application::instance().GetScreenHeight() * (1.f - mPadding));
		mat->SetSolidColor(D2D1::ColorF(.97f, 1.f, .14f));
		mat2 = dynamic_cast<FontMaterial*>(mHighscoreListObj->GetMaterial());
		mat2->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_TRAILING);
		mat2->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
		mat2->SetFontName(L"Another X Display tfb");
		mat2->SetFontSize(24.0f);
		mat2->SetRect((float)Application::instance().GetScreenWidth() * mPadding, (float)Application::instance().GetScreenHeight() * mPadding, (float)Application::instance().GetScreenWidth() * 0.8f, (float)Application::instance().GetScreenHeight() * (1.f - mPadding));
		mat2->SetSolidColor(D2D1::ColorF(.97f, 1.f, .14f));
		HighscoreString();
	}

	virtual void Start() override
	{
		InputManager::instance().AddPlayer(0, Input::InputDevice::KEYBOARD);
	}

	virtual void Update(float dt) override
	{
		char v;
		switch (mState)
		{
		case UIState::HIGHSCORE:

			break;

		case UIState::UI:
			
			if (InputManager::instance().KeyDown((UINT8)0, Input::InputKey::KEY_RETURN))
			{
				if (mEntry.second >= 0)
				{
					mLoader.AddEntry(mEntry);
					mLoader.WriteFile();
					ListString();
				}
				else
				{
					HighscoreString();
				}
			}
			else if (InputManager::instance().KeyDown((UINT8)0, Input::InputKey::KEY_ESC))
			{
				HighscoreString();
			}			
			else if (mEntry.second >= 0)
			{
				if (InputManager::instance().KeyDown((UINT8)0, Input::InputKey::KEY_BS))
				{
					if (mFirst)
					{
						mEntry.first = "";
						mFirst = false;
					}
					else
					{
						if (mEntry.first != "") mEntry.first.pop_back();
					}
				}
				else
				{
					v = (char)InputManager::instance().GetRawValue(0);
					if (v >= 'A' && v <= 'Z' && mEntry.first.size() < 10)
					{
						if (mFirst)
						{
							mEntry.first = "";
							mFirst = false;
						}

						mEntry.first.push_back(v);
					}
				}

				//mCursorTick += dt;
				//if (mCursorTick > 1.f) mCursorTick = mCursorTick - 1.f;

				std::wstringstream ss;
				ss << mEntry.first.c_str();

				ss << "_";

				std::wstring tmp;
				tmp = mText;
				tmp.append(ss.str());
					
				mat->SetText(tmp.c_str());
				
			}
			break;

		case UIState::LIST:
			if (InputManager::instance().KeyDown((UINT8)0, Input::InputKey::KEY_RETURN) || InputManager::instance().KeyDown((UINT8)0, Input::InputKey::KEY_ESC))
			{
				HighscoreString();
			}
			break;
		}
	}

	void ShowList(int score)
	{
		mHighscoreListObj->Enable(false);
		mat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_CENTER);
		mat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
		mat->SetRect((float)Application::instance().GetScreenWidth() * mPadding, (float)Application::instance().GetScreenHeight() * mPadding, (float)Application::instance().GetScreenWidth() * (1.f - mPadding), (float)Application::instance().GetScreenHeight() * (1.f - mPadding));
		mat->SetFontSize(36.0f);

		if (score > mLoader.GetMinScore().second)
		{
			int i = 0;
			for (; i < mLoader.GetList().size(); i++)
			{
				if (score > mLoader.GetList()[i].second) break;
			}

			if (i == 0)
			{
				mText = L"New Highscore!\nEnter Your Name:\n";
			}
			else
			{
				std::wstringstream ss;
				ss << (i + 1);
				mText = ss.str();
				mText.append(L". Place!\nEnter Your Name:\n");
			}
			mState = UIState::UI;
			mEntry.second = score;
			mEntry.first = "PLAYER";

			std::wstringstream ss;
			ss << mEntry.first.c_str();
			std::wstring tmp;
			tmp = mText;
			tmp.append(ss.str());
			mat->SetText(tmp.c_str());
			mFirst = true;
			//mCursorTick = 0.f;
		}
		else
		{
			mat->SetText(L"Press ENTER to start.");
			mEntry.second = -1;
			mState = UIState::UI;
		}
	}

	void ListString()
	{
		mHighscoreListObj->Enable(true);
		mat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_LEADING);
		mat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
		mat->SetRect((float)Application::instance().GetScreenWidth() * 0.2f, (float)Application::instance().GetScreenHeight() * mPadding, (float)Application::instance().GetScreenWidth() * (1.f - mPadding), (float)Application::instance().GetScreenHeight() * (1.f - mPadding));
		mat->SetFontSize(24.0f);

		std::wstringstream sL;
		std::wstringstream sR;
		
		for (auto entry : mLoader.GetList())
		{
			sL << entry.first.c_str() << std::endl;
			sR << entry.second << std::endl;
		}

		mText = L"Highscore:\n\n";
		mText.append(sL.str());

		mat->SetText(mText.c_str());

		mText = L"\n\n";
		mText.append(sR.str());

		mat2->SetText(mText.c_str());

		mState = UIState::LIST;
	}

	void HighscoreString()
	{
		mHighscoreListObj->Enable(false);
		mat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_TRAILING);
		mat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT::DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
		mat->SetRect((float)Application::instance().GetScreenWidth() * mPadding, (float)Application::instance().GetScreenHeight() * mPadding, (float)Application::instance().GetScreenWidth() * (1 - mPadding), (float)Application::instance().GetScreenHeight() * (1 - mPadding));
		mat->SetFontSize(36.0f);

		HighscoreEntry entry = mLoader.GetHighscore();

		std::wstringstream ss;

		ss << entry.first.c_str() << ": " << entry.second;

		mText = L"Highscore\n";
		mText.append(ss.str());

		mat->SetText(mText.c_str());
		mState = UIState::HIGHSCORE;
	}

	UIState GetState()
	{
		return mState;
	}

private:
	FontMaterial* mat;
	FontMaterial* mat2;
	std::wstring mText;
	HighscoreEntry mEntry;
	bool mFirst;
	//float mCursorTick;
	float mPadding;
	GameObject* mHighscoreListObj;

	HighscoreLoader mLoader; 

	UIState mState;
	
};