#pragma once

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Input/InputManager.h"
#include "../../Engine/Scene/SDFParticleSystem.h"

class ParticleTest : public IScript
{
public:
	ParticleTest(std::string& scriptName, SDFParticleSystem* ps)
		: IScript(scriptName) 
	{
		mPs = ps;
		mInitProps.curvatureMin = .5f;
		mInitProps.curvatureMax = .7f;
		mInitProps.turbulenceMin = 10.f;
		mInitProps.turbulenceMax = 30.f;
		mInitProps.obstVelMin = 0.f;
		mInitProps.obstVelMax = .01f;
		mInitProps.energyMin = 10.f;
		mInitProps.energyMax = 100.f;
		mInitProps.maxTu = 100;
		mInitProps.maxWc = 100;
		mInitProps.maxOb = 1000;
	}

	virtual void Start() override
	{
		InputManager::instance().AddPlayer(0, Input::InputDevice::KEYBOARD);
	}

	virtual void FixedUpdate(float dt) override
	{
		// forward
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_W))
		{
			mInitProps.curvatureMin += .01f;
		}

		// left
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_A))
		{
			mInitProps.curvatureMax -= .01f;
		}

		// back
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_S))
		{
			mInitProps.curvatureMin -= .01f;
		}

		// right
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_D))
		{
			mInitProps.curvatureMax += .01f;
		}

		// forward
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_UP))
		{
			mInitProps.energyMin += 1.f;
		}

		// left
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_DOWN))
		{
			mInitProps.energyMin -= 1.f;
		}

		// back
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_LEFT))
		{
			mInitProps.energyMax -= 1.f;
		}

		// right
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::ARROW_RIGHT))
		{
			mInitProps.energyMax += 1.f;
		}
		// right
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_Q))
		{
			mInitProps.maxWc -= 1.f;
		}
		// right
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_E))
		{
			mInitProps.maxWc += 1.f;
		}
		std::cout << "curv: " << mInitProps.curvatureMin << " " << mInitProps.curvatureMax << std::endl;
		std::cout << "energy: " << mInitProps.energyMin << " " << mInitProps.energyMax << std::endl;
		std::cout << "count: " << mInitProps.maxWc << std::endl;

		mPs->SetInitProps(mInitProps);

	}

private:
	SDFParticleSystem* mPs;
	SDFParticleSystem::InitProps mInitProps;
};

