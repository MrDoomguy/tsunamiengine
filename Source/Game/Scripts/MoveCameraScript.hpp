#ifndef _MoveCameraScript_H_
#define _MoveCameraScript_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Input/InputManager.h"

class MoveCameraScript : public IScript
{
public:
	MoveCameraScript(std::string& scriptName)
		: IScript(scriptName) { }

	virtual void Start() override
	{
		InputManager::instance().AddPlayer(0, Input::InputDevice::KEYBOARD);
		InputManager::instance().AddDevice(0, Input::InputDevice::MOUSE);
		InputManager::instance().GetXYPosWheel(&oldX, &oldY, nullptr, nullptr, nullptr, nullptr, nullptr);
	}

	virtual void Update(float dt) override
	{
		// forward
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_W))
		{
			mGo->GetTransform()->Walk(velocity*dt);
		}

		// left
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_A))
		{
			mGo->GetTransform()->Strafe(-velocity*dt);
		}

		// back
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_S))
		{
			mGo->GetTransform()->Walk(-velocity*dt);
		}

		// right
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_D))
		{
			mGo->GetTransform()->Strafe(velocity*dt);
		}

		// down
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_Q))
		{
			mGo->GetTransform()->Fly(-velocity*dt);
		}

		// up
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::KEY_E))
		{
			mGo->GetTransform()->Fly(velocity*dt);
		}

		// rotate Y
		if (InputManager::instance().KeyPressed((UINT8)0, Input::InputKey::MOUSE_RIGHT))
		{
			pressed = true;
			int x, y;
			InputManager::instance().GetXYPosWheel((UINT8)0, &x, &y, nullptr, nullptr, nullptr, nullptr, nullptr);

			//std::cout << (float)(oldX - x) << std::endl;

			mGo->GetTransform()->Rotate(XMFLOAT3(0.f, 1.f, 0.f), (float)(x - oldX) * velocity * dt);
			mGo->GetTransform()->Pitch((float)(y - oldY) * velocity * dt);
			oldX = x;
			oldY = y;
		}

		if (InputManager::instance().KeyUp((UINT8)0, Input::InputKey::MOUSE_RIGHT))
			pressed = false;

		if(!pressed)
			InputManager::instance().GetXYPosWheel((UINT8)0, &oldX, &oldY, nullptr, nullptr, nullptr, nullptr, nullptr);
	}

private:
	float velocity = 20.0f;
	int oldX = 0, oldY = 0;
	bool pressed = false;
};

#endif
