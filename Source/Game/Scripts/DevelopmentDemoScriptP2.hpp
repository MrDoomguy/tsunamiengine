#pragma once

#include "../../Engine/Scene/GameObject.h"
#include "TraceFlyScript.hpp"

class DevelopmentDemoScriptP2 : public IScript
{
public:
	DevelopmentDemoScriptP2(std::string& scriptName, TraceFlyScript* traceFlyScript, GameObject* particleSystem, GameObject* surfer, GameObject* surfboard)
		: IScript(scriptName), mTraceFlyScript(traceFlyScript), mParticleSystem(particleSystem), mSurfer(surfer), mSurfboard(surfboard)
	{
	}

	virtual void Start() override
	{
		mParticleSystem->Enable(false);
		mSurfer->Enable(false);
		mSurfboard->Enable(false);
	}

	virtual void Update(float dt) override
	{
		int idx = mTraceFlyScript->GetIdx();
		if (mIdx != idx)
		{
			mIdx = idx;
			if (mIdx == 6)
			{
				mParticleSystem->Enable(true);
			}
			else if (mIdx == 12)
			{
				mSurfboard->Enable(true);
				mSurfer->Enable(true);
			}
		}
	}

private:
	GameObject* mParticleSystem;
	GameObject* mSurfer;
	GameObject* mSurfboard;
	TraceFlyScript* mTraceFlyScript;
	int mIdx = -1;
};


#pragma once
