#ifndef _GUISCRIPT_HPP_
#define _GUISCRIPT_HPP_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Render/Data/GUIMaterial.h"

class GUIScript : public IScript
{
public:
	GUIScript(std::string& scriptName)
		: IScript(scriptName) { }

	virtual void Start() override
	{
		mat = dynamic_cast<GUIMaterial*>(mGo->GetMaterial());
		//mat->SetPosition(XMFLOAT2(0.325f, 0.3f));
		//mat->SetSize(XMFLOAT2(0.3f, 0.475f));
		//mat->SetTransparency(alpha);
	}

	virtual void Update(float dt) override
	{
		/*if ((mat->GetPosition().x > (1.0f - 0.475f)) || (mat->GetPosition().x < 0.0f))
		{
			speed = -speed;
		}

		mat->SetPosition(XMFLOAT2(mat->GetPosition().x + speed*dt, mat->GetPosition().y));*/

		/*if (mat->GetTransparency() > 1.0f || mat->GetTransparency() < 0.0f)
		{
			alpha = Saturate(alpha);
			speed = -speed;
		}
		alpha += speed*dt;
		mat->SetTransparency(alpha);*/
	}

	float Saturate(const float value)
	{
		float res;
		if (value > 1.0f) res = 1.0f;
		if (value < 0.0f) res = 0.0f;
		return res;
	}

private:
	GUIMaterial* mat;
	float speed = 0.5f;
	float alpha = 0.5f;
};

#endif
