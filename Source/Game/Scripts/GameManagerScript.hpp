#ifndef _GameManagerScript_H_
#define _GameManagerScript_H_

#include "../../Engine/Scene/GameObject.h"
#include "../../Engine/Scene/Rigidbody.h"
#include "../../Engine/Input/InputManager.h"
#include "../../Game/Scripts/OnScore.hpp"
#include "../../Game/Scripts/OnScreenTimer.hpp"
#include "../../Game/Scripts/FlyingCamScript.hpp"
#include "../../Game/Scripts/Highscore.hpp"
#include "../../Engine/Scene/OBBCollider.h"

class GameManagerScript : public IScript
{
public:
	GameManagerScript(std::string& scriptName, GameObject* surfboard, GameObject* surfer, OnScreenTimer* pTimerScript, OnScore* pScoreScript, SurfboardControl* pBoardScript, float upLimit, float minVel, float scoreMul, Highscore* highscoreScript, FlyingCamScript* camScript = nullptr)
		: IScript(scriptName)
	{
		mSurfboard = surfboard;
		mSurfer = surfer;
		timerScript = pTimerScript;
		scoreScript = pScoreScript;
		mMinVel = minVel;
		mScoreMul = scoreMul;
		mUpLimit = upLimit;
		mCamScript = camScript;
		mBoardScript = pBoardScript;
		mHighscoreScript = highscoreScript;
	}

	virtual void Start() override
	{
		mBoardTransform = mSurfboard->GetTransform();
		mBoardStartPos = mBoardTransform->GetPosition();
		mBoardStartRot = mBoardTransform->GetRotation();

		mSurferTransform = mSurfer->GetTransform();
		mSurferStartPos = mSurferTransform->GetPosition();
		mSurferStartScale = mSurferTransform->GetScale();
		mSurferStartRot = mSurferTransform->GetRotation();

		mBoardRigidbody = dynamic_cast<Rigidbody*>(mSurfboard->GetComponent(IComponent::Type::PHYSIC));
		mSurferRigidbody = dynamic_cast<Rigidbody*>(mSurfer->GetComponent(IComponent::Type::PHYSIC));
		mSurferRigidbody->Enable(false);

		timerScript->SetPaused(true);
		mSurfboard->Enable(false);
		mSurfer->Enable(false);
		if (mCamScript != nullptr) mCamScript->Enable(false);
		mBoardScript->Enable(false);
		scoreScript->SetText(L"Press RETURN to start.");
		scoreScript->SetPaused(true);

	}

	virtual void Update(float dt) override
	{
		if (dead)
		{
			if (mHighscoreScript->GetState() == Highscore::UIState::LIST)
			{
				scoreScript->SetText(L"Press RETURN to start.");
				scoreScript->SetPaused(true);
			}
			if (mHighscoreScript->GetState() == Highscore::UIState::HIGHSCORE)
			{
				if (InputManager::instance().KeyDown((UINT8)0, Input::InputKey::KEY_RETURN))
				{
					timerScript->ResetTimer();
					mBoardTransform->SetPosition(mBoardStartPos);
					mBoardTransform->SetRotation(mBoardStartRot);

					mBoardRigidbody->Clear();
					mSurferRigidbody->Enable(false);

					mSurferTransform->SetPosition(mSurferStartPos);
					mSurferTransform->SetScale(mSurferStartScale);
					mSurferTransform->SetRotation(mSurferStartRot);

					mSurferTransform->SetParent(mBoardTransform);

					mSurfboard->Enable(true);
					mSurfer->Enable(true);

					timerScript->StartTimer();
					timerScript->SetPaused(false);
					scoreScript->SetPaused(false);
					scoreScript->SetText(L"");

					if (mCamScript != nullptr) mCamScript->Enable(true);
					mBoardScript->Reset();
					mBoardScript->Enable(true);

					mScore = 0;

					dead = false;
					return;
				}
			}
			 
			if (mBoardTransform->GetPosition().y < WorldY) mSurfboard->Enable(false);
			if (mSurferTransform->GetPosition().y < WorldY) mSurfer->Enable(false);
		}
		else if (mBoardTransform->GetPosition().y < KillY || mBoardTransform->GetPosition().z < KillZ || abs(mBoardTransform->GetUp().y) < mUpLimit)
		{
			dead = true;
			timerScript->SetPaused(true);
			scoreScript->SetText(L"Wasted!\nScore: ");
			if (mCamScript != nullptr) mCamScript->Enable(false);
			mSurferTransform->SetPosition(mSurferTransform->GetWorldPosition());
			mSurferTransform->SetScale(mSurferTransform->GetWorldScale());
			mSurferTransform->SetRotation(mSurferTransform->GetWorldRotation());
			mSurferTransform->SetParent(nullptr);
			mBoardScript->Enable(false);
			mHighscoreScript->ShowList(mScore);
		}
		else
		{
			// TODO: really basic point collector, use instead an accumulation over more frames
			// TODO: consider also turns + fakie
			//mScore += XMVectorGetX(XMVectorClamp(XMVector3LengthSq(XMLoadFloat3(&mBoardRigidbody->GetVelocity())) - XMVectorReplicate(mMinVel), XMVectorReplicate(0), XMVectorReplicate(1000))) * mScoreMul * dt;
			float s = abs(mBoardRigidbody->GetVelocity().x) - mMinVel;
			if (s < 0) s = 0;
			mScore += s * mScoreMul * dt;
		}
		scoreScript->SetScore(mScore);
	}

	virtual void FixedUpdate(float dt) override
	{
		if (!mSurferRigidbody->IsEnabled() && dead)
		{
			mSurferRigidbody->Clear();
			mSurferRigidbody->SetVelocity(mBoardRigidbody->GetVelocity());
			mSurferRigidbody->Enable(true);
		}
	}

private:
	GameObject* mSurfboard;
	GameObject* mSurfer;
	OnScreenTimer* timerScript;
	SurfboardControl* mBoardScript;
	OnScore* scoreScript;
	XMFLOAT3 mBoardStartPos;
	XMFLOAT4 mBoardStartRot;
	XMFLOAT3 mSurferStartPos;
	XMFLOAT3 mSurferStartScale;
	XMFLOAT4 mSurferStartRot;
	float mUpLimit;
	TransformComponent* mBoardTransform;
	TransformComponent* mSurferTransform;
	Rigidbody* mBoardRigidbody;
	Rigidbody* mSurferRigidbody;
	Highscore* mHighscoreScript;
	FlyingCamScript* mCamScript;

	float mScore = 0;
	float mMinVel;
	float mScoreMul;

	bool dead = true;

	const float WorldY = -3.0f;
	const float KillY = -1.0f;
	const float KillZ = -5.0f;
};

#endif
