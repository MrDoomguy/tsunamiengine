#pragma once

#include "../../Engine/Scene/GameObject.h"

class TraceFlyScript : public IScript
{
public:
	TraceFlyScript(std::string& scriptName, const std::vector<std::pair<XMFLOAT3, XMFLOAT3>>& path, bool loop, float totalTime, int grad)
		: IScript(scriptName), mPath(path), mLoop(loop), mTotalTime(totalTime), mGrad(grad) 
	{
		if (mGrad < 0 || mGrad > (int)mPath.size() - 1) mGrad = (int)mPath.size() - 1;
		CalculateKnots();
	}

	~TraceFlyScript() { }

	virtual void Start() override
	{
		mTransform = mGo->GetTransform();
		mIdx = 0;
		mT = 0;
	}

	bool IsFinished() { return mIdx >= (int)mPath.size() && !mLoop; }

	int GetIdx() { return mIdx; }

	void SetPath(const std::vector<std::pair<XMFLOAT3, XMFLOAT3>>& path, float totalTime)
	{
		mPath = path;
		mT = 0;
		mIdx = 0;
		if (mGrad > (int)mPath.size() - 1) mGrad = (int)mPath.size() - 1;
		CalculateKnots();
		mTotalTime = totalTime;
	}

	virtual void Update(float dt) override
	{
		if (dt > 1) return;

		if (!IsFinished())
		{
			mIdx = (int)(mPath.size() * mT);
			std::pair<XMFLOAT3, XMFLOAT3> target = GetDeBoorPoint(mT);

			mTransform->SetPosition(target.first);
			mTransform->LookAt(target.second);

			mT += dt / mTotalTime;

			if (mT >= 1.f && mLoop)
			{
				mT -= (int)mT;
			}
		}
	}

private:
	TransformComponent* mTransform;
	std::vector<std::pair<XMFLOAT3, XMFLOAT3>> mPath;
	std::vector<float> mKnots;
	bool mLoop; 
	float mTotalTime;
	float mT;
	int mGrad;
	int mIdx;

	void CalculateKnots()
	{
		mKnots = std::vector<float>(mPath.size() + mGrad + 1);

		for (int i = 0; i < mPath.size() + mGrad + 1; i++)
		{
			if (i <= mGrad)
			{ 
				mKnots[i] = 0;
			}
			else if (i > mPath.size())
			{
				mKnots[i] = 1;
			}
			else
			{
				mKnots[i] = (i - mGrad) / (float)(mPath.size() - mGrad);
			}
		}
	}

	int GetKnotInterval(float x)
	{
		int index = -1;
		for (int i = 1; i <= mPath.size() + mGrad; i++)
		{
			if (x < mKnots[i])
			{
				index = i - 1;
				break;
			}
		}
		if (x >= mKnots[mPath.size() + mGrad])
		{
			index = (int)mPath.size() - 1;
		}
		return index;
	}

	std::pair<XMFLOAT3, XMFLOAT3> GetDeCasteljauPoint(float t)
	{
		return DeCasteljau((int)mPath.size() - 1, 0, t);
	}

	std::pair<XMFLOAT3, XMFLOAT3> GetDeBoorPoint(float t)
	{
		return DeBoor(mGrad, mGrad, GetKnotInterval(t), t);
	}

	// use de casteljau to get point on path
	std::pair<XMFLOAT3, XMFLOAT3> DeCasteljau(int grad, int i, float t)
	{
		if (grad == 0) return mPath[i];

		std::pair<XMFLOAT3, XMFLOAT3> p0 = DeCasteljau(grad - 1, i, t);
		std::pair<XMFLOAT3, XMFLOAT3> p1 = DeCasteljau(grad - 1, i + 1, t);

		XMFLOAT3 tmp0, tmp1;
		XMStoreFloat3(&tmp0, XMVectorLerp(XMLoadFloat3(&p0.first), XMLoadFloat3(&p1.first), t));
		XMStoreFloat3(&tmp1, XMVectorLerp(XMLoadFloat3(&p0.second), XMLoadFloat3(&p1.second), t));

		return std::pair<XMFLOAT3, XMFLOAT3>(tmp0, tmp1);
	}

	std::pair<XMFLOAT3, XMFLOAT3> DeBoor(int k, int grad, int i, float x)
	{
		if (k == 0) return mPath[i];

		float alpha = (x - mKnots[i])/(mKnots[i + grad + 1 - k] - mKnots[i]);
		//float alpha = (x - i) / (grad + 1 - k);

		std::pair<XMFLOAT3, XMFLOAT3> p0 = DeBoor(k - 1, grad, i - 1, x);
		std::pair<XMFLOAT3, XMFLOAT3> p1 = DeBoor(k - 1, grad, i, x);

		XMFLOAT3 tmp0, tmp1;
		XMStoreFloat3(&tmp0, XMVectorLerp(XMLoadFloat3(&p0.first), XMLoadFloat3(&p1.first), alpha));
		XMStoreFloat3(&tmp1, XMVectorLerp(XMLoadFloat3(&p0.second), XMLoadFloat3(&p1.second), alpha));

		return std::pair<XMFLOAT3, XMFLOAT3>(tmp0, tmp1);
	}
};

