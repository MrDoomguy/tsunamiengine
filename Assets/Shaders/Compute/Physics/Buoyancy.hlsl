#include "WaterPhysicHelper.hlsli"


cbuffer PerObjectData : register(b2)
{
	float4x4 objToSDF;
	float4x4 SDFToObj;

	// Mesh Data
	uint count;
	float3 _pad4;

	// Buoyancy data
	float3 boxScale;
	float _pad5;
	float3 scale;
	float _pad6;
	float3 com;
	float _pad7;
	float3 vel;
	float _pad8;
	float3 rotVel;
	float _pad9;
};

struct Vertex
{
	float3 pos;
	float3 nor;
	float3 tang;
	float2 tex;
	float area;
};

struct Buoyancy
{
	float3 force;
	float _pad10;
	float3 torque;
	float _pad11;
};

StructuredBuffer<Vertex> g_mesh : register(t2);


// output
RWStructuredBuffer<Buoyancy> g_buoyancy : register(u0);
RWTexture3D<float4> g_obstacle : register(u1);


groupshared float3 force_buff[groupdim_1];
groupshared float3 torque_buff[groupdim_1];


[numthreads(groupdim_1, 1, 1)]
void main(uint tid : SV_GroupIndex, uint3 groupIdx : SV_GroupID)
{
	unsigned int idx = groupIdx.x*groupdim_1 + tid;

	force_buff[tid] = float3(0.f, 0.f, 0.f);
	torque_buff[tid] = float3(0.f, 0.f, 0.f);

	if (idx < count)
	{
		Vertex v = g_mesh[idx];

		float3 lpos = mul(objToSDF, float4(v.pos, 1.f)).xyz;
		
		v.nor = normalize(mul(v.nor, (float3x3)SDFToObj));
		float3 nA = v.area * v.nor * scale * scale;
		float depth = SampleSDF(lpos);

		v.pos = (lpos - com) * boxScale;

		float3 obsVel = vel + cross(rotVel, v.pos);

		g_obstacle[lpos + halfVolumeL] = float4(v.nor, dot(obsVel, v.nor));

		if (depth < 0)
		{
			float3 flowVel = SampleVel(lpos) * flowFactor;
			float3 relVel = flowVel - dampingFactor * obsVel;
			float force = dot(relVel, nA);

			if (force > 0) force = 0;

			force -= buoyancyFactor * depth * GRAVITY * length(nA);

			float3 f = v.nor * force + saveNormalize(relVel) * frictionFactor * length(cross(relVel, nA));

			force_buff[tid] = f;
			torque_buff[tid] = cross(v.pos, f);
		}
	}
    GroupMemoryBarrierWithGroupSync();
	// do reduction in shared mem 
    if (groupdim_1 >= 1024)
    {
        if (tid < 512)
        {
            force_buff[tid] += force_buff[tid + 512];
            torque_buff[tid] += torque_buff[tid + 512];
        }
        GroupMemoryBarrierWithGroupSync();
    }
	// do reduction in shared mem 
	if (groupdim_1 >= 512)
	{
		if (tid < 256)
		{
			force_buff[tid] += force_buff[tid + 256];
			torque_buff[tid] += torque_buff[tid + 256];
		}
		GroupMemoryBarrierWithGroupSync();
	}
	if (groupdim_1 >= 256)
	{
		if (tid < 128)
		{
			force_buff[tid] += force_buff[tid + 128];
			torque_buff[tid] += torque_buff[tid + 128];
		}
		GroupMemoryBarrierWithGroupSync();
	}
	if (groupdim_1 >= 128)
	{
		if (tid < 64)
		{
			force_buff[tid] += force_buff[tid + 64];
			torque_buff[tid] += torque_buff[tid + 64];
		}
		GroupMemoryBarrierWithGroupSync();
	}
    
    if (groupdim_1 >= 64)
    {
        if (tid < 32)
        {
            force_buff[tid] += force_buff[tid + 32];
            torque_buff[tid] += torque_buff[tid + 32];
        }
        GroupMemoryBarrierWithGroupSync();
    }
    
    
    if (groupdim_1 >= 32)
    {
        if (tid < 16)
        {
            force_buff[tid] += force_buff[tid + 16];
            torque_buff[tid] += torque_buff[tid + 16];
        }
        GroupMemoryBarrierWithGroupSync();
    }

	if (tid < 8)
	{
		//if (groupdim_1 >= 64) force_buff[tid] += force_buff[tid + 32];
		//if (groupdim_1 >= 32) force_buff[tid] += force_buff[tid + 16];
		if (groupdim_1 >= 16) 
            force_buff[tid] += force_buff[tid + 8];
		if (groupdim_1 >= 8) 
            force_buff[tid] += force_buff[tid + 4];
		if (groupdim_1 >= 4) 
            force_buff[tid] += force_buff[tid + 2];
		if (groupdim_1 >= 2) 
            force_buff[tid] += force_buff[tid + 1];
        
		//if (groupdim_1 >= 64) torque_buff[tid] += torque_buff[tid + 32];
		//if (groupdim_1 >= 32) torque_buff[tid] += torque_buff[tid + 16];
		if (groupdim_1 >= 16) 
            torque_buff[tid] += torque_buff[tid + 8];
		if (groupdim_1 >= 8) 
            torque_buff[tid] += torque_buff[tid + 4];
		if (groupdim_1 >= 4) 
            torque_buff[tid] += torque_buff[tid + 2];
		if (groupdim_1 >= 2) 
            torque_buff[tid] += torque_buff[tid + 1];
    }

	// write result for this block to global mem 
	if (tid == 0)
	{
		Buoyancy b;
		b.force = force_buff[0];
		b.torque = torque_buff[0];
		b._pad10 = 0.f;
		b._pad11 = 0.f;
		g_buoyancy[groupIdx.x] = b;
	}
}
