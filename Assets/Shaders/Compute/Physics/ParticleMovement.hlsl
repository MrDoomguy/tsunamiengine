#define groupdim 128

/* only simple movement script for basic particles, for sdf particle system with volumetexture!*/
struct Particle
{
	float3 pos;
	float _pad0;
	float3 vel;
	float _pad1;
};

cbuffer PerFrameData : register(b0)
{
	float3 force;
	float dt;
}

RWStructuredBuffer<Particle> g_particles : register(u0);

[numthreads(groupdim, 1, 1)]
void main(uint3 threadIdx : SV_GroupThreadID)
{
	uint tid = threadIdx.x;

	Particle p = g_particles[tid];

	p.pos += p.vel * dt;
	p.vel += force * dt;

	g_particles[tid] = p;
}