#include "ParticlePhysic.hlsli"
#include "LightEnvs.hlsli"

// thresholds for particle generation
cbuffer InitProperties : register(b3)
{
	float turbulenceMin;
	float turbulenceMax;
	float curvatureMin;
	float curvatureMax;
	float obstVelMin;
	float obstVelMax;
	float energyMin;
	float energyMax;
	int maxWc;
	int maxTu;
	int maxOb;
	float _pad3;
};


RWStructuredBuffer<uint> g_head : register(u2);


static const float turbulenceDiff = turbulenceMax - turbulenceMin;
static const float curvatureDiff = curvatureMax - curvatureMin;
static const float energyDiff = energyMax - energyMin;
static const float obstVelDiff = obstVelMax - obstVelMin;

// reduces samples to save performance
static const float sampleSize = 2.f;

inline float clampIntensity(in float x, in float min, in float diff)
{
	return saturate((x - min) / diff);
}

[numthreads(groupdim_3, groupdim_3, groupdim_3)]
void main(uint3 threadIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID)
{
	float3 idx = (threadIdx + groupIdx * groupdim_3) * sampleSize;

	if ((uint)idx.x < dim.x && (uint)idx.y < dim.y && (uint)idx.z < dim.z) 
	{
		idx += sampleSize * random3(idx + offset * (1 - dt) + nextOffset * dt);

		idx += float3(0.5f, 0.5f, 0.5f);
		idx -= dim.xyz / 2.f;
		float depth = SampleSDF(idx);

		// only in water new particles!
		if (depth < 0)
		{			
			float Iwc = 0;

			// trapped air
			float Itu = clampIntensity(TrappedAir(idx), turbulenceMin, turbulenceDiff);

			float3 fluidVel = SampleVel(idx);
			float4 obVel = SampleObstacle(idx);
			float Iob = 0;

			if (obVel.x >= -1 && dot(fluidVel, obVel.xyz) < 0)
			{
				float fvl = length(fluidVel);

				float3 rv = reflect(fluidVel / fvl, obVel.xyz);
				fluidVel = rv * fvl * (dot(fluidVel / fvl, rv) + 1) / 2 + obVel.xyz * obVel.w;
				Iob = clampIntensity(dot(fluidVel, fluidVel), obstVelMin, obstVelDiff);
			}

			// only surface -> check curvature
			if (depth > -surfaceTol && dot(normalize(Gradient(idx)), fluidVel) >= 0.6f)
			{
				Iwc = clampIntensity(-Curvature(idx), curvatureMin, curvatureDiff);
			}

			float Ien = clampIntensity(0.5f * dot(fluidVel, fluidVel), energyMin, energyDiff);

			int particleCount = Ien * (maxWc * Iwc + maxTu * Itu + maxOb * Iob) * dt;
			float ttl = 1.f;// clampIntensity(Ien * (Iwc + Itu), 0.5f, 1.f);


			for(int i = 0; i < particleCount; i++)
			{
				Particle p;
				p.ttl = ttl;
				p.state = 1;

				uint storeIdx;
				InterlockedAdd(g_head[0], 1, storeIdx);

				p.radius = ((storeIdx % 9) + 1) * maxScale;

				float3 e0 = normalize(cross(float3(1, 0, 0), fluidVel));
				float3 e1 = normalize(cross(e0, fluidVel));

				float3 rnd = Hammersley3D(i, particleCount);
				rnd.xy = 2 * rnd.xy - float2(1.f, 1.f);
				rnd *= sampleSize;
				
				// TODO: use direct calc of point on circle?!
				if (rnd.x * rnd.x + rnd.y * rnd.y > sampleSize * sampleSize ) continue;

				p.pos = idx + fluidVel * rnd.z * dt + e0 * rnd.x + e1 * rnd.y;

				storeIdx = storeIdx % maxSize;

				g_particles[storeIdx] = p;
				g_velocity[storeIdx] = fluidVel + e0 * rnd.x + e1 * rnd.y;
			}
		}
	}
}