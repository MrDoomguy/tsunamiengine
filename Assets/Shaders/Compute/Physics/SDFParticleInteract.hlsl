#include "ParticlePhysic.hlsli"

static const float kd = 0.2f;
static const float kb = 0.5f;

[numthreads(groupdim_1, 1, 1)] 
void main(uint3 threadIdx : SV_GroupThreadID, uint3 groupIdx : SV_GroupID)
{
	unsigned int idx = groupIdx.x*groupdim_1 + threadIdx.x;
	if (idx < maxSize)
	{
		Particle p = g_particles[idx];

		if (p.radius > 0)
		{
			float3 v = g_velocity[idx];

			p.pos += v * dt;

			float depth = SampleSDF(p.pos);

			// if time to live finished or a spray particle out of box -> destroy particle
			if (p.ttl < 0 || 
				(depth > -surfaceTol && (
					p.pos.x < -halfVolumeL.x || p.pos.x > halfVolumeL.x ||
					p.pos.y < -halfVolumeL.y || p.pos.y > halfVolumeL.y ||
					p.pos.z < -halfVolumeL.z || p.pos.z > halfVolumeL.z)))
			{
				g_particles[idx].radius = -1;
				return;
			}

			// box collision detection
			if (p.pos.x < -halfVolumeL.x) p.pos.x = -halfVolumeL.x;
			else if (p.pos.x > halfVolumeL.x)
			{
				g_particles[idx].radius = -1;
				return;
			}

			if (p.pos.y < -halfVolumeL.y) p.pos.y = -halfVolumeL.y;
			else if (p.pos.y > halfVolumeL.y) p.pos.y = halfVolumeL.y;

			if (p.pos.z < -halfVolumeL.z) p.pos.z = -halfVolumeL.z;
			else if (p.pos.z > halfVolumeL.z) p.pos.z = halfVolumeL.z;

			float3 flowVel = SampleVel(p.pos);

			if (depth < -surfaceTol)
			{
				p.state = 2;
				v += float3(0, -kd * GRAVITY * dt, 0) + kb * (flowVel - v);
			}
			else if (depth > surfaceTol)
			{
				p.state = 0;
				v.y += GRAVITY * dt;
			}
			else
			{
				p.state = 1;
				v = flowVel;

				p.ttl -= dt * ttlFactor;
			}

			float4 obVel = SampleObstacle(p.pos);

			// TODO: clamp dot product between two values
			if (obVel.x >= -1)
			{
				float vl = length(v);
				float3 rv = reflect(v / vl, obVel.xyz);
				v = rv * vl * (dot(v / vl, rv) + 1) / 2 + obVel.xyz * obVel.w;
			}

			g_particles[idx] = p;
			g_velocity[idx] = v;
		}
	}
}