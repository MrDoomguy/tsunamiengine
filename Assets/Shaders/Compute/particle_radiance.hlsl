#include "LightEnvs.hlsli"
#include "ThreadingDefines.hlsli"

cbuffer PerFrameData : register(b0)
{
	uint2 screenSize;
	float2 _pad0;
};
RWTexture2D<float> g_shadow : register(u0); 
RWTexture2D<float3> g_radiance : register(u1);

Texture2D<float> g_particleIntensity : register(t0);
Texture2D<float4> g_particleNormalAndRadii : register(t1);
Texture2D<float> g_particleDepth : register(t2);
Texture2D<float> g_raytracerDepth : register(t3);
Texture2D<float> g_rasterDepth : register(t4);

SamplerState texSampler : register(s0);

static const float ao_init = 1.f;
static const float ao_inc = 7.f;
static const int ao_pass = 3;

static const float ao_dens = 0.5f;
static const int ao_mins = 16;
static const int ao_maxs = 512;

static const float ao_occl = 5.f;

static const float ao_scale = .8f;
static const float ao_exp = 1.5f;
static const float ao_offset = -0.1f;

[numthreads(groupdim_2, groupdim_2, 1)]
void main(uint3 DTid : SV_DispatchThreadID)
{
	float h_fr = g_particleNormalAndRadii.Load(int3(DTid.xy, 0)).a;

	float occl = 0.f;
	float w = 0.f;

	if (h_fr > 0)
	{
		//float p_dep = g_particleDepth.Load(int3(DTid.xy, 0)).r;
		float dep = g_particleDepth.Load(int3(DTid.xy, 0)).r;

		//float p_int = g_particleIntensity.Load(int3(DTid.xy, 0)).r;

		for (int i = 0; i < ao_pass; i++)
		{
			float h_fr_pass = h_fr * (ao_init + ao_inc * i);
			int v = clamp(0.75f * PI * h_fr_pass * h_fr_pass * h_fr_pass * ao_dens, ao_mins, ao_maxs);
			
			for (int j = 0; j < v; j++)
			{
				// TODO: use sphere sampling
				float3 disp = Hammersley3D(j, v) * 2.f - float3(1, 1, 1);
				float l = dot(disp, disp);
				
				if (l <= 1.f)
				{
					disp *= h_fr_pass;
					//float sz = g_particleDepth.SampleLevel(texSampler, (DTid.xy + disp * h_fr_pass + float2(.5f,.5f)) / screenSize, 0).r;
					float p_dep = g_particleDepth.SampleLevel(texSampler, (DTid.xy + disp.xy + float2(.5f,.5f)) / screenSize, 0).r;
					float s_dep = g_rasterDepth.SampleLevel(texSampler, (DTid.xy + disp.xy + float2(.5f, .5f)) / screenSize, 0).r;
					float r_dep = g_raytracerDepth.SampleLevel(texSampler, (DTid.xy + disp.xy + float2(.5f, .5f)) / screenSize, 0).r;
					float p_int = g_particleIntensity.SampleLevel(texSampler, (DTid.xy + disp.xy + float2(.5f,.5f)) / screenSize, 0).r;

					disp.z += dep;

					float f1 = (1 - sqrt(l));
					f1 *= f1;

					float f2 = max(1 - abs(p_dep - disp.z) / ao_occl, 0);
					f2 *= f2;

					// TODO: include also solid + raytrace values/depth
					if (f2 > 0 && f2 < 1)
					{
						if (false && disp.z < p_dep)
						{
							occl += f1 * f2 * p_int;
							w += f1;
						}
						else if (disp.z < s_dep)
						{
							occl += f1 * f2;
							w += f1;
						}
						else if (false && disp.z < r_dep)
						{
							occl += f1 * f2;
							w += f1;
						}
					}
				}

				//float3 n = ImportanceSampleGGX(disp, 1.f, p_nor);
				//col += GetIrradianceFromSphere(n) * dot(n, p_nor);
			}
		}
	}

	if (w > 0.0f)
	{
		//g_shadow[DTid.xy] = clamp(pow(abs(occl * ao_scale / w), ao_exp) + ao_offset, 0, 1);
		g_shadow[DTid.xy] = saturate(pow(abs((occl * ao_scale) / (w + 1e-5)), ao_exp) + ao_offset);
		g_radiance[DTid.xy] = GetIrradianceFromSphere(g_particleNormalAndRadii.Load(int3(DTid.xy, 0)).xyz);
	}
	else
	{
		g_shadow[DTid.xy] = 0;
		g_radiance[DTid.xy] = float3(0,0,0);
	}
}