#include "ThreadingDefines.hlsli"

static const float exposure = 1.0f;
static const float gamma = 2.2f;

Texture2D<float4> g_inFrame : register(t0);
RWTexture2D<float4> g_outFrame : register(u0);

[numthreads(groupdim_2, groupdim_2, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{
	// images should have the same size
	//uint width, height;
	//g_inFrame.GetDimensions(width, height);

	float3 inColor = g_inFrame.Load(int3(DTid.xy, 0)).rgb;

	// Exposure tone mapping
	float3 mapped = (float3)1.0f - exp(-inColor * exposure);

	// Gamma Correction
	mapped = pow(abs(mapped), (float3)1.0f/gamma);

	g_outFrame[DTid.xy] = float4(mapped, 1.0f);
}