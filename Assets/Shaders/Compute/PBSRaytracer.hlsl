#include "LightEnvs.hlsli"
#include "ThreadingDefines.hlsli"

cbuffer PerFrameData : register(b0)
{
	DirectionalLight dirLights[MAX_DIR_LIGHTS];
	uint nDirLights;
	float viewAngle;
	float imgPlaneWidth;
	float imgPlaneHeight;
	float viewZ;
	float4x4 view;
	float4x4 viewLocal;
	float4x4 viewProj;
	float nearPlane;
	float farPlane;
	float _pad2;
};

// Material Data
cbuffer PerObjectData : register(b1)
{
	PBSMaterial mat;
	float4x4 world;
	float4x4 invWorld;
	uint4 dim;
	uint3 extent;
	float _pad3;
	uint3 offset;
	float _pad4;
	uint3 nextOffset;
	float dt;
};

RWTexture2D<float4> g_raytracerOutput : register(u0);
RWTexture2D<float> g_depthBuffer : register(u1);
RWTexture2D<float2> g_displacement : register(u2);

Texture3D<float> g_sdf : register(t0);
Texture3D<float4> g_obstacle : register(t1);
SamplerState g_sdfSampler : register(s0);

struct Ray
{
	float3 origin;
	float3 start;
	float3 current;
	float3 direction;
};

struct AABB
{
	float3 _min;
	float3 _max;
};

static float tout = 3.402823466e+38F;
static float tin = -3.402823466e+38F;

static const float3 halfVolumeL = dim.xyz / 2.f;

inline void swap(inout float t0, inout float t1)
{
	const float tmp = t0;
	t0 = t1;
	t1 = tmp;
}

// values smaller then zero are obstacle
float4 SampleObstacle(float3 worldPos)
{
	return g_obstacle.SampleLevel(g_sdfSampler, (worldPos + halfVolumeL) / dim.xyz, 0);
}

float IsObstacle(float3 worldPos)
{
	float3 v = SampleObstacle(worldPos).xyz;
	return dot(v, v) - 1.1f;
}

float SampleSDF(float3 worldPos)
{
	return
		g_sdf.SampleLevel(g_sdfSampler, (worldPos + offset + halfVolumeL) / extent, 0).r * (1 - dt) +
		g_sdf.SampleLevel(g_sdfSampler, (worldPos + nextOffset + halfVolumeL) / extent, 0).r * dt;
}

float3 Gradient(float3 pos)
{
	float step = 0.5f;
	float3 g;

	g.x = SampleSDF(float3(pos.x + step, pos.yz)) - SampleSDF(float3(pos.x - step, pos.yz));
	g.y = SampleSDF(float3(pos.x, pos.y + step, pos.z)) - SampleSDF(float3(pos.x, pos.y - step, pos.z));
	g.z = SampleSDF(float3(pos.xy, pos.z + step)) - SampleSDF(float3(pos.xy, pos.z - step));

	return g;
}

float4 BlendColor(float4 color1, float4 color2)
{
	float a3 = color1.a + (1 - color1.a) * color2.a;
	
	return float4((color1.a * color1.rgb + (1 - color1.a) * color2.a * color2.rgb) / a3, a3);
}

bool Hit(in Ray ray, inout float tmin, inout float tmax, in float3 boxMin, in float3 boxMax)
{
	const float3 invD = rcp(ray.direction);

	[unroll]
	for(uint i = 0; i < 3; i++)
	{
		float t0 = (boxMin[i] - ray.origin[i]) * invD[i];
		float t1 = (boxMax[i] - ray.origin[i]) * invD[i];

		if(invD[i] < 0.0f)
		{
			swap(t0, t1);
		}

		tmin = (t0 > tmin) ? t0 : tmin;
		tmax = (t1 < tmax) ? t1 : tmax;

		if(tmax <= tmin)
		{
			return false;
		}
	}

	return true;
}

[numthreads(groupdim_2, groupdim_2, 1)]
void main(uint3 DTid : SV_DispatchThreadID)
{
	const float x = (DTid.x - imgPlaneWidth / 2) + 0.5f;
	const float y = (imgPlaneHeight/2 - DTid.y) - 0.5f;

	Ray ray;
	// Camera data
	float4 camPos = mul(invWorld, mul(view, float4(0.f, 0.f, 0.f, 1.f)));
	ray.origin = camPos.xyz / camPos.w;
	ray.current = ray.origin;

	// Pixel Position in world space
	float3 imgPlaneCoord = mul(invWorld, mul(view, float4(x, y, viewZ, 1.0f))).xyz;
	ray.start = imgPlaneCoord;

	// direction
	ray.direction = normalize(imgPlaneCoord - ray.origin);

	// calculate min-max of the volume to raytrace
	// from local space to box world space
	// if camera has transform should be multiplied by camera trasform also
	AABB aabb;
	aabb._min = -halfVolumeL;
	aabb._max = halfVolumeL;

	float4 finalColor = float4(0.0f, 0.0f, 0.0f, 0.0f);

	//  TODO: check if ray origin in volume!
	bool inVolume = false;
	float3 surfacePt = float3(0,0,tout);
	float3 displace = float3(DTid.xy, 0);
	//float d = tout;
	if(Hit(ray, tin, tout, aabb._min, aabb._max))
	{
		float3 p = ray.origin + tin*ray.direction;
		float3 end = ray.origin + tout*ray.direction;

		float lastVal = 0;

		int intensity = 0;

		// Start ray march from p
		for(float i = tin; i < tout; i++)
		{	
			/*float gridVal = IsObstacle(p);
			if (gridVal <= 0)
			{
				p += (gridVal / (lastVal - gridVal)) * ray.direction;
				finalColor = float4((SampleObstacle(p).xyz + float3(1,1,1)) / 2, 1);
				surfacePt = p;
				inVolume = true;
				break;
			}*/
			// sample at NTC
			float gridVal = SampleSDF(p);

			if(gridVal < 0.0f)
			{
				if (inVolume)
				{
					// TODO: more efficient blending!!
					// TODO: fix artefacts!
					finalColor = BlendColor(finalColor, float4(mat.baseColor, 0.0f));
				}
				else
				{
					inVolume = true;
					p += (gridVal / (lastVal - gridVal)) * ray.direction;
					float3 normal = normalize(Gradient(p));

					float3 r = reflect(ray.direction, normal);
					//float2 envUV = float2(atan2(r.x, -r.z)/PI, acos(r.y)/PI);
					float3 irradiance = g_irrMap.SampleLevel(g_envSampler, r, 0).rgb;

					// IBL contribution
					float3 specular = ApproximateSpecularIBL(irradiance, mat.roughness, normal, -ray.direction);

					finalColor = float4(mat.baseColor / PI + specular * mat.metallic, 0.5f);

					surfacePt = p;

					ray.direction = refract(ray.direction, normal, 1.f/1.33f);
				}
				displace = p;
			}
			lastVal = gridVal;
			p += ray.direction;
		}
	}

	float depth = tout;
	if (inVolume)
	{
		depth = mul(viewLocal, mul(world, float4(surfacePt, 1.f))).z;
		float4 ep = mul(viewProj, mul(world, float4(displace, 1.f)));
		displace.xy = (ep.xy / ep.w + float2(1, -1)) / float2(2, -2) * float2(imgPlaneWidth, imgPlaneHeight);
	}

	// HACK: better check before the compute intensive task
	if (depth < 0)
	{
		finalColor = float4(0, 0, 0, 0);
		displace.xy = DTid.xy;
		depth = tout;
	}

	g_raytracerOutput[DTid.xy] = finalColor;
	g_depthBuffer[DTid.xy] = depth;
	g_displacement[DTid.xy] = displace.xy;
}