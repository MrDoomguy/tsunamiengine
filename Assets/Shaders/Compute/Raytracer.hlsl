static const uint MAX_DIR_LIGHTS = 10;

struct PBSMaterial
{
	float3 baseColor;
	float metallic;
	float roughness;
	float ior;
	float2 _pbsPad;
};

struct DirectionalLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 direction;
	float _pad0;
};

/*struct Sphere
{
	float3 pos;
	float radius;
};*/

cbuffer PerFrameData : register(b0)
{
	DirectionalLight dirLights[MAX_DIR_LIGHTS];
	uint nDirLights;
	float viewAngle;
	float imgPlaneWidth;
	float imgPlaneHeight;
	float viewZ;
	float4x4 view;
	float4x4 viewProj;
	float nearPlane;
	float farPlane;
	float _pad2;

	/*Sphere spheres[MAX_DIR_LIGHTS];
	uint nSpheres;*/
};

// Material Data
cbuffer PerObjectData : register(b1)
{
	PBSMaterial mat;
	float4x4 world;
	float4x4 invWorld;
	uint4 dim;
	uint3 extent;
	float _pad3;
	uint3 offset;
	float _pad4;
	uint3 nextOffset;
	float dt;
};

RWTexture2D<float4> g_raytracerOutput : register(u0);
RWTexture2D<float> g_depthBuffer : register(u1);
Texture3D g_sdf : register(t0);
SamplerState g_sdfSampler : register(s0);

struct Ray
{
	float3 origin;
	float3 start;
	float3 current;
	float3 direction;
};

struct AABB
{
	float3 _min;
	float3 _max;
};

static float tout = 3.402823466e+38F;
static float tin = -3.402823466e+38F;
static const float3 halfVolumeL = dim.xyz / 2.f;

inline void swap(inout float t0, inout float t1)
{
	const float tmp = t0;
	t0 = t1;
	t1 = tmp;
}

float SampleSDF(float3 worldPos)
{
	return
		g_sdf.SampleLevel(g_sdfSampler, (worldPos + offset + halfVolumeL) / extent, 0).r * (1 - dt) +
		g_sdf.SampleLevel(g_sdfSampler, (worldPos + nextOffset + halfVolumeL) / extent, 0).r * dt;
}

float3 Gradient(float3 pos)
{
	float step = 0.5f;
	float3 g;

	g.x = SampleSDF(float3(pos.x + step, pos.yz)) - SampleSDF(float3(pos.x - step, pos.yz));
	g.y = SampleSDF(float3(pos.x, pos.y + step, pos.z)) - SampleSDF(float3(pos.x, pos.y - step, pos.z));
	g.z = SampleSDF(float3(pos.xy, pos.z + step)) - SampleSDF(float3(pos.xy, pos.z - step));

	return g;
}

float4 BlendColor(float4 color1, float4 color2)
{
	float a3 = color1.a + (1 - color1.a) * color2.a;
	
	return float4((color1.a * color1.rgb + (1 - color1.a) * color2.a * color2.rgb) / a3, a3);
}

float4 EvaluateLight(float3 normal, float3 viewDir, float4 diffuseColor, float4 specularColor, float4 ambientColor)
{
	diffuseColor.rgb *= diffuseColor.a;
	specularColor.rgb *= specularColor.a;
	ambientColor.rgb *= ambientColor.a;

	float n = 10.0f;

	float4 color = float4(0.f, 0.f, 0.f, 0.f);

	for (uint i = 0; i < nDirLights; i++)
	{
		float3 lightDir = -dirLights[i].direction;

		//float4 coeff = lit(dot(lightDir, normal), dot(reflect(lightDir, normal), viewDir), 10.f);
		float4 coeff = lit(dot(lightDir, normal), dot(normalize(lightDir + viewDir), normal), 128.0f);

		float4 c = diffuseColor * coeff.y + specularColor * coeff.z + ambientColor;
		color += float4(c.rgb * dirLights[i].diffuse, c.a);
	}
	if (color.w > 1) color / color.w;
	return color;
}

bool Hit(in Ray ray, inout float tmin, inout float tmax, in float3 boxMin, in float3 boxMax)
{
	const float3 invD = rcp(ray.direction);

	[unroll]
	for(uint i = 0; i < 3; i++)
	{
		float t0 = (boxMin[i] - ray.origin[i]) * invD[i];
		float t1 = (boxMax[i] - ray.origin[i]) * invD[i];

		if(invD[i] < 0.0f)
		{
			swap(t0, t1);
		}

		tmin = (t0 > tmin) ? t0 : tmin;
		tmax = (t1 < tmax) ? t1 : tmax;

		if(tmax <= tmin)
		{
			return false;
		}
	}

	return true;
}

bool HitSphere(in Ray ray, inout float tmin, inout float tmax, in float3 pos, in float radius)
{
	float radius2 = radius * radius;
	float3 l = pos - ray.origin;
	float tca = dot(l, ray.direction);
	float d2 = dot(l, l) - tca * tca;

	if (d2 > radius2) return false;
	float thc = sqrt(radius2 - d2);
	tmin = tca - thc;
	tmax = tca + thc;

	if (tmax <= tmin)
	{
		return false;
	}
	return true;
}

[numthreads(16, 16, 1)]
void main(uint3 DTid : SV_DispatchThreadID)
{
	const float x = (DTid.x - imgPlaneWidth / 2) + 0.5f;
	const float y = (imgPlaneHeight/2 - DTid.y) - 0.5f;

	Ray ray;
	// Camera data
	float4 camPos = mul(invWorld, mul(view, float4(0.f, 0.f, 0.f, 1.f)));
	ray.origin = camPos.xyz / camPos.w;
	ray.current = ray.origin;

	// Pixel Position in world space
	float3 imgPlaneCoord = mul(invWorld, mul(view, float4(x, y, viewZ, 1.0f))).xyz;
	ray.start = imgPlaneCoord;

	// direction
	ray.direction = normalize(imgPlaneCoord - ray.origin);

	// calculate min-max of the volume to raytrace
	// from local space to box world space
	// if camera has transform should be multiplied by camera trasform also
	AABB aabb;
	aabb._min = -halfVolumeL;
	aabb._max = halfVolumeL;

	float4 finalColor = float4(0.0f, 0.0f, 0.0f, 0.0f);

	//  TODO: check if ray origin in volume!
	bool inVolume = false;
	float3 surfacePt = float3(0,0,tout);
	//float d = tout;
	if(Hit(ray, tin, tout, aabb._min, aabb._max))
	{
		float3 p = ray.origin + tin*ray.direction;
		float3 end = ray.origin + tout*ray.direction;

		float lastVal = 0;

		// Start ray march from p
		for(uint i = tin; i < tout; i++)
		{	
			// sample at NTC
			float gridVal = SampleSDF(p);

			if(gridVal < 0.0f)
			{
				if (inVolume)
				{
					// TODO: more efficient blending!!
					finalColor = BlendColor(finalColor, float4(0.5f, 0.8f, 1.0f, 0.1f));
				}
				else
				{
					inVolume = true;
					p += (gridVal / (lastVal - gridVal)) * ray.direction;
					float3 normal = normalize(Gradient(p));

					float4 diffuseColor = float4(0.f, 0.f, 1.f, 0.1f);
					float4 specularColor = float4(1.f, 1.f, 1.f, 0.4f);
					float4 ambientColor = float4(1.f, 1.f, 1.f, 0.5f);

					finalColor = EvaluateLight(normal, -ray.direction, diffuseColor, specularColor, ambientColor);
					surfacePt = p;
					//d = i;
					//ray.direction = refract(ray.direction, normal, 0.75f);
				}
			}
			lastVal = gridVal;
			p += ray.direction;
		}
	}

	/*for (int i = 0; i < nSpheres; i++)
	{
		float3 sP = mul(invWorld, float4(spheres[i].pos, 1.f)).xyz;
		if (HitSphere(ray, tin, tout, sP, spheres[i].radius))
		{
			float3 normal = normalize(ray.origin + tin * ray.direction - sP);

			float4 diffuseColor = float4(0.f, 1.f, 0.f, 0.4f);
			float4 specularColor = float4(1.f, 1.f, 1.f, 0.4f);
			float4 ambientColor = float4(1.f, 1.f, 1.f, 0.2f);


			float4 col = EvaluateLight(normal, -ray.direction, diffuseColor, specularColor, ambientColor);

			if (d < tin)
			{
				finalColor = BlendColor(finalColor, col);
			}
			else
			{
				finalColor = float4(col.rgb, 1);
				d = tin;
			}
		}
	}*/

	float4 depth = mul(viewProj, mul(world, float4(surfacePt, 1)));
	//finalColor = BlendColor(finalColor, float4(0.f, 0.f, 0.f, 1.f));

	g_raytracerOutput[DTid.xy] = finalColor;
	g_depthBuffer[DTid.xy] = saturate(depth.z / depth.w);
}