#include "ThreadingDefines.hlsli"

cbuffer PerFrame : register(b0)
{
	float2 imgPlaneSize;
	float2 _pad0;
};

static const uint MAX_LAYERS = 10;
struct LayerInfo
{
	float2 pos;
	float2 size;
	float alpha;
	float3 _pad1;
};

cbuffer PerObject : register(b1)
{
	LayerInfo layerInfo[MAX_LAYERS];
};

RWTexture2D<float4> g_output : register(u0);

Texture3D<float4> g_gui : register(t0);
Texture2D<float4> g_osrv : register(t1);
SamplerState g_sampler : register(s0);

float4 BlendColor(in float4 color1, in float4 color2)
{
	float a3 = color1.a + (1.0f - color1.a) * color2.a;
	
	return float4((color1.a * color1.rgb + (1.0f - color1.a) * color2.a * color2.rgb) / a3, a3);
}

[numthreads(groupdim_2, groupdim_2, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{
	uint guiX, guiY, guiZ;
	g_gui.GetDimensions(guiX, guiY, guiZ);

	float4 finalColor = float4(0.0f, 0.0f, 0.0f, 0.0f);

	for(uint l = 0; l < guiZ; l++)
	{
		/*float2 p1 = layerInfo[l].pos * imgPlaneSize;
		float2 p2 = (layerInfo[l].pos + layerInfo[l].size) * imgPlaneSize;

		float2 screenSpaceSize = p2 - p1;
		float2 uv = (DTid.xy - p1) / screenSpaceSize;*/

		// In Pixels
		// top-left corner
		float2 p1 = layerInfo[l].pos - (layerInfo[l].size / 2.0f);
		float2 p2 = layerInfo[l].pos + (layerInfo[l].size / 2.0f);

		float2 screenSpaceSize = p2 - p1;
		float2 uv = (DTid.xy - p1) / screenSpaceSize;

		if(uv.x >= 0.0f && uv.y >= 0 && uv.x <= 1.0f && uv.y <= 1.0f)
		{
			float4 layerColor = g_gui.SampleLevel(g_sampler, float3(uv.xy, (float)l/((float)guiZ-1.0f)), 0).rgba;
			
			if(layerColor.a > 0.0f)
			{
				// dynamic alpha from script
				layerColor.a = layerInfo[l].alpha;

				finalColor = BlendColor(finalColor, layerColor);
			}
		}
	}

	g_output[DTid.xy] = float4(BlendColor(finalColor, g_osrv.Load(int3(DTid.xy, 0)).rgba).rgb, 1.0f);
}