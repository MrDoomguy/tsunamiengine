#include "LightEnvs.hlsli"
#include "ThreadingDefines.hlsli"

RWTexture2D<float4> g_output : register(u0);

Texture2D<float4> g_raytraceColor : register(t0);
Texture2D<float> g_raytraceDepth : register(t1);
Texture2D<float4> g_rasterColor : register(t2);
Texture2D<float> g_rasterDepth : register(t3);
Texture2D<float2> g_displace : register(t4);
Texture2D<float> g_particleIntensity : register(t5);
Texture2D<float> g_particleShadow : register(t6);
Texture2D<float4> g_particleColor : register(t7);
Texture2D<float4> g_particleNorAndSR : register(t8);

SamplerState g_sampler : register(s0);

float4 BlendColor(float4 color1, float4 color2)
{
	float a3 = color1.a + (1 - color1.a) * color2.a;
	
	return float4((color1.a * color1.rgb + (1 - color1.a) * color2.a * color2.rgb) / a3, a3);
}

static const float3 whiteCol = float3(0.5f, 0.2f, .0f);
static const float3 irrColScale = float3(2.f, 2.f, 2.f);

float GetParticleShadowFiltered(in int2 pos, in int w)
{
	float res = 0;
	float weight = 0;
	for (int j = -w; j <= w; j++)
	{
		for (int i = -w; i <= w; i++)
		{
			float tW = g_particleIntensity.Load(int3(pos + int2(i, j), 0));
			res += g_particleShadow.Load(int3(pos + int2(i, j), 0)) * tW;
			weight += tW;
		}
	}
	return res / weight;
}

float3 GetParticleColFiltered(in int2 pos, in int w)
{
	float3 res = 0;
	float weight = 0;
	for (int j = -w; j <= w; j++)
	{
		for (int i = -w; i <= w; i++)
		{
			float tW = g_particleIntensity.Load(int3(pos + int2(i, j), 0));
			res += g_particleColor.Load(int3(pos + int2(i, j), 0)).rgb * tW;
			weight += tW;
		}
	}
	return res / weight;
}

[numthreads(groupdim_2, groupdim_2, 1)]
void main(uint3 DTid : SV_DispatchThreadID)
{
	float4 rayc = g_raytraceColor.Load(int3(DTid.xy, 0)).rgba;
	float pari = g_particleIntensity.Load(int3(DTid.xy, 0)).r;
	float rastd = g_rasterDepth.Load(int3(DTid.xy, 0)).r;
	float rayd = g_raytraceDepth.Load(int3(DTid.xy, 0)).r;

	// HACK: use values of ssao
	float pRad = g_particleNorAndSR.Load(int3(DTid.xy, 0)).a * 15;
	
	float4 finalColor = g_rasterColor.Load(int3(DTid.xy, 0)).rgba;
	[flatten]
	if(rayd < rastd)
	{
		float dis = rastd - rayd;

		// TODO: use sample instead of load
		float2 disp = g_displace.Load(int3(DTid.xy, 0));
		rayd = g_raytraceDepth.Load(int3(disp, 0)).r;
		rastd = g_rasterDepth.Load(int3(disp, 0)).r;

		float f = saturate((rastd - rayd) / dis);
			
		finalColor = BlendColor(rayc, g_rasterColor.Load(int3(DTid.xy * (1-f) + disp * f, 0)));
	}
	else
	{
		finalColor = g_rasterColor.Load(int3(DTid.xy, 0)).rgba;
	}

	float3 shad = saturate(whiteCol * GetParticleShadowFiltered(DTid.xy, pRad));
	float3 intens = saturate(irrColScale * GetParticleColFiltered(DTid.xy, pRad));

	finalColor = BlendColor(float4(intens - shad, pari), finalColor);

	g_output[DTid.xy] = float4(finalColor.rgb, 1.0f);
}