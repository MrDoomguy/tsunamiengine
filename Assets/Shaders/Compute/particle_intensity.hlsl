#include "LightEnvs.hlsli"
#include "ThreadingDefines.hlsli"

RWTexture2D<float> g_output : register(u0);
Texture2D<float> g_particleIntensity : register(t0);

[numthreads(groupdim_2, groupdim_2, 1)]
void main(uint3 DTid : SV_DispatchThreadID)
{
	g_output[DTid.xy] = sigmoid(g_particleIntensity.Load(int3(DTid.xy, 0)).r, 3.f, 1.25f);
}