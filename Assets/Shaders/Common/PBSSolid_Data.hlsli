#ifndef _PBS_SOLID_DATA_HLSLI_
#define _PBS_SOLID_DATA_HLSLI_

#include "Materials.hlsli"

cbuffer PerFrameData : register(b0)
{
	DirectionalLight dirLights[MAX_DIR_LIGHTS];
	uint nDirLights;
	float3 viewDir;
	float4x4 viewProj;
	float3 eyePosW;
	float _pad1;
};

cbuffer PerObject : register(b1)
{
	PBSMaterial mat;
	float4x4 world;
	float4x4 invWorldTranspose;
};

#ifdef SKINNED
static const uint MAX_JOINTS = 128;
cbuffer JointTransform : register(b11)
{
	float4x4 jointTransform[MAX_JOINTS];
}
#endif

Texture2D g_colorMap : register(t0);
Texture2D g_normalMap : register(t1);
SamplerState s : register(s0);

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float3 TangentU : TANGENT;
	float2 TexCoord : TEXCOORD;
	float Area : AREA;
#ifdef SKINNED
	uint4 JointIDs : JOINTIDS;
	float4 JointWeights : WEIGHTS;
#endif
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float3 TangentU : TANGENT;
	float2 TexCoord : TEXCOORD;
	float Area : AREA;
};

#endif