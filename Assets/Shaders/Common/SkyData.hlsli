#ifndef _SKYDATA_HLSLI_
#define _SKYDATA_HLSLI_

#include "LightEnvs.hlsli"

cbuffer PerFrameData : register(b0)
{
	DirectionalLight dirLights[MAX_DIR_LIGHTS];
	uint nDirLights;
	float3 viewDir;
	float4x4 viewProj;
	float3 eyePosW;
	float _pad1;
};

cbuffer PerObject : register(b1)
{
	float4x4 world;
};

TextureCube tex : register(t0);
SamplerState s : register(s0);

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float2 TexCoord : TEXCOORD;
	float Area : AREA;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float2 TexCoord : TEXCOORD;
};

#endif