#include "PhysicHelper.hlsli"

Texture3D<float> g_sdf : register(t0);
Texture3D<float4> g_vel : register(t1);
SamplerState g_sampler : register(s0);

static const float surfaceTol = .4f;

cbuffer FluidProperties : register(b0)
{
	// sdf porperties
	uint4 dim;
	uint3 extent;
	float _pad0;
	uint4 velDim;
	uint3 velExtent;
	float _pad1;

	// fluid properties
	float dampingFactor;
	float buoyancyFactor;
	float flowFactor;
	float frictionFactor;
};

cbuffer PerFrameData : register(b1)
{
	//Per Frame Data
	uint3 offset;
	float sdfScale;
	uint3 nextOffset;
	float blend;
	uint3 velOffset;
	float velScale;
	uint3 velNextOffset;
	float velBlend;
}

static const float3 halfVolumeL = dim.xyz / 2.f;
static const float3 velHalfVolumeL = velDim.xyz / 2.f;
static const float3 velScaleFactor = (float3)velDim.xyz / dim.xyz;

float SampleSDF(float3 worldPos)
{
	return sdfScale *
		(g_sdf.SampleLevel(g_sampler, (worldPos + offset + halfVolumeL) / extent, 0).r * (1 - blend) +
		g_sdf.SampleLevel(g_sampler, (worldPos + nextOffset + halfVolumeL) / extent, 0).r * blend);
}

float3 SampleVel(float3 worldPos)
{
	return velScale *
		(g_vel.SampleLevel(g_sampler, (worldPos * velScaleFactor + velOffset + velHalfVolumeL) / velExtent, 0).xyz * (1 - velBlend) +
		g_vel.SampleLevel(g_sampler, (worldPos * velScaleFactor + velNextOffset + velHalfVolumeL) / velExtent, 0).xyz * velBlend);
}

float3 Gradient(float3 pos)
{
	float step = 0.5f;
	float3 g;

	g.x = SampleSDF(float3(pos.x + step, pos.yz)) - SampleSDF(float3(pos.x - step, pos.yz));
	g.y = SampleSDF(float3(pos.x, pos.y + step, pos.z)) - SampleSDF(float3(pos.x, pos.y - step, pos.z));
	g.z = SampleSDF(float3(pos.xy, pos.z + step)) - SampleSDF(float3(pos.xy, pos.z - step));

	return g;
}

float3x3 Hessian(float3 pos)
{
	float step = 0.5f;

	float3x3 h;
	[unroll]
	for (int j = 0; j < 3; j++) {
		[unroll]
		for (int i = 0; i < 3; i++) {
			float v = 0;
			[unroll]
			for (float jj = -step; jj <= step; jj += 2 * step) {
				[unroll]
				for (float ii = -step; ii <= step; ii += 2 * step) {
					float3 idx = float3(0, 0, 0);

					idx[i] += ii;
					idx[j] += jj;

					v += SampleSDF(pos + idx) * ii * jj / (2 * step);
				}
			}
			h[j][i] = v;
		}
	}
	return h;
}

float Curvature(float3 pos)
{
	float3 g = Gradient(pos);
	float qL = dot(g, g);

	if (qL < 1e-5) return 0;

	float3x3 h = Hessian(pos);

	return (dot(g, mul(h, g)) - qL - h._m00 - h._m11 - h._m22) / (2 * pow(qL, 1.5f));
}

float TrappedAir(float3 pos)
{
	float3 v = SampleVel(pos);
	float I = 0.f;

	float step = 0.5f;

	float3 vn = SampleVel(float3(pos.x - step, pos.yz));
	I += length(v - vn) + vn.x;

	vn = SampleVel(float3(pos.x + step, pos.yz));
	I += length(v - vn) - vn.x;

	vn = SampleVel(float3(pos.x, pos.y - step, pos.z));
	I += length(v - vn) + vn.y;

	vn = SampleVel(float3(pos.x, pos.y + step, pos.z));
	I += length(v - vn) - vn.y;

	vn = SampleVel(float3(pos.xy, pos.z - step));
	I += length(v - vn) + vn.z;

	vn = SampleVel(float3(pos.xy, pos.z + step));
	I += length(v - vn) - vn.z;

	return I;
}