#ifndef _PARTICLEPYHSIC_HLSLI_
#define _PARTICLEPHYSIC_HLSLI_

#include "WaterPhysicHelper.hlsli"

struct Particle
{
	float3 pos;
	float radius;
	// 1: foam
	// 2: bubble
	// else: spray
	uint state;
	float ttl;
};

cbuffer ParticleData : register(b2)
{
	float dt;
	uint maxSize;
	float ttlFactor;
	float maxScale;
};

RWStructuredBuffer<Particle> g_particles : register(u0);
// TODO: check if velocity + obstacle set!
RWStructuredBuffer<float3> g_velocity : register(u1);
Texture3D<float4> g_obstacle : register(t2);


float4 SampleObstacle(float3 worldPos)
{
	return g_obstacle.SampleLevel(g_sampler, (worldPos + halfVolumeL) / dim.xyz, 0);
}

#endif