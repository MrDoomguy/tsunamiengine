//
// Materials
//
#include "Materials.hlsli"

//
// Lights
//
struct DirectionalLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 direction;
	float _pad0;
};

//
// Additional Resources
//
TextureCube g_envMap : register(t11);
TextureCube g_irrMap : register(t12);
SamplerState g_envSampler : register(s11);

//
// Constants
//
static const float PI = 3.141592653589793f;
static const float PI2 = 2.0f*PI;
static const float OneOverPI = rcp(PI);
static const uint MAX_DIR_LIGHTS = 10;
static const uint MAX_IBL_SAMPLES = 4;

//
// Utils
//
inline float2 SphericalSample(in float3 dir)
{
	//return float2(atan2(dir.x, -dir.z) / PI, acos(dir.y) / PI);
	
	//float m = 2.0f * sqrt(dir.x*dir.x + dir.y*dir.y + (dir.z + 1)*(dir.z + 1));
	//return dir.xy / m + 0.5f;

	return float2(0.5f - atan2(dir.x, -dir.z) / PI2, 0.5f + asin(dir.y) / PI);
}
inline float3 GetIrradianceFromSphere(in float3 dir)
{
	return g_irrMap.SampleLevel(g_envSampler, dir, 0).rgb;
}

inline float3 GetEnvironmentFromSphere(in float3 dir)
{
	return g_envMap.SampleLevel(g_envSampler, dir, 0).rgb;
}

inline float AlphaToSpecPow(in float alpha)
{
	return (2.0f * rcp((alpha*alpha) - 2.0f));
}

inline float SpecPowToAlpha(in float s)
{
	return sqrt(2.0f * rcp(s + 2.0f));
}

float rand(float2 co) 
{
	return frac(sin(dot(co.xy, float2(12.9898f, 78.233f))) * 43758.5453f);
}

float3 random3(float3 c)
{
	float j = 4096.f * sin(dot(c, float3(17.f, 59.4f, 15.f)));
	float3 r;
	r.z = frac(512.f * j);
	j *= .125;
	r.x = frac(512.f * j);
	j *= .125;
	r.y = frac(512.f * j);
	return r - 0.5f;
}

uint wang_hash(uint seed)
{
	seed = (seed ^ 61) ^ (seed >> 16);
	seed *= 9;
	seed = seed ^ (seed >> 4);
	seed *= 0x27d4eb2d;
	seed = seed ^ (seed >> 15);
	return seed;
}

float RadicalInverse_VanDerCorput(in uint bits)
{
	bits = (bits << 16u) | (bits >> 16u);
	bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
	bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
	bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
	bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
	return (float)bits * 2.3283064365386963e-10;
}

float NdimQRndSequence(uint p, in uint bits)
{
	float res = 0.0f;
	uint _p = p;

	[unroll(16)]
	while (bits > 0)
	{
		res += (float)(bits % p) / _p;
		bits /= p;
		_p *= p;
	}
	return res;
}

inline float2 Hammersley(in uint i, in uint N)
{
	return float2((float)i / (float)N, RadicalInverse_VanDerCorput(i));
}

// TODO: replace with correct version!
inline float3 Hammersley3D(in uint i, in uint N)
{
	return float3((float)i / (float)N, RadicalInverse_VanDerCorput(i), NdimQRndSequence(3, i));
}

//
// Light Functions
//
void BlinnPhong(in BlinnPhongMaterial mat, in DirectionalLight L, in float3 normal, in float3 toEye, out float4 a, out float4 d, out float4 s)
{
	a = float4(0.0f, 0.0f, 0.0f, 0.0f);
	d = float4(0.0f, 0.0f, 0.0f, 0.0f);
	s = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 lightVec = -L.direction;

	a = mat.ambient * L.ambient;

	float kd = dot(lightVec, normal);

	[flatten]
	if (kd > 0.0f)
	{
		float3 v = reflect(-lightVec, normal);
		float ks = pow(max(dot(v, toEye), 0.0f), mat.specular.w);

		d = kd * mat.diffuse * L.diffuse;
		s = ks * mat.specular * L.specular;
	}
}

inline float falloff(in float x, in float b, in float n, in float m)
{
	return pow(abs(1 - pow(abs(x / b), n)), m);
}

inline float sigmoid(in float x, in float m, in float e)
{
	float tmp = pow(abs(x), e);
	return tmp / (tmp + m);
}

//
// Cook-Torrance helper
//
inline float K_Disney(in float Roughness)
{
	// (roughness + 1)^2 / 8
	return ((Roughness + 1)*(Roughness + 1)) * 0.125f;
}

inline float3 F_Schlick(in float3 F0, in float LoH)
{
	return (F0 + (1 - F0)*pow(1 - LoH, 5));
}

// Karis optimised implementation
float G_SmithGGX(in float Alpha, in float NoV, in float NoL)
{
	const float a2 = Alpha*Alpha;
	const float G_v = NoV + sqrt((NoV - NoV*a2) * NoV + a2);
	const float G_l = NoL + sqrt((NoL - NoL*a2) * NoL + a2);

	return rcp(G_v*G_l);
}

float G_SchlickBeckmann(in float Roughness, in float NoV, in float NoL)
{
	const float k = K_Disney(Roughness);
	const float dV = rcp(NoV * (1 - k) + k);
	const float dL = rcp(NoL * (1 - k) + k);

	return((NoL*dL)*(NoV*dL));
}

inline float G_Implicit(in float NoV, in float NoL)
{
	return NoL * NoV;
}

float G_Smith(in float Roughness, in float NoV, in float NoL)
{
	// Disney "hotness" remapping
	float k = K_Disney(Roughness);

	float OmK = 1 - k;

	float G_v = NoV / (NoV * OmK + k);
	float G_l = NoL / (NoL * OmK + k);

	return G_v*G_l;
}

float D_GGX(in float Roughness, in float NoH)
{
	const float a2 = Roughness*Roughness*Roughness*Roughness;
	const float NoH2 = NoH*NoH;
	float denom = NoH2*(a2 - 1) + 1;

	denom = rcp(PI*(denom*denom));

	return a2*denom;
}

float D_BlinnPhong(in float alpha, in float NoH)
{
	const float specPow = AlphaToSpecPow(alpha);

	return (1.0f / (PI*specPow*specPow)) * pow(NoH, alpha);
}

float3 ImportanceSampleGGX(in float2 Xi, in float Roughness, in float3 N)
{
	const float a = Roughness*Roughness;

	const float Phi = 2 * PI * Xi.x;
	const float CosTheta = sqrt((1 - Xi.y) / (1 + (a*a - 1) * Xi.y));
	const float SinTheta = sqrt(1 - CosTheta*CosTheta);

	float3 H;
	H.x = SinTheta * cos(Phi);
	H.y = SinTheta * sin(Phi);
	H.z = CosTheta;

	const float3 UpVector = abs(N.z) < 0.999f ? float3(0.0f, 0.0f, 1.0f) : float3(1.0f, 0.0f, 0.0f);
	const float3 TangentX = normalize(cross(UpVector, N));
	const float3 TangentY = cross(N, TangentX);

	// Tangent to World Space
	return TangentX*H.x + TangentY*H.y + N*H.z;
}

//
// Tsunami BRDF
//
float3 Specular_Tsunami(in float3 SampleColor, in float SampleIOR, in float SampleRoughness, in float SampleMetallic, in float3 V, in float3 N, in float3 L, out float3 KS)
{
	float3 Specular = 0.0f;
	KS = 0.0f;

	float3 F0 = abs((1.0f - SampleIOR) / (1.0f + SampleIOR));
	F0 = F0*F0;
	F0 = lerp(F0, SampleColor, SampleMetallic);

	const float3 H = normalize(L + V);

	const float NoV = abs(saturate(dot(N, V))) + 1e-5;
	const float NoL = saturate(dot(N, L));
	const float NoH = saturate(dot(N, H));
	const float LoH = saturate(dot(L, H));

	if (NoL > 0.0f)
	{
		const float3 F = F_Schlick(F0, LoH);
		const float G = G_SchlickBeckmann(SampleRoughness, NoV, NoL);
		const float D = D_GGX(SampleRoughness, NoH);

		Specular += F * G * D * rcp(4.0f * NoV * NoL);
		KS += F_Schlick(F0, NoL);
	}

	return Specular * NoL;
}

float3 Diffuse_Tsunami(in float3 Diffuse)
{
	return Diffuse * OneOverPI;
}

//
// Unreal Engine 4 Cook-Torrance specular for IBL
//
float3 SpecularIBL(in float3 SpecularColor, in float Roughness, in float3 N, in float3 V)
{
	float3 SpecularLighting = 0;

	[unroll]
	for (uint i = 0; i < MAX_IBL_SAMPLES; i++)
	{
		const float2 Xi = Hammersley(i, MAX_IBL_SAMPLES);
		const float3 H = ImportanceSampleGGX(Xi, Roughness, N);
		const float3 L = normalize(2 * dot(V, H) * H - V);

		const float NoV = abs(saturate(dot(N, V))) + 1e-5;
		const float NoL = saturate(dot(N, L));
		const float NoH = saturate(dot(N, H));
		const float VoH = saturate(dot(V, H));

		[flatten]
		if (NoL > 0)
		{
			const float3 SampleColor = GetEnvironmentFromSphere(L);

			const float G = G_SchlickBeckmann(Roughness, NoV, NoL);//G_Smith(Roughness, NoV, NoL);
			const float Fc = pow(1 - VoH, 5);
			const float3 F = (1 - Fc)*SpecularColor + Fc;

			// Incident Light = SampleColor * NoL
			// Microfacet specular = D*G*F / (4*NoL*NoV)
			// pdf = D * NoH / (4*VoH)
			SpecularLighting += SampleColor * F * G * VoH / (NoH * NoV);
		}
	}

	return SpecularLighting / MAX_IBL_SAMPLES;
}

float3 PreFilterEnvMap(in float Roughness, in float3 R)
{
	const float3 N = R;
	const float3 V = R;

	float3 PrefilteredColor = 0;
	float TotalWeight = 0;

	[unroll]
	for (uint i = 0; i < MAX_IBL_SAMPLES; i++)
	{
		const float2 Xi = Hammersley(i, MAX_IBL_SAMPLES);
		const float3 H = ImportanceSampleGGX(Xi, Roughness, N);
		const float3 L = normalize(2 * dot(V, H) * H - V);

		const float NoL = saturate(dot(N, L));
		[flatten]
		if (NoL > 0)
		{
			PrefilteredColor += GetEnvironmentFromSphere(L) * NoL;
			TotalWeight += NoL;
		}
	}

	return PrefilteredColor / TotalWeight;
}

float2 IntegrateBRDF(in float Roughness, in float NoV)
{
	const float3 V = float3(sqrt(1.0f - NoV * NoV), 0, NoV);

	float A = 0;
	float B = 0;

	[unroll]
	for (uint i = 0; i < MAX_IBL_SAMPLES; i++)
	{
		const float2 Xi = Hammersley(i, MAX_IBL_SAMPLES);
		const float3 H = ImportanceSampleGGX(Xi, Roughness, V);
		const float3 L = normalize(2 * dot(V, H) * H - V);

		const float NoL = saturate(L.z);
		const float NoH = saturate(H.z);
		const float VoH = saturate(dot(V, H));

		[flatten]
		if (NoL > 0)
		{
			const float G = G_SchlickBeckmann(Roughness, NoV, NoL);//G_Smith(Roughness, NoV, NoL);

			const float G_Vis = G * VoH / (NoH * NoV);
			const float Fc = pow(1 - VoH, 5);
			A += (1 - Fc) * G_Vis;
			B += Fc * G_Vis;
		}
	}

	return float2(A, B) / MAX_IBL_SAMPLES;
}

float3 ApproximateSpecularIBL(in float3 SpecularColor, in float Roughness, in float3 N, in float3 V)
{
	const float NoV = abs(saturate(dot(N, V))) + 1e-5;
	const float3 R = reflect(V, N);

	const float3 PreFilteredColor = PreFilterEnvMap(Roughness, R);
	const float2 EnvBRDF = IntegrateBRDF(Roughness, NoV);

	return PreFilteredColor * (SpecularColor * EnvBRDF.x + EnvBRDF.y);
}

inline float UnpackNormals(in float2 packedNormal)
{
	return sqrt(1.0f - saturate(dot(packedNormal.xy, packedNormal.xy)));
}

float3 BumpNormal(in float3 TexNormal, in float3 N, inout float3 T)
{
	float3 c1 = cross(N, float3(0.0f, 0.0f, 1.0f));
	float3 c2 = cross(N, float3(0.0f, 1.0f, 0.0f));

	if (length(c1) > length(c2))
		T = c1;
	else
		T = c2;
	
	T = normalize(T - dot(T, N) * N);
	float3 B = cross(T, N);

	float4x4 TBN = 
	{
		T.x, T.y, T.z, 0.0f,
		B.x, B.y, B.z, 0.0f,
		N.x, N.y, N.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	return normalize(mul((float3x3)TBN, TexNormal)).xyz;
}
