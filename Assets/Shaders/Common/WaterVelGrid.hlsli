#include "Grid.hlsli"

Texture3D<float3> g_vel : register(t1);
SamplerState g_sdfSampler : register(s0);

cbuffer FluidProperties : register(b0)
{
	// sdf porperties
	uint4 dim;
	uint3 extent;
	float _pad2;

	// fluid properties
	float dampingFactor;
	float flowFactor;
	float frictionFactor;
	float _pad3;
};

cbuffer PerFrameData : register(b1)
{
	//Per Frame Data
	uint3 offset;
	float _pad1;
	uint3 nextOffset;
	float blend;
}

static const float3 halfVolumeL = dim.xyz / 2.f;

float SampleSDF(float3 worldPos)
{
	return
		g_sdf.SampleLevel(g_sdfSampler, (worldPos + offset + halfVolumeL) / extent, 0).r * (1 - blend) +
		g_sdf.SampleLevel(g_sdfSampler, (worldPos + nextOffset + halfVolumeL) / extent, 0).r * blend;
}

float3 Gradient(float3 pos)
{
	float step = 0.5f;
	float3 g;

	g.x = SampleSDF(float3(pos.x + step, pos.yz)) - SampleSDF(float3(pos.x - step, pos.yz));
	g.y = SampleSDF(float3(pos.x, pos.y + step, pos.z)) - SampleSDF(float3(pos.x, pos.y - step, pos.z));
	g.z = SampleSDF(float3(pos.xy, pos.z + step)) - SampleSDF(float3(pos.xy, pos.z - step));

	return g;
}

float3x3 Hessian(float3 pos)
{
	float step = 0.5f;

	float3x3 h;
	[unroll]
	for (int j = 0; j < 3; j++) {
		[unroll]
		for (int i = 0; i < 3; i++) {
			float v = 0;
			[unroll]
			for (float jj = -step; jj <= step; jj += 2 * step) {
				[unroll]
				for (float ii = -step; ii <= step; ii += 2 * step) {
					float3 idx = float3(0, 0, 0);

					idx[i] += ii;
					idx[j] += jj;

					v += SampleSDF(pos + idx) * ii * jj / (2 * step);
				}
			}
			h[j][i] = v;
		}
	}
	return h;
}

float Curvature(float3 pos)
{
	float3 g = Gradient(pos);
	float qL = dot(g, g);

	if (qL < 1e-5) return 0;

	float3x3 h = Hessian(pos);

	return (dot(g, mul(h, g)) - qL - h._m00 - h._m11 - h._m22) / (2 * pow(qL, 1.5f));
}