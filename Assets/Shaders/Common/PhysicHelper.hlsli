#include "ThreadingDefines.hlsli"

#define GRAVITY -9.81f

float3 saveNormalize(float3 v)
{
	float l = dot(v, v);

	return l < 10e-8 ? float3(0.f, 0.f, 0.f) : v * rsqrt(l);
}