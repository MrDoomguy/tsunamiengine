#ifndef _PARTICLEDATA_HLSLI_
#define _PARTICLEDATA_HLSLI_

#include "LightEnvs.hlsli"

struct Particle
{
	float3 particlePos : POSITION;
	float radius : PSIZE;
	uint state : STATE;
	float ttl : TIME;
};

struct Sprite
{
	float4 pos : POSITION;
	float2 scale : DIMENSIONS;
	uint st : STATE;
	float time : TIME;
	float rad : PSIZE;
};

struct PS_Input
{
	float4 p : SV_POSITION;
	float2 tex : TEXCOORD;
	uint s : STATE;
	float t : TIME;
	float r : PSIZE;
};

struct PS_Input_SR
{
	float4 pos : SV_POSITION;
	float2 texC : TEXCOORD;
	float rad : PSIZE0;
	float srad : PSIZE1;
};

cbuffer PerFrameData : register(b0)
{
	float4x4 viewM;
	float4x4 projM;
	float4x4 normalTrans;
	uint2 screenSize;
	float viewZ;
	float _pad0;
};

cbuffer PerObjectData : register(b1)
{
	float4x4 objToWorld;
};

#endif