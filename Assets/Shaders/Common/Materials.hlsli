#ifndef _MATERIALS_HLSLI_
#define _MATERIALS_HLSLI_

struct BlinnPhongMaterial
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
};

struct PBSMaterial
{
	float3 baseColor;
	float metallic;
	float roughness;
	float ior;
	float2 _pbsPad;
};

#endif