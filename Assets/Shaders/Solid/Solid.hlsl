static const uint MAX_DIR_LIGHTS = 10;

struct DirectionalLight
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float3 direction;
	float _pad0;
};

cbuffer PerFrameData : register(b0)
{
	DirectionalLight dirLights[MAX_DIR_LIGHTS];
	uint nDirLights;
	float3 viewDir;
	float4x4 viewProj;
	float3 eyePosW;
	float _pad1;
};

// Material Data
struct Material
{
	float4 ambient;
	float4 diffuse;
	float4 specular;
};

cbuffer PerObject : register(b1)
{
	Material mat;
	float4x4 world;
	float4x4 invWorldTranspose;
};

Texture2D tex : register(t0);
SamplerState s : register(s0);

struct VertexIn
{
	float3 PosL : POSITION;
	float3 NormalL : NORMAL;
	float2 TexCoord : TEXCOORD;
	float Area : AREA;
};

struct VertexOut
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float2 TexCoord : TEXCOORD;
};

VertexOut mainVS(VertexIn vin)
{
	VertexOut vout;

	vout.PosH = mul(viewProj, mul(world, float4(vin.PosL, 1.0f)));
	vout.PosW = mul(world, float4(vin.PosL, 1.0f)).xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)invWorldTranspose);
	vout.TexCoord = vin.TexCoord;

	return vout;
}

/*float4 EvaluateLight(float3 normal, float3 viewDir, float4 diffuseColor, float4 specularColor, float4 ambientColor, float shininess)
{
	diffuseColor.rgb *= diffuseColor.a;
	specularColor.rgb *= specularColor.a;
	ambientColor.rgb *= ambientColor.a;

	float4 color = float4(0.f, 0.f, 0.f, 0.f);

	for (uint i = 0; i < nDirLights; i++)
	{
		float3 lightDir = -dirLights[i].direction;

		float4 coeff = lit(dot(lightDir, normal), dot(normalize(lightDir + viewDir), normal), shininess);

		float4 c = diffuseColor * coeff.y + specularColor * coeff.z + ambientColor;
		color += float4(c.rgb * dirLights[i].color, c.a);
	}
	if (color.w > 1) color / color.w;
	return color;
}*/

void ComputeLight(in DirectionalLight L, in float3 normal, in float3 toEye, out float4 a, out float4 d, out float4 s)
{
	a = float4(0.0f, 0.0f, 0.0f, 0.0f);
	d = float4(0.0f, 0.0f, 0.0f, 0.0f);
	s = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 lightVec = -L.direction;

	a = mat.ambient * L.ambient;
	
	float kd = dot(lightVec, normal);
	
	[flatten]
	if(kd > 0.0f)
	{
		float3 v = reflect(-lightVec, normal);
		float ks = pow(max(dot(v, toEye), 0.0f), mat.specular.w);

		d = kd * mat.diffuse * L.diffuse;
		s = ks * mat.specular * L.specular;
	} 
}

float4 mainPS(VertexOut pin) : SV_Target
{
	// Interpolating normal can unnormalize it
	pin.NormalW = normalize(pin.NormalW);

	float3 toEye = normalize(eyePosW - pin.PosW);

	float4 Ia = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 Id = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 Is = float4(0.0f, 0.0f, 0.0f, 0.0f);

	for(uint i = 0; i < nDirLights; i++)
	{
		float4 A, D, S;
		ComputeLight(dirLights[i], pin.NormalW, toEye, A, D, S);

		Ia += A;
		Id += D;
		Is += S;
	}
	
	float3 texDiffuse = tex.Sample(s, float2(pin.TexCoord.x, 1.0f - pin.TexCoord.y)).rgb;
	float4 finalColor = float4(texDiffuse, 1.0f)*(Ia + Id) + Is;

	return finalColor;
}