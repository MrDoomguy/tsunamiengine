#include "ParticleData.hlsli"

struct POut
{
	float4 normAndSearchRad : SV_Target0;
	float depth : SV_Target1;
	float sD : SV_Depth;
};

// normal + search radius
POut main(PS_Input_SR input)
{
	POut output;

	float t0 = (input.texC.x - 0.5f) * 2.f;
	float t1 = (input.texC.y - 0.5f) * 2.f;

	float l = t0 * t0 + t1 * t1;

	if (l > 1.f)
	{
		discard;
	}

	float t2 = sqrt(1.f - l);

	// correct depth
	output.depth = input.pos.w - t2 * input.rad;
	output.sD = mul(projM, float4(0, 0, output.depth, 1)).z / output.depth;// input.pos.z * input.pos.w / output.depth;

	output.normAndSearchRad.w = input.srad;
	output.normAndSearchRad.xyz = mul(normalTrans, float4(t0, -t1, -t2, 0.f)).xyz;

	return output;
}