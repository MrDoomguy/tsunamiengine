#include "ParticleData.hlsli"

[maxvertexcount(4)]
void main(point Sprite sprite[1], inout TriangleStream<PS_Input> triStream)
{
	Sprite sp = sprite[0];

	if (sp.scale.x > 0)
	{
		PS_Input v;
		v.t = sp.time;
		v.s = sp.st;
		v.r = sp.rad;

		// top left
		v.p = float4(sp.pos.x - sp.scale.x, sp.pos.y + sp.scale.y, sp.pos.zw);
		v.tex = float2(0, 0);
		triStream.Append(v);

		// bottom left
		v.p = float4(sp.pos.x + sp.scale.x, sp.pos.y + sp.scale.y, sp.pos.zw);
		v.tex = float2(1, 0);
		triStream.Append(v);

		// top right
		v.p = float4(sp.pos.x - sp.scale.x, sp.pos.y - sp.scale.y, sp.pos.zw);
		v.tex = float2(0, 1);
		triStream.Append(v);

		// bottom right
		v.p = float4(sp.pos.x + sp.scale.x, sp.pos.y - sp.scale.y, sp.pos.zw);
		v.tex = float2(1, 1);
		triStream.Append(v);
	}
}
