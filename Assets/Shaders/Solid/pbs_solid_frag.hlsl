#include "LightEnvs.hlsli"
#include "PBSSolid_Data.hlsli"

struct POut
{
	float4 col : SV_TARGET0;
	float depth : SV_TARGET1;
};

POut main(VertexOut pin)
{
	POut output;

	// Interpolating normal can unnormalize it
	pin.NormalW = normalize(pin.NormalW);
	pin.TangentU = normalize(pin.TangentU);

	float3 nm = (float3)0.0f;
	nm.rg = g_normalMap.SampleLevel(s, float2(pin.TexCoord.x, 1.0f - pin.TexCoord.y), 0).rg;
	nm.b = UnpackNormals(nm.rg);

	pin.NormalW = BumpNormal(nm, pin.NormalW, pin.TangentU);

	float3 toEye = normalize(eyePosW - pin.PosW);

	float3 r = reflect(toEye, pin.NormalW);

	float3 irradiance = GetIrradianceFromSphere(r);

	float3 diffuse = g_colorMap.Sample(s, float2(pin.TexCoord.x, 1.0f - pin.TexCoord.y)).rgb * mat.baseColor.rgb * (1.0f - saturate(mat.metallic));

	float3 specular = ApproximateSpecularIBL(irradiance, mat.roughness, pin.NormalW, toEye) * saturate(mat.metallic);

	float3 d = 0.01f;
	float3 ks = 0.0f;
	float3 s = 0.0f;

	[unroll]
	for(uint i = 0; i < nDirLights; i++)
	{
		s += Specular_Tsunami(Diffuse_Tsunami(dirLights[i].diffuse.rgb), mat.ior, mat.roughness, mat.metallic, toEye, pin.NormalW, -dirLights[i].direction, ks);
		
		float NoL = saturate(dot(-dirLights[i].direction, pin.NormalW));
		float3 kd = ((1.0f - ks)  - mat.metallic);
		
		d += NoL * dirLights[i].diffuse.rgb * kd;
	}
	
	output.col = float4(Diffuse_Tsunami(diffuse * (d + irradiance.rgb)) + specular + s, 1.0f);
	//output.col = float4((nm.rgb + 1.0f) / 2.0f, 1.0f);
	output.depth = pin.PosH.w;

	return output;
}
