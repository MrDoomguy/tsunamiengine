#include "ParticleData.hlsli"

PS_Input_SR main(PS_Input input)
{
	PS_Input_SR output;

	output.pos = input.p;
	output.texC = input.tex;
	output.rad = input.r;
	output.srad = 2.f * input.r * viewZ / (input.p.w * screenSize.y);

	return output;
}