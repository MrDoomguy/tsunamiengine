#include "SkyData.hlsli"

float4 main(VertexOut pin) : SV_Target
{
	float2 uv = float2(atan2(pin.NormalW.x, -pin.NormalW.z) / PI, acos(pin.NormalW.y) / PI);

	return tex.SampleLevel(s, pin.NormalW, 0).rgba;
}