#include "ParticleData.hlsli"

Texture2D<float> g_solidDepth : register(t0);
Texture2D<float> g_raytraceDepth : register(t1);

SamplerState g_depthSampler : register(s0);

const static float maxDepth = 10.f;

float main(PS_Input input) : SV_Target
{
	float density;
	float sd = g_solidDepth.Load(int3(input.p.xy,0)).r;
	float rd = g_raytraceDepth.Load(int3(input.p.xy,0)).r;

	float t0 = (input.tex.x - 0.5f) * 2.f;
	float t1 = (input.tex.y - 0.5f) * 2.f;

	float l = t0 * t0 + t1 * t1;

	if (l > 1.f)
	{
		discard;
	}

	float t2 = sqrt(1.f - l);

	// correct depth
	input.p.w -= t2 * input.r;

	if (sd < input.p.w || rd + maxDepth < input.p.w)
	{
		discard;
	}

	l = sqrt(l);
		
	if (input.s == 1)
	{
		density = falloff(l, 1.f, 2.25f, 1.f);
	}
	else if (input.s == 2)
	{
		density = 1 - falloff(l, 1.f, 2.f, 1.f);
	}
	else
	{
		density = falloff(l, 1.f, 1.5f, 1.f);
	}

	if (input.t > 0)
	{
		density *= falloff(1.f - input.t, 1.f, 2.f, 0.4f);
	}

	if (rd < input.p.w)
	{
		density *= falloff(input.p.w - rd, maxDepth, 1.5f, 1.f);
	}
	return density;
}