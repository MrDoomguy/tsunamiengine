#include "SkyData.hlsli"

VertexOut main(VertexIn vin)
{
	VertexOut vout;
	
	vout.PosH = mul(viewProj, mul(world, float4(vin.PosL, 1.0f)));
	vout.PosW = mul(world, float4(vin.PosL, 1.0f)).xyz;
	vout.NormalW = vin.NormalL;
	vout.TexCoord = vin.TexCoord;

	return vout;
}