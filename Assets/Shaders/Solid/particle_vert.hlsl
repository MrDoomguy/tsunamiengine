#include "ParticleData.hlsli"

static const float4x4 viewProj = mul(projM, viewM);

Sprite main(Particle input)
{
	// TODO: z - radius, consider also in physics radius
	Sprite sp;

	sp.pos = mul(viewProj, mul(objToWorld, float4(input.particlePos, 1.f)));
	sp.time = input.ttl;
	sp.st = input.state;

	if (input.state == 2) input.radius /= 2.f;

	sp.scale = mul(objToWorld, float4(input.radius, input.radius, input.radius, 0.f)).xy;
	
	sp.rad = sp.scale.x;
	sp.scale *= 2.f * viewZ / screenSize;

	return sp;
}