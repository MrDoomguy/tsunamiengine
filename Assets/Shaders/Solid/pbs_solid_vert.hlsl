#include "LightEnvs.hlsli"
#include "PBSSolid_Data.hlsli"

VertexOut main(VertexIn vin)
{
	VertexOut vout;

#ifdef SKINNED
	/*float4x4 jt = jointTransform[vin.JointIDs.r] * vin.JointWeights.r;
	jt += jointTransform[vin.JointIDs.g] * vin.JointWeights.g;
	jt += jointTransform[vin.JointIDs.b] * vin.JointWeights.b;
	jt += jointTransform[vin.JointIDs.a] * vin.JointWeights.a;

	vin.PosL = mul(jt, float4(vin.PosL, 1.0f)).xyz;
	vin.NormalL = mul(jt, float4(vin.NormalL, 1.0f)).xyz;*/

	float3 PosL = 0.0f;
	float3 NormalL = 0.0f;
	float3 TangentL = 0.0f;
	[unroll]
	for(uint i = 0; i < 4; i++)
	{
		PosL += mul(jointTransform[vin.JointIDs[i]], float4(vin.PosL,1.0f)).xyz * vin.JointWeights[i];
		NormalL += mul((float3x3)jointTransform[vin.JointIDs[i]], vin.NormalL) * vin.JointWeights[i];
		TangentL += mul((float3x3)jointTransform[vin.JointIDs[i]], vin.TangentU) * vin.JointWeights[i];
	}

	vin.PosL = PosL;
	vin.NormalL = NormalL;
	vin.TangentU = TangentL;
#endif

	vout.PosH = mul(viewProj, mul(world, float4(vin.PosL, 1.0f)));
	vout.PosW = mul(world, float4(vin.PosL, 1.0f)).xyz;
	vout.NormalW = mul(vin.NormalL, (float3x3)invWorldTranspose);
	vout.TangentU = mul(vin.TangentU, (float3x3)invWorldTranspose);
	vout.TexCoord = vin.TexCoord;
	vout.Area = vin.Area;

	return vout;
}