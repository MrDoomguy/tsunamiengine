from sys import argv
import os

def write_template(class_name, file):
	code = "#ifndef _"
	code += class_name.upper()
	code += "_H_\n#define _"
	code += class_name.upper()
	code += "_H_\n\n#include \"../../Engine/Scene/GameObject.h\"\n\nclass "
	code += class_name
	code += " : public IScript\n{\npublic:\n	"
	code += class_name
	code += "(GameObject* pOwner, std::string& scriptName) : IScript(pOwner, scriptName) { }\n\n"
	code += "	virtual void Start() override\n	{ \n		\n	}\n\n	virtual void Update(float dt) override\n	{\n		\n	}\n\nprivate:\n\nprotected:\n\n};\n\n#endif"
	
	file.write(code)
	
	return

if len(argv) < 2:
	sys.exit()

print("Writing Script")
name = str(argv[1])
name += ".hpp"
script_file = open("../Source/Game/Scripts/" + name, "w")
write_template(str(argv[1]), script_file)
script_file.close()

print("Completed")
