# IceWave

IceWave is a surf-simulator on the Eisbach-wave, a well-known river wave in Munich.
The idea of the game was to reconstruct the river wave in Munich with the real surroundings and to simulate the surfing as realistic as possible.
The result is a game of skill, controlable with the gyroscope of our own smartphone controller.
The player can use the gyroscope to move the center of mass of the surfer. The surfboard moves then depending on flow and buoyancy forces. Depending on the velocity the player can get a higher score.

The whole game bases on our own "Tsunami" Game Engine, which includes:
- Skeletal animation combined with blend trees
- Physically-based rendering
- Raytracer for implicit surfaces
- Rasterization-based pipeline
- Interactive particle effects
- Rigidbodies
- Primitive-colliders (AABB, OBB, ...)
- BB-Hierarchies
- Realistic fluid-solid interaction

We pre-simulated the wave using FLIP (Mantaflow Framework) and exported a looped implicit representation (SDF grid) and the velocity of the simulation. In the game we render the wave using a raytracer, additionally we can use the SDF representation to calculate efficiently the buoyancy and flow forces on an object in the fluid.  

# Background:
The game was created during the practical course "Computer Game Laboratory" of the Technische Univerität München. The given theme was 'Munich', so we decided to make a simulation game about the river wave of Munich, which is well-known for river surfing.